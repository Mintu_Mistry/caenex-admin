<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('Merchant_modal');
     // $this->load->library('csvimport');
      	$this->load->helper('directory');
      	$this->load->library('twilio');
		
	}
	
		public function sendMail(){
			
		$config['protocol'] = "smtp";
        $config['smtp_host'] = "mail.yilstaging.com";
        $config['smtp_port'] = "587";
        $config['smtp_user'] = "no-reply@yilstaging.com"; 
        $config['smtp_pass'] = "o-lvsT_._19Y";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "text";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
        $this->load->library('email');
        $this->email->initialize($config);
        $subject="Test";
        $message="Test";
        $this->email->set_newline("\r\n");
        $this->email->from('no-reply@yilstaging.com', 'Support Team1'); // change it to yours
        $this->email->to('yesitlabs.mintum@gmail.com');// change it to yours
        $this->email->subject($subject);
        $this->email->message($message);
        if($this->email->send()){
        echo "Success";
        }
        else{
        show_error($this->email->print_debugger());
		
		}
	}
	
	public function index()
	{	
		$submit=$this->input->post('submit');
		if(isset($submit)){

			$data = array('merchant_email' =>$this->input->post('email') ,'user_password' =>$this->input->post('password'));

			$con['merchant_email']=$this->input->post('email');
			$con['user_password']=$this->input->post('password');

			$check_user_details=$this->Merchant_modal->login('*','tbl_merchants',$this->input->post('email'),$this->input->post('password'));
			//echo $this->db->last_query();
			//print_r($check_user_details);die;

			if ($check_user_details) {
				
				if($this->Merchant_modal->get_row1('*','tbl_merchants',$this->input->post('email'),$this->input->post('password'))){
				$this->session->set_userdata('merchant_id',$check_user_details['merchant_id']);
				$this->session->set_userdata('merchant_name',$check_user_details['merchant_name']);
				
				$this->session->set_flashdata('merch_login','Welcome back,'.$check_user_details['merchant_name'].' ('.date('Y-m-d').')  your campaigns are:');
				redirect("Dashboard");
			}else{
				$this->session->set_flashdata('incorrct','Your account is deactivated. Please contact the admin');
			}

			}else{

				$this->session->set_flashdata('incorrct','Email/Phone NUmber or Password is invalid');

			}
		}
		$this->load->view('merchant/login');
	}

	public function Login_mobile_number(){
		$data = array('merchant_phone' =>$this->input->post('mobile'));

			$con['merchant_phone']=$this->input->post('mobile');
			

			$check_user_details=$this->Merchant_modal->get_row('*','tbl_merchants',$con);
			//echo $this->db->last_query();die;
			//print_r($check_user_details);die;

			if ($check_user_details) {
				$con['status']='1';
				if($this->Merchant_modal->get_row('*','tbl_merchants',$con)){
				$this->session->set_userdata('merchant_id',$check_user_details['merchant_id']);
				$this->session->set_userdata('merchant_name',$check_user_details['merchant_name']);
				
				$this->session->set_flashdata('merch_login','Welcome back,'.$check_user_details['merchant_name'].' ('.date('Y-m-d').')  your campaigns are:');
				// redirect("Dashboard");
				echo "1";
			}else{
				$this->session->set_flashdata('incorrct','Your account is deactivated. Please contact the admin');
			}

			}else{

				$this->session->set_flashdata('incorrct','Email or Password is invalid');

			}
	}

	public function Sign_up()
	{
		$countries['country']=$this->Merchant_modal->countries();
		$this->load->view('merchant/sign_up',$countries);

	}

	public function Forgot()
	{
		$this->load->view('merchant/forgot');

	}

	public function reset_link(){
		$password=$this->input->post('password');
		$cpassword=$this->input->post('cpassword');
		$merchant_id=$this->input->post('merchant_id');
		//echo $password."@".$cpassword."@".$merchant_id;
		//die();
		if ($cpassword==$password) {
			$data['user_password']=$password;
			$con['merchant_id']=$merchant_id;
			if($this->Merchant_modal->update('tbl_merchants',$data,$con)){
				$this->session->set_flashdata('corrct','Password changed successfully. Please Login');
				redirect(base_url(''));
			}
		}else{
			$this->session->set_flashdata('incorrct1','Password and confirm password should be same');
		}
		
		//$this->load->view('merchant/reset');
	}
	
	
	public function setsession_data3(){
		$email=$this->input->post('email');
		$data=array('merchant_email'=>$email);
		$con['merchant_email']=$this->input->post('email');
		$data=$this->Merchant_modal->get_row('merchant_id,merchant_email','tbl_merchants',$con);
		if ($data) {
		
		$random_number = mt_rand(10000, 99999);
		$message = 'Your Verification OTP Is :-'.$random_number;
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@yesitlabs.xyz'); // change it to yours
		$this->email->to($this->input->post('email'));// change it to yours
		$this->email->subject('Forgot Password');
		$this->email->message($message);
		$send_status=$this->email->send();
		if ($send_status) {
			echo $random_number."@@".$data['merchant_id'];
		}
		 //$msg = "Your otp for Caenex is ".$random_number;
         //$this->twilio->sms('+12512548900',$mobile1,$msg);
		$this->session->set_userdata($data);
		}else{
			$this->session->set_flashdata('incorrct','Please enter correct email');	
		}
	}
	
	
	public function setsession_data1(){

		$name=$this->input->post('name');
		$business_name=$this->input->post('business_name');
		$email=$this->input->post('email');
		$password=$this->input->post('Password');
		$merchant_phone=$this->input->post('merchant_phone');
		//$merchant_phonepin=$this->input->post('merchant_phonepin');

		$pin=$this->input->post('merchant_phonepin');
		$pin1=explode(" ",$pin);
		 $mobile1=$pin1[0].$merchant_phone;

		// $phonepin=array();
		// $phonepin[]=$merchant_phonepin;
		// print_r($phonepin);
		$data=array('merchant_name'=>$name,'merchant_email'=>$email,'user_password'=>$password,'businessname'=>$business_name,'merchant_phone'=>$mobile1);	

		$random_number = mt_rand(10000, 99999);
		$message = 'Your Verification OTP Is :-'.$random_number;
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@yesitlabs.xyz'); // change it to yours
		$this->email->to($this->input->post('email'));// change it to yours
		$this->email->subject('New User SignUp');
		$this->email->message($message);
		$send_status=$this->email->send();
		if ($send_status) {
			echo $random_number;
		}
		 $msg = "Your otp for Caenex is ".$random_number;
         $this->twilio->sms('+12512548900',$mobile1,$msg);
		$this->session->set_userdata($data);
		
	}

	public function resend_otp(){

		$random_number = mt_rand(10000, 99999);
		$message = 'Your Verification OTP Is :-'.$random_number;
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@yesitlabs.xyz'); // change it to yours
		$this->email->to($this->input->post('email'));// change it to yours
		$this->email->subject('New User SignUp');
		$this->email->message($message);
		$send_status=$this->email->send();
		if ($send_status) {
			echo $random_number;
		}

	}

	public function mobile_otp(){
		$mobile=$this->input->post('mobile_number');
		$pin=$this->input->post('pin');
		$pin1=explode(" ",$pin);
		 $mobile1=$pin1[0].$mobile;
		$random_number = mt_rand(10000, 99999);
		$msg = "Your otp for Caenex is ".$random_number;
         $send_status=$this->twilio->sms('+12512548900',$mobile1,$msg);
         if ($send_status) {
			echo $random_number;
		}
	}

	public function setsession_data2(){

		$texid=$this->input->post('texid');
		$street=$this->input->post('street');
		$city=$this->input->post('city');
		$zip=$this->input->post('zip');
		$country=$this->input->post('country');
		$state=$this->input->post('state');
		$data=array('merchant_taxid'=>$texid,'merchant_address'=>$street,'merchant_city'=>$city,'merchant_state'=>$state,'merchant_country'=>$country,'zip'=>$zip);		
		$this->session->set_userdata($data);
		
	}

	public function Merchant_Signup(){
		
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$expirymonth = $month."/".$year;
		
		$merchant_data = array('merchant_name' => $this->session->userdata('merchant_name'),'merchant_email' => $this->session->userdata('merchant_email'),'user_password' => $this->session->userdata('user_password'),'businessname' => $this->session->userdata('businessname'),'merchant_taxid' => $this->session->userdata('merchant_taxid'),'merchant_address' => $this->session->userdata('merchant_address'),'merchant_city' => $this->session->userdata('merchant_city'),'zip' => $this->session->userdata('zip'),'merchant_country' => $this->session->userdata('merchant_country'),'merchant_state' => $this->session->userdata('merchant_state'),'delete_status'=>'1','status'=>'1' );

		$merchant_id=$this->Merchant_modal->insert('tbl_merchants',$merchant_data);

		$merchant_card_data = array('cardnumber' => $this->input->post('card_number'),'expirymonth' => $expirymonth,'cvv' => $this->input->post('cvv'),'cardholdername' => $this->input->post('card_holder_name'),'nickname' => $this->input->post('nick_name'),'userid'=>$merchant_id );

		if ($merchant_id) {

			$this->Merchant_modal->insert('tbl_paymentmethods',$merchant_card_data);
			$this->session->set_userdata('merchant_id',$merchant_id);

		}
		$title=$this->session->userdata('merchant_name')." has been added to the family as Merchant!";
			$notification = array('title' => $title,'table_data'=>'merchant_management');
			$this->Merchant_modal->insert('tbl_notification',$notification);
		redirect("Dashboard");
	}

	public function check_email(){
		
		$con['merchant_email']=$this->input->post('email');
		$data=$this->Merchant_modal->get_row('merchant_email','tbl_merchants',$con);
		if ($data) {
			echo $data; //"Email Already Exists"
		}else{
			echo $data;
		}		

	}

	public function deleteImage(){
		
		$table = 'tbl_id_verification';
		// $column = $this->input->post('column');
		$con['id'] = $this->input->post('id');
		// $merchant_id['merchant_id'] = $this->session->userdata('merchant_id');
		// $update = array($column => '');
		$data = $this->Merchant_modal->delete($table,$con );
		if ($data) {
			echo $data;
		}else{
			echo $data;
		}		

	}

	public function Dashboard(){

		$nexworld_cash=array();
		$nexworld_coupon=array();
		$con['merchant_id']=$this->session->userdata('merchant_id');
		$con['delete_status']='1';
		$nexworld_cash=$this->Merchant_modal->get_all_data('*','tbl_nexworld_cash',$con);
		$nexworld_coupon=$this->Merchant_modal->get_all_data('*','tbl_nexworld_coupon',$con);
		$nexplay_game = $this->Merchant_modal->get_all_data('*','tbl_nexplay_caenex_game',$con);
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
		usort($array_merge1, array("Merchant", "date_compare"));
		//$data['total_compaigns']=$array_merge1;
		$data['total_nexworld_compaigns']=$nexworld_cash;
		$data['total_nexplay_compaigns']=$nexplay_game;
		//$con['status']='1';
		$con['add_status']='1';
		$nexworld_cash=$this->Merchant_modal->get_all_data('*','tbl_nexworld_cash',$con);
		$nexworld_coupon=$this->Merchant_modal->get_all_data('*','tbl_nexworld_coupon',$con);
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
		usort($array_merge1, array("Merchant", "date_compare"));
		//$data['nexworld']=$array_merge1;
		$data['nexworld']=$nexworld_cash;
		$ceanex_game = $this->Merchant_modal->get_all_data('*','tbl_nexplay_caenex_game',$con);
		//$custom_game = $this->Merchant_modal->get_all_data('*','tbl_nexplay_custom_game',$con);
		//$array_merge2 = array_merge($ceanex_game, $custom_game);
		//usort($array_merge2, array("Merchant", "date_compare"));
		$game_arr = array();
		if(!empty($ceanex_game))
		{
			foreach($ceanex_game as $game)
			{
				$con1['game_id']=$game['gameid'];
				$con1['delete_status']='1';
				$game['game_name'] = $this->Merchant_modal->get_game_name('game_name','tbl_game',$con1);
				$game_arr[] = $game;
			}
		}
		$data['nexplay']=$game_arr;
		
		$con2['user_id'] = $this->session->userdata('merchant_id');
		$wallet_amount = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con2, 'desc', 'wallet_id');
		/* echo "<pre>";
		print_r($wallet_amount);
		die(); */
		if(count($wallet_amount) > 0)
		{
			$total_avail_balance = $wallet_amount[0]['total_avail_balance'];
		}else{
			$total_avail_balance = '0';
		}
		
		$data['wallet_balance'] = $total_avail_balance;
		$this->load->view('merchant/dashboard',$data);
	}

	public function logout(){

		$this->session->sess_destroy();
		redirect(base_url(''));

	}

	public function Profile(){

		$con['merchant_id']=$this->session->userdata('merchant_id');
		$submit=$this->input->post('update');

		if (isset($submit)) {
			$merchant_data = array('merchant_name' => $this->input->post('merchant_name'),'businessname' => $this->input->post('businessname'),'merchant_taxid' => $this->input->post('merchant_taxid'),'merchant_address' => $this->input->post('merchant_address'),'merchant_city' => $this->input->post('merchant_city'),'zip' => $this->input->post('zip'),'merchant_country' => $this->input->post('merchant_country'),'merchant_state' => $this->input->post('merchant_state'),'delete_status'=>'1','status'=>'1','merchant_phone' => $this->input->post('merchant_phone') );
			if (!empty($_FILES['file']['name'])) {
				
				move_uploaded_file($_FILES['file']['tmp_name'], "assets/img/game_img/".$_FILES['file']['name']);
				$merchant_data['img']=$_FILES['file']['name'];
			}
			$update_profile=$this->Merchant_modal->update('tbl_merchants',$merchant_data,$con);
			if ($update_profile) {
				$this->session->set_flashdata('update','Profile updated successfully');
				
			}
		}

		$profile_data['profile_data']=$this->Merchant_modal->get_row_data('*','tbl_merchants',$con)	;
		
		$this->load->view('merchant/profile',$profile_data);
	}

	public function change_password(){

		$this->load->view('merchant/change_password');

	}

	public function check_old_password(){

		$con['user_password']=$this->input->post('password');
		$con['merchant_id']=$this->session->userdata('merchant_id');
		$data=$this->Merchant_modal->get_row('*','tbl_merchants',$con);
		if (!empty($data)) {
			echo "true";
		}else{
			echo "false";
		}


	}

	public function change_old_password(){

		$merchant_data['user_password']=$this->input->post('password');
		$con['merchant_id']=$this->session->userdata('merchant_id');
		$update_profile=$this->Merchant_modal->update('tbl_merchants',$merchant_data,$con);
		if ($update_profile) {
			echo '1';
		}else{
			echo '0';
		}
	}

	function date_compare($element1, $element2) { 

    $datetime1 = strtotime($element1['create_date']); 
    $datetime2 = strtotime($element2['create_date']); 
    // print_r($element1['create_date']);
    // print_r($element2['create_date']);die;
    return $datetime1 - $datetime2; 

	}

	public function campaign_management(){
		
		$nexworld_cash=array();
		$nexworld_coupon=array();
		$con['merchant_id']=$this->session->userdata('merchant_id');
		$con['delete_status']='1';
		$order_by='DESC';
		$colum='id';
		$nexworld_cash=$this->Merchant_modal->get_all_data('*','tbl_nexworld_cash',$con,$order_by,$colum);
		$nexworld_coupon=$this->Merchant_modal->get_all_data('*','tbl_nexworld_coupon',$con,$order_by,$colum);
		
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
		//usort($array_merge1, array("Merchant", "date_compare"));
		$data['nexworld']=$array_merge1;
		/* echo "<pre>";
		print_r($data['nexworld']);
		die(); */
		$ceanex_game = $this->Merchant_modal->get_all_data('*','tbl_nexplay_caenex_game',$con,$order_by,$colum);
		//$custom_game = $this->Merchant_modal->get_all_data('*','tbl_nexplay_custom_game',$con);
		//$array_merge2 = array_merge($ceanex_game, $custom_game);
		//usort($array_merge2, array("Merchant", "date_compare"));
		$game_arr = array();
		if(!empty($ceanex_game))
		{
			foreach($ceanex_game as $game)
			{
				$con1['game_id']=$game['gameid'];
				$con1['delete_status']='1';
				$game['game_name'] = $this->Merchant_modal->get_game_name('game_name','tbl_game',$con1);
				$game_arr[] = $game;
			}
		}
		$data['nexplay']=$game_arr;
		/* echo "<pre>";
		print_r($data['nexplay']);
		die(); */
		$this->load->view('merchant/campaign_management',$data);

	}
	
	
		public function campaign_nexplay(){
		
		$nexworld_cash=array();
		$nexworld_coupon=array();
		$con['merchant_id']=$this->session->userdata('merchant_id');
		$con['delete_status']='1';
		$order_by='DESC';
		$colum='id';
		$nexworld_cash=$this->Merchant_modal->get_all_data('*','tbl_nexworld_cash',$con,$order_by,$colum);
		$nexworld_coupon=$this->Merchant_modal->get_all_data('*','tbl_nexworld_coupon',$con,$order_by,$colum);
		
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
		//usort($array_merge1, array("Merchant", "date_compare"));
		$data['nexworld']=$array_merge1;
		/* echo "<pre>";
		print_r($data['nexworld']);
		die(); */
		$ceanex_game = $this->Merchant_modal->get_all_data('*','tbl_nexplay_caenex_game',$con,$order_by,$colum);
		//$custom_game = $this->Merchant_modal->get_all_data('*','tbl_nexplay_custom_game',$con);
		//$array_merge2 = array_merge($ceanex_game, $custom_game);
		//usort($array_merge2, array("Merchant", "date_compare"));
		$game_arr = array();
		if(!empty($ceanex_game))
		{
			foreach($ceanex_game as $game)
			{
				$con1['game_id']=$game['gameid'];
				$con1['delete_status']='1';
				$game['game_name'] = $this->Merchant_modal->get_game_name('game_name','tbl_game',$con1);
				$game_arr[] = $game;
			}
		}
		$data['nexplay']=$game_arr;
		$data['tab_status'] ='1';
		/* echo "<pre>";
		print_r($data['nexplay']);
		die(); */
		$this->load->view('merchant/campaign_nexplay',$data);

	}
	
	public function campaign_management_NexWorld(){
		$con['user_id']=$this->session->userdata('merchant_id');
		$data['wallet'] = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con, 'desc', 'wallet_id');
		//echo $this->db->last_query();
		$this->load->view('merchant/campaign_management_NexWorld_popup', $data);

	}

	public function campaign_management_NexPlay(){
		$order_by='DESC';
		$colum='id';
		$con['delete_status']='1';
		$con['status']='1';
		$data['category']=$this->Merchant_modal->get_all_data('*','tbl_category',$con,$order_by,$colum);
		$data['ad_type']=$this->Merchant_modal->get_all_data('*','tbl_ad_type',$con,$order_by,$colum);
		$condition['user_id']=$this->session->userdata('merchant_id');
		$data['wallet'] = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $condition, 'desc', 'wallet_id');
		$order_by='DESC';
		$colum='game_id';
		$con['delete_status']='1';
		$game_list = $this->Merchant_modal->get_all_data('*','tbl_game',$con,$order_by,$colum);
		$game_arr =array();
		if(count($game_list)>0)
		{
			foreach($game_list as $game)
			{
				$con1['delete_status']='1';
				$con1['gameid']= $game['game_id'];
				$con1['status']='1';
				$game_qus = $this->Merchant_modal->get_all_data('*','tbl_question',$con1);
				if(count($game_qus)>0)
				{
					$game_arr[] = $game;
				}
			}
		}
		$data['game'] = $game_arr;
		$this->load->view('merchant/campaign_management_NexPlay_popup',$data);

	}
	
	public function account(){
		
		$nexworld_cash=array();
		$nexworld_coupon=array();
		$NexPlay_caenex=array();
		$NexPlay_custom=array();
		
		/* $con['tbl_transaction.type']="1";
		$con['tbl_transaction.user_id']=$this->session->userdata('merchant_id');
		$joinCon="tbl_nexworld_coupon.id=tbl_transaction.data_id";
		$nexworld_cash=$this->Merchant_modal->join_data('tbl_nexworld_coupon.*,tbl_transaction.status,tbl_transaction.type','tbl_nexworld_coupon','tbl_transaction',$joinCon,$con);

		$con1['tbl_transaction.type']="2";
		$con1['tbl_nexworld_cash.merchant_id']=$this->session->userdata('merchant_id');
		$joinCon1="tbl_nexworld_cash.id=tbl_transaction.data_id";
		$nexworld_coupon=$this->Merchant_modal->join_data('tbl_nexworld_cash.*,tbl_transaction.status,tbl_transaction.type','tbl_nexworld_cash','tbl_transaction',$joinCon1,$con1);

		
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);

		$con2['tbl_transaction.type']="3";
		$con2['tbl_nexplay_caenex_game.merchant_id']=$this->session->userdata('merchant_id');
		$joinCon2="tbl_nexplay_caenex_game.id=tbl_transaction.data_id";
		$NexPlay_caenex=$this->Merchant_modal->join_data('tbl_nexplay_caenex_game.*,tbl_transaction.status,tbl_transaction.type','tbl_nexplay_caenex_game','tbl_transaction',$joinCon2,$con2);

		$con3['tbl_transaction.type']="4";
		$con3['tbl_nexplay_custom_game.merchant_id']=$this->session->userdata('merchant_id');
		$joinCon3="tbl_nexplay_custom_game.id=tbl_transaction.data_id";
		$NexPlay_custom=$this->Merchant_modal->join_data('tbl_nexplay_custom_game.*,tbl_transaction.status,tbl_transaction.type','tbl_nexplay_custom_game','tbl_transaction',$joinCon3,$con3);

		$array_merge2=array_merge($NexPlay_caenex,$NexPlay_custom);

		$array_merge3=array_merge($array_merge1,$array_merge2);
		
		
		usort($array_merge3, array("Merchant", "date_compare"));
		$data['nexworld']=$array_merge2; 
		$con['status']='1';
		$con['add_status']='1';
		*/
		
		$transaction_arr =array();
		$con['user_id']=$this->session->userdata('merchant_id');
		$transaction_data = $this->Merchant_modal->get_all_data('*',' tbl_transaction', $con, 'desc', 'id');
		
		if(count($transaction_data) > 0)
		{
			foreach($transaction_data as $tx)
			{
				$con1['id'] = $tx['data_id'];
				if($tx['type']==1)
				{
					$coupon_img = $this->Merchant_modal->get_row_data('*','tbl_nexworld_coupon',$con1);
					if(!empty($coupon_img))
					{
						$tx['coupon_img'] = $coupon_img['coupon_image'];
						$tx['coupon_title'] = $coupon_img['coupon_title'];
						$tx['amount'] = $coupon_img['total_amount'];
					}
				}elseif($tx['type']==2){
					$coupon_img = $this->Merchant_modal->get_row_data('*','tbl_nexworld_cash',$con1);
					if(!empty($coupon_img))
					{
						$tx['coupon_img'] = "No Image";
						$tx['coupon_title'] = "Cash Reward";
						$tx['amount'] = $coupon_img['total_amount'];
					}
				}elseif($tx['type']==3){
					$coupon_img = $this->Merchant_modal->get_row_data('*','tbl_nexplay_caenex_game',$con1);
					if(!empty($coupon_img))
					{
						$tx['coupon_img'] = $coupon_img['coupon_image'];
						$tx['coupon_title'] = "Cash Reward";
						$tx['amount'] = $coupon_img['total_amount'];
					}
				}elseif($tx['type']==4){
					$coupon_img = $this->Merchant_modal->get_row_data('*','tbl_nexplay_custom_game',$con1);
					if(!empty($coupon_img))
					{
						$tx['coupon_img'] = $coupon_img['image'];
						$tx['coupon_title'] = $coupon_img['reward'];
						$tx['amount'] = $coupon_img['total_amount'];
					}
				}
				$transaction_arr[] = $tx;
			}
		}
		/* echo "<pre>";
		print_r($transaction_arr);
		die(); */
		$wallet_amount = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con, 'desc', 'wallet_id');
		
		if(count($wallet_amount) > 0)
		{
			$total_avail_balance = $wallet_amount[0]['total_avail_balance'];
		}else{
			$total_avail_balance = '0';
		}
		
		$data['wallet_balance'] = $total_avail_balance;
		$data['transaction_data'] = $transaction_arr;
		
		$this->load->view('merchant/account',$data);
	}



	public function payment_method(){

		$con['userid']=$this->session->userdata('merchant_id');
		$con['usertype']=2;
		$data['payment']=$this->Merchant_modal->get_all_data('*','tbl_paymentmethods',$con);
		$this->load->view('merchant/payment_method',$data);

	}

	public function add_wallet(){
		$data['user_id'] = $this->session->userdata('merchant_id');
		$data['transaction'] = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $data, 'desc', 'wallet_id');
		$this->load->view('merchant/add_wallet', $data);

	}
	public function add_wallet_payment(){
		$totalAmount = $_POST['amount'];
		//print_r($totalAmount)	;
		$token= $this->input->post('stripeToken');
		//print_r($token);die;
		$stripe = array(
			"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
			"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
		);
		require_once APPPATH."third_party/stripe/init.php";

		\Stripe\Stripe::setApiKey($stripe['secret_key']);
		
		try
		{
			//add customer to stripe
			$customer = \Stripe\Customer::create(array(
			'email' => "test@yopmail.com",
			'source'  => $token
			));
			// print_r($customer);
			//item information
			$itemName = "Ceanex";
			$itemNumber = 'PS123456';
			$itemPrice = $totalAmount * 100;

			$currency = "usd";
			$orderID = "SKA92712382139";

			//charge a credit or a debit card
			// $charge = \Stripe\Charge::create(array(
			// 'customer' => $customer->id,
			// 'amount'   => $itemPrice,
			// 'currency' => $currency,
			// 'description' => $itemNumber,
			// )); 		
			$charge = \Stripe\Charge::create(array(
				"amount" => $itemPrice,
				"currency" => "usd",
				"customer" => $customer->id,
				'description' => $itemNumber,
			));	

			$chargeJson = $charge->jsonSerialize();	
			// print_r($chargeJson);
			// die;
			
			$con['user_id'] = $this->session->userdata('merchant_id');
			$wallet_amount = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con, 'desc', 'wallet_id');
			if(!empty($wallet_amount))
			{
			//echo $this->db->last_query();
			//print_r($wallet_amount[0]['wallet_id']);
			$wallet_id = $wallet_amount[0]['wallet_id'];
			//print_r($wallet_id);
			$total_credit_balance = $_POST['amount'];
			$total_avail_balance = $this->input->post('total_avail_balance');
			$total_credit_balance1 = $total_avail_balance + $total_credit_balance;
			
			$wallet_data = array('total_avail_balance'=> $total_credit_balance1,'total_credit_balance'=> $total_credit_balance,'last_added_amount'=> $total_credit_balance,'user_type'=> 2,'created_date'=> date('Y-m-d'));
			$where = "wallet_id = '$wallet_id'";
			if($wallet_id = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where)){
				$tancation_data=array('user_id'=>$this->session->userdata('merchant_id'),'amount'=>$this->input->post('amount'),'transactioncol_id'=>$chargeJson['id'],'type'=>'1','data_id'=>$wallet_id,'status'=>$chargeJson['status'],"transaction_time"=>date('H:i:s'),"transaction_date"=>date('Y-m-d'),"payment_from"=>1);
				$this->Merchant_modal->insert('tbl_transaction',$tancation_data);
				
				$this->session->set_flashdata('add','Wallet Amount Added Successfully');
			}
			
				$title=$total_credit_balance." has been added to your wallet";
				$notification = array('title' => $title,'table_data'=>'add_wallet','userid' => $this->session->userdata('merchant_id'),'type' => 2);
				$this->Merchant_modal->insert('tbl_notification',$notification);
				
			}else{
				
			$transaction = $this->Merchant_modal->get_all_data('*',' tbl_transaction',$con, 'desc', 'id');
			//print_r($transaction[0]);
			//echo $this->db->last_query();die;
			$total_credit_balance = $_POST['amount'];
			$total_withdraw_balance = 0;
			$total_avail_balance = $this->input->post('total_avail_balance');
			$total_credit_balance1 = $total_avail_balance + $total_credit_balance;
			$total_avail_balance1 = $total_credit_balance1 - $total_withdraw_balance;
			$last_added_amount = $this->input->post('added_amount');
			$data = array('user_id'=> $this->session->userdata('merchant_id'),'total_avail_balance'=> $total_avail_balance1,'total_credit_balance'=> $total_credit_balance,'total_withdraw_balance'=> $total_withdraw_balance,'user_type'=> 2,'last_added_amount'=> $last_added_amount,'created_date'=> date('Y-m-d')); 

			if($last_id = $this->Merchant_modal->insert('tbl_wallet',$data)){
				$tancation_data=array('user_id'=>$this->session->userdata('merchant_id'),'amount'=>$this->input->post('amount'),'transactioncol_id'=>$chargeJson['id'],'type'=>'1','data_id'=>$last_id,'status'=>$chargeJson['status'],'transaction_time'=>date('H:i:s'),'transaction_date'=>date('Y-m-d'),"payment_from"=>1);
				$this->Merchant_modal->insert('tbl_transaction',$tancation_data);

				$this->session->set_flashdata('add','Wallet Amount Added Successfully');
			}
			$title=$total_credit_balance." has been added to your wallet";
				$notification = array('title' => $title,'table_data'=>'add_wallet','userid' => $this->session->userdata('merchant_id'),'type' => 2);
				$this->Merchant_modal->insert('tbl_notification',$notification);
				
			}
			$title=$this->session->userdata('merchant_name')." has added wallet amount";
				$notification = array('title' => $title,'table_data'=>'merchant_management');
			$this->Merchant_modal->insert('tbl_notification',$notification);

			redirect('add_wallet');	
		
		} catch(\Stripe\Exception\CardException $e) {
			  // Since it's a decline, \Stripe\Exception\CardException will be caught
			  /* echo 'Status is:' . $e->getHttpStatus() . '\n';
				echo 'Type is:' . $e->getError()->type . '\n';
				echo 'Code is:' . $e->getError()->code . '\n';
			  // param is '' in this case
				echo 'Param is:' . $e->getError()->param . '\n';
				echo 'Message is:' . $e->getError()->message . '\n'; */
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\RateLimitException $e) {
			  // Too many requests made to the API too quickly
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\InvalidRequestException $e) {
			  // Invalid parameters were supplied to Stripe's API
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\AuthenticationException $e) {
			  // Authentication with Stripe's API failed
			  // (maybe you changed API keys recently)
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\ApiConnectionException $e) {
			  // Network communication with Stripe failed
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\ApiErrorException $e) {
			  // Display a very generic error to the user, and maybe send
			  // yourself an email
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (Exception $e) {
			  // Something else happened, completely unrelated to Stripe
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			}
			
	}

	public function id_verification(){

		$con['merchant_id']=$this->session->userdata('merchant_id');
		$data['id_verification']=$this->Merchant_modal->get_all_data('*','tbl_id_verification1',$con);
		//print_r($data);die;
		$this->load->view('merchant/id_verification',$data);

	}

	public function settings(){
		$con['id']='2';
		$con1['id']='4';
		$data['T_C']=$this->Merchant_modal->get_row('*','tbl_content',$con);
		$data['P_P']=$this->Merchant_modal->get_row('*','tbl_content',$con1);
		$this->load->view('merchant/settings',$data);

	}

	public function support(){

		$con['delete_status']=1;
		$order_by='DESC';
		$colum='id';
		$data['help_articles']=$this->Merchant_modal->get_all_data('*','tbl_help_articles',$con,$order_by,$colum);

		$this->load->view('merchant/support',$data);

	}
	
	public function add_support(){

		$data=array('name'=>$this->input->post('name'),'email'=>$this->input->post('email'),'subject'=>$this->input->post('subject'),'msg'=>$this->input->post('message'),'merchant_id'=>$this->session->userdata('merchant_id'));
		if($this->Merchant_modal->insert('tbl_support',$data)){
			
		$message = '<p>Hello,</p>
					<p>Thank you for contacting our support team. Your query has been registered. Please check the details of your query below:</p>
					<p>Name : '.$this->input->post('name').'</p>
					<p>Email : '.$this->input->post('email').'</p>
					<p>Subject : '.$this->input->post('subject').'</p>
					<p>Message : '.$this->input->post('message').'</p>';
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@yesitlabs.xyz'); // change it to yours
		$this->email->to($this->input->post('email'));// change it to yours
		$this->email->subject('Support Query');
		$this->email->message($message);
		$send_status=$this->email->send();

			$this->session->set_flashdata('add','Your Query Submited Successfully');
		}
		redirect('support');

	}

	public function nexplay_caenex_game(){
		$totalAmount1 = $_POST['total_amount'];	
		$totalBalance = $_POST['total_balance1'];
		if ($this->input->post('wallete_amount_use')) {
			# code...
			$con['user_id'] = $this->session->userdata('merchant_id');
			$wallet_amount = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con, 'desc', 'wallet_id');
			$wallet_id = $wallet_amount[0]['wallet_id'];
			$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - $totalAmount1;
			$totalAmount = $wallet_amount[0]['total_avail_balance'] - $totalAmount1;
		}else{
			$totalAmount = $totalAmount1;
		}
		
		if($_POST['payment_status'] > 0 )
		{
			// payment from card
			
			$token= $this->input->post('stripeToken');
			//print_r($token);die;
			$stripe = array(
				"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
				"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
			);
			require_once APPPATH."third_party/stripe/init.php";

			\Stripe\Stripe::setApiKey($stripe['secret_key']);

			//add customer to stripe
			try{
				$customer = \Stripe\Customer::create(array(
				'email' => "test@yopmail.com",
				'source'  => $token
				));
				$itemName = "Ceanex";
				$itemNumber = 'PS123456';
				$itemPrice = $totalAmount * 100;

				$currency = "usd";
				$orderID = "SKA92712382139";	
				$charge = \Stripe\Charge::create(array(
					"amount" => $itemPrice,
					"currency" => "usd",
					"customer" => $customer->id,
					'description' => $itemNumber,
				));
				
				$chargeJson = $charge->jsonSerialize();
				$chargeJson['type'] = "3";
				$chargeJson['payment_from'] = "1";
				$coupon_image=$_FILES['file']['name'];
				move_uploaded_file($_FILES['file']['tmp_name'], "assets/img/game_img/".$coupon_image);
				$old_date_timestamp = strtotime($this->input->post('state_date'));
				$state_date = date('Y-m-d H:i:s', $old_date_timestamp);
				$end_date = strtotime($this->input->post('end_date'));
				$end_date1 = date('Y-m-d H:i:s', $end_date);
				$data=array('merchant_id'=>$this->session->userdata('merchant_id'),'start_date'=>$state_date,'end_date'=>$end_date1,'gameid'=>$this->input->post('gameid'),'ad_type'=>$this->input->post('ad_type'),'amount_video_view'=>$this->input->post('amount_video_view'),'daily_view'=>$this->input->post('max_daily_view'),'coupon_image'=>$coupon_image,'campaign_budget'=>$this->input->post('campaign_budget'),'total_number_of_impressions'=>$this->input->post('impressions'),'maximum_number_of_impressions_day'=>$this->input->post('impressions_day'),'caenex_fee'=>$this->input->post('caenex_fee'),'handling_charges'=>$this->input->post('handling_charges'),'total_amount'=>$this->input->post('total_amount'));
				if($last_id=$this->Merchant_modal->insert('tbl_nexplay_caenex_game',$data)){
					$tancation_data=array('user_id'=>$this->session->userdata('merchant_id'),'amount'=>$this->input->post('total_amount'),'transactioncol_id'=>$chargeJson['id'],'type'=>$chargeJson['type'],'payment_from'=>$chargeJson['payment_from'],'data_id'=>$last_id,'status'=>$chargeJson['status'],'transaction_time'=>date('H:i:s'),'transaction_date'=>date('Y-m-d'));
					$this->Merchant_modal->insert('tbl_transaction',$tancation_data);
					$this->session->set_flashdata('add','NexPlay Campaign Added Successfully');
				}
			
			} catch(\Stripe\Exception\CardException $e) {
			  // Since it's a decline, \Stripe\Exception\CardException will be caught
			  /* echo 'Status is:' . $e->getHttpStatus() . '\n';
				echo 'Type is:' . $e->getError()->type . '\n';
				echo 'Code is:' . $e->getError()->code . '\n';
			  // param is '' in this case
				echo 'Param is:' . $e->getError()->param . '\n';
				echo 'Message is:' . $e->getError()->message . '\n'; */
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\RateLimitException $e) {
			  // Too many requests made to the API too quickly
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\InvalidRequestException $e) {
			  // Invalid parameters were supplied to Stripe's API
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\AuthenticationException $e) {
			  // Authentication with Stripe's API failed
			  // (maybe you changed API keys recently)
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\ApiConnectionException $e) {
			  // Network communication with Stripe failed
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\ApiErrorException $e) {
			  // Display a very generic error to the user, and maybe send
			  // yourself an email
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (Exception $e) {
			  // Something else happened, completely unrelated to Stripe
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			}
			
			
		}else{
			// payment from wallet
			$bytes = random_bytes(19);
			$txid = bin2hex($bytes);
			$chargeJson['id'] = $txid;
			$chargeJson['status'] = "succeeded";
			$chargeJson['type'] = "3";
			$chargeJson['payment_from'] = "2";
			$coupon_image=$_FILES['file']['name'];
			move_uploaded_file($_FILES['file']['tmp_name'], "assets/img/game_img/".$coupon_image);
			$old_date_timestamp = strtotime($this->input->post('state_date'));
			$state_date = date('Y-m-d H:i:s', $old_date_timestamp);
			$end_date = strtotime($this->input->post('end_date'));
			$end_date1 = date('Y-m-d H:i:s', $end_date);
			$data=array('merchant_id'=>$this->session->userdata('merchant_id'),'start_date'=>$state_date,'end_date'=>$end_date1,'gameid'=>$this->input->post('gameid'),'ad_type'=>$this->input->post('ad_type'),'amount_video_view'=>$this->input->post('amount_video_view'),'daily_view'=>$this->input->post('max_daily_view'),'coupon_image'=>$coupon_image,'campaign_budget'=>$this->input->post('campaign_budget'),'total_number_of_impressions'=>$this->input->post('impressions'),'maximum_number_of_impressions_day'=>$this->input->post('impressions_day'),'caenex_fee'=>$this->input->post('caenex_fee'),'handling_charges'=>$this->input->post('handling_charges'),'total_amount'=>$this->input->post('total_amount'));
			if($last_id=$this->Merchant_modal->insert('tbl_nexplay_caenex_game',$data)){
				$tancation_data=array('user_id'=>$this->session->userdata('merchant_id'),'amount'=>$this->input->post('total_amount'),'transactioncol_id'=>$chargeJson['id'],'type'=>$chargeJson['type'],'payment_from'=>$chargeJson['payment_from'],'data_id'=>$last_id,'status'=>$chargeJson['status'],'transaction_time'=>date('H:i:s'),'transaction_date'=>date('Y-m-d'));
				$this->Merchant_modal->insert('tbl_transaction',$tancation_data);
				$this->session->set_flashdata('add','NexPlay, Campaign Added Successfully');
			}
			
			$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + $totalAmount1;
			$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
			$where = "wallet_id = '$wallet_id'";
			$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
			
			$title=$this->input->post('total_amount')." has been deducted from your wallet for Nexplay campaign";
			$notification = array('title' => $title,'table_data'=>'campaign_management','userid' => $this->session->userdata('merchant_id'),'type' => 2);
			$this->Merchant_modal->insert('tbl_notification',$notification);
			
		}
		$title=$this->session->userdata('merchant_name')." has added a new NexPlay campaign";
			$notification = array('title' => $title,'table_data'=>'billboard_management');
			$this->Merchant_modal->insert('tbl_notification',$notification);

		redirect('campaign_nexplay');
		
	} 

	public function nexplay_custom_game(){

		$totalAmount1 = $_POST['custom_total_amount'];	
		$totalBalance = $_POST['total_balance2'];
		if ($this->input->post('wallete_amount_use1')) {
			# code...
		$totalAmount = $totalAmount1 - $totalBalance;
		}else{
			$totalAmount = $totalAmount1;
		}
		//$totalAmount = $_POST['custom_total_amount'];
		$quesion=$this->input->post('question');
		$answer1=$this->input->post('answer1');
		$answer2=$this->input->post('answer2');
		$answer3=$this->input->post('answer3');
		$answer4=$this->input->post('answer4');

		$coupon_image=$_FILES['custom_file']['name'];
		move_uploaded_file($_FILES['custom_file']['tmp_name'], "assets/img/game_img/".$coupon_image);
		$old_date_timestamp = strtotime($this->input->post('state_date1'));
		$state_date = date('Y-m-d H:i:s', $old_date_timestamp);
		$end_date = strtotime($this->input->post('end_date1'));
		$end_date1 = date('Y-m-d H:i:s', $end_date);

		$token= $this->input->post('stripeToken');
		$stripe = array(
			"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
			"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
		);
		require_once APPPATH."third_party/stripe/init.php";

		\Stripe\Stripe::setApiKey($stripe['secret_key']);

		//add customer to stripe
		$customer = \Stripe\Customer::create(array(
		//'email' => $email,
		'source'  => $token
		));

		//item information
		$itemName = "Ceanex";
		$itemNumber = "Ceanex";
		$itemPrice = $totalAmount * 100;

		$currency = "usd";
		$orderID = "SKA92712382139";

		//charge a credit or a debit card
		$charge = \Stripe\Charge::create(array(
		'customer' => $customer->id,
		'amount'   => $itemPrice,
		'currency' => $currency,
		'description' => $itemNumber,
		)); 			
		$chargeJson = $charge->jsonSerialize();
		$data=array('merchant_id'=>$this->session->userdata('merchant_id'),'start_date'=>$state_date,'end_date'=>$end_date1,'game_category'=>$this->input->post('category'),'ad_type'=>$this->input->post('custom_ad_type'),'reward'=>$this->input->post('custom_reward'),'image'=>$coupon_image,'campaign_budget'=>$this->input->post('custom_campaign_budget'),'total_number_of_impressions'=>$this->input->post('custom_impressions'),'maximum_number_of_impressions_day'=>$this->input->post('custom_impressions_day'),'caenex_fee'=>$this->input->post('custom_caenex_fee'),'handling_charges'=>$this->input->post('custom_handling_charges'),'total_amount'=>$this->input->post('custom_total_amount'));
		$game_id=$this->Merchant_modal->insert('tbl_nexplay_custom_game',$data);
		$tancation_data=array('user_id'=>$this->session->userdata('merchant_id'),'amount'=>$totalAmount,'transactioncol_id'=>$chargeJson['id'],'type'=>'4','data_id'=>$game_id,'status'=>$chargeJson['status'],'transaction_time'=>date('H:i:s'),'transaction_date'=>date('Y-m-d'));
			$this->Merchant_modal->insert('tbl_transaction',$tancation_data);
			$con['user_id'] = $this->session->userdata('merchant_id');
			$wallet_amount = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con, 'desc', 'wallet_id');
			//echo $this->db->last_query();
			//print_r($wallet_amount[0]['wallet_id']);
			$wallet_id = $wallet_amount[0]['wallet_id'];
			//print_r($wallet_id);
			$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - $totalBalance;
			$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $totalBalance);
			$where = "wallet_id = '$wallet_id'";
			if ($this->input->post('wallete_amount_use1')) {
				# code...
			$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
			}
		$b=1;
		for($i=0;$i<count($quesion);$i++  ){
			 $a=$b+$i;			
			$c='radio'.$a;			
			$data1 = array('merchant_id' => $this->session->userdata('merchant_id'),'game_id'=>$game_id,'question'=>$quesion[$i],'ans1'=>$answer1[$i],'ans2'=>$answer2[$i],'ans3'=>$answer3[$i],'ans4'=>$answer4[$i],'right_ans'=>$this->input->post($c) );
			$this->Merchant_modal->insert('tbl_nexplay_question',$data1);

		}
		
		$this->session->set_flashdata('add','NexPlay, Campaign Added Successfully');		
		$title=$this->session->userdata('merchant_name')." has added a new NexPlay campaign";
			$notification = array('title' => $title,'table_data'=>'billboard_management');
			$this->Merchant_modal->insert('tbl_notification',$notification);
		redirect('campaign_nexplay');
		
	}
	

	public function NexWorld_coupon(){
		$chargeJson = array();
		$totalAmount1 = $_POST['Total_Amount'];	
		$totalBalance = $_POST['wallet_amount'];
		
		if($this->input->post('wallete_amount_use1')){
			if($totalBalance >= $totalAmount1)
			{
				$totalAmount = $totalBalance - $totalAmount1;
			}else{
				$totalAmount = $totalAmount1;
			}
			
		}else{
			$totalAmount = $totalAmount1;
		}
		
		
		
		$payment_status = $_POST['payment_status'];
		
		if($payment_status ==1)
		{
			$token= $this->input->post('stripeToken');
			
			$stripe = array(
				"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
				"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
			);
			require_once APPPATH."third_party/stripe/init.php";

			\Stripe\Stripe::setApiKey($stripe['secret_key']);
			
			try
			{
				//add customer to stripe
				$customer = \Stripe\Customer::create(array(
				'email' => "test@yopmail.com",
				'source'  => $token
				));
				
				$itemName = "Ceanex";
				$itemNumber = 'PS123456';
				$itemPrice = $totalAmount * 100;

				$currency = "usd";
				$orderID = "SKA92712382139";

				//charge a credit or a debit card
				// $charge = \Stripe\Charge::create(array(
				// 'customer' => $customer->id,
				// 'amount'   => $itemPrice,
				// 'currency' => $currency,
				// 'description' => $itemNumber,
				// )); 		
				$charge = \Stripe\Charge::create(array(
					"amount" => $itemPrice,
					"currency" => "usd",
					"customer" => $customer->id,
					'description' => $itemNumber,
				));	
				$chargeJson = $charge->jsonSerialize();
				$chargeJson['type'] = "1";
				$chargeJson['payment_from'] = "1";
				
				$coupon_image=$_FILES['file']['name'];
				move_uploaded_file($_FILES['file']['tmp_name'], "assets/img/game_img/".$coupon_image);
				$old_date_timestamp = strtotime($this->input->post('state_date'));
				$state_date = date('Y-m-d H:i:s', $old_date_timestamp);
				$end_date = strtotime($this->input->post('end_date'));
				$end_date1 = date('Y-m-d H:i:s', $end_date);

				/*$Coupon_Generated_on = strtotime($this->input->post('Coupon_Generated_on'));
				$Coupon_Generated_on1 = date('Y-m-d H:i:s', $Coupon_Generated_on);

				$Coupon_Expires_on = strtotime($this->input->post('Coupon_Expires_on'));
				$Coupon_Expires_on1 = date('Y-m-d H:i:s', $Coupon_Expires_on);*/
				$current_location = $this->input->post('location');
				$current_location = str_replace(" ", "+", $current_location);    
				$json = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".$current_location."&key=AIzaSyBh1m5LWl-qV1nVkT1WZeWAzng5eP42RNk");
				$json = json_decode($json);
				if(!empty($json))
				{
					$latitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
					$longitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
				}else{
					$latitude = "";
					$longitude = "";
				}
				
				$data=	array('merchant_id'=>$this->session->userdata('merchant_id'),
				'start_date'=>$state_date,
				'end_date'=>$end_date1,
				'location'=>$this->input->post('location'),
				'coupon_title'=>$this->input->post('Coupon_Title'),
				'coupon_subtitle'=>$this->input->post('Coupon_Subtitle'),
				'coupon_discount'=>$this->input->post('Coupon_Discount'),
				'coupon_description'=>$this->input->post('Coupon_Description'),
				'coupon_terms_condition'=>$this->input->post('Coupon_Terms'),
				'coupon_image'=>$coupon_image,
				'coupon_code'=>$this->input->post('Coupon_Code'),
				'coupon_generated_on'=>$state_date,
				'coupon_expires_on'=>$end_date1,
				'total_number_of_coupons'=>$this->input->post('Total_Number_of_Coupons'),
				'capping'=>$this->input->post('CAPPING'),
				'campaign_budget'=>$this->input->post('Campaign_Budget'),
				'caenex_fee'=>$this->input->post('Caenex_Fee'),
				'handling_charges'=>$this->input->post('Handling_Charges'),
				'total_amount'=>$this->input->post('Total_Amount'),
				'latitude'=>$latitude,
				'longitude'=>$longitude
				);
				if($last_id=$this->Merchant_modal->insert('tbl_nexworld_coupon',$data)){

					$tancation_data=array('user_id'=>$this->session->userdata('merchant_id'),'amount'=>$totalAmount,'transactioncol_id'=>$chargeJson['id'],'type'=>'1','payment_from'=>$chargeJson['payment_from'],'data_id'=>$last_id,'status'=>$chargeJson['status'],'transaction_time'=>date('H:i:s'),'transaction_date'=>date('Y-m-d'));
					$this->Merchant_modal->insert('tbl_transaction',$tancation_data);

					$this->session->set_flashdata('add','NexWorld, Campaign Added Successfully');
			}
			$con['user_id'] = $this->session->userdata('merchant_id');
			
			} catch(\Stripe\Exception\CardException $e) {
			  // Since it's a decline, \Stripe\Exception\CardException will be caught
			  /* echo 'Status is:' . $e->getHttpStatus() . '\n';
				echo 'Type is:' . $e->getError()->type . '\n';
				echo 'Code is:' . $e->getError()->code . '\n';
			  // param is '' in this case
				echo 'Param is:' . $e->getError()->param . '\n';
				echo 'Message is:' . $e->getError()->message . '\n'; */
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\RateLimitException $e) {
			  // Too many requests made to the API too quickly
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\InvalidRequestException $e) {
			  // Invalid parameters were supplied to Stripe's API
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\AuthenticationException $e) {
			  // Authentication with Stripe's API failed
			  // (maybe you changed API keys recently)
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\ApiConnectionException $e) {
			  // Network communication with Stripe failed
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\ApiErrorException $e) {
			  // Display a very generic error to the user, and maybe send
			  // yourself an email
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (Exception $e) {
			  // Something else happened, completely unrelated to Stripe
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			}
			
		}else{
			$bytes = random_bytes(19);
			$txid = bin2hex($bytes);
			$chargeJson['id'] = $txid;
			$chargeJson['status'] = "succeeded";
			$chargeJson['type'] = "2";
			$chargeJson['payment_from'] = "2";
			$coupon_image=$_FILES['file']['name'];
			move_uploaded_file($_FILES['file']['tmp_name'], "assets/img/game_img/".$coupon_image);
			$old_date_timestamp = strtotime($this->input->post('state_date'));
			$state_date = date('Y-m-d H:i:s', $old_date_timestamp);
			$end_date = strtotime($this->input->post('end_date'));
			$end_date1 = date('Y-m-d H:i:s', $end_date);
			$current_location = $this->input->post('location');
			$current_location = str_replace(" ", "+", $current_location);    
			$json = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".$current_location."&key=AIzaSyBh1m5LWl-qV1nVkT1WZeWAzng5eP42RNk");
			$json = json_decode($json);
			if(!empty($json))
			{
				$latitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
				$longitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
			}else{
				$latitude = "";
				$longitude = "";
			}
			
			$data=array('merchant_id'=>$this->session->userdata('merchant_id'),'start_date'=>$state_date,'end_date'=>$end_date1,'location'=>$this->input->post('location'),'coupon_title'=>$this->input->post('Coupon_Title'),'coupon_subtitle'=>$this->input->post('Coupon_Subtitle'),'coupon_discount'=>$this->input->post('Coupon_Discount'),'coupon_description'=>$this->input->post('Coupon_Description'),'coupon_terms_condition'=>$this->input->post('Coupon_Terms'),'coupon_image'=>$coupon_image,'coupon_code'=>$this->input->post('Coupon_Code'),'coupon_generated_on'=>$state_date,'coupon_expires_on'=>$end_date1,'total_number_of_coupons'=>$this->input->post('Total_Number_of_Coupons'),'capping'=>$this->input->post('CAPPING'),'campaign_budget'=>$this->input->post('Campaign_Budget'),'caenex_fee'=>$this->input->post('Caenex_Fee'),'handling_charges'=>$this->input->post('Handling_Charges'),'total_amount'=>$this->input->post('Total_Amount'),'latitude'=>$latitude,'longitude'=>$longitude);
			if($last_id=$this->Merchant_modal->insert('tbl_nexworld_coupon',$data)){

				$tancation_data=array('user_id'=>$this->session->userdata('merchant_id'),'amount'=>$totalAmount,'transactioncol_id'=>$chargeJson['id'],'type'=>'1','payment_from'=>$chargeJson['payment_from'],'data_id'=>$last_id,'status'=>$chargeJson['status'],'transaction_time'=>date('H:i:s'),'transaction_date'=>date('Y-m-d'));
				$this->Merchant_modal->insert('tbl_transaction',$tancation_data);

				$this->session->set_flashdata('add','NexWorld, Campaign Added Successfully');
		}
			$con['user_id'] = $this->session->userdata('merchant_id');
			$wallet_amount = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con, 'desc', 'wallet_id');
			
			$wallet_id = $wallet_amount[0]['wallet_id'];
			$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - $totalAmount1;
			
			$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + $totalAmount1;
			$wallet_data = array('total_avail_balance'=> $total_avail_balance,'total_credit_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
			$where = "wallet_id = '$wallet_id'";
			if($this->input->post('wallete_amount_use')){
			$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
			}
			
			$title=$this->input->post('Total_Amount')." has been deducted from your wallet for NexWorld campaign";
			$notification = array('title' => $title,'table_data'=>'campaign_management','userid' => $this->session->userdata('merchant_id'),'type' => 2);
			$this->Merchant_modal->insert('tbl_notification',$notification);
		
		}
		
		$title=$this->session->userdata('merchant_name')." has added a new NexWorld campaign";
			$notification = array('title' => $title,'table_data'=>'billboard_management');
			$this->Merchant_modal->insert('tbl_notification',$notification);

		redirect('campaign_management');		
		
	}


	public function NexWorld_cash(){
		
		$chargeJson = array();
		$totalAmount1 = $_POST['totalAmount'];	

		$totalBalance = $_POST['wallet_amount1'];
		if($this->input->post('wallete_amount_use1')){
			if($totalBalance >= $totalAmount1)
			{
				$totalAmount = $totalBalance - $totalAmount1;
			}else{
				$totalAmount = $totalAmount1;
			}
			
		}else{
			$totalAmount = $totalAmount1;
		}
		
		$payment_status1 = $_POST['payment_status1'];
		
		/* echo "<pre>";
		print_r($_POST);
		die(); */
		if($payment_status1 ==1)
		{
			/* echo $totalAmount;
			die(); */
			$token= $this->input->post('stripeToken');
			$stripe = array(
				"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
				"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
			);
			require_once APPPATH."third_party/stripe/init.php";

			\Stripe\Stripe::setApiKey($stripe['secret_key']);
			try
			{
				//add customer to stripe
				$customer = \Stripe\Customer::create(array(
				//'email' => $email,
				'source'  => $token
				));

				//item information
				$itemName = "Ceanex";
				$itemNumber = "Ceanex";
				$itemPrice = $totalAmount * 100;
				// $itemPrice = 100;

				$currency = "usd";
				$orderID = "SKA92712382139";

				//charge a credit or a debit card
				$charge = \Stripe\Charge::create(array(
				'customer' => $customer->id,
				'amount'   => $itemPrice,
				'currency' => $currency,
				'description' => $itemNumber,
				
				)); 			
				$chargeJson = $charge->jsonSerialize();
				$chargeJson['type'] = "1";
				$chargeJson['payment_from'] = "1";
				$old_date_timestamp = strtotime($this->input->post('state_date1'));
				$state_date = date('Y-m-d H:i:s', $old_date_timestamp);
				$end_date = strtotime($this->input->post('end_date1'));
				$end_date1 = date('Y-m-d H:i:s', $end_date);
				$current_location = $this->input->post('location1');
				$current_location = str_replace(" ", "+", $current_location);    
				$json = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".$current_location."&key=AIzaSyBh1m5LWl-qV1nVkT1WZeWAzng5eP42RNk");
				$json = json_decode($json);
				if(!empty($json))
				{
					$latitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
					$longitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
				}else{
					$latitude = "";
					$longitude = "";
				}
				
				$data=array('merchant_id'=>$this->session->userdata('merchant_id'),'start_date'=>$state_date,'end_date'=>$end_date1,'location'=>$this->input->post('location1'),'cash_grab'=>$this->input->post('cashPerGrab'),'grabs_day'=>$this->input->post('grabPerDay'),'campaign_budget'=>$this->input->post('campaignBudget'),'caenex_fee'=>$this->input->post('caenexFee'),'handling_charges'=>$this->input->post('handlingCharges'),'total_amount'=>$this->input->post('totalAmount'),'latitude'=>$latitude,'longitude'=>$longitude);
				
				if($last_id=$this->Merchant_modal->insert('tbl_nexworld_cash',$data)){
					$tancation_data=array('user_id'=>$this->session->userdata('merchant_id'),'amount'=>$totalAmount1,'transactioncol_id'=>$chargeJson['id'],'type'=>$chargeJson['type'],'payment_from'=>$chargeJson['payment_from'],'data_id'=>$last_id,'status'=>$chargeJson['status'],'transaction_time'=>date('H:i:s'),'transaction_date'=>date('Y-m-d'));
					$this->Merchant_modal->insert('tbl_transaction',$tancation_data);
					$this->session->set_flashdata('add','NexWorld, Campaign Added Successfully');
				}
				
				$con['user_id'] = $this->session->userdata('merchant_id');
				$title=$this->session->userdata('merchant_name')." has added a new NexWorld campaign";
				$notification = array('title' => $title,'table_data'=>'billboard_management');
				$this->Merchant_modal->insert('tbl_notification',$notification);
				redirect('campaign_management');
			
			} catch(\Stripe\Exception\CardException $e) {
			  // Since it's a decline, \Stripe\Exception\CardException will be caught
			  /* echo 'Status is:' . $e->getHttpStatus() . '\n';
				echo 'Type is:' . $e->getError()->type . '\n';
				echo 'Code is:' . $e->getError()->code . '\n';
			  // param is '' in this case
				echo 'Param is:' . $e->getError()->param . '\n';
				echo 'Message is:' . $e->getError()->message . '\n'; */
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\RateLimitException $e) {
			  // Too many requests made to the API too quickly
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\InvalidRequestException $e) {
			  // Invalid parameters were supplied to Stripe's API
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\AuthenticationException $e) {
			  // Authentication with Stripe's API failed
			  // (maybe you changed API keys recently)
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\ApiConnectionException $e) {
			  // Network communication with Stripe failed
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (\Stripe\Exception\ApiErrorException $e) {
			  // Display a very generic error to the user, and maybe send
			  // yourself an email
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			} catch (Exception $e) {
			  // Something else happened, completely unrelated to Stripe
				$message = $e->getError()->message;
				$this->session->set_flashdata('add',$message);
			}
			
		}else{
			$bytes = random_bytes(19);
			$txid = bin2hex($bytes);
			$chargeJson['id'] = $txid;
			$chargeJson['status'] = "succeeded";
			$chargeJson['type'] = "2";
			$chargeJson['payment_from'] = "2";
			$old_date_timestamp = strtotime($this->input->post('state_date1'));
			$state_date = date('Y-m-d H:i:s', $old_date_timestamp);
			$end_date = strtotime($this->input->post('end_date1'));
			$end_date1 = date('Y-m-d H:i:s', $end_date);
			$current_location = $this->input->post('location1');
			$current_location = str_replace(" ", "+", $current_location);    
			$json = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".$current_location."&key=AIzaSyBh1m5LWl-qV1nVkT1WZeWAzng5eP42RNk");
			$json = json_decode($json);
			if(!empty($json))
			{
				$latitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
				$longitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
			}else{
				$latitude = "";
				$longitude = "";
			}
			
			$data=array('merchant_id'=>$this->session->userdata('merchant_id'),'start_date'=>$state_date,'end_date'=>$end_date1,'location'=>$this->input->post('location1'),'cash_grab'=>$this->input->post('cashPerGrab'),'grabs_day'=>$this->input->post('grabPerDay'),'campaign_budget'=>$this->input->post('campaignBudget'),'caenex_fee'=>$this->input->post('caenexFee'),'handling_charges'=>$this->input->post('handlingCharges'),'total_amount'=>$this->input->post('totalAmount'),'latitude'=>$latitude,'longitude'=>$longitude);
			
			if($last_id=$this->Merchant_modal->insert('tbl_nexworld_cash',$data)){
				$tancation_data=array('user_id'=>$this->session->userdata('merchant_id'),'amount'=>$totalAmount1,'transactioncol_id'=>$chargeJson['id'],'type'=>$chargeJson['type'],'payment_from'=>$chargeJson['payment_from'],'data_id'=>$last_id,'status'=>$chargeJson['status'],'transaction_time'=>date('H:i:s'),'transaction_date'=>date('Y-m-d'));
				$this->Merchant_modal->insert('tbl_transaction',$tancation_data);
				$this->session->set_flashdata('add','NexWorld, Campaign Added Successfully');
			}
			$con['user_id'] = $this->session->userdata('merchant_id');
			$wallet_amount = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con, 'desc', 'wallet_id');
			
			$wallet_id = $wallet_amount[0]['wallet_id'];
			$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - $totalAmount1;
			
			$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + $totalAmount1;
			$wallet_data = array('total_avail_balance'=> $total_avail_balance,'total_credit_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
			$where = "wallet_id = '$wallet_id'";
			if($this->input->post('wallete_amount_use1')){
			$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
			}
			
			$title=$this->input->post('totalAmount')." has been deducted from your wallet for NexWorld campaign";
			$notification = array('title' => $title,'table_data'=>'campaign_management','userid' => $this->session->userdata('merchant_id'),'type' => 2);
			$this->Merchant_modal->insert('tbl_notification',$notification);
			$title=$this->session->userdata('merchant_name')." has added a new NexWorld campaign";
			$notification = array('title' => $title,'table_data'=>'billboard_management');
			$this->Merchant_modal->insert('tbl_notification',$notification);
			redirect('campaign_management');	
		}
	}
	
	
	public function submit_payment(){
		
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$expirydate = $month."/".$year;
		$merchant_card_data = array('cardnumber' => $this->input->post('card_number'),'expirydate' => $expirydate,'cvv' => $this->input->post('cvv'),'cardholdername' => $this->input->post('card_name'),'nickname' => $this->input->post('nickname'),'userid'=>$this->session->userdata('merchant_id'),'usertype'=>2);	

		// print_r($merchant_card_data);die;	
		$paymethod_id=$this->input->post('paymethod_id');
		if (empty($paymethod_id)) {		
		if($this->Merchant_modal->insert('tbl_paymentmethods',$merchant_card_data)){
			$this->session->set_flashdata('add','Card Details Added Successfully');
			redirect('payment_method');
		}
		}elseif (!empty($paymethod_id)) {
			$con['paymethod_id']=$paymethod_id;
			if($this->Merchant_modal->update('tbl_paymentmethods',$merchant_card_data,$con)){
				$this->session->set_flashdata('add','Card Details Updated Successfully');
				redirect('payment_method');
			}
		}
		
	}

	public function uploade_id_verification(){
		$this->load->library('upload');
		 $file1=$_FILES['file1']['name'];
		 $file2=$_FILES['file2']['name'];
		 $file3=$_FILES['file3']['name'];
		 $id_data = array('merchant_id' =>  $this->session->userdata('merchant_id'),'user_type'=>2);
		 //$check_old_data = $this->Merchant_modal->get_row_data('*','tbl_id_verification1',$id_data);
		 $delete  = $this->Merchant_modal->delete('tbl_id_verification1',$id_data);
		 $delete  = $this->Merchant_modal->delete('tbl_id_verification',$id_data);
		 $id_verification=$this->Merchant_modal->insert('tbl_id_verification1',$id_data);
			if (!empty($file1)) {
				for ($i=0; $i <count($file1) ; $i++) { 
				//echo "<pre>";
				
		/* print_r($file1);
		print_r(count($file1));
		die(); */
					if($file1[$i]){
						
					move_uploaded_file($_FILES['file1']['tmp_name'][$i], "assets/img/game_img/".$file1[$i]);
					$data = array('merchant_id' => $this->session->userdata('merchant_id'),'img'=>$file1[$i],'type'=>1,'verification_id'=>$id_verification ,'user_type'=>2);
					$this->Merchant_modal->insert('tbl_id_verification',$data);
					$this->session->set_flashdata('add',"Document(s) submitted successfully. Waiting for admin's approval");
					}
				}
			}

			if (!empty($file2)) {
				
			for ($a=0; $a <count($file2) ; $a++) { 
				if ($file2[$a]) {
					# code...
				move_uploaded_file($_FILES['file2']['tmp_name'][$a], "assets/img/game_img/".$file2[$a]);
				$data1 = array('merchant_id' => $this->session->userdata('merchant_id'),'img'=>$file2[$a],'type'=>2 ,'verification_id'=>$id_verification,'user_type'=>2);
				$this->Merchant_modal->insert('tbl_id_verification',$data1);
			$this->session->set_flashdata('add',"Document(s) submitted successfully. Waiting for admin's approval");
				}
			}
			}

			if (!empty($file3)) {
			
			for ($b=0; $b <count($file3) ; $b++) { 
				if ($file3[$b]) {
					# code...
				move_uploaded_file($_FILES['file3']['tmp_name'][$b], "assets/img/game_img/".$file3[$b]);
				$data3 = array('merchant_id' => $this->session->userdata('merchant_id'),'img'=>$file3[$b],'type'=>3 ,'verification_id'=>$id_verification,'user_type'=>2);
				$this->Merchant_modal->insert('tbl_id_verification',$data3);
			$this->session->set_flashdata('add',"Document(s) submitted successfully. Waiting for admin's approval");
				}
			}
			}
			$title=$this->session->userdata('merchant_name')." has added the document(s)";
			$notification = array('title' => $title,'table_data'=>'id_verification_admin','type'=>1,'userid'=>$this->session->userdata('merchant_id'));
			$this->Merchant_modal->insert('tbl_notification',$notification);
		
			redirect('id_verification');

	}
	
	public function delete(){

		$id=$this->input->post('id');
		$tbl=$this->input->post('tbl');
		$row=$this->input->post('row');
		$con[$row]=$id;
		if($this->Merchant_modal->delete($tbl,$con)){
			echo "1";
		}

	}
	

	public function delete_data(){

		$id=$this->input->post('id');
		$tbl=$this->input->post('tbl');
		$row=$this->input->post('row');
		$data['delete_status']=0;
		$con[$row]=$id;
		
		$con1['user_id'] = $this->session->userdata('merchant_id');
		$wallet_amount = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con1, 'desc', 'wallet_id');
		$wallet_id = $wallet_amount[0]['wallet_id'];
		$this->session->set_flashdata('add_user','Nexplay, Caenex Game Entry Deleted Successfully');
		if($this->Merchant_modal->update($tbl,$data,$con)){
			if ($tbl=='tbl_nexplay_custom_game') {
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 10;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 10;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 10;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
				$this->session->set_flashdata('add_user','Nexplay, Custom Game Entry Deleted Successfully');
				echo "Nexplay";
			}elseif($tbl=='tbl_nexplay_caenex_game'){
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 10;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 10;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 10;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
				$this->session->set_flashdata('add_user','Nexplay, Caenex Game Entry Deleted Successfully');
				echo "Nexplay";
			}elseif ($tbl=='tbl_nexworld_coupon') {
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 100;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 100;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 100;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
				$this->session->set_flashdata('add_user','Nexworld, Coupon Entry Deleted Successfully');
				echo "Nexworld";
			}elseif ($tbl=='tbl_nexworld_cash') {
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 100;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 100;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 100;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
				$this->session->set_flashdata('add_user','Nexworld, Cash Entry Deleted Successfully');
				echo "Nexworld";
			}elseif ($tbl=='tbl_faq') {
				$this->session->set_flashdata('add_user','FAQ Deleted Successfully');
			}elseif ($tbl=='tbl_game') {
				$con2['gameid']=$id;
				$this->Merchant_modal->update('tbl_category',$data,$con2);
				$this->Merchant_modal->update('tbl_question',$data,$con2);
				
			$this->session->set_flashdata('add_user','Game Deleted Successfully');
				echo "Game";
			}else{
				$this->session->set_flashdata('add_user','Data Deleted Successfully');
				echo "Data";
			}
			
		}

	}
	
	
	public function notification(){
		$order_by='DESC';
		$colum='id';
		$con['type']='2';
		$con['read_status']='1';
		$data['notifications']=$this->Merchant_modal->get_all_data('*','tbl_notification',$con,$order_by,$colum);
		$this->load->view('merchant/all_notification',$data);
	}

	public function Login_mobile(){
		$countries['country']=$this->Merchant_modal->countries();
		$this->load->view('merchant/Login_mobile',$countries);
	}
	// public function business_details(){
	// 	$this->load->view('merchant/business_details');
	// }
	// public function business_submit(){

	// }
	public function faq(){
		$search = $this->input->post('search_faq');
		if($search){
			$faq_data['faq_search']=$this->Merchant_modal->faq_select($search);
		}
		else{
			$faq_data['faq']=$this->Merchant_modal->faq_select();
		}
		//echo $this->db->last_query();
		$this->load->view('merchant/faq',$faq_data);
	}

	public function faq_search(){
		
		$this->load->view('merchant/faq',$faq_data);
	}
	public function faq_submit(){
		$faqname=$this->input->post('faq_name');
		$faqmessage=$this->input->post('faq_message');
		$faq_data=array(
			'faq_name'=>$faqname,
			'faq_message'=>$faqmessage,
		);
		if($this->Merchant_modal->faq($faq_data))
		{
			$this->session->set_flashdata('success','FAQ Added successfully');
			redirect('faq');
		}
	}


}
?>