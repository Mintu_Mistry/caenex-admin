<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('Merchant_modal');
     // $this->load->library('csvimport');
      	$this->load->helper('directory');
		
	}

	public function index()
	{	
		$submit=$this->input->post('submit');
		if(isset($submit)){
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			if(!empty($email) && !empty($password))
			{
				$data = array('email' =>$this->input->post('email') ,'password' =>$this->input->post('password'));

				$con['email']=$this->input->post('email');
				$con['password']=$this->input->post('password');

				$check_user_details=$this->Merchant_modal->get_row_data('*','tbl_admin',$data);
				//print_r($check_user_details);die;
				if(!empty($check_user_details))
				{
				if ($check_user_details['email'] == $con['email']) 
				{
					if($check_user_details['email'] == $con['email'] && $check_user_details['password'] == $con['password']){
					$this->session->set_userdata('id',$check_user_details['id']);
					$this->session->set_userdata('name',$check_user_details['name']);
					
					redirect("admin_dashboard");
					}else{
						$this->session->set_flashdata('loginerror','Password is invalid');
					}
				}else{
					$this->session->set_flashdata('loginerror','Email is invalid');
				}
				}else{
					$this->session->set_flashdata('loginerror','Email or Password is invalid');
				}
			}else{
				$this->session->set_flashdata('loginerror','Email or Password field can not be empty.');
			}
		}
		$this->load->view('admin/login');
	}
	
	/* app web view submit for stripe payment */
	public function payment_web($auth_key)
	{
		$data = array();
		$data['auth_key'] = $auth_key;
		$con['status'] = '0';
		$con['auth_key'] = $auth_key;
		$user_data = $this->Merchant_modal->get_all_data('*','tbl_webview_payment',$con);
		if(!empty($user_data))
		{
			$con1['paymethod_id'] = $user_data[0]['card_id'];
			$user_data1 = $this->Merchant_modal->get_all_data('*','tbl_paymentmethods',$con1);
			if(!empty($user_data1))
			{
				$data['card_details'] = $user_data1;
			}
		}
		/* print_r($user_data);
		die(); */
		
		$this->load->view('admin/payment',$data);
	}
	
	public function success()
	{
		$data = array();
		$this->load->view('admin/success',$data);
	}
	
	public function fail()
	{
		$data = array();
		$this->load->view('admin/fail',$data);
	}
	
	public function payment_submit()
	{
		$data = array();
		$user_token = $_POST['user_token'];
		$con['status'] = '0';
		$con['auth_key'] = $user_token;
		$user_data = $this->Merchant_modal->get_all_data('*','tbl_webview_payment',$con);
		if(!empty($user_data))
		{
			
			$con1['id'] = $user_data[0]['userid'];
			$user_data1 = $this->Merchant_modal->get_all_data('*','tbl_user',$con1);
			if(!empty($user_data1))
			{
				$card_holder_name = $_POST['card_holder_name'];
				$card_number = $_POST['card_number'];
				$month = $_POST['month'];
				$year = $_POST['year'];
				$cvv = $_POST['cvv'];
				$amount = $user_data[0]['amount'];
				$nickname = $_POST['nickname'];
				$token= $this->input->post('token');
				//print_r($token);die;
				$stripe = array(
					"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
					"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
				);
				require_once APPPATH."third_party/stripe/init.php";

				\Stripe\Stripe::setApiKey($stripe['secret_key']);
				$customer = \Stripe\Customer::create(array(
				'email' => $user_data1[0]['email'],
				'source'  => $token
				));
				
				$itemName = "Ceanex";
				$itemNumber = 'PS123456';
				$itemPrice = $amount * 100;
				$currency = "usd";
				$orderID = "SKA92712382139"; 		
				$charge = \Stripe\Charge::create(array(
					"amount" => $itemPrice,
					"currency" => "usd",
					"customer" => $customer->id,
					'description' => $itemNumber,
				));	
				$chargeJson = $charge->jsonSerialize();
				/* echo "<pre>";
				print_r($chargeJson);
				die(); */
				$tancation_data=array('friend_id'=>$user_data[0]['userid'],'user_id'=>$user_data[0]['userid'],'amount'=>$amount,'payment_type'=>3,'transaction_id'=>$chargeJson['id'],'request_time'=>date('H:i:s'),'request_date'=>date('Y-m-d'),'transaction_status'=>1,'user_comment'=>'Add to Wallet');
				$this->Merchant_modal->insert('tbl_user_transaction',$tancation_data);
				$con2['user_id'] = $user_data[0]['userid'];
				//user_wallet_id	user_id	total_avail_balance	total_credit_balance	total_withdraw_balance	last_added_amount	last_update_date
				$wallet_amount = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con2, 'desc', 'wallet_id');
				if(!empty($wallet_amount))
				{
					//echo $this->db->last_query();
					//print_r($wallet_amount[0]['wallet_id']);
					$wallet_id = $wallet_amount[0]['wallet_id'];
					//print_r($wallet_id);
					$total_credit_balance = $amount;
					$total_avail_balance = $wallet_amount[0]['total_avail_balance'];
					$total_credit_balance1 = $total_avail_balance + $total_credit_balance;
			
					$wallet_data = array('total_avail_balance'=> $total_credit_balance1,'total_credit_balance'=> $total_credit_balance,'last_added_amount'=> $total_credit_balance,'user_type'=>1,'created_date'=> date('Y-m-d'));
					$where = "wallet_id = '$wallet_id'";
					if($wallet_id = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where)){
					
					$res = $this->Merchant_modal->update_webview_payment_status(array('status' => 1),$user_data[0]['webview_id']);
					}
				}else{
					
					$total_credit_balance = $amount;
					$total_withdraw_balance = 0;
					$total_avail_balance = $amount;
					$total_credit_balance1 = $amount;
					$total_avail_balance1 = $amount;
					$last_added_amount = $amount;
					$data = array('user_id'=> $user_data[0]['userid'],'total_avail_balance'=> $total_avail_balance1,'total_credit_balance'=> $total_credit_balance,'total_withdraw_balance'=> $total_withdraw_balance,'last_added_amount'=> $last_added_amount,'user_type'=>1,'created_date'=> date('Y-m-d')); 
					if($last_id = $this->Merchant_modal->insert('tbl_wallet',$data)){
						$res = $this->Merchant_modal->update_webview_payment_status(array('status' => 1),$user_data[0]['webview_id']);
					}
					
				}
				$this->session->set_flashdata('payment_success', '$'.$amount.'adeed to your wallet successfully');
					//$this->load->view('admin/success',$data);
				redirect(base_url('success'));
			}else{
				$this->session->set_flashdata('payment_error','Payment Fail');
				redirect(base_url('fail'));
				//$this->load->view('admin/fail',$data);
			}
		}else{
			$this->session->set_flashdata('payment_error','Payment Fail , Token Expire!');
			//redirect(base_url('fail'));
		}
		
	}
	
	/* app web view submit for stripe payment */
	
	
	public function admin_dashboard(){
		$con['status']='1';
		$con['delete_status']='1';
		$data['user']=$this->Merchant_modal->get_all_data('*','tbl_user',$con);
		$data['user']=$this->Merchant_modal->get_all_data('*','tbl_user',$con);
		$con2['tbl_nexworld_cash.delete_status']='1';
		$con2['tbl_nexworld_cash.status']='1';
		$joincon="tbl_nexworld_cash.merchant_id=tbl_merchants.merchant_id";
		$nexworld_cash=$this->Merchant_modal->join_data('tbl_nexworld_cash.*,tbl_merchants.merchant_name','tbl_nexworld_cash','tbl_merchants',$joincon,$con2);
		$joincon1="tbl_nexworld_coupon.merchant_id=tbl_merchants.merchant_id";
		$con3['tbl_nexworld_coupon.delete_status']='1';
		$con3['tbl_nexworld_coupon.status']='1';
		$nexworld_coupon=$this->Merchant_modal->join_data('tbl_nexworld_coupon.*,tbl_merchants.merchant_name','tbl_nexworld_coupon','tbl_merchants',$joincon1,$con3);
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
		usort($array_merge1, array("Admin", "date_compare"));
		$data['nexworld']=$array_merge1;


		$con5['tbl_nexplay_caenex_game.delete_status']='1';
		$con5['tbl_nexplay_caenex_game.status']='1';
		$joincon5="tbl_nexplay_caenex_game.merchant_id=tbl_merchants.merchant_id";
		$nexplay_caenex_game=$this->Merchant_modal->join_data('tbl_nexplay_caenex_game.*,tbl_merchants.merchant_name','tbl_nexplay_caenex_game','tbl_merchants',$joincon5,$con5);

		$con4['tbl_nexplay_custom_game.delete_status']='1';
		$con4['tbl_nexplay_custom_game.status']='1';
		$joincon4="tbl_nexplay_custom_game.merchant_id=tbl_merchants.merchant_id";
		$nexplay_custom_game=$this->Merchant_modal->join_data('tbl_nexplay_custom_game.*,tbl_merchants.merchant_name','tbl_nexplay_custom_game','tbl_merchants',$joincon4,$con4);
		$array_merge2=array_merge($nexplay_caenex_game,$nexplay_custom_game);
		if (!empty($array_merge2)) {
		usort($array_merge2, array("Admin", "date_compare"));
		}
		
		$data['nexplay']=$array_merge2;

		$data['merchant']=$this->Merchant_modal->get_all_data('*','tbl_merchants',$con);
		$this->load->view('admin/dashboard',$data);
	}
	

	public function user_management(){
		$con['delete_status']=1;
		$order_by='DESC';
		$colum='id';
		$userarr = $this->Merchant_modal->get_all_data('*','tbl_user',$con,$order_by,$colum);
		$user_arr = array();
		foreach($userarr as $userdata)
		{
			$user_type =1;
			$wallet_money = $this->Merchant_modal->get_user_wallet($userdata['id'],$user_type);
			/* echo "<pre>";
			print_r($wallet_money);
			die(); */
			if(!empty($wallet_money))
			{
				$userdata['wallet_money'] = $wallet_money[0]['total_avail_balance'];
				$userdata['coupon_earned'] = $wallet_money[0]['coupon_money'];
				$userdata['nexplay_money'] = $wallet_money[0]['nexplay_money'];
				$userdata['nexworld_money'] = $wallet_money[0]['nextworld_money'];
			}else{
				$userdata['wallet_money'] = "0";
				$userdata['coupon_earned'] = "0";
				$userdata['nexplay_money'] = "0";
				$userdata['nexworld_money'] = "0";
			}
			$user_arr[] = $userdata;
		}
		/* echo "<pre>";
		print_r($user_arr);
		die(); */
		
		$data['user'] = $user_arr;
		//echo $this->db->last_query();die;
		$this->load->view('admin/user_management',$data);
	}

	public function add_user(){
		$user_id=$this->input->post('user_id');
		// if (empty($user_id)) {
		 $user_data = array('name' =>$this->input->post('name') ,'email' =>$this->input->post('email') ,'password' =>$this->input->post('passowrd') ,'street' =>$this->input->post('street') ,'city' =>$this->input->post('city') ,'state' =>$this->input->post('state') ,'zip' =>$this->input->post('zip') ,'country' =>$this->input->post('country') ,'phone' =>$this->input->post('phone') );
		
		if(!empty($user_id)){
			$user_data = array('name' =>$this->input->post('name') ,'email' =>$this->input->post('email') ,'street' =>$this->input->post('street') ,'city' =>$this->input->post('city') ,'state' =>$this->input->post('state') ,'zip' =>$this->input->post('zip') ,'country' =>$this->input->post('country') ,'phone' =>$this->input->post('phone'),'status'=>$this->input->post('status') );
			$com['id']=$user_id;
		if($this->Merchant_modal->update('tbl_user',$user_data,$com)){
				$this->session->set_flashdata('add_user','User Updated Successfully');
				redirect(base_url('user_management'));
			}
		}else{
			$con['email']=$this->input->post('email');

		if($this->Merchant_modal->get_row('*','tbl_user',$con)){

			$this->session->set_flashdata('user_error','Email already exists');

		}else{

			if($this->Merchant_modal->insert('tbl_user',$user_data)){
				$this->session->set_flashdata('add_user','User Added Successfully');
				
			}
		}
		}
		redirect(base_url('user_management'));
	}


	public function check_email(){
		
		$con['merchant_email']=$this->input->post('email');
		$data=$this->Merchant_modal->get_row('merchant_email','tbl_merchants',$con);
		if ($data) {
			echo $data; //"Email Already Exists"
		}else{
			echo $data;
		}		

	}


	public function merchant_management()
	{
		$order_by='DESC';
		$colum='merchant_id';
		$con['delete_status']=1;
		$merchant_arr = $this->Merchant_modal->get_all_data('*','tbl_merchants',$con,$order_by,$colum);
		$merchant_data_arr = array();
		
		foreach($merchant_arr as $merchant_data)
		{
			//$merchant_data['merchant_id'] = '2';
			$total_revenue = $this->Merchant_modal->get_merchant_revenue($merchant_data['merchant_id']); 
			/* echo "<pre>";
			print_r($total_revenue);
			die(); */
			$merchant_data['total_revenue'] = '0';
			if(count($total_revenue) > 0)
			{
				if(!empty($total_revenue['total_revenue']))
				{
					$merchant_data['total_revenue'] = $total_revenue['total_revenue'];
				}else{
					$merchant_data['total_revenue'] = '0';
				}
			}else{
				$merchant_data['total_revenue'] = '0';
			}
			
			$merchant_data_arr[] = $merchant_data;
		}
		//echo "<pre>";
		//print_r($merchant_data_arr);
		//die();
		$data['merchant'] = $merchant_data_arr;
		$this->load->view('admin/merchant_management',$data);	

	}

	public function add_merchant(){
		$merchant_data = array('merchant_name' =>$this->input->post('merchant_name') ,'businessname' =>$this->input->post('businessname') ,'merchant_email' =>$this->input->post('merchant_email') ,'user_password' =>$this->input->post('user_password') ,'merchant_address' =>$this->input->post('merchant_address') ,'merchant_city' =>$this->input->post('merchant_city') ,'merchant_state' =>$this->input->post('merchant_state') ,'zip' =>$this->input->post('zip') ,'merchant_country' =>$this->input->post('merchant_country'),'merchant_taxid'=>$this->input->post('taxid'));

		$merchant_id=$this->input->post('merchant_id');
		if ($merchant_id) {
			$merchant_data_update = array('merchant_name' =>$this->input->post('merchant_name') ,'businessname' =>$this->input->post('businessname') ,'merchant_email' =>$this->input->post('merchant_email') ,'merchant_address' =>$this->input->post('merchant_address') ,'merchant_city' =>$this->input->post('merchant_city') ,'merchant_state' =>$this->input->post('merchant_state') ,'zip' =>$this->input->post('zip') ,'merchant_country' =>$this->input->post('merchant_country'),'status' =>$this->input->post('status'),'merchant_taxid'=>$this->input->post('taxid'));
			$com['merchant_id']=$merchant_id;
			if($this->Merchant_modal->update('tbl_merchants',$merchant_data_update,$com)){
				$this->session->set_flashdata('corrct','Merchant Updated Successfully');
			}
			
		}else{
			
			$con['merchant_email']=$this->input->post('merchant_email');

			if($this->Merchant_modal->get_row('*','tbl_merchants',$con)){
				$dd = $this->Merchant_modal->get_row('*','tbl_merchants',$con);
				
				$this->session->set_flashdata('merchant_error','Email already exists');

			}else{

				if($this->Merchant_modal->insert('tbl_merchants',$merchant_data)){
					$this->session->set_flashdata('corrct','Merchant Added Successfully');
				}else{
					$this->session->set_flashdata('merchant_error','Please try later');
				}
			}
		}
		redirect(base_url('merchant_management'));
	}

	
	public function get_merchant_details()
	{
		$order_by='DESC';
		$colum='merchant_id';
		$con['delete_status']=1;
		//$from = $this->input->post('from');
		//$to = $this->input->post('to');
		$from = date('Y-m-d', strtotime($this->input->post('from')));
		$to = date('Y-m-d', strtotime($this->input->post('to')));
		$merchant_arr = $this->Merchant_modal->filter_merchant_data('*','tbl_merchants',$con,$order_by,$colum);
		$merchant_data_arr = array();
		$i =0;
		$data = array();
		
		/* [merchant_id] => 26
            [merchant_name] => sZCsdc
            [merchant_email] => sdsa@df.com
            [merchant_phone] => 
            [user_password] => asdashJJ@333
            [merchant_address] => sdcs
            [merchant_city] => Noida
            [merchant_state] => UP
            [merchant_country] => India
            [location_lat] => 
            [location_long] => 
            [businessname] => zsdcs
            [merchant_taxid] => 
            [otp] => 
            [img] => 
            [delete_status] => 1
            [status] => 1
            [create_date] => 2021-01-29 15:01:30
            [zip] => 34353
            [merchant_phonepin] =>  */
		foreach($merchant_arr as $merchant_data)
		{
			$i++;
			//$merchant_data['merchant_id'] = '2';
			$total_revenue = $this->Merchant_modal->get_merchant_revenue($merchant_data['merchant_id'],$from,$to);
			/* echo $this->db->last_query();
			echo "<pre>";
			print_r($total_revenue);
			die(); */
			$merchant_data['total_revenue'] = '0';
			if(count($total_revenue) > 0)
			{
				if(!empty($total_revenue['total_revenue']))
				{
					$merchant_data['total_revenue'] = $total_revenue['total_revenue'];
				}else{
					$merchant_data['total_revenue'] = '0';
				}
			}else{
				$merchant_data['total_revenue'] = '0';
			}
			//echo "<pre>";
			//print_r($merchant_data);
			//die();
			if($merchant_data['status']==1){
				
				$status = "<span class='badge badge-success'>Active</span>";
			}else{
				$status = "<span class='badge badge-warning'>Inactive</span>"; 
			}
			$merchant_id = $merchant_data['merchant_id'];
			$edit = "<a href='javascript:void(0)'><i class='align-middle mr-2 fas fa-fw fa-edit' data-toggle='modal' data-target='#sizedModalSm$i'></i></a>";
											
			$delete = "<a href='javascript:void(0)' onclick='delete_data($merchant_id)'><i class='align-middle mr-2 far fa-fw fa-trash-alt'></i></a>";
			
			$view = "<a href='".base_url('billboard_details/'). $merchant_id."'><i class='align-middle fas fa-eye'></i></a>";
													
			$row = array();
			$row[] = $i;
			$row[] = $merchant_data['merchant_name'];
			$row[] = $merchant_data['businessname'];
			$row[] = $merchant_data['merchant_email'];
			$row[] = $merchant_data['merchant_address'].' , '.$merchant_data['merchant_city'].' , '.$merchant_data['merchant_state'].' , '.$merchant_data['merchant_country'].' , '.$merchant_data['zip'];
			$row[] = $merchant_data['merchant_taxid'];
			$row[] = "$".$merchant_data['total_revenue'];
			$row[] = $status;
			$row[] = $edit.$delete.$view;
			$data[]= $row; 
        	
		}
		
		
		echo json_encode(array("data"=>$data));
	}
	
	public function delete(){

		$id=$this->input->post('id');
		$tbl=$this->input->post('tbl');
		$row=$this->input->post('row');
		$data['delete_status']=0;
		$con[$row]=$id;
		
		$con1['user_id'] = $this->session->userdata('merchant_id');
		$wallet_amount = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con1, 'desc', 'wallet_id');
		$wallet_id = $wallet_amount[0]['wallet_id'];
		
		if($this->Merchant_modal->update($tbl,$data,$con)){
			if ($tbl=='tbl_nexplay_custom_game') {
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 10;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 10;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 10;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
				
				$this->session->set_flashdata('add_user','Nexplay, Custom Game Entry Deleted Successfully');
			}elseif($tbl=='tbl_nexplay_caenex_game'){
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 10;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 10;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 10;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
			
				$this->session->set_flashdata('add_user','Nexplay, Caenex Game Entry Deleted Successfully');
			}elseif ($tbl=='tbl_nexworld_coupon') {
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 100;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 100;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 100;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
				
				$this->session->set_flashdata('add_user','Nexworld, Coupon Entry Deleted Successfully');
			}elseif ($tbl=='tbl_nexworld_cash') {
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 100;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 100;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 100;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
				
				$this->session->set_flashdata('add_user','Nexworld, Cash Entry Deleted Successfully');
			}elseif ($tbl=='tbl_faq') {
				$this->session->set_flashdata('add_user','FAQ Deleted Successfully');
			}elseif ($tbl=='tbl_game') {
				$con2['gameid']=$id;
				$this->Merchant_modal->update('tbl_category',$data,$con2);
				$this->Merchant_modal->update('tbl_question',$data,$con2);
				/* echo "sdfds";
				die(); */
			$this->session->set_flashdata('add_user','Game Deleted Successfully');
			}else{
				$this->session->set_flashdata('add_user','Data Deleted Successfully');
			}
			echo "1";
		}

	}

	public function update_status(){
		
		$data['id_status']=$this->input->post('status');
		$con['id']=$this->input->post('id');
		//echo $this->input->post('id');
		//die();
		if($this->Merchant_modal->update('tbl_id_verification1',$data,$con)){
			
			$id_data = $this->Merchant_modal->get_all_data('*',' tbl_id_verification1', $con);
			
			if($id_data[0]['id_status'] ==1)
			{
				$title= "Your ID document(s) has been approved by the Admin";
				
				if($id_data[0]['user_type'] ==1)
				{
					$con1['userid']=$id_data[0]['merchant_id'];
					$check_token = $this->Merchant_modal->get_all_data('*',' tbl_device_info', $con1);
					if(!empty($check_token))
					{
						foreach($check_token as $user_token)
						{
							
							$token = $user_token['fcm_token'];
							$msg = "Your ID document(s) has been approved by the Admin.";
							$heading = "Id Verified";
							$this->sendGCM($token,$msg,$heading);
						}
						
					}
							
					$notification = array('title' => $title,'table_data'=>'id_verification_user','type'=>3,'userid'=>$id_data[0]['merchant_id']);
				}else{
					$notification = array('title' => $title,'table_data'=>'id_verification','type'=>2,'userid'=>$id_data[0]['merchant_id']);
				}
			}else{
				$title= "Your ID document(s) has been rejected by the Admin. Please upload your document again";
				if($id_data[0]['user_type'] ==1)
				{	
					$con1['userid']=$id_data[0]['merchant_id'];
					$check_token = $this->Merchant_modal->get_all_data('*',' tbl_device_info', $con1);
					if(!empty($check_token))
					{
						foreach($check_token as $user_token)
						{
							
							$token = $user_token['fcm_token'];
							$msg = "Your ID document(s) has been approved by the Admin.";
							$heading = "Id Verified";
							$this->sendGCM($token,$msg,$heading);
						}
						
					}
					$notification = array('title' => $title,'table_data'=>'id_verification_user','type'=>3,'userid'=>$id_data[0]['merchant_id']);
				}else{
					$notification = array('title' => $title,'table_data'=>'id_verification','type'=>2,'userid'=>$id_data[0]['merchant_id']);
				}
			}
			$this->Merchant_modal->insert('tbl_notification',$notification);
			$this->session->set_flashdata('corrct','Status Updated Successfully');
			echo "1";
		}

	}

	public function update_merchant_status(){
		
		$data['add_status']=$this->input->post('status');
		$data['status']=$this->input->post('status');
		$con['id']=$this->input->post('id');
		$tbl=$this->input->post('tbl');
		if($this->Merchant_modal->update($tbl,$data,$con)){
			echo "1";
			if($this->Merchant_modal->update($tbl,$data,$con)){
			if ($tbl=='tbl_nexplay_custom_game') {
				$this->session->set_flashdata('add_user','Nexplay Custom Game Entry Status Changed Successfully');
			}elseif($tbl=='tbl_nexplay_caenex_game'){
				$this->session->set_flashdata('add_user','Nexplay Caenex Game Entry Status Changed Successfully');
			}elseif ($tbl=='tbl_nexworld_coupon') {
				$this->session->set_flashdata('add_user','Nexworld Coupon Entry Status Changed Successfully');
			}elseif ($tbl=='tbl_nexworld_cash') {
				$this->session->set_flashdata('add_user','Nexworld Cash Entry Status Changed Successfully');
			}else{
			$this->session->set_flashdata('add_user','Status Changed Successfully');
		}
			
		}

	}
}


	public function change_merchant_status(){
		
		$data['add_status']=$this->input->post('status');
		$data['status']=$this->input->post('status');
		$con['id']=$this->input->post('id');
		$tbl=$this->input->post('tbl');
		if($this->Merchant_modal->update($tbl,$data,$con)){
			//echo "1";
			if($this->Merchant_modal->update($tbl,$data,$con)){
			if ($tbl=='tbl_nexplay_custom_game') {
				$this->session->set_flashdata('add_user','Nexplay Custom Game Entry Status Changed Successfully');
				echo "Nexplay";
			}elseif($tbl=='tbl_nexplay_caenex_game'){
				$this->session->set_flashdata('add_user','Nexplay Caenex Game Entry Status Changed Successfully');
				echo "Nexplay";
			}elseif ($tbl=='tbl_nexworld_coupon') {
				$this->session->set_flashdata('add_user','Nexworld Coupon Entry Status Changed Successfully');
				echo "Nexworld";
			}elseif ($tbl=='tbl_nexworld_cash') {
				$this->session->set_flashdata('add_user','Nexworld Cash Entry Status Changed Successfully');
				echo "Nexworld";
			}else{
				$this->session->set_flashdata('add_user','Status Changed Successfully');
				echo "Status";
			}
			/* echo $this->db->last_query();
			die(); */
			//echo 1;
		}
	}
}


	function date_compare($element1, $element2) { 

    $datetime1 = strtotime($element1['create_date']); 
    $datetime2 = strtotime($element2['create_date']); 
    // print_r($element1['create_date']);
    // print_r($element2['create_date']);die;
    return $datetime1 - $datetime2; 

	}


	public function billboard_management(){
		$con['tbl_nexworld_cash.delete_status']='1';
		$joincon="tbl_nexworld_cash.merchant_id=tbl_merchants.merchant_id";
		$nexworld_cash=$this->Merchant_modal->join_data('tbl_nexworld_cash.*,tbl_merchants.merchant_name','tbl_nexworld_cash','tbl_merchants',$joincon,$con);
		$con1['tbl_nexworld_coupon.delete_status']='1';
		$joincon1="tbl_nexworld_coupon.merchant_id=tbl_merchants.merchant_id";
		$nexworld_coupon=$this->Merchant_modal->join_data('tbl_nexworld_coupon.*,tbl_merchants.merchant_name','tbl_nexworld_coupon','tbl_merchants',$joincon1,$con1);
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
		usort($array_merge1, array("Admin", "date_compare"));
		$data['nexworld']=$array_merge1;


		$con2['tbl_nexplay_caenex_game.delete_status']='1';
		//$con2['tbl_nexplay_caenex_game.merchant_id']=$id;
		$joincon2="tbl_nexplay_caenex_game.merchant_id=tbl_merchants.merchant_id";
		$nexplay_caenex_game=$this->Merchant_modal->join_data('tbl_nexplay_caenex_game.*,tbl_merchants.merchant_name','tbl_nexplay_caenex_game','tbl_merchants',$joincon2,$con2);

		$con3['tbl_nexplay_custom_game.delete_status']='1';
		//$con3['tbl_nexplay_custom_game.merchant_id']=$id;
		$joincon3="tbl_nexplay_custom_game.merchant_id=tbl_merchants.merchant_id";
		$nexplay_custom_game=$this->Merchant_modal->join_data('tbl_nexplay_custom_game.*,tbl_merchants.merchant_name','tbl_nexplay_custom_game','tbl_merchants',$joincon3,$con3);
		$array_merge2=array_merge($nexplay_caenex_game,$nexplay_custom_game);
		//$array_merge3=array_merge($array_merge1,$array_merge2);
		if (!empty($array_merge2)) {
		usort($array_merge2, array("Admin", "date_compare"));
		}
		//$data['billboard_data']=$array_merge3;
		$data['nexplay']=$array_merge2;

		
		$this->load->view('admin/billboard_management',$data);	
	}
	
	
		public function billboard_nexplay(){
		$con['tbl_nexworld_cash.delete_status']='1';
		$joincon="tbl_nexworld_cash.merchant_id=tbl_merchants.merchant_id";
		$nexworld_cash=$this->Merchant_modal->join_data('tbl_nexworld_cash.*,tbl_merchants.merchant_name','tbl_nexworld_cash','tbl_merchants',$joincon,$con);
		$con1['tbl_nexworld_coupon.delete_status']='1';
		$joincon1="tbl_nexworld_coupon.merchant_id=tbl_merchants.merchant_id";
		$nexworld_coupon=$this->Merchant_modal->join_data('tbl_nexworld_coupon.*,tbl_merchants.merchant_name','tbl_nexworld_coupon','tbl_merchants',$joincon1,$con1);
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
		usort($array_merge1, array("Admin", "date_compare"));
		$data['nexworld']=$array_merge1;


		$con2['tbl_nexplay_caenex_game.delete_status']='1';
		//$con2['tbl_nexplay_caenex_game.merchant_id']=$id;
		$joincon2="tbl_nexplay_caenex_game.merchant_id=tbl_merchants.merchant_id";
		$nexplay_caenex_game=$this->Merchant_modal->join_data('tbl_nexplay_caenex_game.*,tbl_merchants.merchant_name','tbl_nexplay_caenex_game','tbl_merchants',$joincon2,$con2);

		$con3['tbl_nexplay_custom_game.delete_status']='1';
		//$con3['tbl_nexplay_custom_game.merchant_id']=$id;
		$joincon3="tbl_nexplay_custom_game.merchant_id=tbl_merchants.merchant_id";
		$nexplay_custom_game=$this->Merchant_modal->join_data('tbl_nexplay_custom_game.*,tbl_merchants.merchant_name','tbl_nexplay_custom_game','tbl_merchants',$joincon3,$con3);
		$array_merge2=array_merge($nexplay_caenex_game,$nexplay_custom_game);
		//$array_merge3=array_merge($array_merge1,$array_merge2);
		if (!empty($array_merge2)) {
		usort($array_merge2, array("Admin", "date_compare"));
		}
		//$data['billboard_data']=$array_merge3;
		$data['nexplay']=$array_merge2;

		
		$this->load->view('admin/billboard_nexplay',$data);	
	}
	
	public function user_transition()
	{
		$order_by='DESC';
		$colum='user_transaction_id';
		$con1['transaction_status'] = 1;
		$user_transaction=$this->Merchant_modal->get_all_data('*','tbl_user_transaction',$con1,$order_by,$colum);
		$transaction_arr =array();
		foreach($user_transaction as $transaction)
		{
			$con['id'] = $transaction['user_id'];
			$username = $this->Merchant_modal->get_row_data('*','tbl_user',$con);
			if(!empty($username))
			{
				$transaction['user_name'] = $username['name'];
				$transaction_arr[] = $transaction;
			}
		}
		$data['user_transaction'] = $transaction_arr;
		/* echo "<pre>";
		print_r($data);
		die(); */
		$this->load->view('admin/user_transition',$data);	
	}

	public function merchant_transition()
	{
		$order_by='DESC';
		$colum='id';
		$merchant_transaction=$this->Merchant_modal->get_all_data('*','tbl_transaction','',$order_by,$colum);
		
		$transaction_arr =array();
		foreach($merchant_transaction as $transaction)
		{
			$con['merchant_id'] = $transaction['user_id'];
			$username = $this->Merchant_modal->get_row_data('*','tbl_merchants',$con);
			if(!empty($username))
			{
				$transaction['merchant_name'] = $username['merchant_name'];
			}else{
				$transaction['merchant_name'] = "";
			}
			$transaction_arr[] = $transaction;
		}
		$data['merchant_transaction'] = $transaction_arr;
		
		$this->load->view('admin/merchant_transition',$data);
	}

	public function id_verification_admin(){
		$joincon="tbl_id_verification1.merchant_id=tbl_merchants.merchant_id";
		$con2['tbl_merchants.delete_status']='1';
		$con2['tbl_merchants.status']='1';
		$con2['tbl_id_verification1.user_type']='2';
		$order_by='DESC';
		$colum='tbl_id_verification1.id';
		$data['id_verification']=$this->Merchant_modal->join_data('tbl_id_verification1.*,tbl_merchants.merchant_name,tbl_merchants.merchant_email','tbl_id_verification1','tbl_merchants',$joincon,$con2,$order_by,$colum);
		// $order_by='DESC';
		// $colum='id';
		// $data['id_verification']=$this->Merchant_modal->get_all_data('*','tbl_id_verification1','',$order_by,$colum);

		$this->load->view('admin/id_verification',$data);

	}
	
	
	public function id_verification_user(){
		$joincon="tbl_id_verification1.merchant_id=tbl_user.id";
		$con2['tbl_user.delete_status']='1';
		$con2['tbl_user.status']='1';
		$con2['tbl_id_verification1.user_type']='1';
		$order_by='DESC';
		$colum='tbl_id_verification1.id';
		$data['id_verification']=$this->Merchant_modal->join_data('tbl_id_verification1.*,tbl_user.name,tbl_user.email','tbl_id_verification1','tbl_user',$joincon,$con2,$order_by,$colum);
		// $order_by='DESC';
		// $colum='id';
		// $data['id_verification']=$this->Merchant_modal->get_all_data('*','tbl_id_verification1','',$order_by,$colum);

		$this->load->view('admin/id_verification_user',$data);

	}


	public function user_terms_conditions(){
		$con['id']='1';
		$data['content']=$this->Merchant_modal->get_row_data('*','tbl_content',$con);
		//print_r($data['content']);die;
		$this->load->view('admin/user_terms_conditions',$data);
	}

	public function update_user_terms_conditions(){
		$submit=$this->input->post('submit');
		if (isset($submit)) {
			$con['id']=$this->input->post('id');
			$data['details']=$this->input->post('content');
			if($this->Merchant_modal->update('tbl_content',$data,$con)){
				if ($con['id']==1) {
					$this->session->set_flashdata('u_t_c','User Terms & Conditions Updated Successfully');
					redirect('user_terms_conditions');
				}

				if ($con['id']==2) {
					$this->session->set_flashdata('u_t_c','Merchant Terms & Conditions Updated Successfully');
					redirect('merchant_terms_conditions');
				}

				if ($con['id']==3) {
					$this->session->set_flashdata('u_t_c','User Privacy Policy Updated Successfully');
					redirect('user_privacy_policy');
				}

				if ($con['id']==4) {
					$this->session->set_flashdata('u_t_c','Merchant Privacy Policy Updated Successfully');
					redirect('merchant_privacy_policy');
				}

				if ($con['id']==5) {
					$this->session->set_flashdata('u_t_c','About Us Updated Successfully');
					redirect('about');
				}
			}

		}
	}

	public function merchant_terms_conditions(){
		$con['id']='2';
		$data['content']=$this->Merchant_modal->get_row_data('*','tbl_content',$con);
		
		$this->load->view('admin/merchant_terms_conditions',$data);
	}

	public function user_privacy_policy(){
		$con['id']='3';
		$data['content']=$this->Merchant_modal->get_row_data('*','tbl_content',$con);
		$this->load->view('admin/user_privacy_policy',$data);
	}

	public function merchant_privacy_policy(){
		$con['id']='4';
		$data['content']=$this->Merchant_modal->get_row_data('*','tbl_content',$con);
		$this->load->view('admin/merchant_privacy_policy',$data);
	}

	public function admin_profile(){
		$submit=$this->input->post('submit');
		if (isset($submit)) {
			
			$admin_data = array('name' => $this->input->post('name'),'email' => $this->input->post('email'), );
			$com['id']='1';
			if($this->Merchant_modal->update('tbl_admin',$admin_data,$com)){
				$this->session->set_flashdata('admin_data','Profile Updated Successfully');
			}
		}
		$data['data']=$this->Merchant_modal->get_row_data('*','tbl_admin');

		$this->load->view('admin/admin_profile',$data);
	}

	public function logout(){
			$this->session->sess_destroy();
			redirect(base_url('Admin'));
	}

	public function check_password(){
		$con['password']=$this->input->post('oldpassword');

		$data=$this->Merchant_modal->get_row('*','tbl_admin',$con);
		if ($data) {
			echo 1;
		}else{
			echo 2;
		}
		
	}

	public function change_password(){
		$admin_data['password']=$this->input->post('newpassword');
		$com['id']='1';
		if($this->Merchant_modal->update('tbl_admin',$admin_data,$com)){
				$this->session->set_flashdata('admin_data','Password Changed Successfully');
			}

			redirect("admin_profile");
	}
	
	public function change_status(){
		$com['id']=$this->input->post('id');
		$data['read_status']='2';
		if($this->Merchant_modal->update('tbl_notification',$data,$com)){
			echo "1";
		}
	}

	public function admin_notification(){
		$order_by='DESC';
		$colum='id';
		$data['notifications']=$this->Merchant_modal->get_all_data('*','tbl_notification','',$order_by,$colum);
		$this->load->view('admin/all_notification',$data);
	}
	
	public function game(){
		$order_by='DESC';
		$colum='game_id';
		$con['delete_status']='1';
		$data['game']=$this->Merchant_modal->get_all_data('*','tbl_game',$con,$order_by,$colum);
		/* print_r($data);
		die(); */
		$this->load->view('admin/game',$data);
	}
	
	public function add_game(){
		$data = array('game_name' =>$this->input->post('game_name') ,'status'=>$this->input->post('status') );
		$uploaddir = './assets/img/photos/';
		$path = $_FILES['file']['name'];
		if(!empty($path))
		{
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			$user_img = time() . rand() . '.' . $ext;
			$uploadfile = $uploaddir . $user_img;
			if ($_FILES["file"]["name"]) {
				if (move_uploaded_file($_FILES["file"]["tmp_name"],$uploadfile)) {
				$data['game_img'] = $user_img;
				}
			}
		}
		//print_r($data);
		//die();
		if (empty($this->input->post('cat_id'))) {
			# code...
		if($this->Merchant_modal->insert('tbl_game',$data)){
				$this->session->set_flashdata('add_user','Game Added Successfully');
				
			}
		}else{
			
			$com['game_id']=$this->input->post('cat_id');
			if($this->Merchant_modal->update('tbl_game',$data,$com)){
			$this->session->set_flashdata('add_user','Game Updated Successfully');
			
			}
			
		}
			redirect(base_url('game'));
	}
	
	public function game_category(){
		$order_by='DESC';
		$colum='id';
		$con['delete_status']='1';
		$category=$this->Merchant_modal->get_all_data('*','tbl_category',$con,$order_by,$colum);
		$cat_arr =array();
		foreach($category as $cat)
		{
			$con2['delete_status']='1';
			$con2['game_id']=$cat['gameid'];
			$game =$this->Merchant_modal->get_all_data('*','tbl_game',$con2);
			$cat['game_name'] = $game[0]['game_name'];
			$cat_arr[]= $cat;
		}
		$data['category'] = $cat_arr;
		$order_by='DESC';
		$colum='game_id';
		$con['delete_status']='1';
		$data['game']=$this->Merchant_modal->get_all_data('*','tbl_game',$con,$order_by,$colum);
		
		$this->load->view('admin/category',$data);
	}

	public function add_category(){
		$data = array('category' =>$this->input->post('name') ,'status'=>$this->input->post('status'),'gameid' =>$this->input->post('game') );
		$uploaddir = './assets/img/photos/';
		$path = $_FILES['file']['name'];
		if(!empty($path))
		{
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			$user_img = time() . rand() . '.' . $ext;
			$uploadfile = $uploaddir . $user_img;
			if ($_FILES["file"]["name"]) {
				if (move_uploaded_file($_FILES["file"]["tmp_name"],$uploadfile)) {
				$data['cat_img'] = $user_img;
				}
			}
		}
		
		if (empty($this->input->post('cat_id'))) {
			# code...
		if($this->Merchant_modal->insert('tbl_category',$data)){
				$this->session->set_flashdata('add_user','Category Added Successfully');
				
			}
		}else{
			$com['id']=$this->input->post('cat_id');
			if($this->Merchant_modal->update('tbl_category',$data,$com)){
				$this->session->set_flashdata('add_user','Category Updated Successfully');
				
			}
		}
			redirect(base_url('category'));
	}

	public function ad_type(){

		$order_by='DESC';
		$colum='id';
		$con['delete_status']='1';
		$data['ad_type']=$this->Merchant_modal->get_all_data('*','tbl_ad_type',$con,$order_by,$colum);
		$this->load->view('admin/ad_type',$data);
	}

	public function add_ad_type(){
		$data = array('ad_type' =>$this->input->post('name') ,'status'=>$this->input->post('status') );
		if (empty($this->input->post('cat_id'))) {
			# code...
		if($this->Merchant_modal->insert('tbl_ad_type',$data)){
				$this->session->set_flashdata('add_user','Ad type Added Successfully');
				
			}
		}else{
			$com['id']=$this->input->post('cat_id');
			if($this->Merchant_modal->update('tbl_ad_type',$data,$com)){
				$this->session->set_flashdata('add_user','Ad type Updated Successfully');
				
			}
		}
			redirect(base_url('ad_type'));
	}


	public function questions(){


		$order_by='DESC';
		$colum='tbl_question.id';
		$con['tbl_question.delete_status']='1';
		
		$joincon="tbl_question_cat.id=tbl_question.question_id";
		$data['questions']=$this->Merchant_modal->join_data('tbl_question.*,tbl_question_cat.category,tbl_question_cat.reward','tbl_question','tbl_question_cat',$joincon,$con,$order_by,$colum);

		$order_by1='DESC';
		$colum1='id';
		$con1['delete_status']='1';
		$con1['status']='1';
		$data['category']=$this->Merchant_modal->get_all_data('*','tbl_category',$con1,$order_by1,$colum1);
		
		$order_by1='DESC';
		$colum1='game_id';
		$con2['delete_status']='1';
		$data['game']=$this->Merchant_modal->get_all_data('*','tbl_game',$con2,$order_by1,$colum1);
		
		$this->load->view('admin/questions',$data);
	}

	public function add_questions(){
		$order_by='DESC';
		$colum='id';
		$con['delete_status']='1';
		$con['status']='1';
		$data['category']=$this->Merchant_modal->get_all_data('*','tbl_category',$con,$order_by,$colum);
		
		$order_by='DESC';
		$colum='game_id';
		$con['delete_status']='1';
		$data['game']=$this->Merchant_modal->get_all_data('*','tbl_game',$con,$order_by,$colum);
		
		$this->load->view('admin/add_questions',$data);
	}

	public function add_questions_data(){

		$quesion=$this->input->post('question');
		$answer1=$this->input->post('answer1');
		$answer2=$this->input->post('answer2');
		$answer3=$this->input->post('answer3');
		$answer4=$this->input->post('answer4');

		$arrayName = array('category' =>$this->input->post('category') ,'reward' =>$this->input->post('custom_reward') , );
		$last_id=$this->Merchant_modal->insert('tbl_question_cat',$arrayName);
		$b=1;

		for($i=0;$i<count($quesion);$i++  ){
			 $a=$b+$i;			
			$c='radio'.$a;			
			$data1 = array('question'=>$quesion[$i],'ans1'=>$answer1[$i],'ans2'=>$answer2[$i],'ans3'=>$answer3[$i],'ans4'=>$answer4[$i],'right_ans'=>$this->input->post($c),'question_id'=>$last_id ,'category' =>$this->input->post('category'),'gameid' =>$this->input->post('game'));
			$this->Merchant_modal->insert('tbl_question',$data1);

		}

		$this->session->set_flashdata('add_user','Question Added Successfully');		
		
		redirect('questions');
	}

	public function update_questions(){

		$this->load->view('admin/update_questions');
	}

	public function update_questions_data(){

		$data = array('question' => $this->input->post('question'),'ans1' => $this->input->post('answer1'),'ans2' => $this->input->post('answer2'),'ans3' => $this->input->post('answer3'),'ans4' => $this->input->post('answer4'),'right_ans' => $this->input->post('radio1'),'status' => $this->input->post('status'),'category' => $this->input->post('category'),'gameid' =>$this->input->post('game'));

		$com['id']=$this->input->post('question_id');
			if($this->Merchant_modal->update('tbl_question',$data,$com)){
				$this->session->set_flashdata('add_user','Question Updated Successfully');
				
			}

		redirect('questions');
	}

	public function billboard_details($id=null){

		$con['tbl_nexworld_cash.delete_status']='1';
		
		$con['tbl_nexworld_cash.merchant_id']=$id; 
		
		$joincon="tbl_nexworld_cash.merchant_id=tbl_merchants.merchant_id";
		
		$nexworld_cash=$this->Merchant_modal->join_data('tbl_nexworld_cash.*,tbl_merchants.merchant_name','tbl_nexworld_cash','tbl_merchants',$joincon,$con);
		
		$con1['tbl_nexworld_coupon.delete_status']='1';
		
		$con1['tbl_nexworld_coupon.merchant_id']=$id;
		
		$joincon1="tbl_nexworld_coupon.merchant_id=tbl_merchants.merchant_id";
		
		$nexworld_coupon=$this->Merchant_modal->join_data('tbl_nexworld_coupon.*,tbl_merchants.merchant_name','tbl_nexworld_coupon','tbl_merchants',$joincon1,$con1);
		
		/* echo "<pre>";
		print_r($nexworld_cash);
		die; */
		
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
		
		if (!empty($array_merge1)) {
		usort($array_merge1, array("Admin", "date_compare"));
			
		}
		$data['nexworld']=$array_merge1;
		
		$nexworld_amount =0;
		foreach($nexworld_cash as $nexworld_data)
		{
			//$nexworld_amount += $nexworld_data['total_amount'];
			$nexworld_amount += $nexworld_data['total_amount'];
		}
		/* echo "<pre>";
		print_r($data['nexworld']);
		die; */

		$con2['tbl_nexplay_caenex_game.delete_status']='1';
		
		$con2['tbl_nexplay_caenex_game.merchant_id']=$id;
		
		$joincon2="tbl_nexplay_caenex_game.merchant_id=tbl_merchants.merchant_id";
		
		$nexplay_caenex_game=$this->Merchant_modal->join_data('tbl_nexplay_caenex_game.*,tbl_merchants.merchant_name','tbl_nexplay_caenex_game','tbl_merchants',$joincon2,$con2);

		$con3['tbl_nexplay_custom_game.delete_status']='1';
		
		$con3['tbl_nexplay_custom_game.merchant_id']=$id;
		
		$joincon3="tbl_nexplay_custom_game.merchant_id=tbl_merchants.merchant_id";
		
		$nexplay_custom_game=$this->Merchant_modal->join_data('tbl_nexplay_custom_game.*,tbl_merchants.merchant_name','tbl_nexplay_custom_game','tbl_merchants',$joincon3,$con3);
		
		$array_merge2=array_merge($nexplay_caenex_game,$nexplay_custom_game);
		
		//$array_merge3=array_merge($array_merge1,$array_merge2);
		if (!empty($array_merge2)) {
		usort($array_merge2, array("Admin", "date_compare"));
		}
		
		//$data['billboard_data']=$array_merge3;
		$data['nexplay']=$array_merge2;
		//echo "<pre>";
		//print_r($data['nexplay']);
		//die;
		$nexplay_amount =0;
		foreach($array_merge2 as $nexplay_data)
		{
			$nexplay_amount += $nexplay_data['total_amount'];
		}
		$data['nexplay_amount']=$nexplay_amount;
		$data['nexworld_amount']=$nexworld_amount;
		$this->load->view('admin/billboard_details',$data);
	}
	public function coupon_details($coupon_id)
	{
		//echo $coupon_id;
		$data = array();
		$this->load->view('admin/coupon_details',$data);
	}
	
	public function cash_details($cash_id)
	{
		//echo $cash_id;
		$data = array();
		$this->load->view('admin/cash_details',$data);
	}
	
	public function game_details($game_id)
	{
		//echo $game_id;
		$data = array();
		$this->load->view('admin/game_details',$data);
	}

	public function about(){
		$con['id']='5';
		$data['content']=$this->Merchant_modal->get_row_data('*','tbl_content',$con);
		$this->load->view('admin/about_us',$data);

	}

	public function help_articles(){

		$con['delete_status']=1;
		$order_by='DESC';
		$colum='id';
		$data['help_articles']=$this->Merchant_modal->get_all_data('*','tbl_help_articles',$con,$order_by,$colum);

		$this->load->view('admin/help_articles',$data);
	}

	public function add_help_articles(){

		$data = array('title' =>$this->input->post('name') ,'details'=>$this->input->post('details') );
		if (empty($this->input->post('cat_id'))) {
			# code...
		if($this->Merchant_modal->insert('tbl_help_articles',$data)){
				$this->session->set_flashdata('add_user','Help Article Added Successfully');
				
			}
		}else{
			$com['id']=$this->input->post('cat_id');
			if($this->Merchant_modal->update('tbl_help_articles',$data,$com)){
				$this->session->set_flashdata('add_user','Help Article Updated Successfully');
				
			}
		}
			redirect(base_url('help_articles'));
	}

	public function faqs(){
		$faq_data['faq']=$this->Merchant_modal->faq_select();
		$this->load->view('admin/faqs', $faq_data);
	}
	public function faq_submit(){
		$faqname=$this->input->post('faq_name');
		$faqmessage=$this->input->post('faq_message');
		$faq_data=array(
			'faq_name'=>$faqname,
			'faq_message'=>$faqmessage,
		);
		if($this->Merchant_modal->faq($faq_data))
		{
			$this->session->set_flashdata('success','FAQ Added successfully');
			redirect('faqs');
		}
	}
	public function edit_faq(){
		$com['faq_id']=$this->input->post('faq_id');
		$faqname=$this->input->post('faq_name');
		$faqmessage=$this->input->post('faq_message');
		$faq_data=array(
			'faq_name'=>$faqname,
			'faq_message'=>$faqmessage,
		);
		if($this->Merchant_modal->update('tbl_faq',$faq_data,$com)){
			$this->session->set_flashdata('add_user','FAQs Updated Successfully');
		}
		redirect('faqs');
	}
	public function delete_faq(){
		$com['faq_id'] = $this->input->post('faq_id');
		if($this->Merchant_modal->delete('tbl_faq',$com)){
			$this->session->set_flashdata('add_user','FAQs Deleted Successfully');
		}
		redirect('faqs');
	}
	
	public function admin_support()
	{
		$order_by='DESC';
		$colum='id';
		$support_query = $this->Merchant_modal->get_all_data('*','tbl_support','',$order_by,$colum);
		$transaction_arr =array();
		foreach($support_query as $query)
		{
			$con['merchant_id'] = $query['merchant_id'];
			$username = $this->Merchant_modal->get_row_data('*','tbl_merchants',$con);
			if(!empty($username))
			{
				$query['merchant_name'] = $username['merchant_name'];
				$transaction_arr[] = $query;
			}
		}
		$data['support_query'] = $transaction_arr;
		/* echo "<pre>";
		print_r($data);
		die(); */
		$this->load->view('admin/support_query',$data);	
	}
	
	public function user_support()
	{
		$order_by='DESC';
		$colum='contact_id';
		$support_query = $this->Merchant_modal->get_all_data('*','tbl_contact_support','',$order_by,$colum);
		$support_arr =array();
		foreach($support_query as $query)
		{
			$query['user_name'] = '';
			$con1['id'] = $query['userid'];
			
			$userdata = $this->Merchant_modal->get_row('*','tbl_user',$con1);
			if(!empty($userdata))
			{
				$query['user_name'] = $userdata['name'];
			
			//$userdata = $this->Merchant_modal->get_all_data('*','tbl_user',$con1);
			/* echo "<pre>";
			print_r($userdata);
			die(); */
			//$query['user_name'] = $username['name'];
			
			$support_arr[] = $query;
			}
		}
		$data['support_query'] = $support_arr;
		/* echo "<pre>";
		print_r($data);
		die(); */
		$this->load->view('admin/user_support',$data);	
	}
	
		/* Send notification */
	public function sendGCM($token,$msg,$title) {
		//$message = "You got a new match!";
		/* echo $token."/".$msg."/".$title;
		die(); */
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array (
        'to' => $token,
        'notification' => array (
                "body" => $msg,
                "title" => $title,
                "icon" => "myicon"
        )
		);
		$fields = json_encode ( $fields );
		//$firebase_key = "AAAAlQfrVzo:APA91bEjo5rMYM01JFjyqGNuIelC_TC5eiTRtAQW3Ar_2rTYDeNXAc4T2pTw0KTs_ncwv8zGASVlt-i5Sqt-jf5nhOzYMID4g8tz0SoeW5P64bsdE5hikyFyIxha6H-kyIDtn5UtuAjx";
		$firebase_key = "AAAAFg8f74I:APA91bE8QqaFRpG5aYiod3TJ4J8g1m-oB2gWPoy-F3bU4bDcPHKwsfDMju9xkZsNwY9tYbEPDGEJcbOY_rPxq-eUlhJVaG1MHmYOVDlSMQymtjrTqifNqJueJK8DNKAXtJec1uwF7dC2";
		$headers = array (
				'Authorization: key=' . $firebase_key,
				'Content-Type: application/json'
		);

		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
		$result = curl_exec ( $ch );
		//var_dump($result);
		return $result;
		curl_close ( $ch );
		
	}
	
	/* Send notification */
}
?>