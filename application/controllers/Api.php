<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;
class Api extends REST_Controller
{

	public function __construct()
	{
		parent::__construct();	
		$this->load->library('Authorization_Token');
		$this->load->model('Api_modal');
		$this->load->model('Merchant_modal');
		$this->load->helper('directory');		
	}
	private function generateOTP()
	{
		$otp = "";
      	$generator = "1357902468";
      	for ($i = 1; $i <= 4; $i++) { 
	        $otp .= substr($generator, (rand()%(strlen($generator))), 1); 
	    } 
	    return $otp;
	}

	/* public function index()
	{
		$returndata = array(
			"message" => "Caenex Api controller"
		);
		return $this->respond($returndata,200);
	} */

	private function sendTwilioSMS($text,$to)
	{
		try{
			$twilio = new Client($this->TWILIO_SID, $this->TWILIO_TOKEN);
	        return $twilio->messages->create(
			    $to,
			    [
			        'from' => '+12673949525',
			        'body' => $text
			    ]
			);
		}
		catch(Exception $e){
			return 0;
		}
		finally {
			return 0;
		}
	}
	
	/* app web view for stripe payment */
	public function payment_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		
		$userid = $decodedToken['data']->user_id;
		$amount = $this->input->post("amount");
		$card_id = $this->input->post("card_id");
		
		$Cdate = date('Y-m-d H:i:s');
		if(!empty($amount) && !empty($amount))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				//webview_id	userid	amount	auth_key	status	created_on
				$bytes = random_bytes(15);
				$auth_key = bin2hex($bytes);
				$res = $this->Api_modal->create_webview_link(array(
							'userid' => $userid,
							'amount' => $amount,
							'card_id' => $card_id,
							'auth_key' => $auth_key,
							'created_on' => $Cdate
						));
				$url = base_url('payment/'.$auth_key);
				
				$returndata['status'] = true;
				$returndata['payment_link'] = $url;
				$this->response($returndata); 
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not exists.";
				$this->response($returndata); 
			}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	/* app web view for stripe payment */
	
	public function verify_post()
	{  
		$headers = $this->input->request_headers(); 
		
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		$this->response($decodedToken);  
	}
	
	
	public function sign_up_post()
	{   
		$returndata = array();
		$name = $this->input->post("name");
		$mobile = $this->input->post("mobile");
		$email = $this->input->post("email");
		$password = $this->input->post("password");
		$fcm_token = $this->input->post('fcm_token');
		$Cdate = date('Y-m-d H:i:s');
		if((!empty($mobile) || !empty($email)) && !empty($password) && !empty($name))
		{
			if(!empty($email))
			{
				$check_email = $this->Api_modal->check_email_exists($email);
				if($check_email >0)
				{
					$returndata['message'] = "Email already registered.";
					$returndata['status'] = false;
					$this->response($returndata);
				}else{
						$user_id = $this->Api_modal->create_user(array(
							'name' => $name,
							'email' => $email,
							'login_type' => 'App',
							'password' => $password,
							'fcmtoken' => $fcm_token,
							'status' => 1,
							'create_date' => $Cdate
						));
					
				}
			}else if(!empty($mobile)){
			
				$check_mobile = $this->Api_modal->check_mobile_exists($mobile);
				if($check_mobile > 0)
				{
					$returndata['status'] = false;
					$returndata['message'] = "Phone number already exists.";
					$this->response($returndata);
				}else{
					$user_id = $this->Api_modal->create_user(array(
						'name' => $name,
						'login_type' => 'App',
						'phone' => $mobile,
						'password' => $password,
						'fcmtoken' => $fcm_token,
						'status' => 1,
						'create_date' => $Cdate
					));
					
				}
			}
				$returndata['message'] = "Thank you for registration.";
				$returndata['status'] = true;
				$this->response($returndata);
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
			
			$this->response($returndata);  
	}
	
	public function login_post()
	{
		$returndata = array();
		$email = $this->input->post("email");
		$password = $this->input->post("password");
		if((!empty($email)) && !empty($password))
		{
			$userdata = $this->Api_modal->login_by_mail($email,$password);
			if(count($userdata) < 1){
				$returndata['status'] = false;
	        	$returndata['message'] = "Invalid User!";
				$this->response($returndata);
			}
			else{
				$token = array();
				$token['user_id'] = $userdata[0]['id'];
				$tokenData = $this->authorization_token->generateToken($token);
				$this->Api_modal->update_user(array(
						'auth_token' => $tokenData ),$userdata[0]['id']);
						
						
				if($userdata[0]['status'] == 0){
					$returndata['status'] = false;
		        	$returndata['message'] = "User is inactive";
					$this->response($returndata);
				}
				else{
					$rdata = $this->Api_modal->crud_read($userdata[0]['id']);
					if(!empty($rdata))
					{
						$returndata['status'] = true;
						$returndata['data'] = $rdata;
						$returndata['message'] = "Logged in successfully!";
						$this->response($returndata);
					}else{
						$returndata['status'] = false;
						$returndata['data'] = array();
						$returndata['message'] = "Login fail!";
						$this->response($returndata);
					}
					
				}
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function social_login_post()
	{
		$returndata = array();
		$email = $this->input->post("email");
		$mobile = $this->input->post("mobile");
		$login_type = $this->input->post("login_type");
		$Cdate = date('Y-m-d H:i:s');
		if((!empty($email)) || !empty($mobile))
		{
			if(!empty($email))
			{
				$check_email = $this->Api_modal->check_email_exists($email);
				if($check_email > 0)
				{
					//$returndata['status'] = false;
					//$returndata['message'] = "Email already exists.";
					$user_id = $check_email;
				}else{
					$user_id = $this->Api_modal->create_user(array(
						'email' => $email,
						'login_type' => $login_type,
						'status' => 1,
						'create_date' => $Cdate
					));
				}
				
			}else{
				
				$check_mobile = $this->Api_modal->check_mobile_exists($mobile);
				if($check_mobile > 0)
				{
					/* $returndata['status'] = false;
					$returndata['message'] = "Phone number already exists."; */
					$user_id = $check_mobile;
				}else{
					$user_id = $this->Api_modal->create_user(array(
						'phone' => $mobile,
						'login_type' => $login_type,
						'status' => 1,
						'create_date' => $Cdate
					));
				}
			}
				$token = array();
				$token['user_id'] = $user_id;
				$tokenData = $this->authorization_token->generateToken($token);
				$this->Api_modal->update_user(array(
					'auth_token' => $tokenData ),$user_id);
					
				$rdata = $this->Api_modal->crud_read($user_id);
				if(!empty($rdata))
				{
					$returndata['status'] = true;
					$returndata['data'] = $rdata;
					$returndata['message'] = "Logged in successfully!";
					$this->response($returndata);
				}else{
					$returndata['status'] = false;
					$returndata['data'] = array();
					$returndata['message'] = "Login fail!";
					$this->response($returndata);
				}
		}else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function forgot_password_post()
	{
		$returndata = array();
		$email = $this->input->post("email");
		$mobile = $this->input->post("mobile");
		if((!empty($mobile)) || !empty($email))
		{
			$userdata = $this->Api_modal->read_by_mail_or_mobile($email,$mobile);
			if(count($userdata) < 1){
				$returndata['status'] = false;
	        	$returndata['message'] = "Invalid User!";
				$this->response($returndata);
			}
			else{
				$otp = $this->generateOTP();
                $message = 'Your Verification OTP Is :-'.$otp;
                if(!empty($email)){
					$this->load->library('email');
					$this->email->set_newline("\r\n");
					$this->email->from('no-reply@yesitlabs.xyz'); // change it to yours
					$this->email->to($email);// change it to yours
					$this->email->subject('Forgot Password');
					$this->email->message($message);
					$send_status=$this->email->send();
					/* if ($send_status) {
						$this->session->set_flashdata('incorrct','Password Reset link sent successfully. Please check your Email');	
					} */
					$update_user = $this->Api_modal->update_user(array('otp' => $otp),$userdata[0]['id']);
					
	                /* if ($send_status) {
	                    $returndata['status'] = "success";  
	                    $returndata['userid'] = $userdata[0]['id'];  
	                    $returndata['otp'] = $otp;                  
	                    $returndata['message'] = 'OTP is sent on provided email!' ;
	                }else{          
	                    $returndata['status'] = "fail";                   
	                    $returndata['message'] = 'Sorry, there is some internal issue, please try again';
	                } */
					
						$returndata['status'] = true;  
	                    $returndata['userid'] = $userdata[0]['id'];  
	                    $returndata['otp'] = $otp;                  
	                    $returndata['message'] = 'OTP is sent on provided email!' ;
						$this->response($returndata);
                }
                elseif(!empty($mobile)){
                	//$result = $this->sendTwilioSMS($message, $mobile);
                	//print_r($result);
					$update_user = $this->Api_modal->update_user(array('otp' => $otp),$userdata[0]['id']);
                	$returndata['status'] = true;
					$returndata['userid'] = $userdata[0]['id'];  
                    $returndata['otp'] = $otp;                  
                    $returndata['message'] = 'OTP is sent on provided mobile number!' ;
					$this->response($returndata);
					
                }
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function change_password_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		
		$userid = $decodedToken['data']->user_id;
		$new_pass = $this->input->post("new_pass");
		if(!empty($userid) || !empty($new_pass))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if($userdata > 0)
			{
				$user_id = $this->Api_modal->update_user(array(
						'password' => $new_pass ),$userid);
						
				$returndata['status'] = true;
				$returndata['user_id'] = $userid;
				$returndata['message'] = "Password changed successfully!";
				$this->response($returndata);
			}else{
				
				$returndata['status'] = false;
				$returndata['message'] = "User does not exist!";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function reset_password_post()
	{
		$returndata = array();
		$new_pass = $this->input->post("new_pass");
		$confirm_pass = $this->input->post("confirm_pass");
		$email = $this->input->post("email");
		$mobile = $this->input->post("mobile");
		if((!empty($mobile)) || !empty($email))
		{
			if($new_pass == $confirm_pass)
			{
				$userdata = $this->Api_modal->read_by_mail_or_mobile($email,$mobile);
				if($userdata > 0)
				{
					$user_id = $this->Api_modal->update_user(array(
							'password' => $new_pass ),$userdata[0]['id']);
					$returndata['status'] = true;
					$returndata['user_id'] = $user_id;
					$returndata['message'] = "Password changed successfully!";
					$this->response($returndata);
				}else{
					
					$returndata['status'] = false;
					$returndata['message'] = "User does not exist!";
					$this->response($returndata);
				}
			}else{
					$returndata['status'] = false;
					$returndata['message'] = "Password does not matched!";
					$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function verify_email_mobile_post()
	{
		$returndata = array();
		$email = $this->input->post("email");
		$mobile = $this->input->post("mobile");
		if((!empty($mobile)) || !empty($email))
		{
			$otp = $this->generateOTP();
			$message = 'Your Verification OTP Is :-'.$otp;
			if(!empty($email))
			{
				$check_email = $this->Api_modal->check_email_exists($email);
				if($check_email > 0)
				{
					$returndata['status'] = false;
					$returndata['message'] = "Email already exists.";
					$this->response($returndata);
					//$user_id = $check_email;
				}else{
					$this->load->library('email');
					$this->email->set_newline("\r\n");
					$this->email->from('no-reply@yesitlabs.xyz'); // change it to yours
					$this->email->to($email);// change it to yours
					$this->email->subject('Verify Email');
					$this->email->message($message);
					$send_status=$this->email->send();
					
					if($send_status){
						$user_id = $this->Api_modal->create_user_otp(array(
						'email' => $email,
						'otp' => $otp,
						'status' => 0,
						'create_date' => date('Y-m-d')
						),$email);
						
						$returndata['status'] = true;  
						$returndata['userid'] = $user_id;  
						$returndata['otp'] = $otp;                  
						$returndata['message'] = 'OTP is sent on provided email!' ;
						$this->response($returndata);
					}else{                               
						$returndata['status'] = false;                   
						$returndata['message'] = 'Sorry,mail not sent, please try again';
						$this->response($returndata);
					}
					
				}
			}else{
				
				$check_mobile = $this->Api_modal->check_mobile_exists($mobile);
				if($check_mobile > 0)
				{
					$returndata['status'] = false;
					$returndata['message'] = "Phone number already exists.";
					$this->response($returndata);
				}else{
					
					$user_id = $this->Api_modal->create_user_otp(array(
						'phone' => $mobile,
						'otp' => $otp,
						'status' => 0,
						'create_date' => date('Y-m-d')
						),$mobile);
						
					//$result = $this->sendTwilioSMS($message, $mobile);
                	//print_r($result);
					$returndata['status'] = true;
					$returndata['userid'] = $user_id;  
                    $returndata['otp'] = $otp;                  
                    $returndata['message'] = 'OTP is sent on provided mobile number!' ;
					$this->response($returndata);
				}
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function confirm_signup_otp_post()
	{
		$returndata = array();
		$email = $this->input->post("email");
		$mobile = $this->input->post("mobile");
		$otp = $this->input->post("otp");
		if((!empty($mobile)) || !empty($email) && (!empty($otp)))
		{
			$userdata = $this->Api_modal->read_otp_by_mail_or_mobile($email,$mobile);
			if(count($userdata) > 0){
				if($userdata[0]['otp'] == $otp)
				{
					$returndata['status'] = true;
					$returndata['message'] = "OTP Matched";
					$user_id = $this->Api_modal->update_user_otp(array(
						'status' => 1,
						'create_date' => date('Y-m-d')
						),$userdata[0]['id']);
						
					
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "OTP not match";
					$this->response($returndata);
				}
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	
	public function confirm_otp_post()
	{
		$returndata = array();
		$email = $this->input->post("email");
		$mobile = $this->input->post("mobile");
		$otp = $this->input->post("otp");
		if((!empty($mobile)) || !empty($email) && (!empty($otp)))
		{
			$userdata = $this->Api_modal->read_by_mail_or_mobile($email,$mobile);
			if(count($userdata) > 0){
				if($userdata[0]['otp'] == $otp)
				{
					$returndata['status'] = true;
					$returndata['message'] = "OTP Matched";
					$this->response($returndata);
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "OTP not match";
					$this->response($returndata);
				}
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function update_profile_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$name = $this->input->post("name");
		//$name = isset($this->input->post("name")) ? $this->input->post("name") :'';
		$country = $this->input->post("country");
		$state = $this->input->post("state");
		$city = $this->input->post("city");
		$zip = $this->input->post("zip");
		$phone = $this->input->post("phone");
		$street_address = $this->input->post("street_address");
		$pin = $this->input->post("pin");
		$dob = trim($this->input->post("dob"));
		$country_code = trim($this->input->post("country_code"));
		$personel_id = trim($this->input->post("personel_id"));
		//$profile_photo = $this->input->post("profile_photo");
		if(!empty($userid) && !empty($name) && !empty($country) && !empty($state) && !empty($city) && !empty($zip)
			&& !empty($phone) && !empty($street_address) && !empty($pin) && !empty($dob) && !empty($country_code) 
			&& !empty($personel_id))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				/* $uploaddir = './assets/img/photos/';
				$path = $_FILES['profile_photo']['name'];
				if(!empty($path))
				{
					$ext = pathinfo($path, PATHINFO_EXTENSION);
					$user_img = time() . rand() . '.' . $ext;
					$uploadfile = $uploaddir . $user_img;
					if ($_FILES["profile_photo"]["name"]) {
						if (move_uploaded_file($_FILES["profile_photo"]["tmp_name"],$uploadfile)) {
						$profile_photo = $user_img;
						}else{
							$profile_photo = '';
						}
					}
				}else{
					$profile_photo = '';
				} */
				//echo $profile_photo;
				//die();
				
				$check_mobile = $this->Api_modal->check_mobile_exists($phone);
				if($check_mobile > 0)
				{
					$returndata['status'] = false;
					$returndata['message'] = "Phone number already exists.";
					$this->response($returndata);
				}else{
					
					$user_id = $this->Api_modal->update_user(array(
							'name' => $name,
							'country' => $country,
							'state' => $state,
							'city' => $city,
							'phone' => $phone,
							'zip' => $zip,
							'street' => $street_address,
							'dob' => $dob,
							'country_code' => $country_code,
							'personel_id' => $personel_id,
							//'img' => $profile_photo,
							'pin' => $pin
						),$userid);
					$returndata['status'] = true;
					$returndata['user_id'] = $userid;
					$returndata['message'] = "User profile updated successfully!";
					$this->response($returndata);
				}
			}else{
					
				$returndata['status'] = false;
				$returndata['message'] = "User not found!";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function payment_method_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$card_number = $this->input->post("card_number");
		$card_holder_name = $this->input->post("card_holder_name");
		$expirydate = $this->input->post("expirydate");
		$cvv = $this->input->post("cvv");
		$nickname = $this->input->post("nickname");
		$create_date = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$payment_data = $this->Api_modal->crud_read_payment_method($userid,$card_number);
				if(count($payment_data) > 0)
				{
					//echo "Test1";
					/* echo "<pre>";
					print_r($userdata);
					die(); */
					$user_id = $this->Api_modal->update_payment_method(array(
							'cardnumber' => $card_number,
							'cardholdername' => $card_holder_name,
							'expirydate' => $expirydate,
							'cvv' => $cvv,
							'usertype' => 1,
							'nickname' => $nickname,
							'create_date' => $create_date
						),$userid);
					
				}else{
						
					$user_id = $this->Api_modal->add_payment_method(array(
							'userid' => $userid,
							'cardnumber' => $card_number,
							'cardholdername' => $card_holder_name,
							'expirydate' => $expirydate,
							'cvv' => $cvv,
							'usertype' => 1,
							'nickname' => $nickname,
							'create_date' => $create_date
						));
				}
			
				$returndata['status'] = true;
				$returndata['user_id'] = $userid;
				$returndata['message'] = "Payment method added successfully!";
				$this->response($returndata);
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function user_selection_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$selected = $this->input->post("selected");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$selection_data = $this->Api_modal->crud_read_selected_method($userid);
				if(count($selection_data) > 0)
				{
					//echo "Test1";
					/* echo "<pre>";
					print_r($userdata);
					die(); */
					$user_id = $this->Api_modal->update_selected_method(array(
							'userid' => $userid,
							'selected' => $selected
						),$userid);
					
				}else{
						
					$user_id = $this->Api_modal->add_selected_method(array(
							'userid' => $userid,
							'selected' => $selected
						));
				}
			
				$returndata['status'] = true;
				$returndata['user_id'] = $userid;
				$returndata['message'] = "User selected method added successfully!";
				$this->response($returndata);
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function pay_request_money_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$amount = $this->input->post("amount");
		$friend_contact_number = $this->input->post("friend_contact_number");
		$friend_contact_email = $this->input->post("friend_contact_email");
		$payment_type = $this->input->post("payment_type");
		$user_comment = $this->input->post("user_comment");
		$request_id = $this->input->post("request_id");
		$create_date = date('Y-m-d');
		$create_time = date('H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			/* user_transaction_id	user_id	amount	friend_contact_number	payment_type	user_status	friend_status	transaction_id	request_date	request_time */
			if(!empty($friend_contact_email))
			{
				$friend_data = $this->Api_modal->read_by_mail_or_mobile($friend_contact_email,'');
				$merchant_data = $this->Api_modal->crud_read_merchant_by_email($friend_contact_email);
			}else{
				$friend_data = $this->Api_modal->read_by_mail_or_mobile('',$friend_contact_number);
				$merchant_data = $this->Api_modal->crud_read_merchant_by_mobile($friend_contact_number);
			}
			
			
			if(!empty($friend_data))
			{
				$friend_id = $friend_data[0]['id'];
				
			}else{
				$friend_id = '0';
			}
			
			
			
			if(!empty($merchant_data))
			{
				$merchant_id = $merchant_data[0]['merchant_id'];
				
			}else{
				$merchant_id = '0';
			}
			
			if(count($userdata) > 0)
			{
				if($friend_id =='0' && $merchant_id =='0')
				{
					$returndata['status'] = false;
					$returndata['message'] = "Friend not found, please enter correct email or mobile!";
					$this->response($returndata);
				
				}else{
					
					if($payment_type == 1)
					{
						// for payment request
						$user_id = $this->Api_modal->crud_create_user_transaction(array(
								'user_id' => $userid,
								'amount' => $amount,
								'friend_contact_number' => $friend_contact_number,
								'friend_contact_email' => $friend_contact_email,
								'friend_id' => $friend_id,
								'merchant_id' => $merchant_id,
								'payment_type' => $payment_type,
								'user_status' => 1,
								'friend_status' => 0,
								'transaction_status'=>0,
								'user_comment' => $user_comment,
								//'transaction_id' => $transaction_id,
								'request_date' => $create_date,
								'request_time' => $create_time
							));
							
							$check_token = $this->Api_modal->read_device_info($friend_id);
							if(!empty($check_token))
							{
								foreach($check_token as $user_token)
								{
									$token = $user_token['fcm_token'];
									$msg = $userdata[0]['name']." has sent you a payment request of ".$amount."$";
									$title = "Withdraw Payment Request";
									$this->sendGCM($token,$msg,$title);
								}
								
							}
							$returndata['status'] = true;
							$returndata['message'] = "You have requested money successfully!";
							$this->response($returndata);
					}else{
						// for pay money
						
						
						$wallet_data = $this->Api_modal->crud_read_wallet($userid);
						if(count($wallet_data) > 0)
						{
							if($wallet_data[0]['total_avail_balance'] >= $amount)
							{
								$bytes = random_bytes(19);
								$txid = bin2hex($bytes);
								if(!empty($request_id))
								{
									$read_transaction = $this->Api_modal->get_user_transaction_by_transaction_id($request_id);
									if(count($read_transaction) >0)
									{
										// for requested pay
										$update_request = $this->Api_modal->crud_update_user_request(array(
											'friend_status' => 1,
											'transaction_status'=>1,
											'transaction_id'=>$txid,
											'paid_date' => $create_date,
											'paid_time' => $create_time
											),$request_id);
										
										if(!empty($read_transaction[0]['friend_id']))
										{
											$friend_wallet = $this->Api_modal->crud_read_wallet($read_transaction[0]['friend_id']);
										}else{
											$friend_wallet = $this->Api_modal->crud_read_wallet($read_transaction[0]['merchant_id']);
										}
										
										if(count($friend_wallet)>0)
										{
											$total_avail_balance = $friend_wallet[0]['total_avail_balance'] - $amount;
											$total_credit_balance = 0;
											$total_withdraw_balance = $amount; 
											
											$wallet_id = $this->Api_modal->update_wallet(array(
												'total_avail_balance' => $total_avail_balance,
												'total_credit_balance' => $total_credit_balance,
												'total_withdraw_balance' => $total_withdraw_balance,
												'created_date' => $create_date
											),$friend_wallet[0]['wallet_id']);
									
										}
										
										
										if(!empty($read_transaction[0]['user_id']))
										{
											$user_wallet = $this->Api_modal->crud_read_wallet($read_transaction[0]['user_id']);
										
											if(count($user_wallet)>0)
											{
												$total_avail_balance = $user_wallet[0]['total_avail_balance'] + $amount;
												$wallet_id = $this->Api_modal->update_wallet(array(
													'total_avail_balance' => $total_avail_balance,
													'total_credit_balance' => $amount,
													'created_date' => $create_date
												),$user_wallet[0]['wallet_id']);
										
											}else{
												
												$wallet_id = $this->Api_modal->create_wallet(array(
												'user_id' => $read_transaction[0]['user_id'],
												'total_avail_balance' => $amount,
												'total_credit_balance' => $amount,
												'total_withdraw_balance' => 0,
												'last_added_amount' => $amount,
												'user_type' => 1,
												'created_date' => $create_date
											));
												
											}
										
										}
									
									}
									
								}else{
									//for direct pay
									$user_id = $this->Api_modal->crud_create_user_transaction(array(
										'user_id' => $userid,
										'amount' => $amount,
										'friend_contact_number' => $friend_contact_number,
										'friend_contact_email' => $friend_contact_email,
										'friend_id' => $friend_id,
										'merchant_id' => $merchant_id,
										'payment_type' => $payment_type,
										'transaction_id'=>$txid,
										'user_status' => 3,
										'friend_status' => 0,
										'transaction_status'=>1,
										'user_comment' => $user_comment,
										'paid_date' => $create_date,
										'paid_time' => $create_time
									));
									
									$total_avail_balance = $wallet_data[0]['total_avail_balance'] - $amount;
									$total_credit_balance = 0;
									$total_withdraw_balance = $amount; 
									
									$wallet_id = $this->Api_modal->update_wallet(array(
										'total_avail_balance' => $total_avail_balance,
										'total_credit_balance' => $total_credit_balance,
										'total_withdraw_balance' => $total_withdraw_balance,
										'created_date' => $create_date
									),$wallet_data[0]['wallet_id']);
								
								
								if($friend_id =='0')
								{
									$merchant_wallet = $this->Api_modal->crud_read_merchant_wallet($merchant_id);
									
									if(count($merchant_wallet)>0)
									{
										$total_avail_balance1 = $merchant_wallet[0]['total_avail_balance'] + $amount;
										//$total_credit_balance = $amount;
										
										$wallet_id = $this->Api_modal->update_wallet(array(
											'total_avail_balance' => $total_avail_balance1,
											'total_credit_balance' => $amount,
											'last_added_amount' => $amount,
											'created_date' => $create_date
										),$merchant_wallet[0]['wallet_id']);
									
									}else{
										
										$wallet_id = $this->Api_modal->create_wallet(array(
											'user_id' => $merchant_id,
											'total_avail_balance' => $amount,
											'total_credit_balance' => $amount,
											'total_withdraw_balance' => 0,
											'last_added_amount' => $amount,
											'user_type' => 2,
											'created_date' => $create_date
										));
										
									}
									
								}else{
									
									$friend_wallet = $this->Api_modal->crud_read_wallet($friend_id);
									
									if(count($friend_wallet)>0)
									{
										$total_avail_balance = $friend_wallet[0]['total_avail_balance'] + $amount;
										//$total_credit_balance = $amount;
										
										$wallet_id = $this->Api_modal->update_wallet(array(
											'total_avail_balance' => $total_avail_balance,
											'total_credit_balance' => $amount,
											'last_added_amount' => $amount,
											'created_date' => $create_date
										),$friend_wallet[0]['wallet_id']);
									
									}else{
										
										$wallet_id = $this->Api_modal->create_wallet(array(
											'user_id' => $friend_id,
											'total_avail_balance' => $amount,
											'total_credit_balance' => $amount,
											'total_withdraw_balance' => 0,
											'last_added_amount' => $amount,
											'user_type' => 1,
											'created_date' => $create_date
										));
										
									}
									
								}
								}
								
								
								
								
								$returndata['status'] = true;
								$returndata['message'] = "You have paid money successfully!";
								$this->response($returndata);
							}else{
								$returndata['status'] = false;
								$returndata['message'] = "You dont have enough balance in wallet to pay!";
								$this->response($returndata);
							}
						}else{
							$returndata['status'] = false;
							$returndata['message'] = "You dont have enough balance in wallet to pay!";
							$this->response($returndata);
						}
					}
				}
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function withdraw_payment_request_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id; //19
		$user_transaction_id = $this->input->post("user_transaction_id");
		$create_date = date('Y-m-d');
		$create_time = date('H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			/* user_transaction_id	user_id	amount	friend_contact_number	payment_type	user_status	friend_status	transaction_id	request_date	request_time */
			
			if(count($userdata) > 0)
			{
				$read_transaction = $this->Api_modal->get_user_transaction_by_transaction_id($user_transaction_id,$userid);
				
				if(count($read_transaction) >0)
				{
					$withdraw_request = $this->Api_modal->crud_update_user_request(array('user_status' => 2,'transaction_status' => 1),$user_transaction_id);
					$returndata['status'] = true;
					$returndata['message'] = "Your have successfully withdraw your payment request!";
					$this->response($returndata);
					/* echo $read_transaction[0]['user_id'];
					die();
					echo "<pre>";
					print_r($read_transaction);
					die(); */
					$check_token = $this->Api_modal->read_device_info($read_transaction[0]['user_id']);
					if(!empty($check_token))
					{
						foreach($check_token as $user_token)
						{
							$token = $user_token['fcm_token'];
							$msg = "Your have withdraw your payment request!";
							$title = "Withdraw Payment Request";
							$this->sendGCM($token,$msg,$title);
						}
						
					}
					
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "No transaction data Found !";
					$this->response($returndata);
				}
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function reject_payment_request_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers();
		
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
			
			/* echo "<pre>";
			print_r($decodedToken);
			die(); */
			$userid = $decodedToken['data']->user_id; //19
			$user_transaction_id = $this->input->post("user_transaction_id");
			$create_date = date('Y-m-d');
			$create_time = date('H:i:s');
			if(!empty($userid))
			{
				$userdata = $this->Api_modal->crud_read($userid);
				/* user_transaction_id	user_id	amount	friend_contact_number	payment_type	user_status	friend_status	transaction_id	request_date	request_time */
				
				if(count($userdata) > 0)
				{
					$read_transaction = $this->Api_modal->get_friend_transaction_by_transaction_id($user_transaction_id,$userid);
					/* echo "<pre>";
					print_r($read_transaction);
					die(); */
					if(count($read_transaction) >0)
					{
						$reject_request = $this->Api_modal->crud_update_user_request(array('friend_status' => 2,'transaction_status' => 1),$user_transaction_id);
						
						$check_token = $this->Api_modal->read_device_info($read_transaction[0]['user_id']);
						if(!empty($check_token))
						{
							foreach($check_token as $user_token)
							{
								$token = $user_token['fcm_token'];
								$msg = "Your request for payment from " .$userdata[0]['name'] . " is rejected !";
								$title = "Reject Payment Request";
								$this->sendGCM($token,$msg,$title);
							}
							
						}
						$returndata['status'] = true;
						$returndata['message'] = "Your have successfully reject payment request!";
						$this->response($returndata);
					
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "No transaction record with this transaction id Found!";
						$this->response($returndata);
					}
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "User not found !";
					$this->response($returndata);
				}
			}
			else
			{
				$returndata['status'] = false;
				$returndata['message'] = "Please provide required fields.";
				$this->response($returndata);
			}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function send_reminder_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id; //19
		$user_transaction_id = $this->input->post("user_transaction_id");
		$create_date = date('Y-m-d');
		$create_time = date('H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			/* user_transaction_id	user_id	amount	friend_contact_number	payment_type	user_status	friend_status	transaction_id	request_date	request_time */
			
			if(count($userdata) > 0)
			{
				$read_transaction = $this->Api_modal->get_user_transaction_by_transaction_id($user_transaction_id,$userid);
				if(count($read_transaction) >0)
				{
					$friend_data = $this->Api_modal->read_device_info($read_transaction[0]['friend_id']);
					/* print_r($friend_data);
					die(); */
					if(count($friend_data) >0)
					{
						foreach($friend_data as $fdata)
						{
							/* print_r($friend_data);
							die(); */
							if(!empty($fdata['fcm_token']))
							{	
								$token = $fdata['fcm_token'];
								$title = "Payment Reminder";
								$message = $userdata[0]['name']. " has requested ".$read_transaction[0]['amount']."$ from you.";
								//$message = $userdata[0]['name']. " has requested a payment from you.";
								$res = $this->sendGCM($token,$message,$title);
								//print_r($res);
								//die();
								$msg_status = json_decode($res);
								
								if($msg_status->success ==1)
								{
									$returndata['status'] = true;
									$returndata['message'] = "Reminder sent successfully!";
									//$this->response($returndata);
									
								}else if($msg_status->failure ==1){
									$returndata['status'] = false;
									$returndata['message'] = "Unable to send reminder !";
									//$this->response($returndata);
								}
							}
						}
						
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Unable to send reminder !";
						$this->response($returndata);
					}
					
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "No transaction record with this transaction id Found!";
					$this->response($returndata);
				}
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function set_username_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$username = $this->input->post("username");
		$avatar = $this->input->post("avatar");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$user_id = $this->Api_modal->update_user(array('username' => $username,'avatar' => $avatar),$userid);
				$returndata['status'] = true;
				$returndata['user_id'] = $userid;
				$returndata['message'] = "Username saved successfully!";
				$this->response($returndata);
			}else{
					
				$returndata['status'] = false;
				$returndata['message'] = "User not found!";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function recent_activity_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$min_range = $this->input->post("min_range");
		$max_range = $this->input->post("max_range");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			/* user_transaction_id	user_id	amount	friend_contact_number	payment_type	user_status	friend_status	transaction_id	request_date	request_time */
			if(count($userdata) > 0)
			{
				$recent_activity = $this->Api_modal->crud_read_user_transaction($userid,$min_range,$max_range);
				if(!empty($recent_activity))
				{
					$returndata['status'] = true;
					$returndata['recent_activity'] = $recent_activity;
					$returndata['message'] = "Recent transaction fetched successfully!";
					$this->response($returndata);
				}else{
					$returndata['status'] = false;
					$returndata['recent_activity'] = array();
					$returndata['message'] = "No recent transaction record found !";
					$this->response($returndata);
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function recent_payment_contact_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$min_range = $this->input->post("min_range");
		$max_range = $this->input->post("max_range");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			/* user_transaction_id	user_id	amount	friend_contact_number	payment_type	user_status	friend_status	transaction_id	request_date	request_time */
			if(count($userdata) > 0)
			{
				$recent_transaction = $this->Api_modal->crud_read_user_transaction($userid,$min_range,$max_range);
				if(!empty($recent_transaction))
				{
					$returndata['status'] = true;
					$returndata['recent_transaction'] = $recent_transaction;
					$returndata['message'] = "Recent payment contact fetched successfully!";
					$this->response($returndata);
				}else{
					$returndata['status'] = false;
					$returndata['recent_transaction'] = array();
					$returndata['message'] = "No recent payment contact record found!";
					$this->response($returndata);
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function sender_receiver_payment_details_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$user_transaction_id = $this->input->post("user_transaction_id");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			/* user_transaction_id	user_id	amount	friend_contact_number	payment_type	user_status	friend_status	transaction_id	request_date	request_time */
			if(count($userdata) > 0)
			{
				$payment_details = $this->Api_modal->get_user_transaction_by_transaction_id($user_transaction_id);
				if(!empty($payment_details))
				{
					$returndata['status'] = true;
					$returndata['payment_details'] = $payment_details;
					$returndata['message'] = "Payment details fetched successfully!";
					$this->response($returndata);
				}else{
					$returndata['status'] = false;
					$returndata['payment_details'] = array();
					$returndata['message'] = "No payment details record Found!";
					$this->response($returndata);
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function contact_list_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			$all_user_arr =array();
			if(count($userdata) > 0)
			{
				$all_users = $this->Api_modal->crud_read();
				if(count($all_users) > 0)
				{
					foreach($all_users as $users)
					{
						if($users['id'] != $userid)
						{
							if(!empty($users['img']))
							{
								//$uploaddir = './assets/img/photos/';
								//base_url()."/assets/img/photos/".
								$user_img = base_url()."/assets/img/photos/".$users['img'];
							}else{
								$user_img = "";
							}
							$row = array();
							$row['name'] = $users['name'];
							$row['email'] = $users['email'];
							$row['phone'] = $users['phone'];
							$row['prifile_img'] = $user_img;
							$all_user_arr[] = $row;
						}
					}
					
					/* echo "<pre>";
					print_r($all_user_arr);
					die(); */
					
					$returndata['status'] = true;
					$returndata['contact_list'] = $all_user_arr;
					$returndata['message'] = "contact list fetched successfully!";
					$this->response($returndata);
					
				}else{
					$returndata['status'] = false;
					$returndata['contact_list'] = array();
					$returndata['message'] = "No contact list Found!";
					$this->response($returndata);
				}
				
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function check_balance_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$wallet_data = $this->Api_modal->crud_read_wallet($userid);
					/* echo "<pre>";
					print_r($wallet_data);
					die(); */
				if(!empty($wallet_data))
				{
					$returndata['status'] = true;
					$returndata['wallet_data'] = $wallet_data;
					$returndata['message'] = "User Wallet fetched successfully!";
					$this->response($returndata);
				}else{
					$sample_arr[] = array(
					"total_avail_balance"=> "0",
					"total_credit_balance"=> "0",
					"total_withdraw_balance"=> "0",
					"last_added_amount"=> "0",
					);
					
					$returndata['status'] = true;
					$returndata['wallet_data'] = $sample_arr;
					$returndata['message'] = "User Wallet fetched successfully!";
					$this->response($returndata);
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function add_wallet_money_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$amount = $this->input->post("amount");
		$type = $this->input->post("type"); //1= nexworld coupon ,2= nexworld cash
		$campaign_id = $this->input->post("campaign_id");
		$created_date = date('Y-m-d H:i:s');
		$transaction_date = date('Y-m-d');
		$transaction_time = date('H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				/* $res_id = $this->Api_modal->add_wallet_money(array(
							'user_id' => $userid,
							'amount' => $amount,
							'transactioncol_id' => $transactioncol_id,
							'type' => $type,
							'data_id' => $data_id,
							'transaction_date' => $transaction_date,
							'transaction_time' => $transaction_time,
							'status' => 'succeeded'
						)); */
				$wallet_data = $this->Api_modal->crud_read_wallet($userid);
				if(count($wallet_data) > 0)
				{
					foreach($wallet_data as $wallet)
					{
						$total_avail_balance = $wallet['total_avail_balance'] + $amount;
						$total_credit_balance = $wallet['total_credit_balance'] + $amount;
						$last_added_amount = $amount; 
						
						$wallet_id = $this->Api_modal->update_wallet(array(
							'total_avail_balance' => $total_avail_balance,
							'total_credit_balance' => $total_credit_balance,
							'last_added_amount' => $last_added_amount,
							'created_date' => $created_date
						),$wallet['wallet_id']);
					}
				}else{
					$wallet_id = $this->Api_modal->create_wallet(array(
							'user_id' => $userid,
							'total_avail_balance' => $amount,
							'total_credit_balance' => $amount,
							'total_withdraw_balance' => 0,
							'last_added_amount' => $amount,
							'user_type' => 1,
							'created_date' => $created_date
						));
				}
				$check_token = $this->Api_modal->read_device_info($userid);
				if(!empty($check_token))
				{
					foreach($check_token as $user_token)
					{
						$token = $user_token['fcm_token'];
						$msg = "You have successfully added " .$amount."$ in your wallet!";
						$title = "Add Wallet Money";
						$this->sendGCM($token,$msg,$title);
					}
					
				}
							
					/* echo "<pre>";
					print_r($wallet_data);
					die(); */
				$wallet_data = $this->Api_modal->crud_read_wallet($userid);
				if(!empty($wallet_data))
				{
					$returndata['status'] = true;
					$returndata['wallet_data'] = $wallet_data;
					$returndata['message'] = "Wallet data fetched successfully!";
					$this->response($returndata);
				}else{
					$returndata['status'] = false;
					$returndata['wallet_data'] = array();
					$returndata['message'] = "No Wallet data found!";
					$this->response($returndata);
				}
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function search_contact_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$contact_number = $this->input->post("contact_number");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			$searched_arr =array();
			if(count($userdata) > 0)
			{
				$searched_data = $this->Api_modal->read_by_mail_or_mobile('',$contact_number);
				if(count($searched_data) > 0)
				{
					/* echo "<pre>";
					print_r($searched_data);
					die(); */
					foreach($searched_data as $users)
					{
						if($users['id'] != $userid)
						{
							$row = array();
							$row['name'] = $users['name'];
							$row['email'] = $users['email'];
							$row['phone'] = $users['phone'];
							$searched_arr[] = $row;
						}
					}
					if(count($searched_arr) > 0)
					{
						$returndata['status'] = true;
						$returndata['search_result'] = $searched_arr;
						$returndata['message'] = "contact list fetched successfully!";
						$this->response($returndata);
					}else{
						$returndata['status'] = false;
						$returndata['search_result'] = array();
						$returndata['message'] = "No contact list found!";
						$this->response($returndata);
					}
				}else{
					$returndata['status'] = false;
					$returndata['search_result'] = array();
					$returndata['message'] = "No contact list found!";
					$this->response($returndata);
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function confirm_pin_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$pin = $this->input->post("pin");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			$searched_arr =array();
			if(count($userdata) > 0)
			{
				if($userdata[0]['pin'] == $pin)
				{
					$returndata['status'] = true;
					$returndata['message'] = "Pin Matched";
					$this->response($returndata);
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "Incorrect pin, please try again";
					$this->response($returndata);
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function change_pin_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$oldpin = $this->input->post("oldpin");
		$newpin = $this->input->post("newpin");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				if($userdata[0]['pin'] == $oldpin)
				{
					$user_id = $this->Api_modal->update_user(array(
							'pin' => $newpin),$userid);
						
					$returndata['status'] = true;
					$returndata['message'] = "Pin changed successfully";
					$this->response($returndata);
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "Old Pin not match";
					$this->response($returndata);
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function save_card_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$card_number = $this->input->post("card_number");
		$card_holder_name = $this->input->post("card_holder_name");
		$expirydate = $this->input->post("expirydate");
		$cvv = $this->input->post("cvv");
		$nickname = $this->input->post("nickname");
		$create_date = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$payment_data = $this->Api_modal->crud_read_payment_method($userid,$card_number);
				if(count($payment_data) > 0)
				{
					//echo "Test1";
					/* echo "<pre>";
					print_r($userdata);
					die(); */
					/* $user_id = $this->Api_modal->update_payment_method(array(
							'cardnumber' => $card_number,
							'cardholdername' => $card_holder_name,
							'expirydate' => $expirydate,
							'cvv' => $cvv,
							'nickname' => $nickname,
							'create_date' => $create_date
						),$userid); */
						$returndata['status'] = false;
						$returndata['message'] = "Card already exists!";
						$this->response($returndata);
					
				}else{
						
					$user_id = $this->Api_modal->add_payment_method(array(
							'userid' => $userid,
							'cardnumber' => $card_number,
							'cardholdername' => $card_holder_name,
							'expirydate' => $expirydate,
							'cvv' => $cvv,
							'nickname' => $nickname,
							'create_date' => $create_date
						));
				}
			
				$returndata['status'] = true;
				$returndata['user_id'] = $userid;
				$returndata['message'] = "Card details added successfully!";
				$this->response($returndata);
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function get_saved_card_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$card_details = $this->Api_modal->crud_read_payment_method($userid);
				if(!empty($card_details))
				{
					$returndata['status'] = true;
					$returndata['card_details'] = $this->Api_modal->crud_read_payment_method($userid);
					$returndata['user_id'] = $userid;
					$returndata['message'] = "Card details fetched successfully!";
					$this->response($returndata);
				}else{
					$returndata['status'] = false;
					$returndata['card_details'] = array();
					$returndata['user_id'] = $userid;
					$returndata['message'] = "No card details Found!";
					$this->response($returndata);
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function delete_card_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$card_number = $this->input->post("card_number");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$res = $this->Api_modal->crud_read_delete_card($userid,$card_number);
				//echo "Test1";
					/* echo "<pre>";
					print_r($userdata);
					die(); */
			
				$returndata['status'] = true;
				$returndata['user_id'] = $userid;
				$returndata['message'] = "Card details deleted successfully!";
				$this->response($returndata);
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
				$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function get_recent_transaction_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$min_range = $this->input->post("min_range");
		$max_range = $this->input->post("max_range");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				
				$user_transaction_data = $this->Api_modal->crud_read_user_transaction($userid,$min_range,$max_range);
				$friend_transaction_data = $this->Api_modal->crud_read_friend_transaction($userid,$min_range,$max_range);
				//echo $this->Api_modal->getLastQuery();
				/* print_r($user_transaction_data);
				print_r($friend_transaction_data);
				die(); */
				if(!empty($user_transaction_data) || !empty($friend_transaction_data))
				{
					$contact_data_arr = array();
					if(count($user_transaction_data) > 0)
					{
						
						foreach($user_transaction_data as $tdata)
						{
							$userdata1 = $this->Api_modal->crud_read($tdata['friend_id']);
							if(!empty($userdata1))
							{
								if(!empty($userdata1[0]['img']))
								{
									//base_url()."/writable/uploads/".
									$user_img = base_url()."/assets/img/photos/".$userdata1[0]['img'];
								}else{
									$user_img = "";
								}
								
								if(!empty($userdata1[0]['name']))
								{
									$user_name = $userdata1[0]['name'];
								}else{
									$user_name = '';
								}
							
							}else{
								$user_name = '';
								$user_img = "";
							}
								$tdata['profile_img'] = $user_img;
								$tdata['name'] = $user_name;
								$contact_data_arr[] = $tdata;
							
						}
					}
					
					
					if(count($friend_transaction_data) > 0)
					{
						
						foreach($friend_transaction_data as $tdata)
						{
							$unique_status =0;
							foreach($contact_data_arr as $contact)
							{
								//echo $contact['user_transaction_id']."@".$tdata['user_transaction_id']."<br/>";
								
								if($contact['user_transaction_id'] == $tdata['user_transaction_id'])
								{
									$unique_status = 1;
									break;
								}else{
									continue;
								}
							}
							if($unique_status !=1)
							{
								$userdata1 = $this->Api_modal->crud_read($tdata['user_id']);
								if(!empty($userdata1))
								{
									if(!empty($userdata1[0]['img']))
									{
										//base_url()."/writable/uploads/".
										$user_img = base_url()."/assets/img/photos/".$userdata1[0]['img'];
									}else{
										$user_img = "";
									}
									
									if(!empty($userdata1[0]['name']))
									{
										$user_name = $userdata1[0]['name'];
									}else{
										$user_name = '';
									}
								
								}else{
									$user_name = '';
									$user_img = "";
								}
								$tdata['profile_img'] = $user_img;
								$tdata['name'] = $user_name;
								$contact_data_arr[] = $tdata;
							}
						}
					}
					/* echo "<pre>";
					print_r($contact_data_arr);
					die(); */
					$returndata['status'] = true;
					$returndata['recent_transaction'] = $contact_data_arr;
					$returndata['message'] = "Transaction data fetched successfully!";
					$this->response($returndata);
					
					
				}else{
					$returndata['status'] = false;
					$returndata['recent_transaction'] = array();
					$returndata['message'] = "No transaction record found!";
					$this->response($returndata);
				}
			}else{
					$returndata['status'] = false;
					$returndata['message'] = "User not found !";
					$this->response($returndata);
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function get_requested_transaction_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$min_range = $this->input->post("min_range");
		$max_range = $this->input->post("max_range");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				
				$transaction_data = $this->Api_modal->crud_read_requested_transaction($userid,$min_range,$max_range);
				/* echo $this->db->last_query();
				print_r($transaction_data);
				die(); */
				if(count($transaction_data) > 0)
				{
					$contact_data_arr = array();
					foreach($transaction_data as $tdata)
					{
						$userdata1 = $this->Api_modal->crud_read($tdata['friend_id']);
						if(!empty($userdata1))
						{
							if(!empty($userdata1[0]['img']))
							{
								//base_url()."/writable/uploads/".
								$user_img = base_url()."/assets/img/photos/".$userdata1[0]['img'];
							}else{
								$user_img = "";
							}
							
							if(!empty($userdata1[0]['name']))
							{
								$user_name = $userdata1[0]['name'];
							}else{
								$user_name = '';
							}
							
						}else{
							$user_name = '';
							$user_img = "";
						}
							
						$tdata['profile_img'] = $user_img;
						$tdata['name'] = $user_name;
						$contact_data_arr[] = $tdata;
					}
					$returndata['status'] = true;
					$returndata['requested_transaction'] = $contact_data_arr;
					$returndata['message'] = "Requested Transaction data fetched successfully!";
				}else{
					$returndata['status'] = false;
					$returndata['requested_transaction'] = array();
					$returndata['message'] = "No requested transaction Data Found!";
				}
			}else{
					$returndata['status'] = false;
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function filter_transaction_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$from_date = $this->input->post("from_date");
		$to_date = $this->input->post("to_date");
		$type = $this->input->post("type");
		$min_range = $this->input->post("min_range");
		$max_range = $this->input->post("max_range");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$transaction_data = $this->Api_modal->crud_filter_transaction($userid,$from_date,$to_date,$type,$min_range,$max_range);
				if(count($transaction_data) > 0)
				{
					$returndata['status'] = true;
					$returndata['search_result'] = $transaction_data;
					$returndata['message'] = "Transaction data fetched successfully!";
				}else{
					$returndata['status'] = false;
					$returndata['search_result'] = array();
					$returndata['message'] = "No transaction data Found!";
				}
			}else{
					$returndata['status'] = false;
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function get_pending_request_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$min_range = $this->input->post("min_range");
		$max_range = $this->input->post("max_range");
		if(!empty($userid))
		{
			
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$transaction_data = $this->Api_modal->crud_read_pending_request($userid,$min_range,$max_range);
				
				/* echo $this->db->last_query();
				echo "<pre>";
				print_r($transaction_data);
				die(); */
				if(count($transaction_data) > 0)
				{
					$contact_data_arr = array();
					foreach($transaction_data as $tdata)
					{
						$userdata1 = $this->Api_modal->crud_read($tdata['user_id']);
						if(!empty($userdata1[0]['img']))
						{
							//base_url()."/writable/uploads/".
							$user_img = base_url()."/assets/img/photos/".$userdata1[0]['img'];
						}else{
							$user_img = "";
						}
						$tdata['profile_img'] = $user_img;
						$tdata['name'] = $userdata1[0]['name'];
						$contact_data_arr[] = $tdata;
					}
					
					$returndata['status'] = true;
					$returndata['pending_request'] = $contact_data_arr;
					$returndata['message'] = "Transaction data fetched successfully!";
				}else{
					$returndata['status'] = false;
					$returndata['pending_request'] = array();
					$returndata['message'] = "No transaction data Found!";
				}
			}else{
					$returndata['status'] = false;
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function withdraw_money_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$amount = $this->input->post("amount");
		$user_bank_id = $this->input->post("user_bank_id");
		$created_date = date('Y-m-d H:i:s');
		$transaction_date = date('Y-m-d');
		$transaction_time = date('H:i:s');
		$bytes = random_bytes(19);
		$txid = bin2hex($bytes);
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$history_id = $this->Api_modal->crud_read_user_bank_by_id($userid,$user_bank_id);
				if(count($history_id) >0)
				{
							
					$wallet_data = $this->Api_modal->crud_read_wallet($userid);
					if(count($wallet_data) > 0)
					{
						if($wallet_data[0]['total_avail_balance'] >= 50)
						{
							require_once APPPATH."third_party/stripe/init.php";
							//$token= $this->input->post('stripeToken');
							$stripe = array(
								"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
								"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
							);
							\Stripe\Stripe::setApiKey($stripe['secret_key']);
							
							/* // for add balance
							\Stripe\Charge::create(array(
							'currency' => 'USD',
							'amount'   => 99999900,
							//'card'     => 4000000000000077
							'source' => 'tok_bypassPending'
						)); */

						try{
							$transfer = \Stripe\Transfer::create([ //Transfer
							"amount" => $amount * 100,
							"currency" => "usd",
							"destination" => $history_id[0]['stripe_account_id'],
							//"destination" => "acct_1IgrXHPIj2sJImuC",
							"transfer_group" => "registry pay",
							]);
						} catch(\Stripe\Exception\CardException $e) {
						  // Since it's a decline, \Stripe\Exception\CardException will be caught
						  /* echo 'Status is:' . $e->getHttpStatus() . '\n';
							echo 'Type is:' . $e->getError()->type . '\n';
							echo 'Code is:' . $e->getError()->code . '\n';
						  // param is '' in this case
							echo 'Param is:' . $e->getError()->param . '\n';
							echo 'Message is:' . $e->getError()->message . '\n'; */
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\RateLimitException $e) {
						  // Too many requests made to the API too quickly
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\InvalidRequestException $e) {
						  // Invalid parameters were supplied to Stripe's API
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\AuthenticationException $e) {
						  // Authentication with Stripe's API failed
						  // (maybe you changed API keys recently)
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\ApiConnectionException $e) {
						  // Network communication with Stripe failed
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\ApiErrorException $e) {
						  // Display a very generic error to the user, and maybe send
						  // yourself an email
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (Exception $e) {
						  // Something else happened, completely unrelated to Stripe
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						}
						
						try{
							$payout = \Stripe\Payout::create([ //payout
							'amount' => $amount * 100,
							'currency' => 'usd',
							],
							['stripe_account' => $history_id[0]['stripe_account_id'],
							//['stripe_account' => "acct_1IgrXHPIj2sJImuC",
							]);
						} catch(\Stripe\Exception\CardException $e) {
						  // Since it's a decline, \Stripe\Exception\CardException will be caught
						  /* echo 'Status is:' . $e->getHttpStatus() . '\n';
							echo 'Type is:' . $e->getError()->type . '\n';
							echo 'Code is:' . $e->getError()->code . '\n';
						  // param is '' in this case
							echo 'Param is:' . $e->getError()->param . '\n';
							echo 'Message is:' . $e->getError()->message . '\n'; */
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\RateLimitException $e) {
						  // Too many requests made to the API too quickly
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\InvalidRequestException $e) {
						  // Invalid parameters were supplied to Stripe's API
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\AuthenticationException $e) {
						  // Authentication with Stripe's API failed
						  // (maybe you changed API keys recently)
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\ApiConnectionException $e) {
						  // Network communication with Stripe failed
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\ApiErrorException $e) {
						  // Display a very generic error to the user, and maybe send
						  // yourself an email
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (Exception $e) {
						  // Something else happened, completely unrelated to Stripe
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						}
							$payout = $payout->jsonSerialize();
							//echo $payout;
							//print_r($payout);
							//die();
							
							$res_id = $this->Api_modal->create_user_payout(array(
							'user_id' => $userid,
							'bank_id' => $user_bank_id,
							'withdraw_amount' => $amount,
							'transaction_number' => $payout['balance_transaction'],
							'created_date' => date('Y-m-d H:i:s'),
							'status' => 'succeeded'
							));
						
							$total_avail_balance = $wallet_data[0]['total_avail_balance'] - $amount;
							$total_credit_balance = 0;
							$total_withdraw_balance = $amount; 
							
							$wallet_id = $this->Api_modal->update_wallet(array(
								'total_avail_balance' => $total_avail_balance,
								'total_credit_balance' => $total_credit_balance,
								'total_withdraw_balance' => $total_withdraw_balance,
								'created_date' => $created_date
							),$wallet_data[0]['wallet_id']);
							
							/* $check_token = $this->Api_modal->read_device_info($userid);
							if(!empty($check_token))
							{
								foreach($check_token as $user_token)
								{
									$token = $user_token['fcm_token'];
									$msg = "You have successfully withdraw " .$amount."$ from your wallet!";
									$title = "Wallet Withdraw";
									$this->sendGCM($token,$msg,$title);
								}
								
							} */
							
							
							$returndata['status'] = true;
							$returndata['message'] = "You have successfully withdraw $" .$amount." from your wallet!";
								
							$wallet_data1 = $this->Api_modal->crud_read_wallet($userid);
							if(!empty($wallet_data1))
							{
								$returndata['wallet_data'] = $wallet_data1;
								
							}else{
								$returndata['wallet_data'] = array();
							}
				
						}else{
							$returndata['status'] = false;
							$returndata['message'] = "You dont have enough balance to withdraw!";
						}
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "You dont have enough balance to withdraw!";
					}
				
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "Bank details not match !";
				}
			}else{
					$returndata['status'] = false;
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function check_username_availability_post()
	{
		$returndata = array();
		$username = $this->input->post("username");
		if(!empty($username))
		{
			$userdata = $this->Api_modal->check_username($username);
			if(count($userdata) > 0)
			{
				$returndata['status'] = false;
				$returndata['message'] = "Username already taken";
				
			}else{
				$returndata['status'] = true;
				$returndata['message'] = "Username available!";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->response($returndata);
	}
	
	public function get_merchant_details_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$merchant_id = $this->input->post("merchant_id");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$merchant_data = $this->Api_modal->crud_read_merchant($merchant_id);
				$returndata['merchant_data'] = $merchant_data;
				$returndata['status'] = true;
				$returndata['message'] = "Merchant data fetched successfully!";
				
			}else{
				$returndata['status'] = false;
				$returndata['merchant_data'] = array();
				$returndata['message'] = "Merchant details not Found!";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		
		$this->response($returndata);
	}
	
	public function id_verification_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$ssn = $this->input->post("ssn");
		$created_date = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$id_data = array('merchant_id' => $userid,'user_type'=>1);
				$delete  = $this->Merchant_modal->delete('tbl_id_verification1',$id_data);
				$delete  = $this->Merchant_modal->delete('tbl_id_verification',$id_data);
				$id_verification = $this->Api_modal->add_verification(array('merchant_id' => $userid,'user_type' => 1,'ssn' => $ssn));
				$uploaddir = './assets/img/photos/';
				$path = $_FILES['frontid']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				$user_img = time() . rand() . '.' . $ext;
				$uploadfile = $uploaddir . $user_img;
				if ($_FILES["frontid"]["name"]) {
					if (move_uploaded_file($_FILES["frontid"]["tmp_name"],$uploadfile)) {
					$frontid = $user_img;
					}else{
						$frontid = '';
					}
				}
				
				$res_id = $this->Api_modal->add_verification_ids(array(
						'merchant_id' => $userid,
						'verification_id' => $id_verification,
						'img' => $frontid,
						'type' => 1,
						'user_type' => 1
					));
					
				$path1 = $_FILES['backid']['name'];
				$ext1 = pathinfo($path, PATHINFO_EXTENSION);
				$user_img1 = time() . rand() . '.' . $ext1;
				$uploadfile1 = $uploaddir . $user_img1;
				if ($_FILES["backid"]["name"]) {
					if (move_uploaded_file($_FILES["backid"]["tmp_name"],$uploadfile1)) {
					$backid = $user_img1;
					}else{
						$backid = '';
					}
				}
				$res_id = $this->Api_modal->add_verification_ids(array(
						'merchant_id' => $userid,
						'verification_id' => $id_verification,
						'img' => $backid,
						'type' => 2,
						'user_type' => 1
					));
					
				$path2 = $_FILES['selfiewithid']['name'];
				$ext2 = pathinfo($path, PATHINFO_EXTENSION);
				$user_img2 = time() . rand() . '.' . $ext2;
				$uploadfile2 = $uploaddir . $user_img2;
				if ($_FILES["selfiewithid"]["name"]) {
					if (move_uploaded_file($_FILES["selfiewithid"]["tmp_name"],$uploadfile2)) {
					$selfiewithid = $user_img2;
					}else{
						$selfiewithid = '';
					}
				}
				$res_id = $this->Api_modal->add_verification_ids(array(
						'merchant_id' => $userid,
						'verification_id' => $id_verification,
						'img' => $selfiewithid,
						'type' => 3,
						'user_type' => 1
					));
					
				$title = $userdata[0]['name']." has added the document(s)";
				$notification = array('title' => $title,'table_data'=>'id_verification_user','type'=>1,'userid'=>$userid);
				$this->Merchant_modal->insert('tbl_notification',$notification);
				
				$returndata['status'] = true;
				$returndata['message'] = "Verification Ids uploaded successfully!";
				
			}else{
					$returndata['status'] = false;
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		
		$this->response($returndata);
	}
	
	public function get_saved_ids_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$user_ids = $this->Api_modal->crud_read_verification($userid);
				
				$ids_arr =array();
				if(!empty($user_ids))
				{
					foreach($user_ids as $ids)
					{
						$ids_details = $this->Api_modal->curd_read_user_all_ids($ids['id']);
						$details_arr =array();
						if(!empty($ids_details))
						{
							foreach($ids_details as $details)
							{
								$details['img'] = base_url()."assets/img/photos/".$details['img'];
								$details_arr[] = $details;
							}
						}
						$ids['img_arr'] = $details_arr;
						$ids_arr[] = $ids;
					}
					
					$returndata['status'] = true;
					$returndata['user_ids'] = $ids_arr;
					$returndata['message'] = "User Ids fetched successfully!";
					
				}else{
					$returndata['status'] = false;
					$returndata['user_ids'] = $ids_arr;
					$returndata['message'] = "User Ids not found!";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		
		$this->response($returndata);
	}
	
	
	public function delete_account_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$user_id = $this->Api_modal->update_user(array(
							'delete_status' => 1
						),$userid);
						
				$returndata['status'] = true;
				$returndata['message'] = "User account deleted successfully!";
				
			}else{
					$returndata['status'] = false;
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function about_us_post()
	{
		$returndata = array();
		$returndata['about_us'] = $this->Api_modal->crud_read_about_us();
		$returndata['status'] = true;
		$returndata['message'] = "About us fetched successfully!";
		$this->response($returndata);
	}
	
	public function privacy_policy_post()
	{
		$returndata = array();
		$returndata['privacy_policy'] = $this->Api_modal->crud_read_privacy_policy();
		$returndata['status'] = true;
		$returndata['message'] = "Privacy policy fetched successfully!";
		$this->response($returndata);
	}
	
	public function terms_and_conditions_post()
	{
		$returndata = array();
		$returndata['terms_and_conditions'] = $this->Api_modal->crud_read_tandc();
		$returndata['status'] = true;
		$returndata['message'] = "Terms & conditions fetched successfully!";
		$this->response($returndata);
	}
	
	public function contact_support_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$email = $this->input->post("email");
		$msg = $this->input->post("msg");
		$comment = $this->input->post("comment");
		$create_date = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$res_id = $this->Api_modal->crud_create_contact_support(array(
							'userid' => $userid,
							'email' => $email,
							'msg' => $msg,
							'comment' => $comment,
							'create_date' => $create_date
						));
				$returndata['status'] = true;
				$returndata['message'] = "Contact query added successfully!";
				
			}else{
					$returndata['status'] = false;
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function get_merchant_rewards_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$lat = $this->input->post("lat");
		$long = $this->input->post("long");
		$min_range = $this->input->post("min_range");
		$max_range = $this->input->post("max_range");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				//$coupon_data = $this->Api_modal->crud_read_coupons($lat,$long,$min_range,$max_range);
				$cash_data = $this->Api_modal->crud_read_cash($lat,$long,$min_range,$max_range);
				
				/* echo "<pre>";
				print_r($wallet_data);
				die(); */
				/* if(!empty($coupon_data))
				{ */
					if(!empty($cash_data))
					{
						$returndata['status'] = true;
						$returndata['coupons'] = array();
						$returndata['cash'] = $cash_data;
						$returndata['message'] = "Rewards fetched successfully!";
					}else{
						$returndata['status'] = false;
						$returndata['coupons'] = array();
						$returndata['cash'] = array();
						$returndata['message'] = " No merchant reward found!";
					}
					
				/* }else{
					if(!empty($cash_data))
					{
						$returndata['status'] = true;
						$returndata['coupons'] = array();
						$returndata['cash'] = $cash_data;
						$returndata['message'] = "Rewards fetched successfully!";
					}else{
						$returndata['status'] = false;
						$returndata['coupons'] = array();
						$returndata['cash'] = array();
						$returndata['message'] = "Rewards fetched successfully!";
					}
				} */
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function save_rewards_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$coupon_id = $this->input->post("coupon_id");
		$reward_source_type = $this->input->post("reward_source_type");
		$create_date_time = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				if($reward_source_type =='1')
				{
					$reward_type = 1;
				}else{
					$reward_type = 2;
				}
				$check_history =$this->Api_modal->crud_read_reward_by_id($userid,$coupon_id);
				if(count($check_history)>0)
				{
					
				}else{
					$history_id = $this->Api_modal->crud_create_user_reward_history(array('userid' => $userid,
						'coupon_id' => $coupon_id,
						'reward_type' => $reward_type,
						'reward_source_type' => $reward_source_type,
						'create_date_time' => $create_date_time
					));
				}
				
				$returndata['status'] = true;
				$returndata['message'] = "Rewards saved successfully!";
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	public function get_reward_list_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$reward_type = $this->input->post("reward_type");
		$min_range = $this->input->post("min_range");
		$max_range = $this->input->post("max_range");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$reward_list = $this->Api_modal->crud_read_user_reward_history($userid,$reward_type,$min_range,$max_range);
				if(!empty($reward_list))
				{
					$returndata['status'] = true;
					$returndata['reward_list'] = $reward_list;
					$returndata['message'] = "Rewards fetched successfully!";
				}else{
					$returndata['status'] = false;
					$returndata['reward_list'] = array();
					$returndata['message'] = "No reward Found!";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		$this->response($returndata);
	}
	
	
	public function get_coupon_list_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$lat = $this->input->post("lat");
		$long = $this->input->post("long");
		$min_range = $this->input->post("min_range");
		$max_range = $this->input->post("max_range");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				//$coupon_list = $this->Api_modal->crud_read_coupons($lat,$long,$min_range,$max_range);
				$cash_list = $this->Api_modal->crud_read_cash($lat,$long,$min_range,$max_range);
				//$coupon_arr = array_merge($coupon_list,$cash_list);
				//usort($coupon_arr);
				if(!empty($cash_list))
				{
					$returndata['status'] = true;
					$returndata['coupon_list'] = $cash_list;
					$returndata['message'] = "Reward list fetched successfully!";
				}else{
					$returndata['status'] = false;
					$returndata['coupon_list'] = array();
					$returndata['message'] = "No Reward Found!";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	
	public function get_nearby_coupon_list_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$lat = $this->input->post("lat");
		$long = $this->input->post("long");
		$min_range = $this->input->post("min_range");
		$max_range = $this->input->post("max_range");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$coupon_arr =array();
				//$coupon_list = $this->Api_modal->crud_read_coupons($lat,$long,$min_range,$max_range);
				/* echo $this->db->last_query();
				echo "<pre>";
				print_r($coupon_list);
				die(); */
				/* if(count($coupon_list) >0)
				{
					foreach($coupon_list as $coupon)
					{
						
						$check_saved_coupon = $this->Api_modal->crud_read_saved_coupon($userid,$coupon['id']);
						
						if(empty($check_saved_coupon))
						{
						
							$merchant_logo = $this->Api_modal->crud_read_merchant($coupon['merchant_id']);
							
							if(!empty($merchant_logo[0]['img']))
							{
								$coupon['merchant_logo'] = base_url()."assets/img/photos/".$merchant_logo[0]['img'];
							}else{
								$coupon['merchant_logo'] = "";
							}
						
							$coupon_arr[] = $coupon;
						}
						
					}
				} */
				$cash_arr =array();
				$cash_list = $this->Api_modal->crud_read_cash($lat,$long,$min_range,$max_range);
				/* echo $this->db->last_query();
				echo "<pre>";
				print_r($cash_list);
				die(); */
				if(count($cash_list) >0)
				{
					foreach($cash_list as $cash)
					{
						$check_saved_cash = $this->Api_modal->crud_read_saved_cash($userid,$cash['id']);
						
						if(empty($check_saved_cash))
						{
							
							$merchant_logo = $this->Api_modal->crud_read_merchant($cash['merchant_id']);
							
							if(!empty($merchant_logo[0]['img']))
							{
								$cash['merchant_logo'] = base_url()."assets/img/photos/".$merchant_logo[0]['img'];
							}else{
								$cash['merchant_logo'] = "";
							}
							
							$cash_arr[] = $cash;
						}
						
					}
				}
				
				
				//$mixed_arr = array_merge($coupon_arr,$cash_arr);
				//usort($coupon_arr);
				if(!empty($cash_arr))
				{
					$returndata['status'] = true;
					$returndata['coupon_list'] = $cash_arr;
					$returndata['message'] = "Reward List fetched successfully!";
				}else{
					$returndata['status'] = false;
					$returndata['coupon_list'] = array();
					$returndata['message'] = "No reward found!";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function get_coupon_details_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$coupon_id = $this->input->post("coupon_id");
		$type = $this->input->post("type"); // 1= coupon , 2 =cash
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				if($type == 1)
				{
					$coupon_arr = $this->Api_modal->crud_read_coupon_by_id($coupon_id);
					
				}elseif($type == 2){
					$coupon_arr = $this->Api_modal->crud_read_cash_by_id($coupon_id);
				}
				if(!empty($coupon_arr))
				{
					$returndata['status'] = true;
					$returndata['coupon_details'] = $coupon_arr;
					$returndata['message'] = "Reward details fetched successfully!";
				}else{
					$returndata['status'] = false;
					$returndata['coupon_details'] = array();
					$returndata['message'] = "Reward details not found!";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		
		$this->response($returndata);
	}
	
	public function search_coupon_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$coupon_name = $this->input->post("coupon_name");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$coupon_list = $this->Api_modal->crud_read_filter_coupon($coupon_name);
				if(!empty($coupon_list))
				{
					$returndata['status'] = true;
					$returndata['coupon_details'] = $coupon_list;
					$returndata['message'] = "Reward fetched successfully!";
				}else{
					$returndata['status'] = false;
					$returndata['coupon_details'] = array();
					$returndata['message'] = "Reward not found!";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function redeem_coupon_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$reward_id = $this->input->post("reward_id");
		$amount = $this->input->post("amount");
		if(!empty($userid) && !empty($reward_id) && !empty($amount))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$check_coupon = $this->Api_modal->crud_read_coupon_id($userid,$reward_id);
				/* print_r($check_coupon);
				die(); */
				if(count($check_coupon)>0)
				{
					if($check_coupon[0]['reward_status'] ==0)
					{
						$res = $this->Api_modal->crud_redeem_coupon(array(
								'reward_status' => '1',
								'cash_won' => $amount
							),$reward_id);
						
						$wallet_data = $this->Api_modal->crud_read_wallet($userid);
						if(count($wallet_data) > 0)
						{
							foreach($wallet_data as $wallet)
							{
								$total_avail_balance = $wallet['total_avail_balance'] + $amount;
								$total_credit_balance = $wallet['total_credit_balance'] + $amount;
								$nextworld_money = $wallet['nextworld_money'] + $amount;
								$last_added_amount = $amount; 
								
								$wallet_id = $this->Api_modal->update_wallet(array(
									'total_avail_balance' => $total_avail_balance,
									'total_credit_balance' => $total_credit_balance,
									'nextworld_money' => $nextworld_money,
									'last_added_amount' => $last_added_amount,
									'created_date' => date('Y-m-d')
								),$wallet['wallet_id']);
							}
						}else{
							$wallet_id = $this->Api_modal->create_wallet(array(
									'user_id' => $userid,
									'total_avail_balance' => $amount,
									'total_credit_balance' => $amount,
									'total_withdraw_balance' => 0,
									'nextworld_money' => $amount,
									'last_added_amount' => $amount,
									'user_type' => 1,
									'created_date' => date('Y-m-d')
								));
						}
						$returndata['status'] = true;
						$returndata['message'] = "Reward redeem successfully!";
					}else{
					$returndata['status'] = false;
					$returndata['message'] = "Reward is expired or already redeemed!";
				}
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "Reward details not found!";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function get_user_coupon_list_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;//19
		$min_range = $this->input->post("min_range");
		$max_range = $this->input->post("max_range");
		
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$coupon_list = $this->Api_modal->crud_read_coupon($userid,$min_range,$max_range);
				/* echo $this->db->last_query();
				echo "<pre>";
				print_r($coupon_list);
				die(); */
				$coupons_arr =array();
				if(!empty($coupon_list))
				{
					foreach($coupon_list as $coupon)
					{
						if($coupon['reward_source_type'] ==1)
						{
							$coupon_details = $this->Api_modal->crud_read_coupon_by_id($coupon['coupon_id']);
							 
							if(!empty($coupon_details))
							{
								if(!empty($coupon_details[0]['coupon_image']))
								{
									$coupon_details[0]['coupon_image'] = base_url()."assets/img/photos/".$coupon_details[0]['coupon_image'];
								}
							}
							$coupon['coupon_details'] = $coupon_details;
							
						}else if($coupon['reward_source_type'] ==2)
						{
							$coupon['coupon_details'] = $this->Api_modal->crud_read_cash_by_id($coupon['coupon_id']);
							
						}else if($coupon['reward_source_type'] ==3)
						{
							$coupon_details = $this->Api_modal->crud_read_nexplay($coupon['coupon_id']);
							 
							if(!empty($coupon_details))
							{
								if(!empty($coupon_details[0]['coupon_image']))
								{
									$coupon_details[0]['coupon_image'] = base_url()."assets/img/photos/".$coupon_details[0]['coupon_image'];
								}
							}
							
							$coupon['coupon_details'] = $coupon_details;
							
						}/* else if($coupon['reward_source_type'] ==4)
						{
							$coupons = $this->Api_modal->crud_read_nexplay($coupon['coupon_id']);
							
						} */
						
						/* echo "<pre>";
						print_r($coupon);
						die(); */
						if(!empty($coupon['coupon_details']))
						{
							$coupons_arr[] = $coupon;
						}
					}
					
					$returndata['status'] = true;
					$returndata['coupon_list'] = $coupons_arr;
					$returndata['message'] = "Reward fetched successfully!";
				}else{
					$returndata['status'] = false;
					$returndata['coupon_list'] = array();
					$returndata['message'] = "You have not saved any reward yet!";
				}
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function notification_setting_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$notification_status = $this->input->post("notification_status");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$user_id = $this->Api_modal->update_user(array(
							'notification_status' => 1
						),$userid);
						
				$returndata['status'] = true;
				$returndata['message'] = "Setting changed successfully!";
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function device_info_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$fcm_token = $this->input->post("fcm_token");
		$device_id = $this->input->post("device_id");
		$imei = $this->input->post("imei");
		$model = $this->input->post("model");
		$brand = $this->input->post("brand");
		$app_version = $this->input->post("app_version");
		if(!empty($userid) && !empty($fcm_token))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$check_old_record = $this->Api_modal->read_device_info($userid,$fcm_token);
				if(count($check_old_record) > 0)
				{	
				$res = $this->Api_modal->update_device_info(array(
							'fcm_token' => $fcm_token,
							'userid' => $userid,
							'device_id' => $device_id,
							'imei' => $imei,
							'model' => $model,
							'brand' => $brand,
							'app_version' => $app_version
						),$check_old_record[0]['device_info_id']);
				}else{
					$res = $this->Api_modal->create_device_info(array(
							'fcm_token' => $fcm_token,
							'userid' => $userid,
							'device_id' => $device_id,
							'imei' => $imei,
							'model' => $model,
							'brand' => $brand,
							'app_version' => $app_version
						));
				}	
				$returndata['status'] = true;
				$returndata['message'] = "Device info updated successfully!";
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	/* ----Nexplay game api  starts here*/
	
	public function get_nexplay_games_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$nexplay_game_list = $this->Api_modal->read_nexplay_games();
				if(count($nexplay_game_list) > 0)
				{	
					$game_arr = array();
					foreach($nexplay_game_list as $game_list)
					{
						$quiz_ques_list = $this->Api_modal->read_quiz_questions($game_list['game_id']);
						/* echo $this->db->last_query();
						echo "<pre>";
						print_r($quiz_ques_list); */
						//die();
						if(count($quiz_ques_list)>0)
						{
							$campaigned_game = $this->Api_modal->read_campaign_started_games($game_list['game_id']);
							
							if(count($campaigned_game)>0)
							{
								if(!empty($game_list['game_img']))
								{
									$game_list['game_img'] = base_url()."assets/img/photos/".$game_list['game_img'];
								}else{
									$game_list['game_img'] = "";
								}
								$game_arr[] = $game_list;
							}
						}
						
					}
					if(count($game_arr)>0)
					{
						$returndata['status'] = true;
						$returndata['nexplay_game_list'] = $game_arr;
						$returndata['message'] = "Nexplay Games fetched successfully!";
					}else{
						$returndata['status'] = false;
						$returndata['nexplay_game_list'] = array();
						$returndata['message'] = "No nexplay games found!";
					}
						
					
				}else{
					$returndata['status'] = false;
					$returndata['nexplay_game_list'] = array();
					$returndata['message'] = "No nexplay games found!";
				}	
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	
	public function get_quiz_category_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$gameid = $this->input->post("gameid");
		
		if(!empty($userid) && !empty($gameid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$quiz_cat_list = $this->Api_modal->read_quiz_category($gameid);
				/* echo "<pre>";
				print_r($quiz_cat_list);
				die(); */
				if(count($quiz_cat_list) > 0)
				{	
					$cat_arr = array();
					foreach($quiz_cat_list as $cat_list)
					{
						if(!empty($cat_list['cat_img']))
						{
							$cat_list['cat_img'] = base_url()."assets/img/photos/".$cat_list['cat_img'];
						}else{
							$cat_list['cat_img'] = "";
						}
						
						$cat_arr[] = $cat_list;
					}
						
						
					$returndata['status'] = true;
					$returndata['quiz_cat_list'] = $cat_arr;
					$returndata['message'] = "Quiz Category fetched successfully!";
				}else{
					$returndata['status'] = false;
					$returndata['quiz_cat_list'] = array();
					$returndata['message'] = "Quiz category not found!";
				}	
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function get_quiz_questions_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$gameid = $this->input->post("gameid");
		$catid = $this->input->post("catid");
		
		if(!empty($userid) && !empty($gameid) && !empty($catid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				//quiz_result_id	gameid	catid	userid	tscore	win	loose	create_date
				$user_quiz_history = $this->Api_modal->read_quiz_result_by_user($gameid,$catid,$userid);
				
				if(count($user_quiz_history) > 0)
				{
					
					if($user_quiz_history[0]['win']==1)
					{
						
						$returndata['status'] = false;
						$returndata['message'] = "You have played all the questions in this category; please select another category to try additional questions.";
						$this->response($returndata);
					}else{
						
						/* $today = date('Y-m-d H:i:s');
						$stop_date = $user_quiz_history[0]['create_date'];
						//echo 'date before day adding: ' . $stop_date; 
						$stop_date1 = date('Y-m-d H:i:s', strtotime($stop_date . ' +1 day'));
						
						if(strtotime($today) < strtotime($stop_date1))
						{
							
							$returndata['status'] = false;
							$returndata['message'] = "You can play again after 24 hours.";
							$this->response($returndata);
							
						}else{ */
							
							$user_play_history = $this->Api_modal->read_user_play_history($userid,$gameid,$catid);
					
							if(count($user_play_history)>0)
							{
							
								$quiz_ques_list = $this->Api_modal->read_quiz_set_questions($gameid,$catid,$user_play_history[0]['played_ques']);
							}else{
								$quiz_ques_list = $this->Api_modal->read_quiz_set_questions($gameid,$catid);
							}
							$ad_list = $this->Api_modal->read_nexplay_campaign($gameid);
							
							if(count($ad_list) > 0)
							{	
								if(count($quiz_ques_list) >0)
								{	
									$add_arr = array();

									foreach($ad_list as $list)
									{
										if(!empty($list['coupon_image']))
										{
											$list['coupon_image'] = base_url()."assets/img/game_img/".$list['coupon_image'];
										}else{
											$list['coupon_image'] = "";
										}
										
										$add_arr[] = $list;
									}
									
									foreach($quiz_ques_list as $ques_list)
									{
									//played_id	userid	gameid	catid	played_ques	create_date
										$this->Api_modal->create_user_play_history(array(
											'userid'=>$userid,
											'gameid'=>$ques_list['gameid'],
											'catid'=>$ques_list['category'],
											'played_ques'=>$ques_list['id'],
											'create_date'=>date('Y-m-d')
										));
										
									}
											
									
									//echo $this->db->last_query();
									/* echo "<pre>";
									print_r($quiz_ques_list);
									print_r($ad_list);
									die(); */
								
									$returndata['status'] = true;
									$returndata['quiz_ques_list'] = $quiz_ques_list;
									$returndata['sponsor_list'] = $add_arr;
									$returndata['message'] = "Quiz Questions fetched successfully!";
								}else{
									$returndata['status'] = false;
									$returndata['quiz_ques_list'] = array();
									$returndata['sponsor_list'] = array();
									$returndata['message'] = "You have played all the questions in this category, please select another category to try additional questions.";
								}
							}else{
									$returndata['status'] = false;
									$returndata['quiz_ques_list'] = array();
									$returndata['sponsor_list'] = array();
									$returndata['message'] = "Nexplay campaign has not started on this game yet!";
							}
							
						//}
					}
				}else{
					
					$user_play_history = $this->Api_modal->read_user_play_history($userid,$gameid,$catid);
					
					if(count($user_play_history)>0)
					{
					
						$quiz_ques_list = $this->Api_modal->read_quiz_set_questions($gameid,$catid,$user_play_history[0]['played_ques']);
					}else{
						$quiz_ques_list = $this->Api_modal->read_quiz_set_questions($gameid,$catid);
					}
					
					$ad_list = $this->Api_modal->read_nexplay_campaign($gameid);
					
					if(count($ad_list) > 0)
					{	
						if(count($quiz_ques_list) >0)
						{	
							$add_arr = array();

							foreach($ad_list as $list)
							{
								if(!empty($list['coupon_image']))
								{
									$list['coupon_image'] = base_url()."assets/img/game_img/".$list['coupon_image'];
								}else{
									$list['coupon_image'] = "";
								}
								
								$add_arr[] = $list;
							}
							
							foreach($quiz_ques_list as $ques_list)
							{
							//played_id	userid	gameid	catid	played_ques	create_date
								$this->Api_modal->create_user_play_history(array(
									'userid'=>$userid,
									'gameid'=>$ques_list['gameid'],
									'catid'=>$ques_list['category'],
									'played_ques'=>$ques_list['id'],
									'create_date'=>date('Y-m-d')
								));
								
							}
									
							
							//echo $this->db->last_query();
							/* echo "<pre>";
							print_r($quiz_ques_list);
							print_r($ad_list);
							die(); */
						
							$returndata['status'] = true;
							$returndata['quiz_ques_list'] = $quiz_ques_list;
							$returndata['sponsor_list'] = $add_arr;
							$returndata['message'] = "Quiz Questions fetched successfully!";
						}else{
							$returndata['status'] = false;
							$returndata['quiz_ques_list'] = array();
							$returndata['sponsor_list'] = array();
							$returndata['message'] = "You have played all the questions in this category, please select another category to try additional questions.";
						}
					}else{
							$returndata['status'] = false;
							$returndata['quiz_ques_list'] = array();
							$returndata['sponsor_list'] = array();
							$returndata['message'] = "Nexplay campaign has not started on this game yet!";
					}
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function quiz_submit_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$gameid = $this->input->post("gameid");
		$catid = $this->input->post("catid");
		$campaign_id = $this->input->post("campaign_id");
		$answer_arr = $this->input->post("answer_arr");
		
		if(!empty($userid) && !empty($gameid) && !empty($catid) && !empty($campaign_id) && !empty($answer_arr))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				/* $answer_arr = '{
				"ansList": [
					{
						"question_id": "40",
						"selected_option": "1"
					},
					{
						"question_id": "41",
						"selected_option": "2"
					},
					{
						"question_id": "42",
						"selected_option": "2"
					}
				]
				}'; */
	
				if(!empty($answer_arr))
				{
					$res = json_decode($answer_arr);
					$response = $res->ansList;
					$this->Api_modal->delete_quiz_submit($gameid,$catid,$userid);
					foreach($response as $user_answer)
					{
						//quiz_submit_id	gameid	catid	userid	quesid	user_answer	right_ans	answer_status	score	create_date
						$question_id = $user_answer->question_id;
						$selected_option = $user_answer->selected_option;
						
						$get_quiz = $this->Api_modal->read_quiz_questions($gameid,$catid,$question_id);
						if(count($get_quiz) >0 )
						{
							if($selected_option == $get_quiz[0]['right_ans'])
							{
								$answer_status = 1;
								$score = 100;
							}else{
								$answer_status = 0;
								$score = 0;
							}
							$insert = $this->Api_modal->crud_quiz_submit(array("gameid"=>$gameid,"catid"=>$catid,"userid"=>$userid,"quesid"=>$question_id,"user_answer"=>$selected_option,"right_ans"=>$get_quiz[0]['right_ans'],"answer_status"=>$answer_status,"score"=>$score));
						}
					}
					//quiz_result_id	gameid	catid	userid	tscore	win	loose	create_date
					$user_result = $this->Api_modal->crud_read_user_answer($gameid,$catid,$userid);
					if(count($user_result) > 0)
					{
						$tscore = 0;
						$total_score = 0;
						foreach($user_result as $result)
						{
							$tscore+= $result['score'];
							$total_score+= 100;
						}
						if($tscore < $total_score)
						{
							$create_result = $this->Api_modal->crud_create_quiz_result(array("gameid"=>$gameid,"catid"=>$catid,"userid"=>$userid,"tscore"=>$tscore,"win"=>0,"loose"=>1),$gameid,$catid,$userid);
							
							$get_nexplay = $this->Api_modal->crud_read_nexplay($campaign_id);
							$total_win = 0;
							$total_loose = 0;
							if(count($get_nexplay) >0 )
							{
								$total_win += $get_nexplay[0]['total_win'];
								$total_loose += $get_nexplay[0]['total_loose']+1;
								$update_nexplay = $this->Api_modal->update_nexplay(array("total_win"=>$total_win,"total_loose"=>$total_loose),$campaign_id);
							}
							$returndata['status'] = false;
							$returndata['message'] = "You loose";
							$this->response($returndata);
						}else{
								$cash_won = "0.001";
								$create_result = $this->Api_modal->crud_create_quiz_result(array("gameid"=>$gameid,"catid"=>$catid,"userid"=>$userid,"tscore"=>$tscore,"win"=>1,"loose"=>0,"cash_won"=>$cash_won),$gameid,$catid,$userid);
								
								$get_nexplay = $this->Api_modal->crud_read_nexplay($campaign_id);
								$total_win = 0;
								$total_loose = 0;
								
								if(count($get_nexplay) >0 )
								{
									$total_win += $get_nexplay[0]['total_win']+1;
									$total_loose += $get_nexplay[0]['total_loose'];
									$update_nexplay = $this->Api_modal->update_nexplay(array("total_win"=>$total_win,"total_loose"=>$total_loose,"total_payout"=>$cash_won),$campaign_id);
								}
								
								$history_id = $this->Api_modal->crud_create_user_reward_history(array('userid' => $userid,
								'coupon_id' => $campaign_id,
								'reward_type' => 2,
								'reward_source_type' => 3,
								'reward_status' => 1,
								'cash_won' => $cash_won,
								'create_date_time' => date('Y-m-d h:i:s')
								));
					
					
								$wallet_data = $this->Api_modal->crud_read_nexplay_wallet($userid);
								//$this->db->last_query();
								/* echo "<pre>";
								print_r($wallet_data);
								die(); */
								if(count($wallet_data) > 0)
								{
									foreach($wallet_data as $wallet)
									{
										$total_avail_balance = $wallet['total_avail_balance'] + $cash_won;
										$total_credit_balance = $wallet['total_credit_balance'] + $cash_won;
										$total_earned_balance = $wallet['total_earned_balance'] + $cash_won;
										$last_added_amount = $cash_won; 
										
										$wallet_id = $this->Api_modal->update_nexplay_wallet(array(
											'total_avail_balance' => $total_avail_balance,
											'total_credit_balance' => $total_credit_balance,
											'campaign_id' => $campaign_id,
											'total_earned_balance' => $total_earned_balance,
											'last_added_amount' => $last_added_amount,
											'last_update_date' => date('Y-m-d')
										),$wallet['user_wallet_id']);
									}
								}else{
									$wallet_id = $this->Api_modal->create_nexplay_wallet(array(
											'user_id' => $userid,
											'total_avail_balance' => $cash_won,
											'total_credit_balance' => $cash_won,
											'campaign_id' => $campaign_id,
											'total_withdraw_balance' => 0,
											'total_earned_balance' => $cash_won,
											'last_added_amount' => $cash_won,
											'last_update_date' => date('Y-m-d')
										));
								}
								
								
							$wallet_data1 = $this->Api_modal->crud_read_wallet($userid);
							if(count($wallet_data1)>0)
							{
								$nexplay_money = $wallet_data1[0]['nexplay_money'] + $cash_won;
								
								$wallet_id = $this->Api_modal->update_wallet(array(
								'nexplay_money' => $nexplay_money
								),$wallet_data1[0]['wallet_id']);
							
							}else{
								
								$wallet_id = $this->Api_modal->create_wallet(array(
								'user_id' => $userid,
								'total_avail_balance' => 0,
								'total_credit_balance' => 0,
								'total_withdraw_balance' => 0,
								'last_added_amount' => 0,
								'user_type' => 1,
								'created_date' => date('Y-m-d'),
								'nexplay_money' => $cash_won
								));
							
							}
								$returndata['status'] = true;
								$returndata['message'] = "Great, You won $".$cash_won."!";
						}
						
					}
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
		/* ----Nexplay game api  end here*/
		
	
	public function caenex_logo_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$merchant_logo_list = $this->Api_modal->crud_read_merchant_by_mobile();
				/* echo $this->db->last_query();
				print_r($merchant_logo_list);
				die(); */
				$logo_arr = array();
				if(count($merchant_logo_list) > 0)
				{	
					foreach($merchant_logo_list as $logo)
					{
						$user_img = base_url()."/assets/img/photos/".$logo['img'];
						$logo['img'] = $user_img;
						$logo_arr[] = $logo;
					}
					$returndata['status'] = true;
					$returndata['merchant_logo_list'] = $logo_arr;
					$returndata['message'] = "Merchant logo fetched successfully!";
				}else{
					$returndata['status'] = true;
					$returndata['merchant_logo_list'] = array();
					$returndata['message'] = "Merchant data not found!";
				}	
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function get_country_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$returndata['country_list'] = $this->Api_modal->get_country();
				$returndata['status'] = true;
				$returndata['message'] = "Country List";
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function get_state_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$country = $this->input->post("country");
		if(!empty($userid) && !empty($country))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$returndata['state_list'] = $this->Api_modal->get_state($country);
				$returndata['status'] = true;
				$returndata['message'] = "State List";
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function get_city_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$state = $this->input->post("state");
		if(!empty($userid) && !empty($state))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$returndata['city_list'] = $this->Api_modal->get_city($state);
				$returndata['status'] = true;
				$returndata['message'] = "City List";
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	/* Send notification */
	public function sendGCM($token,$msg,$title) {
		//$message = "You got a new match!";
		/* echo $token."/".$msg."/".$title;
		die(); */
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array (
        'to' => $token,
        'notification' => array (
                "body" => $msg,
                "title" => $title,
                "icon" => "myicon"
        )
		);
		$fields = json_encode ( $fields );
		//$firebase_key = "AAAAlQfrVzo:APA91bEjo5rMYM01JFjyqGNuIelC_TC5eiTRtAQW3Ar_2rTYDeNXAc4T2pTw0KTs_ncwv8zGASVlt-i5Sqt-jf5nhOzYMID4g8tz0SoeW5P64bsdE5hikyFyIxha6H-kyIDtn5UtuAjx";
		$firebase_key = "AAAAFg8f74I:APA91bE8QqaFRpG5aYiod3TJ4J8g1m-oB2gWPoy-F3bU4bDcPHKwsfDMju9xkZsNwY9tYbEPDGEJcbOY_rPxq-eUlhJVaG1MHmYOVDlSMQymtjrTqifNqJueJK8DNKAXtJec1uwF7dC2";
		$headers = array (
				'Authorization: key=' . $firebase_key,
				'Content-Type: application/json'
		);

		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
		$result = curl_exec ( $ch );
		//var_dump($result);
		return $result;
		curl_close ( $ch );
		
	}
	
	/* Send notification */
	
	public function add_bank_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$account_holder_name = $this->input->post("account_holder_name");
		$account_holder_type = $this->input->post("account_holder_type");
		$routing_number = trim($this->input->post("routing_number"));
		$account_number = trim($this->input->post("account_number"));
		$ssn_last_4 = trim($this->input->post("ssn_last_4"));			
		$create_date_time = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				if(strlen($routing_number)>=9)
				{
					if(!empty($account_holder_name))
					{
						$account_holder_name = $account_holder_name;
					
					if(!empty($account_holder_type))
					{
						$account_holder_type = $account_holder_type;
						
					
					if(!empty($account_number))
					{
						$account_number = $account_number;
						
					if(!empty($ssn_last_4))
					{
						$ssn_last_4 = $ssn_last_4;
						
					if(!empty($userdata[0]['name']))
					{
						
						$name = explode(" ",$userdata[0]['name']);
						if(count($name)>1)
						{
							$first_name = $name[0];
							$last_name = $name[1];
						}else{
							$first_name = $name[0];
							$last_name = 'Test';
						}
					
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank1!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['email']))
					{
						$email = $userdata[0]['email'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank2!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['phone']))
					{
						$phone = $userdata[0]['phone'];
					}else{
						
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank3!";
						$this->response($returndata);
						
					}
					
					if(!empty($userdata[0]['street']))
					{
						$street = $userdata[0]['street'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank4!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['city']))
					{
						$city = $userdata[0]['city'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank5!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['state']))
					{
						$state = $userdata[0]['state'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank6!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['zip']))
					{
						$zip = $userdata[0]['zip'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank7!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['country_code']))
					{
						$country_code = $userdata[0]['country_code'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank8!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['personel_id']))
					{
						$personel_id = $userdata[0]['personel_id'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank9!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['dob']))
					{
						$dob = explode("-",$userdata[0]['dob']);
						$year = $dob[0];
						$month = $dob[1];
						$day = $dob[2];
						
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank10!";
						$this->response($returndata);
					}
					
					$history_id = $this->Api_modal->crud_read_user_bank($userid,$account_number);
					if(count($history_id) >0)
					{
						$returndata['status'] = false;
						$returndata['message'] = "Bank details already exists";
						$this->response($returndata);
					}else{
						
					//require_once APPPATH."third_party/stripe/lib/Stripe.php";
					require_once APPPATH."third_party/stripe/init.php";
					//$token= $this->input->post('stripeToken');
					$stripe = array(
						"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
						"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
					);
					\Stripe\Stripe::setApiKey($stripe['secret_key']);
					
					
					/* echo "<pre>";
					print_r($account_update);
					die(); */
					/* $retrieve_account =\Stripe\Account::retrieve(
									'acct_1IivSvPDV8CdzDmT',
									'person_4IivSv00PhaLgqzG',
									[]
								); */
					/* $stripe = new \Stripe\StripeClient(
					  'sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP'
					);
					$stripe->accounts->retrieve(
					  'acct_1IivSvPDV8CdzDmT',
					  []
					); */
					
					/* $stripe = new \Stripe\StripeClient(
					  'sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP'
					);
					$stripe->accounts->allCapabilities(
					  'acct_1IivSvPDV8CdzDmT',
					  []
					); */

					//https://api.stripe.com/v1/accounts/acct_1F9X6KAgvgEeUMyl \

						
					    /* $url = 'https://api.stripe.com/v1/accounts/acct_1F9X6KAgvgEeUMyl';
					    $secret_key = 'sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP';
					    $fields = array();
						$headers = array('Authorization: Bearer '. $secret_key);
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						$output = curl_exec($ch);
						curl_close($ch);
						var_dump(json_decode($output, true)); // return php array with api response */
						
					/* $stripe = new \Stripe\StripeClient(
					  'sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP'
					);
					$res = $stripe->accounts->allExternalAccounts(
					  'acct_1F9X6KAgvgEeUMyl',
					  ['object' => 'bank_account', 'limit' => 3]
					); */
						/* echo "<pre>";
						print_r($res);
						die(); */
						
					try{
						$account = \Stripe\Token::create([
						'bank_account' => [
						'country' => 'US',
						'currency' => 'usd',
						'account_holder_name' => $account_holder_name,
						'account_holder_type' => $account_holder_type,
						'routing_number' => $routing_number,
						'account_number' => $account_number,
						/* 'account_holder_name' => 'Jenny Rosen',
							'account_holder_type' => 'individual',
							'routing_number' => '110000000',
							'account_number' => '000123456789', */
						],
						]);
					} catch(\Stripe\Exception\CardException $e) {
						  // Since it's a decline, \Stripe\Exception\CardException will be caught
						  /* echo 'Status is:' . $e->getHttpStatus() . '\n';
							echo 'Type is:' . $e->getError()->type . '\n';
							echo 'Code is:' . $e->getError()->code . '\n';
						  // param is '' in this case
							echo 'Param is:' . $e->getError()->param . '\n';
							echo 'Message is:' . $e->getError()->message . '\n'; */
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\RateLimitException $e) {
						  // Too many requests made to the API too quickly
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\InvalidRequestException $e) {
						  // Invalid parameters were supplied to Stripe's API
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\AuthenticationException $e) {
						  // Authentication with Stripe's API failed
						  // (maybe you changed API keys recently)
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\ApiConnectionException $e) {
						  // Network communication with Stripe failed
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\ApiErrorException $e) {
						  // Display a very generic error to the user, and maybe send
						  // yourself an email
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (Exception $e) {
						  // Something else happened, completely unrelated to Stripe
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						}
						
					if($account->id){
						
						try {
							$bank_result = \Stripe\Account::create(array(
							"type" => "custom",
							"country" => "US",
							"email" => $email,
							"business_type"=>"individual",
							"requested_capabilities"=> ['card_payments', 'transfers'],
							"external_account" => $account->id,
							'tos_acceptance' => ['date' => time(),'ip' => $_SERVER['REMOTE_ADDR']],
							'individual' =>['id_number' => $personel_id,'phone' => $phone,'email' => $email,'first_name' => $first_name, 'last_name' =>$last_name,'ssn_last_4' => $ssn_last_4, 'dob' => ['day' => $day, 'month' => $month, 'year' => $year], 'address' => ['city' => $city, 'country' => $country_code, 'line1' => $street, 'postal_code' =>$zip, 'state' => $state]],
							'business_profile' =>['url' => "https://caenex.com",'mcc' => "5818"],
							));
						} catch(\Stripe\Exception\CardException $e) {
						  // Since it's a decline, \Stripe\Exception\CardException will be caught
						  /* echo 'Status is:' . $e->getHttpStatus() . '\n';
							echo 'Type is:' . $e->getError()->type . '\n';
							echo 'Code is:' . $e->getError()->code . '\n';
						  // param is '' in this case
							echo 'Param is:' . $e->getError()->param . '\n';
							echo 'Message is:' . $e->getError()->message . '\n'; */
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\RateLimitException $e) {
						  // Too many requests made to the API too quickly
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\InvalidRequestException $e) {
						  // Invalid parameters were supplied to Stripe's API
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\AuthenticationException $e) {
						  // Authentication with Stripe's API failed
						  // (maybe you changed API keys recently)
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\ApiConnectionException $e) {
						  // Network communication with Stripe failed
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (\Stripe\Exception\ApiErrorException $e) {
						  // Display a very generic error to the user, and maybe send
						  // yourself an email
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						} catch (Exception $e) {
						  // Something else happened, completely unrelated to Stripe
							$returndata['status'] = false;
							$returndata['message'] = $e->getError()->message;
							$this->response($returndata);
						}

						
						if($bank_result->id)
						{
							$stripe_account_id = $bank_result->id;
							$person_id = $bank_result->individual->id;
							$stripe_account_status = $bank_result->status;
							//$external_account_id = $bank_result->external_accounts->data->id;
							
							$account_update =\Stripe\Account::update(
							  $stripe_account_id,
							  [
								'settings' => [
								  'payouts' => [
									'schedule' => [
									  'interval' => 'manual',
									],
								  ],
								],
							  ]
							);
						
							$this->Api_modal->crud_create_user_bank(array(
							'userid' => $userid,
							'account_holder_name' => $account_holder_name,
							'account_holder_type' => $account_holder_type,
							'routing_number' => $routing_number,
							'account_number' => $account_number,
							'status' => $stripe_account_status,
							'stripe_account_id' => $stripe_account_id,
							//'external_account_id' => $external_account_id,
							'person_id' => $person_id,
							'create_date_time' => $create_date_time
							));
						
					
							/* $imgUrl = 'assets/img/game_img/003.jpg';
							$imgUrl1 = 'assets/img/game_img/004.jpg'; */
							//$img_arr = array("assets/img/game_img/003.jpg","assets/img/game_img/004.jpg");
							//$filePath = get_home_path() . strstr($imgUrl, '/wp-content');
							
							$this->bank_verification_ids($userid,$stripe_account_id,$person_id);
							$returndata['status'] = true;
							$returndata['message'] = "Bank details saved successfully. It would may take 24-48 hours for verification!";
							$this->response($returndata);
						
							/* echo "<pre>";
							print_r($doc_arr);
							die(); */
							
							/* $personel_id = \Stripe\Customer::create(
							  ["email" => "Test_yes2@yopmail.com"],
							  ["stripe_account" => $stripe_account_id]
							);
							if($personel_id->id)
							{
								
							}
							echo "<pre>";
							print_r($personel_id);
							die(); */
							
							
						}else{
							$returndata['status'] = false;
							$returndata['message'] = "There is problem in adding a bank , kindly correct your bank details and try again.";
							$this->response($returndata);
						}
						
					
					}else{
							$returndata['status'] = false;
							$returndata['message'] = "There is problem in adding a bank , kindly correct your bank details and try again.";
					}
					}
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "SSN last 4 digits can not be empty!";
					}
					
					
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Account number can not be empty!";
					}
					
					
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Account holder type can not be empty!";
					}
					
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Account holder name can not be empty!";
					}
					
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "Routing number must be at least 9 number !";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		
		$this->response($returndata);
	}
	
	public function bank_verification_ids($userid,$stripe_account_id,$person_id)
	{
		//require_once APPPATH."third_party/stripe/lib/Stripe.php";
		require_once APPPATH."third_party/stripe/init.php";
		//$token= $this->input->post('stripeToken');
		$stripe = array(
			"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
			"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
		);
		\Stripe\Stripe::setApiKey($stripe['secret_key']);
		$doc_arr =array();
		$get_user_ids = $this->Api_modal->curd_read_user_ids($userid);
		if(!empty($get_user_ids))
		{
			foreach($get_user_ids as $img)
			{
				if($img['type']==1)
				{
					
					//$imgUrl = base_url('assets/img/photos/'.$img['img']);
					$imgUrl = './assets/img/photos/'.$img['img'];
					$fp = fopen($imgUrl, 'r');
				
					$upload_identity= \Stripe\File::create([
					  'purpose' => 'identity_document',
					  'file' => $fp,
					], [
					  'stripe_account' => $stripe_account_id,
					]);
				
					if($upload_identity->id)
					{
						$update_bank =\Stripe\Account::updatePerson(
						  $stripe_account_id,
						  $person_id,
						  [
							'verification' => [
							  'document' => [
								'front' => $upload_identity->id,
							  ],
							],
						  ]
						);
					
					}
				
				}else{
					
					$imgUrl = './assets/img/photos/'.$img['img'];
					$fp = fopen($imgUrl, 'r');
				
					$upload_identity= \Stripe\File::create([
					  'purpose' => 'identity_document',
					  'file' => $fp,
					], [
					  'stripe_account' => $stripe_account_id,
					]);
				
					if($upload_identity->id)
					{
						$update_bank =\Stripe\Account::updatePerson(
						  $stripe_account_id,
						  $person_id,
						  [
							'verification' => [
							  'document' => [
								'back' => $upload_identity->id,
							  ],
							],
						  ]
						);
					
					}
					
				}
			}	
				return 1;
			}else{
				return 0;
			}	
	}
		
	public function get_user_bank_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$bank_list = $this->Api_modal->crud_read_user_bank($userid,'');
				if(count($bank_list)>0)
				{
					$returndata['bank_list'] = $this->Api_modal->crud_read_user_bank($userid,'');
					$returndata['status'] = true;
					$returndata['message'] = "Bank List";
				}else{
					$returndata['bank_list'] = array();
					$returndata['status'] = false;
					$returndata['message'] = "Please add your bank information to complete your transfer";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function check_idverification_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			$verify_arr =array();
			if(count($userdata) > 0)
			{
				$res = $this->Api_modal->crud_read_verification($userid);
				if(!empty($res))
				{
					foreach($res as $list)
					{
						if($list['id_status'] ==0)
						{
							$list['msg'] = "Please complete your ID verification before proceeding the transaction to keep Caenex safe for its users.";
						
						}else if($list['id_status'] ==1)
						{
							$list['msg'] = "Your ID verified successfully!";
						}else if($list['id_status'] ==2)
						{
							$list['msg'] = "Your ID verification has been rejected, please contact our support team for more detail!";
						}
						$verify_arr[] = $list;
					}
				}else{
					$verify_arr[] =array("msg"=>"Please upload your Ids and get verified your ID before doing any transactions.");
				}
				
				/* print_r($verify_arr);
				die(); */
				if(empty($verify_arr))
				{
					$returndata['status'] = false;
					$returndata['verification_status'] = $verify_arr;
					$returndata['message'] = "Please upload Verification Ids!";
				}else{
					$returndata['status'] = true;
					$returndata['verification_status'] = $verify_arr;
					$returndata['message'] = "Verification status";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	public function check_nexplay_balance_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$wallet_data = $this->Api_modal->crud_read_nexplay_wallet($userid);
					/* echo $userid;
					echo "<pre>";
					print_r($wallet_data);
					die(); */
				$wallet_arr =array();
				if(!empty($wallet_data))
				{
					foreach($wallet_data as $wallet)
					{
						if($wallet['total_avail_balance'] < 50)
						{
							$wallet['available_balance_to_transfer'] = '0';
						}else{
							$wallet['available_balance_to_transfer'] = $wallet['total_avail_balance'];
						}
						$wallet_arr[] = $wallet;
					}
					
					$returndata['status'] = true;
					$returndata['wallet_data'] = $wallet_arr;
					$returndata['message'] = "Nexplay wallet fetched successfully!";
				}else{
					$sample_arr[] = array(
					"total_earned_balance"=> "0",
					"total_withdraw_balance"=> "0",
					"total_avail_balance"=> "0",
					"available_balance_to_transfer"=> "0",
					);
					$returndata['status'] = true;
					$returndata['wallet_data'] = $sample_arr;
					$returndata['message'] = "Nexplay wallet fetched successfully!";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	
	public function transfer_nexplay_balance_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$amount = $this->input->post("amount");
		$created_date = date('Y-m-d');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$wallet_data = $this->Api_modal->crud_read_nexplay_wallet($userid);
					/* echo "<pre>";
					print_r($wallet_data);
					die(); */
				if(count($wallet_data)>0)
				{
					if($wallet_data[0]['total_avail_balance'] >= 50)
					{
						
						$total_avail_balance = $wallet_data[0]['total_avail_balance'] - $amount;
						$total_withdraw_balance = $amount; 
						
						$wallet_id = $this->Api_modal->update_nexplay_wallet(array(
							'total_avail_balance' => $total_avail_balance,
							'total_withdraw_balance' => $total_withdraw_balance,
							'last_update_date' => $created_date
						),$wallet_data[0]['user_wallet_id']);
						
						$wallet_data1 = $this->Api_modal->crud_read_wallet($userid);
						if(count($wallet_data1)>0)
						{
							$total_avail_balance1 = $wallet_data1[0]['total_avail_balance'] + $amount;
							$total_credit_balance1 = $amount;
							
							$wallet_id = $this->Api_modal->update_wallet(array(
							'total_avail_balance' => $total_avail_balance1,
							'total_credit_balance' => $total_credit_balance1,
							'created_date' => $created_date
							),$wallet_data1[0]['wallet_id']);
						
						}else{
							
							$wallet_id = $this->Api_modal->create_wallet(array(
							'user_id' => $userid,
							'total_avail_balance' => $amount,
							'total_credit_balance' => $amount,
							'total_withdraw_balance' => 0,
							'user_type' => 1,
							'last_added_amount' => $amount,
							'created_date' => $created_date
							));
						
						}
						
						$wallet_data = $this->Api_modal->crud_read_nexplay_wallet($userid);
						$returndata['status'] = true;
						$returndata['wallet_data'] = $wallet_data;
						$returndata['message'] = "You have successfully add $" .$amount." in your main wallet from your nexplay wallet!";
						
					}else{
						$returndata['status'] = false;
						$returndata['wallet_data'] = array();
						$returndata['message'] = "You dont have enough balance in nexplay wallet to transfer in main wallet ,Minimum balance required is :50$.";
					}
				}else{
					
					$returndata['status'] = false;
					$returndata['wallet_data'] = array();
					$returndata['message'] = "You dont have nexplay balance yet!";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
	
	public function get_video_clicked_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		if(isset($headers['Authorization']) &&  !empty($headers['Authorization']))
		{
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		if(!empty($decodedToken['status']) &&  $decodedToken['status']==1)
		{
		$userid = $decodedToken['data']->user_id;
		$campaign_id = $this->input->post("campaign_id");
		$is_full_streamed = $this->input->post("is_full_streamed");
		$created_date = date('Y-m-d');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$nexplay_data = $this->Api_modal->crud_read_nexplay($campaign_id);
				/* echo "<pre>";
				print_r($nexplay_data);
				die(); */
				$video_clicked =0;
				$video_full_streamed=0;
				if(count($nexplay_data)>0)
				{
					if($is_full_streamed ==1)
					{
						$video_clicked += $nexplay_data[0]['video_clicked'];
						$video_full_streamed += $nexplay_data[0]['video_full_streamed'];
						$update_nexplay = $this->Api_modal->update_nexplay(array("video_clicked"=>$video_clicked,"video_full_streamed"=>$video_full_streamed),$campaign_id);
					}else{
						$video_clicked += $nexplay_data[0]['video_clicked'];
						$video_full_streamed += $nexplay_data[0]['video_full_streamed'];
						$update_nexplay = $this->Api_modal->update_nexplay(array("video_clicked"=>$video_clicked),$campaign_id);
						
					}
					$returndata['status'] = true;
					$returndata['message'] = "Nexplay campaign updated!";
				}else{
					
					$returndata['status'] = false;
					$returndata['message'] = "Nexplay campaign not found!";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Token Expired.";
			$this->response($returndata);
		}
		}else{
			$returndata['status'] = false;
        	$returndata['message'] = "Authentication Fail.";
			$this->response($returndata);
		}
		
		$this->response($returndata);
	}
	
}

