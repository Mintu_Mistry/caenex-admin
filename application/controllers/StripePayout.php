<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;
class StripePayout extends REST_Controller
{

	public function __construct()
	{
		parent::__construct();	
		$this->load->library('Authorization_Token');
		$this->load->model('Api_modal');
		$this->load->helper('directory');		
	}
	
	public function add_bank_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		$userid = $decodedToken['data']->user_id;
		$account_holder_name = $this->input->post("account_holder_name");
		$account_holder_type = $this->input->post("account_holder_type");
		$routing_number = trim($this->input->post("routing_number"));
		$account_number = trim($this->input->post("account_number"));
		$ssn_last_4 = trim($this->input->post("ssn_last_4"));
		
		$create_date_time = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				if(strlen($routing_number)>=9)
				{
					if(!empty($account_holder_name))
					{
						$account_holder_name = $account_holder_name;
					
					if(!empty($account_holder_type))
					{
						$account_holder_type = $account_holder_type;
						
					
					if(!empty($account_number))
					{
						$account_number = $account_number;
						
					if(!empty($ssn_last_4))
					{
						$ssn_last_4 = $ssn_last_4;
						
					if(!empty($userdata[0]['name']))
					{
						
						$name = explode(" ",$userdata[0]['name']);
						if(count($name)>1)
						{
							$first_name = $name[0];
							$last_name = $name[1];
						}else{
							$first_name = $name[0];
							$last_name = '';
						}
					
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank1!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['email']))
					{
						$email = $userdata[0]['email'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank2!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['phone']))
					{
						$phone = $userdata[0]['phone'];
					}else{
						
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank3!";
						$this->response($returndata);
						
					}
					
					if(!empty($userdata[0]['street']))
					{
						$street = $userdata[0]['street'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank4!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['city']))
					{
						$city = $userdata[0]['city'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank5!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['state']))
					{
						$state = $userdata[0]['state'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank6!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['zip']))
					{
						$zip = $userdata[0]['zip'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank7!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['country_code']))
					{
						$country_code = $userdata[0]['country_code'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank8!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['personel_id']))
					{
						$personel_id = $userdata[0]['personel_id'];
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank9!";
						$this->response($returndata);
					}
					
					if(!empty($userdata[0]['dob']))
					{
						$dob = explode("-",$userdata[0]['dob']);
						$year = $dob[0];
						$month = $dob[1];
						$day = $dob[2];
						
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Please complete your profile before adding bank10!";
						$this->response($returndata);
					}
					
				/* $imgUrl = base_url('assets/img/game_img/003.jpg');
				$filePath = strstr($imgUrl, '/wp-content');
				$fp = fopen($filePath, 'r');
					echo $filePath;
					die(); */
					require_once APPPATH."third_party/stripe/init.php";
					//$token= $this->input->post('stripeToken');
					$stripe = array(
						"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
						"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
					);
					\Stripe\Stripe::setApiKey($stripe['secret_key']);
					
					/* $retrieve_account =\Stripe\Account::retrieve(
									'acct_1IivSvPDV8CdzDmT',
									'person_4IivSv00PhaLgqzG',
									[]
								); */
					/* $stripe = new \Stripe\StripeClient(
					  'sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP'
					);
					$stripe->accounts->retrieve(
					  'acct_1IivSvPDV8CdzDmT',
					  []
					); */
					
					/* $stripe = new \Stripe\StripeClient(
					  'sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP'
					);
					$stripe->accounts->allCapabilities(
					  'acct_1IivSvPDV8CdzDmT',
					  []
					); */

					//https://api.stripe.com/v1/accounts/acct_1F9X6KAgvgEeUMyl \

						
					    /* $url = 'https://api.stripe.com/v1/accounts/acct_1F9X6KAgvgEeUMyl';
					    $secret_key = 'sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP';
					    $fields = array();
						$headers = array('Authorization: Bearer '. $secret_key);
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						$output = curl_exec($ch);
						curl_close($ch);
						var_dump(json_decode($output, true)); // return php array with api response */
						
					/* $stripe = new \Stripe\StripeClient(
					  'sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP'
					);
					$res = $stripe->accounts->allExternalAccounts(
					  'acct_1F9X6KAgvgEeUMyl',
					  ['object' => 'bank_account', 'limit' => 3]
					); */
						/* echo "<pre>";
						print_r($res);
						die(); */
						
					
					$account = \Stripe\Token::create([
					'bank_account' => [
					'country' => 'US',
					'currency' => 'usd',
					'account_holder_name' => $account_holder_name,
					'account_holder_type' => $account_holder_type,
					'routing_number' => $routing_number,
					'account_number' => $account_number,
					/* 'account_holder_name' => 'Jenny Rosen',
						'account_holder_type' => 'individual',
						'routing_number' => '110000000',
						'account_number' => '000123456789', */
					],
					]);
					
					if($account->id){
						
						$bank_result = \Stripe\Account::create(array(
						"type" => "custom",
						"country" => "US",
						"email" => $email,
						"business_type"=>"individual",
						"requested_capabilities"=> ['card_payments', 'transfers'],
						"external_account" => $account->id,
						'tos_acceptance' => ['date' => time(),'ip' => $_SERVER['REMOTE_ADDR']],
						'individual' =>['id_number' => $personel_id,'phone' => $phone,'email' => $email,'first_name' => $first_name, 'last_name' =>$last_name,'ssn_last_4' => $ssn_last_4, 'dob' => ['day' => $day, 'month' => $month, 'year' => $year], 'address' => ['city' => $city, 'country' => $country_code, 'line1' => $street, 'postal_code' =>$zip, 'state' => $state]],
						'business_profile' =>['url' => "https://caenex.com",'mcc' => "5818"],
						));
						
						if($bank_result->id)
						{
							$stripe_account_id = $bank_result->id;
							$person_id = $bank_result->individual->id;
							$stripe_account_status = $bank_result->status;
							
							/* $imgUrl = 'assets/img/game_img/003.jpg';
							$imgUrl1 = 'assets/img/game_img/004.jpg'; */
							//$img_arr = array("assets/img/game_img/003.jpg","assets/img/game_img/004.jpg");
							//$filePath = get_home_path() . strstr($imgUrl, '/wp-content');
							$doc_arr =array();
							$get_user_ids = $this->Api_modal->curd_read_user_ids($userid);
							if(!empty($get_user_ids))
							{
								foreach($get_user_ids as $img)
								{
									if($img['type']==1)
									{
										$fp = fopen($img, 'r');
										$upload_identity= \Stripe\File::create([
										  'purpose' => 'identity_document',
										  'file' => $fp,
										], [
										  'stripe_account' => $stripe_account_id,
										]);
										if($upload_identity->id)
										{
											$update_bank =\Stripe\Account::updatePerson(
											  $stripe_account_id,
											  $person_id,
											  [
												'verification' => [
												  'document' => [
													'front' => $upload_identity->id,
												  ],
												],
											  ]
											);
										
										}
									}else{
										
										$fp = fopen($img, 'r');
										$upload_identity= \Stripe\File::create([
										  'purpose' => 'identity_document',
										  'file' => $fp,
										], [
										  'stripe_account' => $stripe_account_id,
										]);
										if($upload_identity->id)
										{
											$update_bank =\Stripe\Account::updatePerson(
											  $stripe_account_id,
											  $person_id,
											  [
												'verification' => [
												  'document' => [
													'back' => $upload_identity->id,
												  ],
												],
											  ]
											);
										
										}
										
									}
								}
							}
							
							/* echo "<pre>";
							print_r($doc_arr);
							die(); */
							
							/* $personel_id = \Stripe\Customer::create(
							  ["email" => "Test_yes2@yopmail.com"],
							  ["stripe_account" => $stripe_account_id]
							);
							if($personel_id->id)
							{
								
							}
							echo "<pre>";
							print_r($personel_id);
							die(); */
							
							$history_id = $this->Api_modal->crud_read_user_bank($userid,$account_number);
							if(count($history_id) >0)
							{
								$returndata['status'] = false;
								$returndata['message'] = "Bank details already exists";
								
							}else{
							
								$this->Api_modal->crud_create_user_bank(array(
									'userid' => $userid,
									'account_holder_name' => $account_holder_name,
									'account_holder_type' => $account_holder_type,
									'routing_number' => $routing_number,
									'account_number' => $account_number,
									'status' => $stripe_account_status,
									'stripe_account_id' => $stripe_account_id,
									'person_id' => $person_id,
									'create_date_time' => $create_date_time
								));
								$returndata['status'] = true;
								$returndata['message'] = "Bank details saved successfully!";
							}
						}else{
							$returndata['status'] = false;
							$returndata['message'] = "There is problem in adding a bank , kindly correct your bank details and try again.";
						}
					}else{
							$returndata['status'] = false;
							$returndata['message'] = "There is problem in adding a bank , kindly correct your bank details and try again.";
					}
					
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "SSN last 4 digits can not be empty!";
					}
					
					
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Account number can not be empty!";
					}
					
					
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Account holder type can not be empty!";
					}
					
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "Account holder name can not be empty!";
					}
					
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "Routing number must be at least 9 number !";
				}
				
			}else{
				$returndata['status'] = false;
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->response($returndata);
	}
	
	
	public function withdraw_money_post()
	{
		$returndata = array();
		$headers = $this->input->request_headers(); 
		$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
		$userid = $decodedToken['data']->user_id;
		$amount = $this->input->post("amount");
		$user_bank_id = $this->input->post("user_bank_id");
		$created_date = date('Y-m-d H:i:s');
		$transaction_date = date('Y-m-d');
		$transaction_time = date('H:i:s');
		$bytes = random_bytes(19);
		$txid = bin2hex($bytes);
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$history_id = $this->Api_modal->crud_read_user_bank_by_id($userid,$user_bank_id);
				if(count($history_id) >0)
				{
							
					$wallet_data = $this->Api_modal->crud_read_wallet($userid);
					if(count($wallet_data) > 0)
					{
						if($wallet_data[0]['total_avail_balance'] >= 50)
						{
							require_once APPPATH."third_party/stripe/init.php";
							//$token= $this->input->post('stripeToken');
							$stripe = array(
								"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
								"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
							);
							\Stripe\Stripe::setApiKey($stripe['secret_key']);
							
							/* // for add balance
							\Stripe\Charge::create(array(
							'currency' => 'USD',
							'amount'   => 99999900,
							//'card'     => 4000000000000077
							'source' => 'tok_bypassPending'
						)); */


							$transfer = \Stripe\Transfer::create([ //Transfer
							"amount" => $amount * 100,
							"currency" => "usd",
							//"destination" => $history_id[0]['stripe_account_id'],
							"destination" => "acct_1IgrXHPIj2sJImuC",
							"transfer_group" => "registry pay",
							]);
							\Stripe\Stripe::setApiKey($stripe['secret_key']);

							$payout = \Stripe\Payout::create([ //payout
							'amount' => $amount * 100,
							'currency' => 'usd',
							],
							//['stripe_account' => $history_id[0]['stripe_account_id'],
							['stripe_account' => "acct_1IgrXHPIj2sJImuC",
							]);
							$payout = $payout->jsonSerialize();
							//echo $payout;
							//print_r($payout);
							//die();
							
							$res_id = $this->Api_modal->create_user_payout(array(
							'user_id' => $userid,
							'bank_id' => $user_bank_id,
							'withdraw_amount' => $amount,
							'transaction_number' => $payout['balance_transaction'],
							'created_date' => date('Y-m-d H:i:s'),
							'status' => 'succeeded'
							));
						
							$total_avail_balance = $wallet_data[0]['total_avail_balance'] - $amount;
							$total_credit_balance = 0;
							$total_withdraw_balance = $amount; 
							
							$wallet_id = $this->Api_modal->update_wallet(array(
								'total_avail_balance' => $total_avail_balance,
								'total_credit_balance' => $total_credit_balance,
								'total_withdraw_balance' => $total_withdraw_balance,
								'created_date' => $created_date
							),$wallet_data[0]['wallet_id']);
							
							/* $check_token = $this->Api_modal->read_device_info($userid);
							if(!empty($check_token))
							{
								foreach($check_token as $user_token)
								{
									$token = $user_token['fcm_token'];
									$msg = "You have successfully withdraw $" .$amount." from your wallet!";
									$title = "Wallet Withdraw";
									$this->sendGCM($token,$msg,$title);
								}
								
							} */
							
							
							$returndata['status'] = true;
							$returndata['message'] = "You have successfully withdraw $" .$amount." from your wallet!";
								
							$wallet_data1 = $this->Api_modal->crud_read_wallet($userid);
							if(!empty($wallet_data1))
							{
								$returndata['wallet_data'] = $wallet_data1;
								
							}else{
								$returndata['wallet_data'] = array();
							}
				
						}else{
							$returndata['status'] = false;
							$returndata['message'] = "You dont have enough balance to withdraw!";
						}
					}else{
						$returndata['status'] = false;
						$returndata['message'] = "You dont have enough balance to withdraw!";
					}
				
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "Bank details not match !";
				}
			}else{
					$returndata['status'] = false;
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->response($returndata);
	}
	
	public function StripeNotification()
	{
		$payload = @file_get_contents('php://input');
		$event = null;
		require_once APPPATH."third_party/stripe/init.php";
		//$token= $this->input->post('stripeToken');
		$stripe = array(
			"secret_key"      => "sk_test_51F9X6KAgvgEeUMylRNXJoFfuZXdRvl01mnAFmAam6bmgMjA8oeSmuOX1fgZs4ktLEUsapVvnFjuyHe5oOLwOkP6V00pIZxS4rP",
			"publishable_key" => "pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt"
		);
		\Stripe\Stripe::setApiKey($stripe['secret_key']);
		try {
			$event = \Stripe\Event::constructFrom(
				json_decode($payload, true)
			);
		} catch(\UnexpectedValueException $e) {
			// Invalid payload
			http_response_code(400);
			exit();
		}

		// Handle the event
		switch ($event->type) {
			case 'account.updated':
				$paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent
				// Then define and call a method to handle the successful payment intent.
				// handlePaymentIntentSucceeded($paymentIntent);
				$save_logs =$this->Api_modal->save_response($paymentIntent);
				break;
			case 'capability.updated':
				$paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
				// Then define and call a method to handle the successful attachment of a PaymentMethod.
				// handlePaymentMethodAttached($paymentMethod);
				$save_logs =$this->Api_modal->save_response($paymentIntent);
				break;
			case 'payout.paid':
				$paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
				// Then define and call a method to handle the successful attachment of a PaymentMethod.
				// handlePaymentMethodAttached($paymentMethod);
				$save_logs =$this->Api_modal->save_response($paymentIntent);
				break;
			case 'payout.canceled':
				$paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
				// Then define and call a method to handle the successful attachment of a PaymentMethod.
				// handlePaymentMethodAttached($paymentMethod);
				$save_logs =$this->Api_modal->save_response($paymentIntent);
				break;
			case 'payout.created':
				$paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
				// Then define and call a method to handle the successful attachment of a PaymentMethod.
				// handlePaymentMethodAttached($paymentMethod);
				$save_logs =$this->Api_modal->save_response($paymentIntent);
				break;
			case 'payout.updated':
				$paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
				// Then define and call a method to handle the successful attachment of a PaymentMethod.
				// handlePaymentMethodAttached($paymentMethod);
				$save_logs =$this->Api_modal->save_response($paymentIntent);
				break;
			case 'payout.failed':
				$paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
				// Then define and call a method to handle the successful attachment of a PaymentMethod.
				// handlePaymentMethodAttached($paymentMethod);
				$save_logs =$this->Api_modal->save_response($paymentIntent);
				break;
			// ... handle other event types
			default:
				echo 'Received unknown event type ' . $event->type;
		}

		http_response_code(200);
	}
}
?>