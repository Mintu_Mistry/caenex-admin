<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	/**
	 Caenex Api controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_modal');
        //$this->load->library('twilio');
		//$this->load->library('email');
      	$this->load->helper('directory');
		
	}
	private function apiResponse($data)
	{
		header('Content-Type: application/json');
		echo json_encode($data);
	}
	
	private function generateOTP()
	{
		$otp = "";
      	$generator = "1357902468";
      	for ($i = 1; $i <= 4; $i++) { 
	        $otp .= substr($generator, (rand()%(strlen($generator))), 1); 
	    } 
	    return $otp;
	}

	public function index()
	{
		$returndata = array(
			"message" => "Caenex Api controller"
		);
		return $this->respond($returndata,200);
	}

	private function sendTwilioSMS($text,$to)
	{
		try{
			$twilio = new Client($this->TWILIO_SID, $this->TWILIO_TOKEN);
	        return $twilio->messages->create(
			    $to,
			    [
			        'from' => '+12673949525',
			        'body' => $text
			    ]
			);
		}
		catch(Exception $e){
			return 0;
		}
		finally {
			return 0;
		}
	}
	
	/* private function sendMail($to,$subject,$message)
	{
		mail($to,$subject,$message,array("from" => "no-reply@karaoke.yesitlabs.xyz"));
	} */
	
	
	public function sign_up()
	{
		$returndata = array();
		$name = $this->input->post("name");
		$mobile = $this->input->post("mobile");
		$email = $this->input->post("email");
		$password = $this->input->post("password");
		$fcm_token = $this->input->post('fcm_token');
		$Cdate = date('Y-m-d H:i:s');
		if((!empty($mobile) || !empty($email)) && !empty($password) && !empty($name))
		{
			$check_email = $this->Api_modal->check_email_exists($email);
			
			if($check_email > 0)
			{
				$user_id = $check_email;
			}else{
					$user_id = $this->Api_modal->create_user(array(
						'name' => $name,
						'email' => $email,
						'phone' => $mobile,
						'password' => $password,
						'fcmtoken' => $fcm_token,
						'status' => 1,
						'create_date' => $Cdate
					));
				
			}
				$returndata['status'] = "success";
				$returndata['user_id'] = $user_id;
				$returndata['message'] = "User registered successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function login()
	{
		$returndata = array();
		$email = $this->input->post("email");
		$password = $this->input->post("password");
		$fcm_token = $this->input->post('fcm_token');
		if((!empty($email)) && !empty($password))
		{
			$userdata = $this->Api_modal->login_by_mail($email,$password);
			if(count($userdata) < 1){
				$returndata['status'] = "success";
	        	$returndata['message'] = "Invalid User!";
			}
			else{
				if($userdata[0]['status'] == 0){
					$returndata['status'] = "success";
		        	$returndata['message'] = "User is inactive";
				}
				else{
					$this->Api_modal->update_user(array('fcmtoken'=> $fcm_token), $userdata[0]['id']);
					$returndata['status'] = "success";
					$returndata['user_id'] = $userdata[0]['id'];
		        	$returndata['message'] = "Logged in successfully!";
				}
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function forgot_password()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$email = $this->input->post("email");
		$mobile = $this->input->post("mobile");
		if((!empty($userid)) && !empty($email))
		{
			$userdata = $this->Api_modal->read_by_mail_or_mobile($email,$mobile);
			if(count($userdata) < 1){
				$returndata['status'] = "success";
	        	$returndata['message'] = "Invalid User!";
			}
			else{
				$otp = $this->generateOTP();
                $message = 'Your Verification OTP Is :-'.$otp;
                if(!empty($email)){
                	//$send_status = $this->sendMail($email, "Email Verification", $message);
					/* $send_status = $this->email
							->from('no-reply@karaoke.yesitlabs.xyz')
							->to($email)
							->subject('Email Verification')
							->message($message)
							->send();

						var_dump($send_status);
						echo '<br />';
						echo $this->email->print_debugger();

						exit; */
					
	                /* if ($send_status) {
	                    $returndata['status'] = "success";  
	                    $returndata['userid'] = $userdata[0]['id'];  
	                    $returndata['otp'] = $otp;                  
	                    $returndata['message'] = 'OTP is sent on provided email!' ;
	                } else {                               
	                    $returndata['status'] = "fail";                   
	                    $returndata['message'] = 'Sorry, there is some internal issue, please try again';
	                } */
					
						$returndata['status'] = "success";  
	                    $returndata['userid'] = $userdata[0]['id'];  
	                    $returndata['otp'] = $otp;                  
	                    $returndata['message'] = 'OTP is sent on provided email!' ;
                }
                elseif(!empty($mobile)){
                	//$result = $this->sendTwilioSMS($message, $mobile);
                	//print_r($result);
                	$returndata['status'] = "success";
					$returndata['userid'] = $userdata[0]['id'];  
                    $returndata['otp'] = $otp;                  
                    $returndata['message'] = 'OTP is sent on provided mobile number!' ;
                }
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function change_password()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$new_pass = $this->input->post("new_pass");
		if(!empty($userid) || !empty($new_pass))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if($userdata > 0)
			{
				$user_id = $this->Api_modal->update_user(array(
						'password' => $new_pass ),$userid);
						
				$returndata['status'] = "success";
				$returndata['user_id'] = $userid;
				$returndata['message'] = "Password changed successfully!";
			}else{
				
				$returndata['status'] = "error";
				$returndata['message'] = "User not exists!";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function verify_email_mobile()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$email = $this->input->post("email");
		$mobile = $this->input->post("mobile");
		if((!empty($userid)) && !empty($email))
		{
			$userdata = $this->Api_modal->read_by_mail_or_mobile($email,$mobile);
			if(count($userdata) < 1){
				$returndata['status'] = "success";
	        	$returndata['message'] = "Invalid User!";
			}
			else{
				$otp = $this->generateOTP();
                $message = 'Your Verification OTP Is :-'.$otp;
                if(!empty($email)){
                	//$send_status = $this->sendMail($email, "Email Verification", $message);
					/* $send_status = $this->email
							->from('no-reply@karaoke.yesitlabs.xyz')
							->to($email)
							->subject('Email Verification')
							->message($message)
							->send();

						var_dump($send_status);
						echo '<br />';
						echo $this->email->print_debugger();

						exit; */

	                /* if ($send_status) {
	                    $returndata['status'] = "success";  
	                    $returndata['userid'] = $userdata[0]['id'];  
	                    $returndata['otp'] = $otp;                  
	                    $returndata['message'] = 'OTP is sent on provided email!' ;
	                } else {                               
	                    $returndata['status'] = "fail";                   
	                    $returndata['message'] = 'Sorry, there is some internal issue, please try again';
	                } */
					$returndata['status'] = "success";  
					$returndata['userid'] = $userdata[0]['id'];  
					$returndata['otp'] = $otp;                  
					$returndata['message'] = 'OTP is sent on provided email!' ;
                }
                elseif(!empty($mobile)){
                	//$result = $this->sendTwilioSMS($message, $mobile);
                	//print_r($result);
                	$returndata['status'] = "success";
					$returndata['userid'] = $userdata[0]['id'];  
                    $returndata['otp'] = $otp;                  
                    $returndata['message'] = 'OTP is sent on provided mobile number!' ;
                }
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function update_profile()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$name = $this->input->post("name");
		$country = $this->input->post("country");
		$state = $this->input->post("state");
		$city = $this->input->post("city");
		$zip = $this->input->post("zip");
		$street_address = $this->input->post("street_address");
		$pin = $this->input->post("pin");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$user_id = $this->Api_modal->update_user(array(
						'name' => $name,
						'country' => $country,
						'state' => $state,
						'city' => $city,
						'zip' => $zip,
						'street' => $street_address,
						'pin' => $pin
					),$userid);
				$returndata['status'] = "success";
				$returndata['user_id'] = $userid;
				$returndata['message'] = "User profile updated successfully!";
			}else{
					
				$returndata['status'] = "error";
				$returndata['message'] = "User not found!";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function payment_method()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$card_number = $this->input->post("card_number");
		$card_holder_name = $this->input->post("card_holder_name");
		$expirydate = $this->input->post("expirydate");
		$cvv = $this->input->post("cvv");
		$nickname = $this->input->post("nickname");
		$create_date = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$payment_data = $this->Api_modal->crud_read_payment_method($userid);
				if(count($payment_data) > 0)
				{
					//echo "Test1";
					/* echo "<pre>";
					print_r($userdata);
					die(); */
					$user_id = $this->Api_modal->update_payment_method(array(
							'cardnumber' => $card_number,
							'cardholdername' => $card_holder_name,
							'expirydate' => $expirydate,
							'cvv' => $cvv,
							'nickname' => $nickname,
							'create_date' => $create_date
						),$userid);
					
				}else{
						
					$user_id = $this->Api_modal->add_payment_method(array(
							'userid' => $userid,
							'cardnumber' => $card_number,
							'cardholdername' => $card_holder_name,
							'expirydate' => $expirydate,
							'cvv' => $cvv,
							'nickname' => $nickname,
							'create_date' => $create_date
						));
				}
			
				$returndata['status'] = "success";
				$returndata['user_id'] = $userid;
				$returndata['message'] = "Payment method added successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function user_selection()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$selected = $this->input->post("selected");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$selection_data = $this->Api_modal->crud_read_selected_method($userid);
				if(count($selection_data) > 0)
				{
					//echo "Test1";
					/* echo "<pre>";
					print_r($userdata);
					die(); */
					$user_id = $this->Api_modal->update_selected_method(array(
							'userid' => $userid,
							'selected' => $selected
						),$userid);
					
				}else{
						
					$user_id = $this->Api_modal->add_selected_method(array(
							'userid' => $userid,
							'selected' => $selected
						));
				}
			
				$returndata['status'] = "success";
				$returndata['user_id'] = $userid;
				$returndata['message'] = "User selected method added successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function pay_request_money()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$amount = $this->input->post("amount");
		$friend_contact_number = $this->input->post("friend_contact_number");
		$payment_type = $this->input->post("payment_type");
		$transaction_id = $this->input->post("transaction_id");
		$create_date = date('Y-m-d');
		$create_time = date('H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			/* user_transaction_id	user_id	amount	friend_contact_number	payment_type	user_status	friend_status	transaction_id	request_date	request_time */
			if(count($userdata) > 0)
			{
				$user_id = $this->Api_modal->crud_create_user_transaction(array(
							'user_id' => $userid,
							'amount' => $amount,
							'friend_contact_number' => $friend_contact_number,
							'payment_type' => $payment_type,
							'user_status' => 1,
							'friend_status' => 0,
							'transaction_id' => $transaction_id,
							'request_date' => $create_date,
							'request_time' => $create_time
						));
						$returndata['status'] = "success";
						if($payment_type == 1)
						{
							$returndata['message'] = "Your have rquested money successfully!";
						}else{
							$returndata['message'] = "Your have paid money successfully!";
						}
				
				
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function recent_activity()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			/* user_transaction_id	user_id	amount	friend_contact_number	payment_type	user_status	friend_status	transaction_id	request_date	request_time */
			if(count($userdata) > 0)
			{
				$recent_activity = $this->Api_modal->crud_read_user_transaction($userid);
				$returndata['status'] = "success";
				$returndata['recent_activity'] = $recent_activity;
				$returndata['message'] = "Recent transaction fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function recent_payment_contact()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			/* user_transaction_id	user_id	amount	friend_contact_number	payment_type	user_status	friend_status	transaction_id	request_date	request_time */
			if(count($userdata) > 0)
			{
				$recent_transaction = $this->Api_modal->crud_read_user_transaction($userid);
				$returndata['status'] = "success";
				$returndata['recent_transaction'] = $recent_transaction;
				$returndata['message'] = "Recent payment contact fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function sender_receiver_payment_details()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$user_transaction_id = $this->input->post("user_transaction_id");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			/* user_transaction_id	user_id	amount	friend_contact_number	payment_type	user_status	friend_status	transaction_id	request_date	request_time */
			if(count($userdata) > 0)
			{
				$payment_details = $this->Api_modal->get_user_transaction_by_transaction_id($user_transaction_id);
				$returndata['status'] = "success";
				$returndata['payment_details'] = $payment_details;
				$returndata['message'] = "Payment_details fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function contact_list()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			$all_user_arr =array();
			if(count($userdata) > 0)
			{
				$all_users = $this->Api_modal->crud_read();
				if(count($all_users) > 0)
				{
					/* echo "<pre>";
					print_r($all_users);
					die(); */
					foreach($all_users as $users)
					{
						$row = array();
						$row['name'] = $users['name'];
						$row['email'] = $users['email'];
						$row['phone'] = $users['phone'];
						$all_user_arr[] = $row;
					}
					
				}else{
					$all_user_arr = array();
				}
				$returndata['status'] = "success";
				$returndata['contact_list'] = $all_user_arr;
				$returndata['message'] = "contact list fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function check_balance()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$wallet_data = $this->Api_modal->crud_read_wallet($userid);
					/* echo "<pre>";
					print_r($wallet_data);
					die(); */
				$returndata['status'] = "success";
				$returndata['wallet_data'] = $wallet_data;
				$returndata['message'] = "contact list fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
 	public function add_wallet_money()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$amount = $this->input->post("amount");
		$transactioncol_id = $this->input->post("transactioncol_id");
		$type = $this->input->post("type");
		$data_id = $this->input->post("data_id");
		$created_date = date('Y-m-d H:i:s');
		$transaction_date = date('Y-m-d');
		$transaction_time = date('H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$res_id = $this->Api_modal->add_wallet_money(array(
							'user_id' => $userid,
							'amount' => $amount,
							'transactioncol_id' => $transactioncol_id,
							'type' => $type,
							'data_id' => $data_id,
							'transaction_date' => $transaction_date,
							'transaction_time' => $transaction_time,
							'status' => 'succeeded'
						));
				$wallet_data = $this->Api_modal->crud_read_wallet($userid);
				if(count($wallet_data) > 0)
				{
					foreach($wallet_data as $wallet)
					{
						$total_avail_balance = $wallet['total_avail_balance'] + $amount;
						$total_credit_balance = $wallet['total_credit_balance'] + $amount;
						$last_added_amount = $amount; 
						
						$wallet_id = $this->Api_modal->update_wallet(array(
							'total_avail_balance' => $total_avail_balance,
							'total_credit_balance' => $total_credit_balance,
							'last_added_amount' => $last_added_amount,
							'created_date' => $created_date
						),$wallet['wallet_id']);
					}
				}else{
					$wallet_id = $this->Api_modal->create_wallet(array(
							'user_id' => $userid,
							'total_avail_balance' => $amount,
							'total_credit_balance' => $amount,
							'total_withdraw_balance' => 0,
							'last_added_amount' => $amount,
							'created_date' => $created_date
						),$wallet['wallet_id']);
				}
					/* echo "<pre>";
					print_r($wallet_data);
					die(); */
				$wallet_data = $this->Api_modal->crud_read_wallet($userid);
				$returndata['status'] = "success";
				$returndata['wallet_data'] = $wallet_data;
				$returndata['message'] = "Wallet data fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function search_contact()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$contact_number = $this->input->post("contact_number");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			$searched_arr =array();
			if(count($userdata) > 0)
			{
				$searched_data = $this->Api_modal->read_by_mail_or_mobile('',$contact_number);
				if(count($searched_data) > 0)
				{
					/* echo "<pre>";
					print_r($searched_data);
					die(); */
					foreach($searched_data as $users)
					{
						$row = array();
						$row['name'] = $users['name'];
						$row['email'] = $users['email'];
						$row['phone'] = $users['phone'];
						$searched_arr[] = $row;
					}
					
				}else{
					$searched_arr = array();
				}
				$returndata['status'] = "success";
				$returndata['search_result'] = $searched_arr;
				$returndata['message'] = "contact list fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function confirm_pin()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$pin = $this->input->post("pin");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			$searched_arr =array();
			if(count($userdata) > 0)
			{
				if($userdata[0]['pin'] == $pin)
				{
					$returndata['status'] = "success";
					$returndata['message'] = "Pin Matched";
				}else{
					$returndata['status'] = "error";
					$returndata['message'] = "Pin not match";
				}
				
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function change_pin()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$oldpin = $this->input->post("oldpin");
		$newpin = $this->input->post("newpin");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				if($userdata[0]['pin'] == $oldpin)
				{
					$user_id = $this->Api_modal->update_user(array(
							'pin' => $newpin),$userid);
						
					$returndata['status'] = "success";
					$returndata['message'] = "Pin changed successfully";
				}else{
					$returndata['status'] = "error";
					$returndata['message'] = "Old Pin not match";
				}
				
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function save_card()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$card_number = $this->input->post("card_number");
		$card_holder_name = $this->input->post("card_holder_name");
		$expirydate = $this->input->post("expirydate");
		$cvv = $this->input->post("cvv");
		$nickname = $this->input->post("nickname");
		$create_date = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$payment_data = $this->Api_modal->crud_read_payment_method($userid);
				if(count($payment_data) > 0)
				{
					//echo "Test1";
					/* echo "<pre>";
					print_r($userdata);
					die(); */
					$user_id = $this->Api_modal->update_payment_method(array(
							'cardnumber' => $card_number,
							'cardholdername' => $card_holder_name,
							'expirydate' => $expirydate,
							'cvv' => $cvv,
							'nickname' => $nickname,
							'create_date' => $create_date
						),$userid);
					
				}else{
						
					$user_id = $this->Api_modal->add_payment_method(array(
							'userid' => $userid,
							'cardnumber' => $card_number,
							'cardholdername' => $card_holder_name,
							'expirydate' => $expirydate,
							'cvv' => $cvv,
							'nickname' => $nickname,
							'create_date' => $create_date
						));
				}
			
				$returndata['status'] = "success";
				$returndata['user_id'] = $userid;
				$returndata['message'] = "Card details added successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_recent_transaction()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				
				$transaction_data = $this->Api_modal->crud_read_transaction($userid);
				if(count($transaction_data) > 0)
				{
					$returndata['status'] = "success";
					$returndata['recent_transaction'] = $transaction_data;
					$returndata['message'] = "Transaction data fetched successfully!";
				}else{
					$returndata['status'] = "error";
					$returndata['message'] = "Data not found !";
				}
			}else{
					$returndata['status'] = "error";
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function filter_transaction()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$from_date = $this->input->post("from_date");
		$to_date = $this->input->post("to_date");
		$type = $this->input->post("type");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$transaction_data = $this->Api_modal->crud_filter_transaction($userid,$from_date,$to_date,$type);
				if(count($transaction_data) > 0)
				{
					$returndata['status'] = "success";
					$returndata['search_result'] = $transaction_data;
					$returndata['message'] = "Transaction data fetched successfully!";
				}else{
					$returndata['status'] = "error";
					$returndata['message'] = "Data not found !";
				}
			}else{
					$returndata['status'] = "error";
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_pending_request()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$transaction_data = $this->Api_modal->crud_read_pending_transaction($userid);
				if(count($transaction_data) > 0)
				{
					$returndata['status'] = "success";
					$returndata['pending_request'] = $transaction_data;
					$returndata['message'] = "Transaction data fetched successfully!";
				}else{
					$returndata['status'] = "error";
					$returndata['message'] = "Data not found !";
				}
			}else{
					$returndata['status'] = "error";
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function withdraw_money()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$amount = $this->input->post("amount");
		$transactioncol_id = $this->input->post("transactioncol_id");
		$type = $this->input->post("type");
		$data_id = $this->input->post("data_id");
		$created_date = date('Y-m-d H:i:s');
		$transaction_date = date('Y-m-d');
		$transaction_time = date('H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$res_id = $this->Api_modal->add_wallet_money(array(
							'user_id' => $userid,
							'amount' => $amount,
							'transactioncol_id' => $transactioncol_id,
							'type' => $type,
							'data_id' => $data_id,
							'transaction_date' => $transaction_date,
							'transaction_time' => $transaction_time,
							'status' => 'succeeded'
						));
				$wallet_data = $this->Api_modal->crud_read_wallet($userid);
				if(count($wallet_data) > 0)
				{
					foreach($wallet_data as $wallet)
					{
						$total_avail_balance = $wallet['total_avail_balance'] - $amount;
						$total_credit_balance = $wallet['total_credit_balance'] - $amount;
						$total_withdraw_balance = $amount; 
						
						$wallet_id = $this->Api_modal->update_wallet(array(
							'total_avail_balance' => $total_avail_balance,
							'total_credit_balance' => $total_credit_balance,
							'total_withdraw_balance' => $total_withdraw_balance,
							'created_date' => $created_date
						),$wallet['wallet_id']);
					}
				}
				$wallet_data = $this->Api_modal->crud_read_wallet($userid);
				$returndata['status'] = "success";
				$returndata['wallet_data'] = $wallet_data;
				$returndata['message'] = "Wallet data fetched successfully!";
				
			}else{
					$returndata['status'] = "error";
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function check_username_availability()
	{
		$returndata = array();
		$username = $this->input->post("username");
		if(!empty($username))
		{
			$userdata = $this->Api_modal->check_username($username);
			if(count($userdata) > 0)
			{
				$returndata['status'] = "error";
				$returndata['message'] = "Username already taken";
				
			}else{
				$returndata['status'] = "success";
				$returndata['message'] = "Username available!";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_merchant_details()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$merchant_id = $this->input->post("merchant_id");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$merchant_data = $this->Api_modal->crud_read_merchant($merchant_id);
				$returndata['merchant_data'] = $merchant_data;
				$returndata['status'] = "success";
				$returndata['message'] = "Merchant data fetched successfully!";
				
			}else{
				$returndata['status'] = "success";
				$returndata['message'] = "Data not found!";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function id_verification()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$ssn = $this->input->post("ssn");
		$created_date = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$uploaddir = './assets/img/photos/';
				$path = $_FILES['frontid']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				$user_img = time() . rand() . '.' . $ext;
				$uploadfile = $uploaddir . $user_img;
				if ($_FILES["frontid"]["name"]) {
					if (move_uploaded_file($_FILES["frontid"]["tmp_name"],$uploadfile)) {
					$frontid = $user_img;
					}else{
						$frontid = '';
					}
				}
				
				$res_id = $this->Api_modal->add_verification_ids(array(
						'merchant_id' => $userid,
						'verification_id' => 1,
						'img' => $frontid,
						'type' => 1
					));
					
				$path1 = $_FILES['backid']['name'];
				$ext1 = pathinfo($path, PATHINFO_EXTENSION);
				$user_img1 = time() . rand() . '.' . $ext1;
				$uploadfile1 = $uploaddir . $user_img1;
				if ($_FILES["backid"]["name"]) {
					if (move_uploaded_file($_FILES["backid"]["tmp_name"],$uploadfile1)) {
					$backid = $user_img1;
					}else{
						$backid = '';
					}
				}
				$res_id = $this->Api_modal->add_verification_ids(array(
						'merchant_id' => $userid,
						'verification_id' => 2,
						'img' => $backid,
						'type' => 2
					));
					
				$path2 = $_FILES['selfiewithid']['name'];
				$ext2 = pathinfo($path, PATHINFO_EXTENSION);
				$user_img2 = time() . rand() . '.' . $ext2;
				$uploadfile2 = $uploaddir . $user_img2;
				if ($_FILES["selfiewithid"]["name"]) {
					if (move_uploaded_file($_FILES["selfiewithid"]["tmp_name"],$uploadfile2)) {
					$selfiewithid = $user_img2;
					}else{
						$selfiewithid = '';
					}
				}
				$res_id = $this->Api_modal->add_verification_ids(array(
						'merchant_id' => $userid,
						'verification_id' => 3,
						'img' => $selfiewithid,
						'type' => 3
					));
					
				$returndata['status'] = "success";
				$returndata['message'] = "Verification Ids uploaded successfully!";
				
			}else{
					$returndata['status'] = "error";
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function delete_account()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$user_id = $this->Api_modal->update_user(array(
							'delete_status' => 1
						),$userid);
						
				$returndata['status'] = "success";
				$returndata['message'] = "User account deleted successfully!";
				
			}else{
					$returndata['status'] = "error";
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function about_us()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$returndata['about_us'] = $this->Api_modal->crud_read_about_us();
				$returndata['status'] = "success";
				$returndata['message'] = "About us fetched successfully!";
				
			}else{
					$returndata['status'] = "error";
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function privacy_policy()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$returndata['privacy_policy'] = $this->Api_modal->crud_read_privacy_policy();
				$returndata['status'] = "success";
				$returndata['message'] = "Privacy policy fetched successfully!";
				
			}else{
					$returndata['status'] = "error";
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function terms_and_conditions()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				//$res_id = $this->Api_modal->crud_read_tandc();
				$returndata['terms_and_conditions'] = $this->Api_modal->crud_read_tandc();
				$returndata['status'] = "success";
				$returndata['message'] = "Terms & conditions fetched successfully!";
				
			}else{
					$returndata['status'] = "error";
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function contact_support()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$email = $this->input->post("email");
		$msg = $this->input->post("msg");
		$comment = $this->input->post("comment");
		$create_date = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			
			if(count($userdata) > 0)
			{
				$res_id = $this->Api_modal->crud_create_contact_support(array(
							'userid' => $userid,
							'email' => $email,
							'msg' => $msg,
							'comment' => $comment,
							'create_date' => $create_date
						));
				$returndata['status'] = "success";
				$returndata['message'] = "Contact query added successfully!";
				
			}else{
					$returndata['status'] = "error";
					$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_merchant_rewards()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$lat = $this->input->post("lat");
		$long = $this->input->post("long");
		
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$coupon_data = $this->Api_modal->crud_read_coupons($lat,$long);
				$cash_data = $this->Api_modal->crud_read_cash($lat,$long);
				
				/* echo "<pre>";
				print_r($wallet_data);
				die(); */
				$returndata['status'] = "success";
				$returndata['coupons'] = $coupon_data;
				$returndata['cash'] = $cash_data;
				$returndata['message'] = "Rewards fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_quiz_questions()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$category = $this->input->post("category");
		
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$category_data = $this->Api_modal->crud_read_question_category($category);
				$ques_arr = array();
				if(count($category_data) > 0)
				{
					foreach($category_data as $cat)
					{
						$cat['questions_data'] = $this->Api_modal->crud_read_questions($cat['category']);
						$ques_arr[] = $cat;
						
					}
				}
				/* echo "<pre>";
				print_r($wallet_data);
				die(); */
				$returndata['status'] = "success";
				$returndata['questions'] = $ques_arr;
				$returndata['message'] = "Questions fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function save_rewards()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$coupon_id = $this->input->post("coupon_id");
		$cash_id = $this->input->post("cash_id");
		$reward_type = $this->input->post("reward_type");
		$reward_status = $this->input->post("reward_status");
		$create_date_time = date('Y-m-d H:i:s');
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$history_id = $this->Api_modal->crud_create_user_reward_history(array('userid' => $userid,
						'coupon_id' => $coupon_id,
						'cash_id' => $cash_id,
						'reward_type' => $reward_type,
						'reward_status' => $reward_status,
						'create_date_time' => $create_date_time
					));
				$returndata['status'] = "success";
				$returndata['message'] = "Rewards saved successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_reward_list()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$reward_type = $this->input->post("reward_type");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$reward_list = $this->Api_modal->crud_read_user_reward_history($userid,$reward_type);
				$returndata['status'] = "success";
				$returndata['reward_list'] = $reward_list;
				$returndata['message'] = "Rewards fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function search_coupon()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$coupon_name = $this->input->post("coupon_name");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$coupon_list = $this->Api_modal->crud_read_filter_coupon($coupon_name);
				$returndata['status'] = "success";
				$returndata['coupon_details'] = $coupon_list;
				$returndata['message'] = "Coupon fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function redeem_coupon()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$reward_id = $this->input->post("reward_id");
		$reward_status = $this->input->post("reward_status");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$res = $this->Api_modal->crud_redeem_coupon(array(
						'reward_status' => $reward_status
					),$reward_id);
				$returndata['status'] = "success";
				$returndata['message'] = "Coupon redeem successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_coupon_list()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$coupon_list = $this->Api_modal->crud_read_coupon($userid);
				$returndata['status'] = "success";
				$returndata['coupon_details'] = $coupon_list;
				$returndata['message'] = "Coupon fetched successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function notification_setting()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$notification_status = $this->input->post("notification_status");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$user_id = $this->Api_modal->update_user(array(
							'notification_status' => 1
						),$userid);
						
				$returndata['status'] = "success";
				$returndata['message'] = "Setting changed successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function get_notification()
	{
		$returndata = array();
		$userid = $this->input->post("userid");
		$notification_status = $this->input->post("notification_status");
		if(!empty($userid))
		{
			$userdata = $this->Api_modal->crud_read($userid);
			if(count($userdata) > 0)
			{
				$user_id = $this->Api_modal->update_user(array(
							'notification_status' => 1
						),$userid);
						
				$returndata['status'] = "success";
				$returndata['message'] = "Setting changed successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "User not found !";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
}
?>