<?php
defined('BASEPATH') or die ('Not Allow To Access');

class Api_modal extends CI_Model
{
	
	public function check_email_exists($email)
	{	
		$this->db->select('*');
		$this->db->from('tbl_user');  	
		$this->db->where("email",$email);
		$query=$this->db->get();
		$res = $query->row_array();
		if(!empty($res))
		{
			return $res['id'];
		}else{
			return 0;
		}
	}
	
	public function check_mobile_exists($mobile)
	{	
		$this->db->select('*');
		$this->db->from('tbl_user');  	
		$this->db->where("phone",$mobile);
		$query=$this->db->get();
		$res = $query->row_array();
		if(!empty($res))
		{
			return $res['id'];
		}else{
			return 0;
		}
	}
	
	public function create_user($data){
		$check=$this->db->insert('tbl_user',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function login_by_mail($email,$password)
	{	
		$this->db->select('*');
		$this->db->from('tbl_user');  	
		$this->db->where("email",$email);
		$this->db->where("password",$password);
		$query=$this->db->get();
		return $query->result_array();
	}
	
	public function read_by_mail_or_mobile($email ='',$mobile ='')
	{	
		if($email !='' && $mobile !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_user');  	
			$this->db->where("email",$email);
			$this->db->or_where("phone",$mobile);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			
			if($email !='')
			{
				$this->db->select('*');
				$this->db->from('tbl_user');  	
				$this->db->where("email",$email);
				$query=$this->db->get();
				return $query->result_array();
			}else if($mobile !='')
			{
				$this->db->select('*');
				$this->db->from('tbl_user');  	
				$this->db->where("phone",$mobile);
				$query=$this->db->get();
				return $query->result_array();
			}
		}
	}
	
	
	public function crud_read_merchant_by_mobile($mobile ='')
	{	
		if($mobile !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_merchants');
			$this->db->or_where("merchant_phone",$mobile);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_merchants');
			//$this->db->or_where("merchant_phone",$mobile);
			$query=$this->db->get();
			return $query->result_array();

		}
	}
	
	public function crud_read_merchant_by_email($email ='')
	{	
		if($email !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_merchants');
			$this->db->or_where("merchant_email",$email);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_merchants');
			//$this->db->or_where("merchant_email",$mobile);
			$query=$this->db->get();
			return $query->result_array();

		}
	}
	
	public function crud_read($userid ='')
	{	
		if($userid !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_user');  	
			$this->db->where("id",$userid);
			$query=$this->db->get();
			return $query->result_array();
			
		}else{
			$this->db->select('*');
			$this->db->from('tbl_user');
			$this->db->order_by('id','DESC');			
			$query=$this->db->get();
			return $query->result_array();
		}
	}
	
	public function check_username($username)
	{	
		$this->db->select('*');
		$this->db->from('tbl_user');  	
		$this->db->where("username",$username);
		$query=$this->db->get();
		return $query->result_array();
	}
	
	public function read_device_info($userid,$fcm_token ='')
	{	
		if($fcm_token !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_device_info');  	
			$this->db->where("userid",$userid);
			$this->db->where("fcm_token",$fcm_token);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_device_info');  	
			$this->db->where("userid",$userid);
			$query=$this->db->get();
			return $query->result_array();
		}
	}
	
	public function create_device_info($data){
		$check=$this->db->insert('tbl_device_info',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function update_device_info($data,$device_info_id)
	{	
		$this->db->where("device_info_id",$device_info_id);
		$this->db->update('tbl_device_info',$data);
		return $this->db->insert_id();
	}
	
	public function update_user($data,$id)
	{	
		$this->db->where("id",$id);
		$this->db->update('tbl_user',$data);
		return $this->db->insert_id();
	}
	
	public function crud_read_merchant($merchant_id)
	{	
		$this->db->select('*');
		$this->db->from('tbl_merchants');  	
		$this->db->where("merchant_id",$merchant_id);
		$query=$this->db->get();
		return $query->result_array();
	}
	
	public function crud_read_payment_method($userid,$cardnumber = '')
	{	
		if($cardnumber == '')
		{
			$this->db->select('*');
			$this->db->from('tbl_paymentmethods');  	
			$this->db->where("userid",$userid);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_paymentmethods');  	
			$this->db->where("userid",$userid);
			$this->db->where("cardnumber",$cardnumber);
			$query=$this->db->get();
			return $query->result_array();
		}
	}
	
	public function crud_read_delete_card($userid,$cardnumber){
		
		$this->db->where("userid",$userid);
		$this->db->where("cardnumber",$cardnumber);
		$this->db->delete('tbl_paymentmethods');
		return true;
	}
	
	public function add_payment_method($data){
		$check=$this->db->insert('tbl_paymentmethods',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function update_payment_method($data,$userid)
	{	
		$this->db->where("userid",$userid);
		$this->db->update('tbl_paymentmethods',$data);
		return $this->db->insert_id();
	}
	
	
	public function crud_read_selected_method($userid)
	{	
		$this->db->select('*');
		$this->db->from('tbl_user_selection');  	
		$this->db->where("userid",$userid);
		$query=$this->db->get();
		return $query->result_array();
	}
	
	public function add_selected_method($data){
		$check=$this->db->insert('tbl_user_selection',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function update_selected_method($data,$userid)
	{	
		$this->db->where("userid",$userid);
		$this->db->update('tbl_user_selection',$data);
		return $this->db->insert_id();
	}
	
	public function crud_create_user_transaction($data){
		$check=$this->db->insert('tbl_user_transaction',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function crud_read_user_transaction($userid,$minrange ='',$maxrange ='')
	{	
		if($minrange !='' && $maxrange !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			$this->db->where("user_id",$userid);
			//$this->db->or_where("friend_id",$userid);
			$this->db->where("transaction_status",1);
			$this->db->order_by('user_transaction_id','DESC');
			$this->db->limit($maxrange,$minrange);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			$this->db->where("user_id",$userid);
			//$this->db->or_where("friend_id",$userid);
			$this->db->where("transaction_status",1);
			$this->db->order_by('user_transaction_id','DESC');
			//$this->db->limit(10,0);
			$query=$this->db->get();
			return $query->result_array();
		}
	}
	
	
	public function crud_read_friend_transaction($userid,$minrange ='',$maxrange ='')
	{	
		if($minrange !='' && $maxrange !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			$this->db->where("payment_type!=",3);
			$this->db->where("friend_id",$userid);
			$this->db->where("transaction_status",1);
			$this->db->order_by('user_transaction_id','DESC');
			$this->db->limit($maxrange,$minrange);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			//$this->db->where("user_id",$userid);
			$this->db->where("friend_id",$userid);
			$this->db->where("transaction_status",1);
			$this->db->order_by('user_transaction_id','DESC');
			//$this->db->limit(10,0);
			$query=$this->db->get();
			return $query->result_array();
		}
	}
	
	
	public function crud_update_user_request($data,$user_transaction_id)
	{	
		$this->db->where("user_transaction_id",$user_transaction_id);
		$this->db->update('tbl_user_transaction',$data);
		return $this->db->insert_id();
	}
	
	public function get_user_transaction_by_transaction_id($user_transaction_id,$userid ='')
	{	
		if($userid !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			$this->db->where("user_transaction_id",$user_transaction_id);
			$this->db->where("user_id",$userid);
			//$this->db->order_by('user_transaction_id','DESC');
			//$this->db->limit(10,0);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			$this->db->where("user_transaction_id",$user_transaction_id);
			//$this->db->order_by('user_transaction_id','DESC');
			//$this->db->limit(10,0);
			$query=$this->db->get();
			return $query->result_array();
		}
		
	}
	
	public function get_friend_transaction_by_transaction_id($user_transaction_id,$friend_id ='')
	{	
		if($friend_id !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			$this->db->where("user_transaction_id",$user_transaction_id);
			$this->db->where("friend_id",$friend_id);
			//$this->db->order_by('user_transaction_id','DESC');
			//$this->db->limit(10,0);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			$this->db->where("user_transaction_id",$user_transaction_id);
			//$this->db->order_by('user_transaction_id','DESC');
			//$this->db->limit(10,0);
			$query=$this->db->get();
			return $query->result_array();
		}
		
	}
	
	public function crud_read_wallet($userid ='')
	{	
		if($userid !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_wallet');  	
			$this->db->where("user_type",1);
			$this->db->where("user_id",$userid);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_wallet');  	
			$this->db->order_by('wallet_id','DESC');
			$query=$this->db->get();
			return $query->result_array();
		}
		
	}
	
	public function crud_read_merchant_wallet($userid ='')
	{	
		if($userid !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_wallet');  	
			$this->db->where("user_type",2);
			$this->db->where("user_id",$userid);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_wallet');  	
			$this->db->order_by('wallet_id','DESC');
			$query=$this->db->get();
			return $query->result_array();
		}
		
	}
	
	
	public function add_wallet_money($data){
		$check=$this->db->insert('tbl_transaction',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function create_wallet($data){
		$check=$this->db->insert('tbl_wallet',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function update_wallet($data,$wallet_id)
	{	
		$this->db->where("wallet_id",$wallet_id);
		$this->db->update('tbl_wallet',$data);
		return $this->db->insert_id();
	}
	
	public function crud_read_transaction($userid ='')
	{	
	
		if($userid !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_transaction');  	
			$this->db->where("user_id",$userid);
			$this->db->order_by('id','DESC');
			$this->db->limit(1,0);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_transaction');  	
			$this->db->order_by('id','DESC');
			$query=$this->db->get();
			return $query->result_array();
		}
		
	}
	
	
	
	public function crud_filter_transaction($userid,$from_date,$to_date,$type,$minrange ='',$maxrange ='')
	{	
		if($minrange !='' && $maxrange !='')
		{	
			if($userid !='')
			{
				$this->db->select('*');
				$this->db->from('tbl_transaction');  	
				$this->db->where("user_id",$userid);
				$this->db->where("transaction_date >=",$from_date);
				$this->db->where("transaction_date <=",$to_date);
				$this->db->where("type",$type);
				$this->db->order_by('id','DESC');
				$this->db->limit($maxrange,$minrange);
				$query=$this->db->get();
				return $query->result_array();
			}
		}else{
			if($userid !='')
			{
				$this->db->select('*');
				$this->db->from('tbl_transaction');  	
				$this->db->where("user_id",$userid);
				$this->db->where("transaction_date >=",$from_date);
				$this->db->where("transaction_date <=",$to_date);
				$this->db->where("type",$type);
				$this->db->order_by('id','DESC');
				$query=$this->db->get();
				return $query->result_array();
			}
		}
		
	}
	
	public function crud_read_pending_transaction($userid)
	{	
		$this->db->select('*');
		$this->db->from('tbl_transaction');  	
		$this->db->where("user_id",$userid);
		$this->db->where("type",1);
		$this->db->order_by('id','DESC');
		//$this->db->limit(1,0);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function crud_read_pending_request($userid,$minrange ='',$maxrange ='')
	{	
		if($minrange !='' && $maxrange !='')
		{	
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			$this->db->where("friend_id",$userid);
			$this->db->where("transaction_status!=",1);
			$this->db->where("friend_status!=",2);
			$this->db->where("payment_type",1);
			$this->db->order_by('user_transaction_id','DESC');
			$this->db->limit($maxrange,$minrange);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			$this->db->where("friend_id",$userid);
			$this->db->where("transaction_status!=",1);
			//$this->db->where("user_status!=",1);
			$this->db->where("friend_status!=",2);
			$this->db->where("payment_type",1);
			$this->db->order_by('user_transaction_id','DESC');
			//$this->db->limit(10,0);
			$query=$this->db->get();
			return $query->result_array();
		}
		
	}
	
	public function crud_read_requested_transaction($userid,$minrange ='',$maxrange ='')
	{	
		if($minrange !='' && $maxrange !='')
		{	
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			$this->db->where("user_id",$userid);
			$this->db->where("user_status!=",2);
			$this->db->where("transaction_status!=",1);
			//$this->db->where("friend_status!=",2);
			$this->db->where("payment_type",1);
			$this->db->order_by('user_transaction_id','DESC');
			$this->db->limit($maxrange,$minrange);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_user_transaction');  	
			$this->db->where("user_id",$userid);
			$this->db->where("user_status!=",2);
			$this->db->where("transaction_status!=",1);
			//$this->db->where("friend_status!=",2);
			$this->db->where("payment_type",1);
			$this->db->order_by('user_transaction_id','DESC');
			//$this->db->limit(10,0);
			$query=$this->db->get();
			return $query->result_array();
		}
		
	}
	
	
	public function crud_create_contact_support($data){
		$check=$this->db->insert('tbl_contact_support',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function crud_read_about_us()
	{	
		$this->db->select('*');
		$this->db->from('tbl_content');  	
		$this->db->where("id",5);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	
	public function crud_read_privacy_policy()
	{	
		$this->db->select('*');
		$this->db->from('tbl_content');  	
		$this->db->where("id",3);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function crud_read_tandc()
	{	
		$this->db->select('*');
		$this->db->from('tbl_content');  	
		$this->db->where("id",1);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function add_verification($data){
		$check=$this->db->insert('tbl_id_verification1',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function add_verification_ids($data){
		$check=$this->db->insert('tbl_id_verification',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function crud_read_verification($userid)
	{	
		$this->db->select('*');
		$this->db->from('tbl_id_verification1');  	
		$this->db->where("merchant_id",$userid);
		$this->db->order_by('id','DESC');
		$this->db->limit(1,0);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function crud_read_coupons($lat,$long,$minrange ='',$maxrange ='',$merchant_id ='')
	{	
		if($minrange !='' && $maxrange !='')
		{
			$min_range = '0';
			$max_range = '50';
			$unit = 'miles';
			
			if($unit == 'meter'){
				$earth_radius = '6371000';
			}
			else{
				$earth_radius = '3958.755866';
			}
			if($merchant_id =='')
			{
				$this->db->select("*, ( $earth_radius * acos ( cos ( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin ( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance");
				$this->db->from("tbl_nexworld_coupon"); 				
				$this->db->having("distance >= $min_range AND distance <= $max_range AND add_status=1 AND status=1");
				$this->db->limit($maxrange,$minrange);
				$query = $this->db->get();
				return $query->result_array();
			}else{
				$this->db->select("*, ( $earth_radius * acos ( cos ( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin ( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance");
				$this->db->from("tbl_nexworld_coupon");				
				$this->db->having("distance >= $min_range AND distance <= $max_range AND add_status=1 AND status=1 AND merchant_id=$merchant_id ");
				$this->db->limit($maxrange,$minrange);
				$query = $this->db->get();
				return $query->result_array();
			}
		}else{
			$min_range = '0';
			$max_range = '50';
			$unit = 'miles';
			
			if($unit == 'meter'){
				$earth_radius = '6371000';
			}
			else{
				$earth_radius = '3958.755866';
			}
			if($merchant_id =='')
			{
				$this->db->select("*, ( $earth_radius * acos ( cos ( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin ( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance");
				$this->db->from("tbl_nexworld_coupon");  	
				$this->db->having("distance >= $min_range AND distance <= $max_range AND add_status=1 AND status=1");
				$query = $this->db->get();
				return $query->result_array();
			}else{
				$this->db->select("*, ( $earth_radius * acos ( cos ( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin ( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance");
				$this->db->from("tbl_nexworld_coupon");  	
				$this->db->having("distance >= $min_range AND distance <= $max_range AND add_status=1 AND status=1 AND merchant_id=$merchant_id");
				$query = $this->db->get();
				return $query->result_array();

			}
		}
		
	}
	
	public function crud_read_cash($lat,$long,$minrange ='',$maxrange ='')
	{	
		if($minrange !='' && $maxrange !='')
		{
			$min_range = '0';
			$max_range = '50';
			$unit = 'miles';
			
			if($unit == 'meter'){
				$earth_radius = '6371000';
			}
			else{
				$earth_radius = '3958.755866';
			}
			$this->db->select("*, ( $earth_radius * acos ( cos ( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin ( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance");
			$this->db->from("tbl_nexworld_cash");  	
			$this->db->having("distance >= $min_range AND distance <= $max_range AND add_status=1 AND status=1");
			$this->db->limit($maxrange,$minrange);
			$query = $this->db->get();
			return $query->result_array();
		}else{
			$min_range = '0';
			$max_range = '50';
			$unit = 'miles';
			
			if($unit == 'meter'){
				$earth_radius = '6371000';
			}
			else{
				$earth_radius = '3958.755866';
			}
			$this->db->select("*, ( $earth_radius * acos ( cos ( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin ( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance");
			$this->db->from("tbl_nexworld_cash");  	
			$this->db->having("distance >= $min_range AND distance <= $max_range AND add_status=1 AND status=1");
			$query = $this->db->get();
			return $query->result_array();
		}
		
	}
	
	
	public function crud_read_coupon_by_id($coupon_id)
	{
		$this->db->select("*");
		$this->db->from("tbl_nexworld_coupon");
		$this->db->where("id",$coupon_id);
		$query = $this->db->get();
		return $query->result_array();
		
	}
	
	public function crud_read_cash_by_id($coupon_id)
	{
		$this->db->select("*");
		$this->db->from("tbl_nexworld_cash");
		$this->db->where("id",$coupon_id);
		$query = $this->db->get();
		return $query->result_array();
		
	}
	
	
	
	public function crud_read_question_category($category)
	{	
		$this->db->select('*');
		$this->db->from('tbl_question_cat');  	
		$this->db->where("category",$category);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function crud_read_questions($category_name)
	{	
		$this->db->select('*');
		$this->db->from('tbl_question');  	
		$this->db->where("category",$category_name);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	public function crud_create_user_reward_history($data){
		$check=$this->db->insert('tbl_user_reward_history',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function crud_read_user_reward_history($userid,$reward_type,$minrange ='',$maxrange ='')
	{	
		if($minrange !='' && $maxrange !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_user_reward_history');  	
			$this->db->where("userid",$userid);
			$this->db->where("reward_type",$reward_type);
			$this->db->limit($maxrange,$minrange);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_user_reward_history');  	
			$this->db->where("userid",$userid);
			$this->db->where("reward_type",$reward_type);
			$query=$this->db->get();
			return $query->result_array();
		}
		
	}
	
	public function crud_read_reward_by_id($userid,$coupon_id)
	{	

		$this->db->select('*');
		$this->db->from('tbl_user_reward_history');  	
		$this->db->where("userid",$userid);
		$this->db->where("coupon_id",$coupon_id);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function crud_read_filter_coupon($coupon_name)
	{	
		$this->db->select('*');
		$this->db->from('tbl_nexworld_coupon');  	
		$this->db->where("coupon_title",$coupon_name);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function crud_redeem_coupon($data,$reward_id)
	{	
		$this->db->where("reward_history_id",$reward_id);
		$this->db->update('tbl_user_reward_history',$data);
		return $this->db->insert_id();
	}
	
	public function crud_read_coupon($userid,$min_range ='',$max_range ='')
	{	
		if($min_range !='' && $max_range !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_user_reward_history');  	
			$this->db->where("userid",$userid);
			$this->db->limit($max_range,$min_range);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_user_reward_history');  	
			$this->db->where("userid",$userid);
			$this->db->limit(10,0);
			$query=$this->db->get();
			return $query->result_array();
		}
	}
	
	/* ----Nexplay game queries starts here*/
	
	public function read_nexplay_games()
	{	
		$this->db->select('*');
		$this->db->from('tbl_game');  	
		$this->db->where("status",1);
		$this->db->where("delete_status",1);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function read_campaign_started_games($game_id)
	{	
		$this->db->select('*');
		$this->db->from('tbl_nexplay_caenex_game');  	
		//$this->db->where("status",1);
		$this->db->where("add_status",1);
		$this->db->where("gameid",$game_id);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	
	
	public function read_quiz_category($gameid)
	{	
		$this->db->select('*');
		$this->db->from('tbl_category');  	
		$this->db->where("delete_status",1);
		$this->db->where("gameid",$gameid);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function read_quiz_questions($gameid,$catid='',$id='')
	{	
		if($id =='')
		{
			if($catid=='')
			{
				$this->db->select('*');
				$this->db->from('tbl_question');  	
				$this->db->where("delete_status",1);
				$this->db->where("gameid",$gameid);
				$query=$this->db->get();
				return $query->result_array();
			}else{
				$this->db->select('*');
				$this->db->from('tbl_question');  	
				$this->db->where("delete_status",1);
				$this->db->where("gameid",$gameid);
				$this->db->where("category",$catid);
				$query=$this->db->get();
				return $query->result_array();
			}
			
		}else{
			
			$this->db->select('*');
			$this->db->from('tbl_question');
			$this->db->where("id",$id);
			$this->db->where("gameid",$gameid);
			$this->db->where("category",$catid);
			$query=$this->db->get();
			return $query->result_array();
		}
		
	}
	
	
	public function read_quiz_set_questions($gameid,$catid,$last_played_ques='')
	{	
		if($last_played_ques=='')
		{
			$this->db->select('*');
			$this->db->from('tbl_question');  	
			$this->db->where("delete_status",1);
			$this->db->where("gameid",$gameid);
			$this->db->where("category",$catid);
			//$this->db->order_by("id",'ASC');
			$this->db->limit(3,0);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_question');  	
			$this->db->where("delete_status",1);
			$this->db->where("gameid",$gameid);
			$this->db->where("category",$catid);
			$this->db->where("id >",$last_played_ques);
			$this->db->limit(3,0);
			$query=$this->db->get();
			return $query->result_array();
		}
	}
	
	public function delete_quiz_submit($gameid,$catid,$userid)
	{
		
		$this->db->where("gameid",$gameid);
		$this->db->where("catid",$catid);
		$this->db->where("userid",$userid);
		$this->db->delete('tbl_nexplay_quiz_submit');
		return true;
	}
	
	public function crud_quiz_submit($data)
	{
		$check=$this->db->insert('tbl_nexplay_quiz_submit',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function read_quiz_result_by_user($gameid,$catid,$userid)
	{	
	//quiz_result_id	gameid	catid	userid	tscore	win	loose	create_date
	
		$this->db->select('*');
		$this->db->from('tbl_nexplay_quiz_result');  	
		//$this->db->where("win",1);
		$this->db->where("gameid",$gameid);
		$this->db->where("catid",$catid);
		$this->db->where("userid",$userid);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function crud_read_user_answer($gameid,$catid,$userid)
	{	
	//quiz_result_id	gameid	catid	userid	tscore	win	loose	create_date
	
		$this->db->select('*');
		$this->db->from('tbl_nexplay_quiz_submit');
		$this->db->where("gameid",$gameid);
		$this->db->where("catid",$catid);
		$this->db->where("userid",$userid);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function crud_create_quiz_result($data,$gameid,$catid,$userid){
		$this->db->where("gameid",$gameid);
		$this->db->where("catid",$catid);
		$this->db->where("userid",$userid);
		$this->db->delete('tbl_nexplay_quiz_result');
		
		$check=$this->db->insert('tbl_nexplay_quiz_result',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	
	public function crud_update_quiz_result($data,$quiz_result_id)
	{	
		$this->db->where("quiz_result_id",$quiz_result_id);
		$this->db->update('tbl_nexplay_quiz_result',$data);
		return $this->db->insert_id();
	}
	
	public function crud_read_nexplay($campaign_id)
	{
		$this->db->select('*');
		$this->db->from('tbl_nexplay_caenex_game');
		$this->db->where("id",$campaign_id);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function update_nexplay($data,$campaign_id)
	{	
		$this->db->where("id",$campaign_id);
		$this->db->update('tbl_nexplay_caenex_game',$data);
		return $this->db->insert_id();
	}
	
	
	public function read_nexplay_campaign($gameid)
	{	
		$today = date('Y-m-d');
		$date = str_replace("'","",$today);
		//echo $date;
		//die();
		//$this->db->select("* FROM tbl_nexplay_caenex_game WHERE start_date >=$today AND delete_status =1 AND add_status =1 AND gameid=" .$gameid." order_by merchant_id DESC LIMIT 1 ");
		$this->db->select('*');
		$this->db->from('tbl_nexplay_caenex_game');  	
		$this->db->where('end_date >=',$today);
		$this->db->where('delete_status',1);
		$this->db->where('gameid',$gameid);
		$this->db->where('add_status',1);
		$this->db->where('status',1);
		$this->db->limit(1,0);
		$this->db->order_by('merchant_id','DESC');
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	/* ----Nexplay game queries ends here*/
	
	function get_country()
	{
		$this->db->select('*');
		$this->db->from('countries');  	
		$query=$this->db->get();
		return $query->result_array();
	}
	
	function get_state($country)
	{
		$this->db->select('*');
		$this->db->from('states');  	
		$this->db->where("country_id",$country);
		$query=$this->db->get();
		return $query->result_array();
	}
	
	function get_city($state)
	{
		$this->db->select('*');
		$this->db->from('cities');
		$this->db->where("state_id",$state);
		$query=$this->db->get();
		return $query->result_array();
	}
	
	public function create_webview_link($data)
	{
		$check=$this->db->insert('tbl_webview_payment',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function crud_create_user_bank($data)
	{
		$check=$this->db->insert('tbl_user_bank',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	function crud_read_user_bank($userid,$account_number='')
	{
		
		if($account_number=='')
		{
			
			$this->db->select('*');
			$this->db->from('tbl_user_bank');
			$this->db->where("userid",$userid);
			$query=$this->db->get();
			return $query->result_array();
			
		}else{
			
			$this->db->select('*');
			$this->db->from('tbl_user_bank');
			$this->db->where("userid",$userid);
			$this->db->where("account_number",$account_number);
			$query=$this->db->get();
			return $query->result_array();
		}
	}
	
	
	function crud_read_user_bank_by_id($userid,$user_bank_id='')
	{
		
		if($user_bank_id=='')
		{
			
			$this->db->select('*');
			$this->db->from('tbl_user_bank');
			$this->db->where("userid",$userid);
			$query=$this->db->get();
			return $query->result_array();
			
		}else{
			
			$this->db->select('*');
			$this->db->from('tbl_user_bank');
			$this->db->where("userid",$userid);
			$this->db->where("user_bank_id",$user_bank_id);
			$query=$this->db->get();
			return $query->result_array();
		}
	}
	
	function create_user_payout($data){
		$check=$this->db->insert('tbl_user_payout_history',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	
	public function crud_read_saved_coupon($userid,$coupon_id)
	{	
		$this->db->select('*');
		$this->db->from('tbl_user_reward_history');  	
		$this->db->where("userid",$userid);
		$this->db->where("coupon_id",$coupon_id);
		$this->db->where("reward_type",1);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function crud_read_saved_cash($userid,$coupon_id)
	{	
		$this->db->select('*');
		$this->db->from('tbl_user_reward_history');  	
		$this->db->where("userid",$userid);
		$this->db->where("coupon_id",$coupon_id);
		$this->db->where("reward_type",2);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function crud_read_coupon_id($userid,$coupon_id)
	{	
		$this->db->select('*');
		$this->db->from('tbl_user_reward_history');  	
		$this->db->where("userid",$userid);
		$this->db->where("coupon_id",$coupon_id);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	
	
	public function crud_read_nexplay_wallet($userid ='')
	{	
		if($userid !='')
		{
			$this->db->select('*');
			$this->db->from('tbl_nexplay_wallet');  	
			$this->db->where("user_id",$userid);
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_nexplay_wallet');  	
			$this->db->order_by('user_wallet_id','DESC');
			$query=$this->db->get();
			return $query->result_array();
		}
		
	}
	
	public function update_nexplay_wallet($data,$wallet_id)
	{	
		$this->db->where("user_wallet_id",$wallet_id);
		$this->db->update('tbl_nexplay_wallet',$data);
		return $this->db->insert_id();
	}
	
	public function create_nexplay_wallet($data)
	{
		$check=$this->db->insert('tbl_nexplay_wallet',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function curd_read_user_ids($userid)
	{	
		$this->db->select('*');
		$this->db->from('tbl_id_verification');  	
		$this->db->where("merchant_id",$userid);
		$this->db->where("user_type",1);
		$this->db->where_in("type",array(1,2));
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function curd_read_user_all_ids($verification_id)
	{	
		$this->db->select('*');
		$this->db->from('tbl_id_verification');  	
		$this->db->where("verification_id",$verification_id);
		$this->db->where("user_type",1);
		$query=$this->db->get();
		return $query->result_array();
		
	}
	
	public function save_response($data)
	{
		$check=$this->db->insert('stripe_response',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function read_otp_by_mail_or_mobile($email ='',$mobile ='')
	{	
		if($email !='')
		{
			$this->db->select('*');
			$this->db->from('otp_tbl');  	
			$this->db->where("email",$email);
			$query=$this->db->get();
			return $query->result_array();
		}else if($mobile !='')
		{
			$this->db->select('*');
			$this->db->from('otp_tbl');  	
			$this->db->where("phone",$mobile);
			$query=$this->db->get();
			return $query->result_array();
		}
	}
	
	
	public function update_user_otp($data,$userid)
	{	
		$this->db->where("id",$userid);
		$this->db->update('otp_tbl',$data);
		return $this->db->insert_id();
	}
	
	public function create_user_otp($data,$email ='',$mobile ='')
	{
		if($email !='')
		{
			$this->db->where("email",$email);
			$this->db->delete('otp_tbl');
		}else if($mobile !='')
		{
			$this->db->where("phone",$mobile);
			$this->db->delete('otp_tbl');
		}
		
		$check=$this->db->insert('otp_tbl',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function create_user_play_history($data)
	{
		$check=$this->db->insert('user_played_ques',$data);
		if($check){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function read_user_play_history($userid,$gameid,$catid)
	{	
		$this->db->select('*');
		$this->db->from('user_played_ques');  	
		$this->db->where("userid",$userid);
		$this->db->where("gameid",$gameid);
		$this->db->where("catid",$catid);
		$this->db->order_by('played_id','DESC');
		$this->db->limit(1,0);
		$query=$this->db->get();
		return $query->result_array();
	}
}