<?php
defined('BASEPATH') or die ('Not Allow To Access');

class Merchant_modal extends CI_Model
{

  public function insert($tbl,$data){
  	$check=$this->db->insert($tbl,$data);
  	if($check){
  		return $this->db->insert_id();
  	}else{
  		return false;
  	}

  }

  public function get_row1($row_data,$tbl,$email,$pass){
  	$this->db->select($row_data);
  	$this->db->from($tbl);
	$this->db->where('merchant_email',$email);   
    $this->db->or_where('merchant_phone',$email);   
    $this->db->where('user_password',$pass);
    $this->db->where('status','1');
  	$query=$this->db->get();
  	return $query->row_array();

  }
  
  public function get_row($row_data,$tbl,$con,$order_by=null){
  	$this->db->select($row_data);
  	$this->db->from($tbl);  	
  	$this->db->where($con);  	
    
  	$query=$this->db->get();
  	return $query->row_array();

  }
  
   public function get_game_name($row_data,$tbl,$con,$order_by=null){
  	$this->db->select($row_data);
  	$this->db->from($tbl);  	
  	$this->db->where($con);  	
    
  	$query=$this->db->get();
  	$ad = $query->row_array();
	return $ad['game_name'];
  }
  
  public function get_merchant_revenue($merchant_id ,$from ='',$to = '')
  {
	if($from !='' && $to !='')
	{
		$this->db->select('SUM(amount) as total_revenue');
		$this->db->from('tbl_user_transaction');  	
		$this->db->where('merchant_id',$merchant_id);  	
		$this->db->where('request_date >=',$from);  	
		$this->db->where('request_date <=',$to);  	
		$query=$this->db->get();
		return $query->row_array();
		
	}else{
		$this->db->select('SUM(amount) as total_revenue');
		$this->db->from('tbl_user_transaction');  	
		$this->db->where('merchant_id',$merchant_id);  	
		$query=$this->db->get();
		return $query->row_array();
	}

  }
	
	public function get_user_wallet($userid,$user_type='')
	{
		if($user_type=='')
		{
			$this->db->select('*');
			$this->db->from('tbl_wallet');  	
			$this->db->where('user_id',$userid);  	 	
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('tbl_wallet');  	
			$this->db->where('user_type',$user_type);  	 	
			$this->db->where('user_id',$userid);  	 	
			$query=$this->db->get();
			return $query->result_array();
		}

  }
  
   public function get_row_data($row_data,$tbl,$con=null,$order_by=null){
  	$this->db->select($row_data);
  	$this->db->from($tbl);  	
    if (!empty($con)) {
  	$this->db->where($con);  	
    }
   
  	$query=$this->db->get();
  	return $query->row_array();

  }

  public function login($row_data,$tbl,$email,$pass){
    $this->db->select($row_data);
    $this->db->from($tbl);
    $this->db->where('merchant_email',$email);   
    $this->db->or_where('merchant_phone',$email);   
    $this->db->where('user_password',$pass);
    $this->db->where('delete_status','1');
    $query=$this->db->get();
    return $query->row_array();

  }

  public function update($tbl,$data,$con){
  	$this->db->where($con);
  	$this->db->update($tbl,$data);
  	return true;
  }

  public function get_all_data($row_data,$tbl,$con=null,$order_by=null,$colum=null,$start=null,$end=null)
  {
    $this->db->select($row_data);
    $this->db->from($tbl);  
     if (!empty($con)) {  
    $this->db->where($con); 
    }  
    if (!empty($order_by)) {
    $this->db->order_by($colum,$order_by);
    }
    if ( $end ) {
     $this->db->limit($end,$start);  
    }
   // $this->db->order_by("id", "DESC");
    $query=$this->db->get();
    return $query->result_array();
  }
	
	public function filter_merchant_data($row_data,$tbl,$con=null,$order_by=null,$colum=null,$start=null,$end=null)
	{
		$this->db->select($row_data);
		$this->db->from($tbl);
		//$this->db->where('create_date >=',$from);	//,$from,$to	
		//$this->db->where('create_date <=',$to);		
		 if (!empty($con)) {  
		$this->db->where($con); 
		}  
		if (!empty($order_by)) {
		$this->db->order_by($colum,$order_by);
		}
		if ( $end ) {
		 $this->db->limit($end,$start);  
		}
	   // $this->db->order_by("id", "DESC");
		$query=$this->db->get();
		return $query->result_array();
  }
  
  public function delete($tbl,$con){
    $this->db->where($con);
    $this->db->delete($tbl);
    return true;
  }

  public function join_data($row_data,$tbl1,$tbl2,$joincon,$con=null,$order_by=null,$colum=null){
    $this->db->select($row_data);
    $this->db->from($tbl1);
    $this->db->join($tbl2,$joincon);
    if ($con) {
      $this->db->where($con);
    }
	if (!empty($order_by)) {
		$this->db->order_by($colum,$order_by);
		}
     $query=$this->db->get();
    return $query->result_array();
  }
  public function countries(){
    $this->db->select('*');
    $this->db->from('countries');
    $query=$this->db->get();
    return $query->result_array();
  }
  public function faq($faq_data){
    $this->db->insert('tbl_faq',$faq_data);
    return true;
  }
  public function faq_select($search = null){
    $this->db->select('*');
    $this->db->from('tbl_faq');
    if($search != null) {
      $this->db->like('faq_name', $search);
      $this->db->or_like('faq_message', $search);
    }
    $query=$this->db->get();
    return $query->result_array();
  }

  /*public function faq_search_data($search){
    $this->db->select('*');
    $this->db->from('tbl_faq');
    $this->db->like('faq_name' = $search);
    $query=$this->db->get();
    return $query->result_array();
  }*/
	
	public function update_webview_payment_status($data,$webview_id)
	{	
		$this->db->where("webview_id",$webview_id);
		$this->db->update('tbl_webview_payment',$data);
		return $this->db->insert_id();
	}
	
	/* public function read_device_info($userid)
	{	
		$this->db->select('*');
		$this->db->from('tbl_device_info');  	
		$this->db->where("userid",$userid);
		$query=$this->db->get();
		return $query->result_array();
	} */
}