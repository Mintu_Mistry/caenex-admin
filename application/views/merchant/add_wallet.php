<?php
$this->load->view('merchant/include/head');
?>
<body>
    <div class="wrapper">
        <?php
            $this->load->view('merchant/include/nev');
        ?>
        <div class="main">
            <?php
                $this->load->view('merchant/include/header');
            ?>
            <style type="text/css">
                .popover .popover-content {
                    font-weight: bold;
                    color:red;
                }
            </style>
        <main class="content">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-md-12 col-xl-12">
                        <div class="card">
                            <?php
                                if ($this->session->flashdata('add')) {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="alert-message">
                                    <strong>Success!</strong> <?php echo $this->session->flashdata('add'); ?>
                                </div>
                                </div>
                            <?php
                                } 
                            ?>
                            <div class="popbox">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 col-xl-4">
                                            <form method="post" id="paymentFrm" enctype="multipart/form-data" action="<?php echo base_url('Merchant/add_wallet_payment') ?>" autocomplete="off">
                                                <div class="form-group">
                                                   <label>Total Available balance : </label>
                                                    <!-- <div class="input-group"> -->
                                                        <?php 
                                                        if(!empty($transaction[0]['total_avail_balance'])){
                                                           $balance = $transaction[0]['total_avail_balance'];
                                                        }
                                                        else {
                                                            $balance = "0";
                                                        } 
                                                        ?>
                                                        <label><b>$<?php echo $balance;?></b></label>
                                                        <input type="hidden" class="form-control" readonly="" name="total_avail_balance" value="<?php echo $balance;?>">
                                                   <!--  </div> -->
                                                </div>
                                                <div class="form-group">
                                                   <label>Enter Amount</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"name="amount" id="amount" placeholder="Enter Amount" required="" >
                                                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                                                        <?php 
                                                        if(!empty($transaction[0]['total_credit_balance'])){
                                                           $balance = $transaction[0]['total_credit_balance'];
                                                        }
                                                        else {
                                                            $balance = "0";
                                                        } 
                                                        ?>
                                                        <input type="hidden" name="added_amount" value="<?php echo $balance; ?>">
                                                    </div>
                                                </div>
                                                
                                                <div class="row text-center">
                                                    <div class="col-12 col-xl-12">
                                                        <a class="btn btn-success payment_btn" onclick="showpayment()" > Add to wallet </a>
                                                        <div class="modal fade" data-keyboard="false" data-backdrop="static" id="defaultModalPrimary12" tabindex="-1" role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog modal-lg newmodel_size" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Payment</h4>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body m-3">
                                                                        <label id="payment-errors"></label>
                                                                        <div class="card-body">
                                                                            <div class="form-row posi_relative">
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="inputEmail4" style="float: left;">Card number</label>
                                                                                    <input type="text" class="form-control" required onkeypress="return autoMask(this,event, '#### #### #### ####');"  name="card_number" id="cardnum" placeholder="Card number">
                                                                                    <!-- <div class="visabox"><img src="<?php echo base_url('assets/img/')?>visa_icon.png"></div> -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="inputEmail4" style="float: left;">Cardholder name</label>
                                                                                    <input type="text" class="form-control" id="card_name"  oninput="validate_only_alphabates_allowed(this.id)" name="card_name" placeholder="Cardholder name"  required>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-8">
                                                                                    <label for="inputEmail4" style="float: left;">Exp. date</label>
                                                                                    <!-- <div class="form-group col-md-12">
                                                                                    <input type="text" class="form-control" name="exp_date" onkeypress="return autoMask(this,event, '##');" id="expmonth" maxlength="2" placeholder="Month"  required>
                                                                                    </div> -->
                                                                                    <div class="form-group col-md-12" style="margin-left: -14px;">
                                                                                        <input type="text" class="form-control" name="exp_date" onkeypress="return autoMask(this,event, '##/####');" id="expmonth" maxlength="7" minlength="7" placeholder="Month / Year"  required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group col-md-4">
                                                                                    <label for="inputAddress2" style="float: left;">CVV</label>
                                                                                    <input type="text" class="form-control" placeholder="CVV" id="cvv" name="cvv"  onkeypress="return autoMask(this,event, '###');"  required>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-12">
                                                                                <label for="inputEmail4" style="float: left;">Card nickname(Optional)</label>
                                                                                <input type="text" class="form-control" placeholder="Card nickname(Optional)" id="Nickname" oninput="validate_only_alphabates_allowed(this.id)" name="nickname">
                                                                            </div>
                                                                        </div>                                  
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        <!-- <button class="btn btn-primary">Pay Now</button> -->
                                                                        <button id="payBtn" class="btn btn-primary">PROCEED TO PAYMENT</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<!-- <label for="from">From</label> <input type="text" id="from" name="from"/> <label for="to">to</label> <input type="text" id="to" name="to"/>
-->
<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script> -->
<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/redmond/jquery-ui.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>
<script>
    function validate_only_alphabates_allowed(input_id){
        let input_value = document.getElementById(input_id).value;
        let regExp = /^[a-z A-Z]+$/;
        if (regExp.test(input_value)) {   
        } else{
            document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
        }
    }

    function showpayment(){
        var amt = $('#amount').val();
        if (amt == "") {
            $("#amount").focus();   
            swal("Please enter amount")
        }
        else{
            showpayment_popup();
        }
    }

    function showpayment_popup(){
        $('body').addClass('modal-open');
        $('#defaultModalPrimary12').modal('toggle');
    }

    function autoMask(field, event, sMask) {
        var KeyTyped = String.fromCharCode(getKeyCode(event));
        if (getKeyCode(event) == 8) {
            return
        }
        if (getKeyCode(event) == 0) {
            return
        }
        if (field.value.length == sMask.length && getKeyCode(event) == 13) {
            return true
        }

        var targ = getTarget(event);
        keyCount = targ.value.length;
        if (keyCount == sMask.length) {
            return false
        }
        if (sMask.charAt(keyCount + 1) != "#" && sMask.charAt(keyCount + 1) != "A") {
            field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
            return false
        }
        if (sMask.charAt(keyCount) == "*")
            return true;
        if (sMask.charAt(keyCount) == KeyTyped) {
            return true
        }
        if (sMask.charAt(keyCount) == "#" && isNumeric(KeyTyped))
            return true;
        if (sMask.charAt(keyCount) == "A" && isAlpha(KeyTyped))
            return true;
        if (sMask.charAt(keyCount + 1) == "?") {
            field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
            return true
        }
        if (KeyTyped.charCodeAt(0) < 32)
            return true;
        return false
    }

    function getKeyCode(e) {
        if (e.srcElement) {
            return e.keyCode
        }
        if (e.target) {
            return e.which
        }
    }

    function getTarget(e) {
        if (e.srcElement) {
            return e.srcElement
        }
        if (e.target) {
            return e.target
        }
    }

    function isNumeric(c) {
        var sNumbers = "01234567890-";
        if (sNumbers.indexOf(c) == -1)
            return false;
        else
            return true
    }

    function isAlpha(c) {
        var sNumbers = "01234567890-";
        if (sNumbers.indexOf(c) == -1)
            return false;
        else
            return true
    }

    function delete_data(id,tbl,row){
        var result = confirm("Are You Sure");
        if (result) {
            $.post("<?php echo base_url('merchant/delete') ?>",{id:id,tbl:tbl,row:row},function(res){
                $("#saved_card"+id+"").fadeOut();
            })
        }
    }
</script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script> 
<script type="text/javascript">

    //set your publishable key
    Stripe.setPublishableKey('pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt');
    //  Stripe.setPublishableKey('pk_live_nxQI6OVkFFzTXtd6L2wcq7Kl');
    //callback to handle the response from stripe
    function stripeResponseHandler(status, response) {
        if (response.error) {
            //enable the submit button
            $('#payBtn').removeAttr("disabled");
            //display the errors on the form
            // $('#payment-errors').attr('hidden', 'false');
            $('#payment-errors').addClass('alert alert-danger');
            $("#payment-errors").html(response.error.message);
        } else {
            var form$ = $("#paymentFrm");
            //get token id
            var token = response['id'];
            //insert the token into the form
            form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
            //submit form to the server
            form$.get(0).submit();
        }
    }

    function stripeResponseHandler1(status, response) {
        if (response.error) {
        //enable the submit button
            $('#payBtn1').removeAttr("disabled");
            //display the errors on the form
            // $('#payment-errors').attr('hidden', 'false');
            $('#payment-errors1').addClass('alert alert-danger');
            $("#payment-errors1").html(response.error.message);
        } else {
            var form$ = $("#paymentFrm1");
            //get token id
            var token = response['id'];
            //insert the token into the form
            form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
            //submit form to the server
            form$.get(0).submit();
        }
    }
    $(document).ready(function () {
        //on form submit
        $("#paymentFrm").submit(function (event) {
            //disable the submit button to prevent repeated clicks
            $('#payBtn').attr("disabled", "disabled");

            //create single-use token to charge the user

            var exp_data1= $('#expmonth').val()
            var fields = exp_data1.split('/');

            var expmonth = fields[0];
            var expyear = fields[1];
            Stripe.createToken({
                number: $('#cardnum').val(),
                cvc: $('#cvv').val(),
                exp_month: expmonth,
                exp_year: expyear
            }, stripeResponseHandler);

            //submit from callback
            return false;
        });

        $("#paymentFrm1").submit(function (event) {
            //disable the submit button to prevent repeated clicks
            $('#payBtn1').attr("disabled", "disabled");

            //create single-use token to charge the user
            var exp_data= $('#expmonth1').val()
            var fields = exp_data.split('/');

            var expmonth1 = fields[0];
            var expyear1 = fields[1];


            Stripe.createToken({
                number: $('#cardnum1').val(),
                cvc: $('#cvv1').val(),
                exp_month: expmonth1,
                exp_year: expyear1
            }, stripeResponseHandler1);

            //submit from callback
            return false;
        });
    });
</script>
<script>
    $('[rel=popover]').popover({ html : true });
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });
</script>
</body>

</html>