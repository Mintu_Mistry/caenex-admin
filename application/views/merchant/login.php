<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Bootstrap 4 Admin &amp; Dashboard Template">
	<meta name="author" content="Bootlab">
	<title>Caenex</title>
	<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin="">
	<style>
		body {
			opacity: 0;
		}
	</style>
	<script src="<?php echo base_url('assets/js/settings.js')?>"></script>
	<!-- END SETTINGS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-120946860-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120946860-6');
</script></head>

<body>
	<main class="main d-flex w-100 conexbg">
		<div class="container d-flex flex-column">
			<div class="corner"><img src="<?php echo base_url('assets/img/corner.png') ?>" alt="" class="img-fluid"></div>
			<div class="corner_logo"><img src="<?php echo base_url('assets/img/corner_logo.png')?>" alt="" class="img-fluid"></div>
			<div class="row h-100 mt-small">
				<div class="col-sm-10 col-md-8 col-lg-7 mx-auto d-table h-100">

						<div class="text-center mt-4">
							<h1 class="h2 loghed">Login</h1>
						</div>

						<div class="card">
							<div class="card-body">
									<div id="loginpage">
										<form method="post" name="myForm1" onsubmit="return validateForm1()">
											<div class="form-group">
												<i class="align-middle mr-2 fas fa-fw fa-envelope"></i>
												<input class="form-control form-control-lg" type="text" name="email" placeholder="Email/Phone Number" required="required" readonly onfocus ="this.removeAttribute('readonly');">
											</div>
											<div class="form-group">
												<i class="align-middle mr-2 fas fa-fw fa-lock"></i>
												<input class="form-control form-control-lg" type="password" name="password" placeholder="Enter your password" required="required" onload="password_value()" id="password" readonly onfocus ="this.removeAttribute('readonly');">
												
												<div class="forgetp text-right"><small><a href="<?php echo base_url('Forgot'); ?>">Forgot password?</a></small></div>
											</div>
											<div class="text-center mt-3">
												<!-- <a href="#" class="btn btn-lg btn-primary logbtn"> Login</a> -->
												<button type="submit" name="submit" class="btn btn-lg btn-primary logbtn">Sign in</button>
											</div>
											<div class="singupbox"> New to Caenex?<a href="<?php echo base_url('Sign_up') ?>"> Signup </a></div>
										</form>
									</div>
							</div>
							<p  id="error_email1" style="text-align: center;color: red;font-size: 20px;">
								<?php
								if ($this->session->flashdata('incorrct')) {
									echo $this->session->flashdata('incorrct');
								}
								?>
							</p>
							<p style="text-align: center;color:green;font-size: 20px;">
								<?php
								if ($this->session->flashdata('corrct')) {
									echo $this->session->flashdata('corrct');
								}
								?>
							</p>
						</div>
						<div class="formbtext text-center">
							<h2> Do you have more than one business location?</h2>
							<h6 class="registertext"> If you are looking to advertise at multiple locations, please <u><b><a style="color: #d8e5db" href="<?php echo base_url('Sign_up') ?>"> register here </a></b></u></h6>
							<img src="<?php echo base_url('assets/') ?>img/border.jpg">
						</div>
				</div>
			</div>
		</div>
	</main>

	<script src="<?php echo base_url('assets/js/app.js') ?>"></script>

</body>
<script type="text/javascript">
	function password_value(){
			document.getElementById('password').value= '';
		}
</script>
</html>
<script type="text/javascript">
		function isNumeric(value) {
			return /^-?\d+$/.test(value);
		}
		
		function validateForm1() {
		 
		  var email = document.forms["myForm1"]["email"].value;
		  if(isNumeric(email) ==true)
		  {
			  //alert("number");
		  }else{
			  
          // let regex = /^[A-Za-z]\w{7,30}$/;
          let email_pettern=/^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/;
          let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
			if (email_pettern.test(email)) {
          	document.getElementById('error_email1').innerHTML = '';
          	return true;
         
			}else{
				document.getElementById('error_email1').innerHTML = 'Please Enter Valid Email Format';
			return false;  
			}
		 }

    
		 
			 // return false;
	}
</script>