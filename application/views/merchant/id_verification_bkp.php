<?php
$this->load->view('merchant/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('merchant/include/nev');
?>

<div class="main">
<?php
$this->load->view('merchant/include/header');
?>

			<main class="content">
				<div class="container-fluid p-0">
				<h1 class="h3 mb-3 profile_tab mar-b0">ID verification </h1>

					<?php
					if ($this->session->flashdata('add')) {
						?>									
						<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
               			<span aria-hidden="true">×</span>
              			</button>
						<div class="alert-message">
						<?php echo $this->session->flashdata('add'); ?>
						</div>
					</div>
					<?php		
					}
					?>

				<div id="payment_box" class="pt-5">
					<div class="row">
					
							<form method="post" enctype="multipart/form-data" action="<?php echo base_url('Merchant/uploade_id_verification') ?>" style="display: contents;">
						<div class="col-md-6 col-xl-6">
							<div id="idupload">
								<div class="idproof mb-4">
									<div class="form-row">
										<h3 class="had"> ID PROOF<span>*</span></h3>
										<?php
										if (!empty($id_verification)) {
											
											foreach ($id_verification as $key => $value) {
								    			if ($value['type']==1 && ($value['id_status']==1 || $value['id_status']==2)) {
								    				$required_1 = "1";
								    			}
								    			if ($value['type']==2 && ($value['id_status']==1 || $value['id_status']==2)) {
								    				$required_2 = "2";
								    			}
								    			if ($value['type']==3 && ($value['id_status']==1 || $value['id_status']==2)) {
								    				$required_3 = "3";
								    			}
								    		}
										}
										?>
								    	<div class="col-lg-6">
								    		<div class="btn upladbtn">
								    			<input type="file"  name="file1[]" class="form-control-file text-primary font-weight-bold document"  accept="image/*" id="gallery-photo-add"  data-title="Drag and drop a file" >
								    	<i class="align-middle mr-2 fas fa-fw fa-cloud-upload-alt"></i> Upload your document
								    		</div>
								    	</div>
								    	
								    	<?php
								    	
								    	if (!empty($id_verification)) {
								    	
								    	?>
								    	<div class="col-lg-6" >
								    		<div class="uploaed_imgbox gallery" >
								    			<?php
								    			foreach ($id_verification as $key => $value) {
								    			if ($value['id_status']==1 ) {
								    			$con['verification_id']=$value['id'];
												$img_data=$this->Merchant_modal->get_all_data('*','tbl_id_verification',$con);
								    				foreach ($img_data as $key => $valu) {
								    				if ($valu['type']==1) {
								    					
								    			?>
												<span class="uploadimgbox" id="idProof<?php echo $valu['id'] ?>">
													<img class="img-preview" id="img1" src="<?php echo base_url('assets/img/game_img/').$valu['img'] ?>" style="height: 46px;width: 46px;">
													<span class="crossd" onclick="DeleteImage('idProof', 'id1',<?php echo $valu['id'] ?>)">x</span>
											</span>
											<?php

								    				}

								    				}
									    	}	
									    	}
									    	?>
												
											</div>
								    	</div>
								    	<?php
								    	
								    	}else{
								    		
								    		?>
								    		<div  class="col-lg-6 gallery">
								    			
								    		</div>
								    		<!-- <div class="col-lg-6" id="img-preview" style="display: none;">
								    		<div class="uploaed_imgbox">
												<span class="uploadimgbox"><img class="img-preview" src="<?php echo base_url('assets/img/doc.png')?>" style="height: 46px;width: 46px;">
													<span class="crossd" onclick="fadeout('img-preview')">x</span>
											</span>
												
											</div>
								    	</div> -->
								    	<?php
								    	}
								    	?>
							 		</div>
							 	</div>

							 	<div class="idproof mb-4">
									<div class="form-row">
										<h3 class="had"> ADDRESS PROOF<span>*</span></h3>
								    	<div class="col-lg-6">
								    		<div class="btn upladbtn">
								    			<input type="file" name="file2[]" class="form-control-file text-primary font-weight-bold document"  accept="image/*"  src="" id="file2" data-title="Drag and drop a file" <?php if (@$required_2) {
								    				
								    			}else{
								    				echo "required";
								    			}  ?> >
								    	<i class="align-middle mr-2 fas fa-fw fa-cloud-upload-alt"></i> Upload your document
								    		</div>
								    	</div>
								    	<?php
								    	
								    	if (!empty($id_verification)) {
								    		
								    	?>
								    	<div class="col-lg-6" id="ADDRESS_PROOF">
								    		<div class="uploaed_imgbox gallery2">
												

									    	<?php
								    			foreach ($id_verification as $key => $value2) {
								    			if ($value2['id_status']==1 ) {
								    			$con1['verification_id']=$value2['id'];
												$img_data1=$this->Merchant_modal->get_all_data('*','tbl_id_verification',$con1);
								    				foreach ($img_data1 as $key => $valu1) {
								    				if ($valu1['type']==2) {
								    					
								    			?>
												<span class="uploadimgbox" id="idProof<?php echo $valu1['id'] ?>">
													<img class="img-preview" id="img1" src="<?php echo base_url('assets/img/game_img/').$valu1['img'] ?>" style="height: 46px;width: 46px;">
													<span class="crossd" onclick="DeleteImage('idProof', 'id1',<?php echo $valu1['id'] ?>)">x</span>
											</span>
											<?php

								    				}

								    				}
									    	}	
									    	}
									    	?>
												
											</div>
								    	</div>
								    	<?php
								    	}else{ ?>
								    		<div  class="col-lg-6 gallery2"></div>
								    		<!-- <div class="col-lg-6" style="display: none;" id="img-preview1">
								    		<div class="uploaed_imgbox">
												<span class="uploadimgbox"><img class="img-preview1" src="<?php echo base_url('assets/img/doc.png')?>" style="height: 46px;width: 46px;">
													<span class="crossd" onclick="fadeout('img-preview1')">x</span>
												</span>
												
											</div>
								    	</div> -->

								    	<?php	
								    	}
								    	?>
							 		</div>
							 	</div>
							 	<div class="idproof mb-4">
									<div class="form-row">
										<h3 class="had"> SELFIE WITH YOUR ID<span>*</span></h3>
								    	<div class="col-lg-6">
								    		<div class="btn upladbtn">
								    			<input type="file" name="file3[]" class="form-control-file text-primary font-weight-bold document"  accept="image/*"  src="" id="file3" data-title="Drag and drop a file" <?php if (@$required_3) {
								    				
								    			}else{
								    				echo "required";
								    			}  ?> >
								    	<i class="align-middle mr-2 fas fa-fw fa-cloud-upload-alt"></i> Upload your document
								    		</div>
								    	</div>
								    	<?php
								    	
								    	if (!empty($id_verification)) {
								    		
								    	?>
								    	<div class="col-lg-6" id="SELFIE_WITH_YOUR_ID">
								    		<div class="uploaed_imgbox gallery3">
											

									    	<?php
								    			foreach ($id_verification as $key => $value3) {
								    			if ($value3['id_status']==1 ) {
								    			$con2['verification_id']=$value3['id'];
												$img_data2=$this->Merchant_modal->get_all_data('*','tbl_id_verification',$con2);
								    				foreach ($img_data2 as $key => $valu2) {
								    				if ($valu2['type']==3) {
								    					
								    			?>
												<span class="uploadimgbox" id="idProof<?php echo $valu2['id'] ?>">
													<img class="img-preview" id="img1" src="<?php echo base_url('assets/img/game_img/').$valu2['img'] ?>" style="height: 46px;width: 46px;">
													<span class="crossd" onclick="DeleteImage('idProof', 'id1',<?php echo $valu2['id'] ?>)">x</span>
											</span>
											<?php

								    				}

								    				}
									    	}	
									    	}
									    	?>
												
											</div>
								    	</div>
								    	<?php
								    	}else{ ?>
								    		<div  class="col-lg-6 gallery3"></div>
								    		<!-- <div class="col-lg-6" style="display: none;" id="img-preview2">
								    		<div class="uploaed_imgbox">
												<span class="uploadimgbox"><img class="img-preview2" src="<?php echo base_url('assets/img/doc.png')?>" style="height: 46px;width: 46px;">
													<span class="crossd" onclick="fadeout('img-preview2')">x</span>
												</span>
												
											</div>
								    	</div> -->
								    	<?php	
								    	}
								    	?>
							 		</div>
							 	</div>
							</div>
						</div>

						<div class="col-md-6 col-xl-6">
							<div class="idtext_box">
								<h4>ACCEPTED DOCUMENTS</h4>
								<div class="idtext_hed"><span> 1. </span> ID PROOF (Upload any one)</div>
								<ul>
									<li>Lorem Ipsum Dolor Sit Amet.</li>
									<li>Lorem Ipsum Dolor</li>
									<li>Sit Amet.</li>
									<li>Ipsum Dolor Sit Amet.</li>
								</ul>
								<div class="idtext_hed"><span> 1. </span> ID PROOF (Upload any one)</div>
								<ul>
									<li>Lorem Ipsum Dolor Sit Amet.</li>
									<li>Lorem Ipsum Dolor</li>
									<li>Sit Amet.</li>
								</ul>
							</div>
						</div>

						<div class="col-md-12 col-xl-12">
							<div class="idtext_hed">Important: </div>
							<ul class="imptext">
								<li>Take a selfie of you and your ID.  </li>
								<li>All two elements (you and your ID) must be visible without cropping or editing.</li>
								<li>Then place your ID on a flat, well-lit surface and take a photo of it and submit both photos.</li>
							</ul>
						</div>
						<?php 
						if (!empty($id_verification[0]['id'])) {
							?>
						<input type="hidden" name="verification_id" value="<?php echo $id_verification[0]['id']; ?>">	
						<input type="submit" name="" style="background-color: #008aa7;background: linear-gradient(90deg, rgb(0, 138, 167) 0%, rgb(0, 138, 167) 50%, rgb(0, 138, 167) 100%);border-color: #008aa7;font-size: 15px;margin-left: 69%;" class="btn btn-success payment_btn" value="Update">	
						<?php
						}else{
						?>
						<input type="submit" name="" style="background-color: #008aa7;background: linear-gradient(90deg, rgb(0, 138, 167) 0%, rgb(0, 138, 167) 50%, rgb(0, 138, 167) 100%);border-color: #008aa7;font-size: 15px;margin-left: 69%;" class="btn btn-success payment_btn">
						<?php
						}
						?>
						<input type="reset" name="" value="Cancel"style="background-color: #008aa7;background: linear-gradient(90deg, rgb(69, 71, 72) 0%, rgb(142, 142, 142) 50%, rgb(76, 100, 105) 100%);border-color: #008aa7;font-size: 15px;margin-left: 15px;" class="btn btn-success payment_btn" onclick="reloade()">
					</form>
					</div>
				</div>
</div>

			</main>
		</div>
	</div>

</body>

</html>
<script>
// function readUrl(input) {

// if (input.files && input.files[0]) {
// let reader = new FileReader();
// reader.onload = e => {
// let imgData = e.target.result;
// let imgName = input.files[0].name;
// input.setAttribute("data-title", imgName);
// console.log(e.target.result);
// };
// reader.readAsDataURL(input.files[0]);
// }

// }
</script>

<script type="text/javascript">

 function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.img-preview').attr('src', e.target.result);
      $("#img-preview").css("display", "block");
    }

    reader.readAsDataURL(input.files[0]);
  }
}



function readURL1(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('.img-preview1').attr('src', e.target.result);
      $("#img-preview1").css("display", "block");
    }


    reader.readAsDataURL(input.files[0]);
  }
}

function readURL2(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('.img-preview2').attr('src', e.target.result);
      $("#img-preview2").css("display", "block");
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#file").change(function() {
  readURL(this);
});

$("#file2").change(function() {
  readURL1(this);
});

$("#file3").change(function() {
  readURL2(this);
});

function reloade(){
	location.reload();
}

function DeleteImage(imageId, column,id){
	$("#" + imageId+id).fadeOut( "slow", function() {   
	});
	$.post("<?php echo base_url('merchant/deleteImage') ?>",{column:column,id:id},function(res){
   		console.log(res);
   	})
}

function fadeout(id){
	$("#" + id).fadeOut( "slow", function() {   
	});
}
$("form").on('click', '.delbtn', function (e) {
    reset($(this));
});
function reset(elm, prserveFileName) {
    if (elm && elm.length > 0) {
        var $input = elm;
        $input.prev('.img-preview').attr('src', '').hide();
        if (!prserveFileName) {
            $($input).parent().parent().parent().find('input.fileUpload ').val("");
            //input.fileUpload and input#uploadre both need to empty values for particular div
        }
        elm.remove();
    }
}

$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;
			alert(filesAmount);
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
					var span = document.getElementsByClassName("uploadimgbox");
					if(span.length >0)
					{
						span.parentNode.removeChild(span);  
					}
					//$("#img1").src(event.target.result);
                    // $($.parseHTML('<span class="uploadimgbox "><img class="img-preview" id="img1"  style="height: 46px;width: 46px;"><span class="crossd delbtn" >x</span></span>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    $($.parseHTML('<span class="uploadimgbox "><img src="'+event.target.result+'" class="img-preview" id="img1"  style="height: 46px;width: 46px;"><span class="crossd delbtn" >x</span></span>')).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });

    $('#file2').on('change', function() {
        imagesPreview(this, 'div.gallery2');
    });
    $('#file3').on('change', function() {
        imagesPreview(this, 'div.gallery3');
    });
    
});

 function deletePreview(ele, i) {
      "use strict";
      try {
        $(ele).parent().remove();
        window.filesToUpload.splice(i, 1);
      } catch (e) {
        console.log(e.message);
      }
    }

</script>