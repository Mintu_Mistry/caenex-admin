<?php
$this->load->view('merchant/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('merchant/include/nev');
?>

<div class="main">
<?php
$this->load->view('merchant/include/header');
?>

<main class="content">
<div class="container-fluid p-0">

<h1 class="nexplay_tab"><a href="<?php echo base_url('campaign_management') ?>" style="float: left;border: aliceblue;color: #ffffff;"><i class="fa fa-arrow-circle-left"></i></a> Add New NexPlay Campaign </h1>

<div class="row">
<div class="col-md-12 col-xl-12">
<div class="card">
<div class="popbox">
<div class="card-body">
<div class="row">
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Start Date</label>
<div class="input-group date" id="datetimepicker-minimum" data-target-input="nearest">
<input type="text" class="form-control datetimepicker-input" id="state_date"  oninput="clearOldPayementDetails('campaign_budget', 'caenex_fee', 'handling_charges', 'total_amount', 'impressions', 'custom_campaign_budget', 'custom_caenex_fee', 'custom_handling_charges', 'custom_total_amount', 'custom_impressions')" onkeypress="return autoMask(this,event, '');">
<div class="input-group-append" >
<div class="input-group-text calfont"><i class="fas fa-fw fa-calendar-alt"></i></div>
</div>
</div>
</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label"> End Date </label>
<div class="input-group date" id="datetimepicker-date" data-target-input="nearest">
<input type="text" class="form-control datetimepicker-input" id="end_date"  oninput="clearOldPayementDetails('campaign_budget', 'caenex_fee', 'handling_charges', 'total_amount', 'impressions', 'custom_campaign_budget', 'custom_caenex_fee', 'custom_handling_charges', 'custom_total_amount', 'custom_impressions')" onkeypress="return autoMask(this,event, '');">
<div class="input-group-append">
<div class="input-group-text calfont"><i class="fas fa-fw fa-calendar-alt"></i></div>
</div>
</div>
</div>
</div>

<div class="col-12 col-xl-4">
<div class="form-group">
<label for="inputEmail4">Select Game</label>
<select  class="form-control" name="game" id="game" required="">
<option value="">Select Game</option>
<?php
  foreach ($game as $key => $game_list) {
    ?>
<option value="<?php echo $game_list['game_id'] ?>"><?php echo $game_list['game_name'] ?></option>
    <?php
  }
?>
</select>
</div>
</div>

</div>
<div class="row">
<div class="col-12 col-xl-12">
<h4> Select Game Type </h4>
<div class="tab">
<ul class="nav nav-tabs mb20" role="tablist">
<li class="nav-item"><a class="nav-link active" href="#Rewardtab-1" data-toggle="tab" role="tab">Caenex Game</a></li>
</ul>
<div class="tab-content pad0 boxshadow0">
<div class="tab-pane active" id="Rewardtab-1" role="tabpanel">
<form method="post" enctype="multipart/form-data" id="paymentFrm" action="<?php echo base_url('Merchant/nexplay_caenex_game') ?>">
	<input type="hidden" name="state_date" id="state_date1">
	<input type="hidden" name="end_date" id="end_date1">
	<input type="hidden" name="payment_status" id="payment_status">
	<input type="hidden" name="gameid" id="gameid">
<div class="form-row">
<div class="form-group col-md-4">
<label for="inputEmail4">Choose Ad type</label>
<select  class="form-control" name="ad_type" id="ad_type">
<option value="">Choose Ad type</option>
<?php
foreach ($ad_type as $key => $ad_types) {
	?>
<option value="<?php echo $ad_types['ad_type'] ?>"><?php echo $ad_types['ad_type'] ?></option>
	<?php
}
?>
</select>
</div>
<div class="form-group col-md-4">
<label for="inputPassword4">Amount / Video View</label>
<input type="text" class="form-control"  name="amount_video_view" id="amount_video_view" oninput="Validate_Only_Numeric_Decimal_Input('amount_video_view')">
</div>
<div class="form-group col-md-4">
<label for="inputPassword4">Maximum Daily Views <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle"  rel="popover" data-trigger="hover" data-toggle="tooltip" data-content="The maximum number merchant wishes to show / pay for, per day"></span></a></label>
<input type="text" class="form-control"  placeholder="" name="max_daily_view" id="max_daily_view" oninput="Validate_Only_Numeric_Input('max_daily_view')">
</div>
</div>
<div class="form-row">
<div class="form-group col-md-4">
<label for="inputPassword4">Upload Media <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" data-toggle="tooltip" rel="popover" data-trigger="hover"  data-content="Recommended Banner size: 714x320 Recommended Video size: 8mb Recommended Video duration: 1min"></span></a></label>
<div class="form-group inputDnD">
<label class="sr-only" for="inputFile">File Upload</label>
<input type="file" name="file" class="form-control-file text-primary font-weight-bold"  onchange="readUrl(this)" src="" id="file" data-title="Drag and drop a file" accept="video/*,image/*">
</div>
</div>
</div>


<div class="row">
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Campaign Budget <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" data-toggle="tooltip" rel="popover" data-trigger="hover"  data-content="Min. Campaign Budget : $100"></span></a></label>
<input type="text" class="form-control" name="campaign_budget" id="campaign_budget"  oninput="Validate_Only_Numeric_Decimal_Input_And_Calculate_Total_Amount('campaign_budget', 'caenex_fee', 'handling_charges', 'total_amount', 'impressions')">

</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Total Number of Impressions <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" data-placement="auto" data-toggle="tooltip" rel="popover" data-trigger="hover"  data-content="Total No. of Impressions = Campaign Budget x 100 ($1=100 impressions)"></span></a></label>
<input type="text" class="form-control" name="impressions" id="impressions" readonly>

</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label"><!-- Numbr of Impressions/Day (CAPPING) -->Maximum Number of Impressions / Day <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" data-toggle="tooltip" rel="popover" data-trigger="hover"  data-content="The maximum number merchant wishes to show / pay for, per day"></span></a></label>
<input type="text" class="form-control" name="impressions_day" id="impressions_day"  oninput="Validate_Only_Numeric_Input('impressions_day')">
</div>
</div>
</div>
<div class="row">
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label"> Caenex Fee <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" data-toggle="tooltip" rel="popover" data-trigger="hover"  data-content="Number of Days * 25"></span></a></label>
<input type="text" class="form-control" name="caenex_fee" id="caenex_fee"  readonly>
</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Handling Charges <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" data-toggle="tooltip" rel="popover" data-trigger="hover"  data-content="3% of (Campaign Budget + Caenex Fee)"></span></a></label>
<input type="text" class="form-control" name="handling_charges" id="handling_charges"  readonly>
</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Total Amount <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" data-toggle="tooltip" rel="popover" data-trigger="hover"  data-content="Campaign Budget  + Caenex Fee + Handling Charges"></span></a></label>
<input type="text" class="form-control" name="total_amount" id="total_amount" readonly>
</div>
</div>
</div>
<div class="row text-center">
<div class="col-12 col-xl-12">
<!-- <a  href="javascript:void(0)" class="btn btn-success payment_btn" onclick="check_date()" id="caenex_game" data-toggle="modal" data-target="javascript:void(0)defaultModalPrimary"> Submit </a> -->
<a  href="javascript:void(0)" class="btn btn-success payment_btn" onclick="check_date()" > Submit </a>


<div class="modal fade" data-keyboard="false" data-backdrop="static" id="wallet_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg newmodel_size" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Campaign Payment</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body m-3">
                <label id="payment-errors"></label>
                <div class="card-body">
                    <div class="form-row posi_relative">

                        <div class="form-group col-md-12">
                       <label style="float: left;font-size: 17px;"><b>Total campaign amount : </b></label>

                        <b><label id="total_pay" style="font-size: 17px;"></label></b>


                        </div>
                        <div class="form-group col-md-12">
                            <label style="float: left;font-size: 17px;"><b>Available wallet balance : 
                            <input type="checkbox" name="wallete_amount_use" style="float: left;margin: 4px;" id="wallete_amount_use"  checked="" >
                        </b></label>

                            <?php 
                                if(!empty($wallet[0]['total_avail_balance'])){
                                   $balance = $wallet[0]['total_avail_balance'];
                                }
                                else {
                                    $balance = "0";
                                } 
                            ?>
                            <label id="total_balance13" style="font-size: 17px;" ><b><?php echo $balance; ?></b></label>
                            <input type="hidden" id="total_balance1" value="<?php echo $balance; ?>" readonly="">
                        </div>
                        <div class="form-group col-md-12">
                        <label style="float: left;font-size: 17px;"><b>You have to pay : </b></label>
                       <b> <label id="total_balance_to_pay" style="margin-left: 13%;;font-size: 17px;"></label></b>
                        <input type="hidden" name="total_balance1" id="amountfill1" readonly>

                        </div>
                        <div class="col-md-4" style="display: contents;">
                            <a href="javascript:void(0)" class="btn btn-confirm btn-block" name="confirmAddNewCamp" onclick="showpayment()">  Pay now</a>
                            <!-- <button class="btn btn-confirm btn-block"> Confirm & Proceed to Payment </button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="payment_popup" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg newmodel_size" role="document">
<div class="modal-content">
<div class="modal-header">

<h4 class="modal-title">Payment</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body m-3">
<label id="payment-errors"></label>
<div class="card-body">
<div class="form-row posi_relative">
<div class="form-group col-md-12">
<label for="inputEmail4" style="float: left;">Card number</label>
<input type="text" class="form-control" required onkeypress="return autoMask(this,event, '#### #### #### ####');"  name="card_number" id="cardnum" placeholder="Card number">
<!-- <div class="visabox"><img src="<?php echo base_url('assets/img/')?>visa_icon.png"></div> -->
</div>
</div>
<div class="form-row">
<div class="form-group col-md-12">
<label for="inputEmail4" style="float: left;">Cardholder name</label>
<input type="text" class="form-control" id="card_name"  oninput="validate_only_alphabates_allowed(this.id)" name="card_name" placeholder="Cardholder name"  required>
</div>
</div>
<div class="form-row">
<div class="form-group col-md-8">
<label for="inputEmail4" style="float: left;">Exp. date</label>

<!-- <div class="form-group col-md-6">
<input type="text" class="form-control" name="exp_date" onkeypress="return autoMask(this,event, '##');" id="expmonth" maxlength="2" placeholder="Month"  required>
</div>
<div class="form-group col-md-6">
<input type="text" class="form-control" placeholder="Year" id="expyear" name="year" onkeypress="return autoMask(this,event, '####');"  required>
</div> -->

<div class="form-group col-md-12" style="margin-left: -14px;">
<input type="text" class="form-control" name="exp_date" onkeypress="return autoMask(this,event, '##/####');" id="expmonth" maxlength="7" minlength="7" placeholder="Month / Year"  required>
</div>

</div>

<div class="form-group col-md-4">
<label for="inputAddress2" style="float: left;">CVV</label>
<input type="text" class="form-control" placeholder="CVV" id="cvv" name="cvv"  onkeypress="return autoMask(this,event, '###');"  required>
</div>
</div>
<div class="form-row">
<div class="form-group col-md-12">
<label for="inputEmail4" style="float: left;">Card nickname(Optional)</label>
<input type="text" class="form-control" placeholder="Card nickname(Optional)" id="Nickname" oninput="validate_only_alphabates_allowed(this.id)" name="nickname">
</div>
</div>									

<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<!-- <button class="btn btn-primary">Pay Now</button> -->
<button id="payBtn" class="btn btn-primary">PROCEED TO PAYMENT</button>

</div>
</div>
</div>

</div>
</div>

</div>
</div>
</form>
</div><!--- tab close -->
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>

</div>
</main>
</div>
</div>

<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/redmond/jquery-ui.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>
<script>
	$('[rel=popover]').popover({ html : true });
	$("#state_date").datepicker({
	  minDate: 0,
	  onSelect: function(date) {

	  	//d = new Date();
		//date.add(-1).days();
		//alert(date);
	  	//date.setDate(date.getDate() - 1);
		
		$("#end_date").datepicker('option', 'minDate', date);
	  //  $("#end_date").datepicker('option', 'minDate', date);
	  }
	});

	$("#end_date").datepicker({});
	// $("#state_date").datepicker({ minDate: 0 });
	// $("#end_date").datepicker({ minDate: 0 });
function readUrl(input) {

if (input.files && input.files[0]) {
let reader = new FileReader();
reader.onload = e => {
let imgData = e.target.result;
let imgName = input.files[0].name;
input.setAttribute("data-title", imgName);
console.log(e.target.result);
};
reader.readAsDataURL(input.files[0]);
}

}
</script>
</body>

</html>

	<script type="text/javascript">
	$(document).ready(function(){

       var max_fields = 100;
       var x = 1;
    $('.tr_clone_add').click(function(){
      var x=document.getElementById("number").value;
    $(this).attr('value', (x+1)); 
      var question_number_int=document.getElementById("question_number").value;
      var question_number=parseInt(question_number_int);
      document.getElementById("question_number").value=question_number+4;
       var question_number1=question_number+1;
       var question_number2=question_number+2;
       var question_number3=question_number+3;
       var question_number4=question_number+4;
            $("#item_list").show();
         
         if(x < max_fields){
		  x++;
        document.getElementById("number").value= x;
         $("#new_table").append('<div class="col-12 col-xl-3"><label class="question_num">Question '+question_number1+'</label><div class="form-group"><input type="text" class="form-control" id="Q'+question_number1+'" name="question[]" placeholder="Enter question"></div><div class="form-group"><input type="text" class="form-control" id="Q'+question_number1+'A1" name="answer1[]" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number1+'" id="Q'+question_number1+'A1C" value="1" type="radio" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer2[]" id="Q'+question_number1+'A2" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number1+'" id="Q'+question_number1+'A2C" type="radio" value="2" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer3[]" id="Q'+question_number1+'A3" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number1+'" id="Q'+question_number1+'A3C" type="radio" value="3" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer4[]" id="Q'+question_number1+'A4" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number1+'" id="Q'+question_number1+'A4C" type="radio" value="4" id="autoSizingCheck"></div></div><div class="col-12 col-xl-3"><label class="question_num">Question '+question_number2+'</label><div class="form-group"><input type="text" class="form-control" id="Q'+question_number2+'" name="question[]" placeholder="Enter question"></div><div class="form-group"><input type="text" class="form-control" id="Q'+question_number2+'A1" name="answer1[]" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number2+'" id="Q'+question_number2+'A1C" value="1" type="radio" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer2[]" id="Q'+question_number2+'A2" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number2+'" id="Q'+question_number2+'A2C" value="2" type="radio" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer3[]" id="Q'+question_number2+'A3" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number2+'" id="Q'+question_number2+'A3C" type="radio" value="3" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer4[]" id="Q'+question_number2+'A4" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number2+'" id="Q'+question_number2+'A4C" type="radio" value="4" id="autoSizingCheck"></div></div><div class="col-12 col-xl-3"><label class="question_num">Question '+question_number3+'</label><div class="form-group"><input type="text" class="form-control" id="Q'+question_number3+'" name="question[]" placeholder="Enter question"></div><div class="form-group"><input type="text" class="form-control" id="Q'+question_number3+'A1" name="answer1[]" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number3+'" id="Q'+question_number3+'A1C" value="1" type="radio" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer2[]" id="Q'+question_number3+'A2" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number3+'" id="Q'+question_number3+'A2C" value="2" type="radio" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer3[]" id="Q'+question_number3+'A3" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number3+'" id="Q'+question_number3+'A3C" type="radio" value="3" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer4[]" id="Q'+question_number3+'A4" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number3+'" id="Q'+question_number3+'A4C" type="radio" value="4" id="autoSizingCheck"></div></div><div class="col-12 col-xl-3"><label class="question_num">Question '+question_number4+'</label><div class="form-group"><input type="text" class="form-control" id="Q'+question_number4+'" name="question[]" placeholder="Enter question"></div><div class="form-group"><input type="text" class="form-control" id="Q'+question_number4+'A1" name="answer1[]" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number4+'" id="Q'+question_number4+'A1C" value="1" type="radio" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer2[]" id="Q'+question_number4+'A2" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number4+'" id="Q'+question_number4+'A2C" value="2" type="radio" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer3[]" id="Q'+question_number4+'A3" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number4+'" id="Q'+question_number4+'A3C" type="radio" value="3" id="autoSizingCheck"></div><div class="form-group"><input type="text" class="form-control" name="answer4[]" id="Q'+question_number4+'A4" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number4+'" id="Q'+question_number4+'A4C" type="radio" value="4" id="autoSizingCheck"></div></div>');          
}    
    })
  })
	</script>	
	<script type="text/javascript">
		$(document).ready(function(){
		  $('[data-toggle="tooltip"]').tooltip();   
		});
		function check_date(){
			// swal("Here's a message!")
			var state_date=document.getElementById("state_date").value;
			document.getElementById("state_date1").value=state_date;
			var end_date=document.getElementById("end_date").value;
			var game=document.getElementById("game").value;
			document.getElementById("gameid").value=game;
			
			let currentDate = new Date();
			// let startDate = new Date(state_date.split(" ", 1));
			// let endDate = new Date(end_date.split(" ", 1));

			let startDate = new Date(state_date);
			let endDate = new Date(end_date);
			var Difference_In_Time = endDate.getTime() - startDate.getTime();
			var Difference_In_Time3 = startDate.getTime() - currentDate.getTime();
			var Difference_In_Time4 = endDate.getTime() - currentDate.getTime();

			document.getElementById("end_date1").value=end_date;
			var e = document.getElementById("ad_type");
			var ad_type = e.options[e.selectedIndex].value;
			var amount_video_view=document.getElementById("amount_video_view").value;
			var max_daily_view=document.getElementById("max_daily_view").value;	
			var file = $('#file').val(); 
		    var campaign_budget=document.getElementById("campaign_budget").value;
		    var impressions=document.getElementById("impressions").value;	    
		    var impressions_day=document.getElementById("impressions_day").value;
		    var caenex_fee=document.getElementById("caenex_fee").value;
		    var handling_charges=document.getElementById("handling_charges").value;
		    var total_amount=document.getElementById("total_amount").value;	    
			
			if (state_date=="") {
				$("#campaign_budget").val('');
			$("#handling_charges").val('');
			$("#caenex_fee").val('');
			$("#total_amount").val('');
			$("#impressions").val('')
				$("#state_date").focus();	
			swal("Start Date should Not Be Empty")
			}else if (end_date=="") {
				$("#campaign_budget").val('');
			$("#handling_charges").val('');
			$("#caenex_fee").val('');
			$("#total_amount").val('');
			$("#impressions").val('')
				$("#end_date").focus();	
			swal("End Date should Not Be Empty")
			}else if (campaign_budget < 100) {
				$("#campaign_budget").val('');
			$("#handling_charges").val('');
			$("#caenex_fee").val('');
			$("#total_amount").val('');
			$("#impressions").val('')
				$("#campaign_budget").focus();	
			swal("Campaign budget should be greater or equal to $100.")
			}
			// else if (Difference_In_Time3 < 0) {
			// $("#state_date").val('');
			// $("#campaign_budget").val('');
			// $("#caenex_fee").val('');
			// $("#handling_charges").val('');
			// $("#total_amount").val('');
			// $("#impressions").val('')
			// $("#state_date").focus();	
			// swal("Satrt Date should not Be past date.")
			// }
			// else if (Difference_In_Time <= 0) {
			// $("#end_date").val('');
			// $("#campaign_budget").val('');
			// $("#caenex_fee").val('');
			// $("#handling_charges").val('');
			// $("#total_amount").val('');
			// $("#impressions").val('')
			// $("#end_date").focus();	
			// swal("End date-time should not be less or equal to Start date-time.")
			// }
			// else if (Difference_In_Time4 < 0) {
			// $("#end_date").val('');
			// $("#campaign_budget").val('');
			// $("#caenex_fee").val('');
			// $("#handling_charges").val('');
			// $("#total_amount").val('');
			// $("#impressions").val('')
			// $("#end_date").focus();	
			// swal("End Date should not Be past date.")
			// }
			else if (ad_type=="") {
				$("#ad_type").focus();	
			swal("Ad type should Not Be Empty")
			}else if (amount_video_view=="") {
				$("#amount_video_view").focus();	
			swal("Amount / Video View should Not Be Empty")
			}else if (max_daily_view=="") {
				$("#max_daily_view").focus();	
			swal("Maximum Daily Views should Not Be Empty")
			}else if (file=="") {
				$("#file").focus();	
			swal("Coupon Image Views should Not Be Empty")
			}else if (campaign_budget=="") {
				$("#campaign_budget").focus();	
			swal("Campaign Budget Views should Not Be Empty")
			}else if (impressions=="") {
				$("#impressions").focus();	
			swal("Total Number of Impressions should Not Be Empty")
			}else if (impressions_day=="") {
				$("#impressions_day").focus();	
			swal("Maximum Number of Impressions / Day should Not Be Empty")
			}else if (caenex_fee=="") {
				$("#caenex_fee").focus();	
			swal("Caenex Fee should Not Be Empty")
			}else if (handling_charges=="") {
				$("#handling_charges").focus();	
			swal("Handling Charges should Not Be Empty")
			}
			else if (total_amount=="") {
				$("#total_amount").focus();	
			swal("Total Amount should Not Be Empty")
			}
			else if (game=="") {
				$("#game").focus();	
			swal("Please select a Game")
			
			}else{
				showwallet();
				//$('#wallet_popup').modal('toggle');
			}
			
		}

		function clearOldPayementDetails(input1, input2, input3, input4, input5, input6, input7, input8, input9, input10){

			document.getElementById(input1).value = '';
			document.getElementById(input2).value = '';
			document.getElementById(input3).value = '';
			document.getElementById(input4).value = '';
			document.getElementById(input5).value = '';
			document.getElementById(input6).value = '';
			document.getElementById(input7).value = '';
			document.getElementById(input8).value = '';
			document.getElementById(input9).value = '';
			document.getElementById(input10).value = '';

		}

		
		function custom_model(){
			// swal("Here's a message!")
			var state_date=document.getElementById("state_date").value;
			document.getElementById("state_date2").value=state_date;
			var end_date=document.getElementById("end_date").value;

			
			let currentDate = new Date();
			// let startDate = new Date(state_date.split(" ", 1));
			// let endDate = new Date(end_date.split(" ", 1));

			let startDate = new Date(state_date);
			let endDate = new Date(end_date);

			var Difference_In_Time = endDate.getTime() - startDate.getTime();
			var Difference_In_Time2 = startDate.getTime() - currentDate.getTime();
			var Difference_In_Time3 = endDate.getTime() - currentDate.getTime();

			document.getElementById("end_date2").value=end_date;
			var e = document.getElementById("custom_category");
			var custom_category = e.options[e.selectedIndex].value;
			var c = document.getElementById("custom_ad_type");
			var custom_ad_type = c.options[c.selectedIndex].value;
			var file = $('#custom_file').val(); 
			
			var custom_reward=document.getElementById("custom_reward").value;
			var max_daily_view=document.getElementById("max_daily_view").value;	
		    var custom_campaign_budget=document.getElementById("custom_campaign_budget").value;
		    var custom_impressions=document.getElementById("custom_impressions").value;	    
		    var custom_impressions_day=document.getElementById("custom_impressions_day").value;
		    var custom_caenex_fee=document.getElementById("custom_caenex_fee").value;
		    var custom_handling_charges=document.getElementById("custom_handling_charges").value;
		    var custom_total_amount=document.getElementById("custom_total_amount").value;
			var Q1 = document.getElementById("Q1").value;	    
			var Q1A1 = document.getElementById("Q1A1").value;
			var Q1A2 = document.getElementById("Q1A2").value;
			var Q1A3 = document.getElementById("Q1A3").value;
			var Q1A4 = document.getElementById("Q1A4").value;
			var Q2 = document.getElementById("Q2");
			var Q2A1 = document.getElementById("Q2A1");
			var Q2A2 = document.getElementById("Q2A2");
			var Q2A3 = document.getElementById("Q2A3");
			var Q2A4 = document.getElementById("Q2A4");
			var Q3 = document.getElementById("Q3").value;
			var Q3A1 = document.getElementById("Q3A1");
			var Q3A2 = document.getElementById("Q3A2");
			var Q3A3 = document.getElementById("Q3A3");
			var Q3A4 = document.getElementById("Q3A4");
			var Q4 = document.getElementById("Q4");
			var Q4A1 = document.getElementById("Q4A1");
			var Q4A2 = document.getElementById("Q4A2");
			var Q4A3 = document.getElementById("Q4A3");
			var Q4A4 = document.getElementById("Q4A4");
			var Q5 = document.getElementById("Q5");
			var Q5A1 = document.getElementById("Q5A1");
			var Q5A2 = document.getElementById("Q5A2");
			var Q5A3 = document.getElementById("Q5A3");
			var Q5A4 = document.getElementById("Q5A4");
			var Q6 = document.getElementById("Q6");
			var Q6A1 = document.getElementById("Q6A1");
			var Q6A2 = document.getElementById("Q6A2");
			var Q6A3 = document.getElementById("Q6A3");
			var Q6A4 = document.getElementById("Q6A4");
			var Q7 = document.getElementById("Q7");
			var Q7A1 = document.getElementById("Q7A1");
			var Q7A2 = document.getElementById("Q7A2");
			var Q7A3 = document.getElementById("Q7A3");
			var Q7A4 = document.getElementById("Q7A4");
			var Q8 = document.getElementById("Q8");
			var Q8A1 = document.getElementById("Q8A1");
			var Q8A2 = document.getElementById("Q8A2");
			var Q8A3 = document.getElementById("Q8A3");
			var Q8A4 = document.getElementById("Q8A4");
			var Q9 = document.getElementById("Q9");
			var Q9A1 = document.getElementById("Q9A1");
			var Q9A2 = document.getElementById("Q9A2");
			var Q9A3 = document.getElementById("Q9A3");
			var Q9A4 = document.getElementById("Q9A4");
			var Q10 = document.getElementById("Q10");
			var Q10A1 = document.getElementById("Q10A1");
			var Q10A2 = document.getElementById("Q10A2");
			var Q10A3 = document.getElementById("Q10A3");
			var Q10A4 = document.getElementById("Q10A4");
			

			if (state_date=="") {
				$("#custom_campaign_budget").val('');
				$("#custom_caenex_fee").val('');
				$("#custom_handling_charges").val('');
				$("#custom_total_amount").val('');
				$("#custom_impressions").val('');
				$("#state_date").focus();
			swal("Start Date Should Not Be Empty")
			}else if (end_date=="") {
				$("#custom_campaign_budget").val('');
				$("#custom_caenex_fee").val('');
				$("#custom_handling_charges").val('');
				$("#custom_total_amount").val('');
				$("#custom_impressions").val('');
				$("#end_date").focus();
			swal("End Date Should Not Be Empty")
			}
			// else if (custom_campaign_budget < 100) {
			// 	$("#custom_campaign_budget").val('');
			// 	$("#custom_caenex_fee").val('');
			// 	$("#custom_handling_charges").val('');
			// 	$("#custom_total_amount").val('');
			// 	$("#custom_impressions").val('');
			// 	$("#custom_campaign_budget").focus();
			// swal("Start date should not be of past")
			// }else if (Difference_In_Time2 < 0) {
			// 	$("#state_date").val('');
			// 	$("#custom_campaign_budget").val('');
			// 	$("#custom_caenex_fee").val('');
			// 	$("#custom_handling_charges").val('');
			// 	$("#custom_total_amount").val('');
			// 	$("#custom_impressions").val('');
			// 	$("#state_date").focus();
			// swal("Start date should not be of past")
			// }else if (Difference_In_Time <= 0) {
			// 	$("#end_date").val('');
			// 	$("#custom_campaign_budget").val('');
			// 	$("#custom_caenex_fee").val('');
			// 	$("#custom_handling_charges").val('');
			// 	$("#custom_total_amount").val('');
			// 	$("#custom_impressions").val('');
			// 	$("#end_date").focus();
			// swal("End date-time should not be less or equal to Start date-time.")
			// }else if (Difference_In_Time3 < 0) {
			// 	$("#end_date").val('');
			// 	$("#custom_campaign_budget").val('');
			// 	$("#custom_caenex_fee").val('');
			// 	$("#custom_handling_charges").val('');
			// 	$("#custom_total_amount").val('');
			// 	$("#custom_impressions").val('');
			// 	$("#end_date").focus();
			// swal("End Date should not be of past")
			// }
			else if (custom_category=="") {
				$("#custom_category").focus();
			swal("Game Category Should Not Be Empty")
			}else if (custom_ad_type=="") {
				$("#custom_ad_type").focus();
			swal("Ad type Should Not Be Empty")
			}else if (custom_reward=="") {
				$("#custom_reward").focus();
			swal("Reward / Correct Question Should Not Be Empty")
			}else if (file=="") {
				$("#custom_file").focus();
			swal("Media Should Not Be Empty")
			}else if ($("#Q1").val()=="") {
				$("#Q1").focus();
			swal("Question Should Not Be Empty")
			}else if ($("#Q1A1").val()=="") {
				$("#Q1A1").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q1A2").val()=="") {
				$("#Q1A2").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q1A3").val()=="") {
				$("#Q1A3").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q1A4").val()=="") {
				$("#Q1A4").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q2").val()=="") {
				$("#Q2").focus();
			swal("Question Should Not Be Empty")
			}else if ($("#Q2A1").val()=="") {
				$("#Q2A1").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q2A2").val()=="") {
				$("#Q2A2").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q2A3").val()=="") {
				$("#Q2A3").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q2A4").val()=="") {
				$("#Q2A4").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q3").val()=="") {
				$("#Q3").focus();
			swal("Question Should Not Be Empty")
			}else if ($("#Q3A1").val()=="") {
				$("#Q3A1").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q3A2").val()=="") {
				$("#Q3A2").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q3A3").val()=="") {
				$("#Q3A3").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q3A4").val()=="") {
				$("#Q3A4").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q4").val()=="") {
				$("#Q4").focus();
			swal("Question Should Not Be Empty")
			}else if ($("#Q4A1").val()=="") {
				$("#Q4A1").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q4A2").val()=="") {
				$("#Q4A2").focus();
			swal("End Date Should Not Be Empty")
			}else if ($("#Q4A2").val()=="") {
				$("#Q4A2").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q4A3").val()=="") {
				$("#Q4A3").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q4A4").val()=="") {
				$("#Q4A4").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q5").val()=="") {
				$("#Q5").focus();
			swal("Question Should Not Be Empty")
			}else if ($("#Q5A1").val()=="") {
				$("#Q5A1").focus();
			swal("End Date Should Not Be Empty")
			}else if ($("#Q5A2").val()=="") {
				$("#Q5A2").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q5A3").val()=="") {
				$("#Q5A3").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q5A4").val()=="") {
				$("#Q5A4").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q6").val()=="") {
				$("#Q6").focus();
			swal("Question Should Not Be Empty")
			}else if ($("#Q6A1").val()=="") {
				$("#Q6A1").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q6A2").val()=="") {
				$("#Q6A2").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q6A3").val()=="") {
				$("#Q6A3").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q6A4").val()=="") {
				$("#Q6A4").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q7").val()=="") {
				$("#Q7").focus();
			swal("Question Should Not Be Empty")
			}else if ($("#Q7A1").val()=="") {
				$("#Q7A1").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q7A2").val()=="") {
				$("#Q7A2").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q7A3").val()=="") {
				$("#Q7A3").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q7A4").val()=="") {
				$("#Q7A4").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q8").val()=="") {
				$("#Q8").focus();
			swal("Question Should Not Be Empty")
			}else if ($("#Q8A1").val()=="") {
				$("#Q8A1").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q8A2").val()=="") {
				$("#Q8A2").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q8A3").val()=="") {
				$("#Q8A3").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q8A4").val()=="") {
				$("#Q8A4").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q9").val()=="") {
				$("#Q9").focus();
			swal("Question Should Not Be Empty")
			}else if ($("#Q9A1").val()=="") {
				$("#Q9A1").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q9A2").val()=="") {
				$("#Q9A2").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q9A3").val()=="") {
				$("#Q9A3").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q9A4").val()=="") {
				$("#Q9A4").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q10").val()=="") {
				$("#Q10").focus();
			swal("Question Should Not Be Empty")
			}else if ($("#Q10A1").val()=="") {
				$("#Q10A1").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q10A2").val()=="") {
				$("#Q10A2").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q10A3").val()=="") {
				$("#Q10A3").focus();
			swal("Answer Should Not Be Empty")
			}else if ($("#Q10A4").val()=="") {
				$("#Q10A4").focus();
			swal("Answer Should Not Be Empty")
			}else if (custom_campaign_budget=="") {
				$("#custom_campaign_budget").focus();
			swal("Campaign Budget Views Should Not Be Empty")
			}else if (custom_impressions=="") {
				$("#custom_impressions").focus();
			swal("Total Number of Impressions Should Not Be Empty")
			}else if (custom_impressions_day=="") {
				$("#custom_impressions_day").focus();
			swal("Maximum Number of Impressions / Day Should Not Be Empty")
			}else if (custom_caenex_fee=="") {
			swal("Caenex Fee should Not Be Empty")
			}else if (custom_handling_charges=="") {
			swal("Handling Charges Should Not Be Empty")
			}
			else if (custom_total_amount=="") {
			swal("Total Amount Should Not Be Empty")
			}else{
				$('#defaultModalP').modal('toggle');
			}
			
		}

		function Validate_Only_Numeric_Input(input_id){
			let Total_Number_of_Coupons = document.getElementById(input_id).value;
			let regExp = /^[0-9]+$/;
			if (regExp.test(Total_Number_of_Coupons)) {
			console.log("Valid");
			}else{
			
			document.getElementById(input_id).value  = Total_Number_of_Coupons.substring(0, Total_Number_of_Coupons.length-1);
			swal("Only numeric value is allowed");

			}
		}	

		function Validate_Only_Numeric_Decimal_Input(input_id){
			let Total_Number_of_Coupons = document.getElementById(input_id).value;
			let regExp = /^[0-9.]+$/;
			if (regExp.test(Total_Number_of_Coupons)) {
			console.log("Valid");
			}else{
			
			document.getElementById(input_id).value  = Total_Number_of_Coupons.substring(0, Total_Number_of_Coupons.length-1);
			// document.getElementById(input_id).value  = '';
			swal("Only numeric value is allowed");

			}
		}

		function Validate_Only_Numeric_Decimal_Input_And_Calculate_Total_Amount(input_field, caenex_fee, handling_charges, total_amount, total_impression){
		let CampaignBudget = document.getElementById(input_field).value;
		let startDate = document.getElementById("state_date").value;
		let endDate = document.getElementById("end_date").value;

		let startDateArray = new Date(startDate.split(" ", 1));
		let endDateArray = new Date(endDate.split(" ", 1));
		var Difference_In_Time = endDateArray.getTime() - startDateArray.getTime();
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24); 
		let regExp = /^[0-9.]+$/;
		if(regExp.test(CampaignBudget)) {
			if(Difference_In_Days == 0){
		let totalImpressions = CampaignBudget*100;	
		let caenexFee = 25;
		let handlingCharges = ((Number(caenexFee) + Number(CampaignBudget))*3)/100;
		let totalAmount = Math.ceil(Number(CampaignBudget) + Number(caenexFee) + Number(handlingCharges));
		document.getElementById(caenex_fee).value = caenexFee;
		document.getElementById(total_impression).value = totalImpressions;
		document.getElementById(handling_charges).value = handlingCharges;
		document.getElementById(total_amount).value = totalAmount;
			}else{
				let totalImpressions = CampaignBudget*100;	
		let caenexFee = Number(Difference_In_Days) * 25;
		let handlingCharges = ((Number(caenexFee) + Number(CampaignBudget))*3)/100;
		let totalAmount = Math.ceil(Number(CampaignBudget) + Number(caenexFee) + Number(handlingCharges));
		document.getElementById(caenex_fee).value = caenexFee;
		document.getElementById(total_impression).value = totalImpressions;
		document.getElementById(handling_charges).value = handlingCharges;
		document.getElementById(total_amount).value = totalAmount;
			}
		
		}else{
		document.getElementById(input_field).value = CampaignBudget.substring(0, CampaignBudget.length-1);
		document.getElementById(caenex_fee).value = 0;
		document.getElementById(handling_charges).value = 0;
		document.getElementById(total_amount).value = 0;
		document.getElementById(total_impression).value = 0;
		swal("Only numeric value is allowed");
		}

	}

	function validate_only_alphabates_allowed(input_id){
			let input_value = document.getElementById(input_id).value;
			let regExp = /^[a-z A-Z]+$/;
			if (regExp.test(input_value)) {
			console.log("Valid");
			}else{
			
			document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
		
			}
		}


	function autoMask(field, event, sMask) {
    var KeyTyped = String.fromCharCode(getKeyCode(event));
    if (getKeyCode(event) == 8) {
        return
    }
    if (getKeyCode(event) == 0) {
        return
    }
    if (field.value.length == sMask.length && getKeyCode(event) == 13) {
        return true
    }
    // if (field.value.length == sMask.length && getKeyCode(event) != 13) {
    //     field.value = ""
    // }
    var targ = getTarget(event);
    keyCount = targ.value.length;
    if (keyCount == sMask.length) {
        return false
    }
    if (sMask.charAt(keyCount + 1) != "#" && sMask.charAt(keyCount + 1) != "A") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return false
    }
    if (sMask.charAt(keyCount) == "*")
        return true;
    if (sMask.charAt(keyCount) == KeyTyped) {
        return true
    }
    if (sMask.charAt(keyCount) == "#" && isNumeric(KeyTyped))
        return true;
    if (sMask.charAt(keyCount) == "A" && isAlpha(KeyTyped))
        return true;
    if (sMask.charAt(keyCount + 1) == "?") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return true
    }
    if (KeyTyped.charCodeAt(0) < 32)
        return true;
    return false
}

function getKeyCode(e) {
    if (e.srcElement) {
        return e.keyCode
    }
    if (e.target) {
        return e.which
    }
}

function getTarget(e) {
    if (e.srcElement) {
        return e.srcElement
    }
    if (e.target) {
        return e.target
    }
}

function isNumeric(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

function isAlpha(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

function delete_data(id,tbl,row){
	var result = confirm("Are You Sure");
	if (result) {
   	$.post("<?php echo base_url('merchant/delete') ?>",{id:id,tbl:tbl,row:row},function(res){
   		$("#saved_card"+id+"").fadeOut();
   	})
	}
}
</script>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script> 

<script type="text/javascript">
 
 function showpayment(){
	
	//document.getElementById("defaultModalPrimary").style.display = "none";
	//$('#defaultModalPrimary').modal('hide');
    check_balance1();
		//$('#payment_popup').modal('toggle');
}

function showpayment1(){
	
	document.getElementById("defaultModalP").style.display = "none";
	$('#defaultModalP').modal('hide');
	check_balance();
}
	function showpayment_popup(){
		$('body').addClass('modal-open');
		$('#defaultModalPrimary12').modal('toggle');
	}
	function showwallet(){
        //document.getElementById("defaultModalPrimary").style.display = "none";
        //$('#defaultModalPrimary').modal('hide');
        showwallet_popup();
    }
    function showwallet_popup(){
        $('body').addClass('modal-open');
		
        var totalAmount=document.getElementById("total_amount").value;
        document.getElementById("total_pay").innerHTML = totalAmount;
        var total_balance1=document.getElementById("total_balance1").value;
        //var amountfill1 = totalAmount-total_balance1;
        document.getElementById("amountfill1").value=totalAmount;
        document.getElementById("total_balance_to_pay").innerHTML = totalAmount;
        $('#wallet_popup').modal('toggle');
    }
    function showwallet1(){
        
        document.getElementById("defaultModalP").style.display = "none";
        $('#defaultModalP').modal('hide');
        showwallet_popup1();
    }
    function showwallet_popup1(){
        $('body').addClass('modal-open');

        var totalAmount=document.getElementById("custom_total_amount").value;
        document.getElementById("total_pay1").innerHTML = totalAmount;
        var total_balance1=document.getElementById("total_balance2").value;
        var amountfill1 = totalAmount-total_balance1;
        document.getElementById("amountfill").value=amountfill1;
        document.getElementById("total_balance_to_pay1").innerHTML = amountfill1;
        $('#wallet_popup1').modal('toggle');
    }
function check_balance(){
    var totalBalance2 = document.getElementById("total_balance2").value;
    var amountfill = document.getElementById("amountfill").value;
	
    if (totalBalance2 == 0 && amountfill != "") {
        swal("Insufficient Wallet Amount")
    }
    else {
        showpayment_popup();
    }
}
function check_balance1(){
    var wallet_money = parseInt(document.getElementById("total_balance1").value);
    var money_to_pay = parseInt(document.getElementById("amountfill1").value);
	var check_val= $('#wallete_amount_use').prop('checked');
	
	//alert(check_val + " d " + money_to_pay + "xfgdf" + wallet_money);
	//return false;
	
	if (wallet_money == 0 && money_to_pay != "") {
        swal("Insufficient Wallet Amount");
		
    }else {
		
		if (check_val==true) {
			if(wallet_money >= money_to_pay)
			{
				//alert("1");
				/* var amountfill1 = totalAmount-total_balance1;
				alert(amountfill1 + "d" + "w" + totalAmount + "ui" + total_balance1);
				document.getElementById("amountfill12").value=amountfill1;
				document.getElementById("total_balance_to_pay1").innerHTML = totalAmount; */
				document.getElementById("payment_status").value=0;
				var form$ = $("#paymentFrm");
				//submit form to the server
				form$.get(0).submit();
				
			}else{
				swal("Insufficient Wallet Amount");
				//alert("2");
				/* alert("2");
				var amountfill1 = total_balance1-totalAmount;
				document.getElementById("amountfill12").value=amountfill1;
				document.getElementById("total_balance_to_pay1").innerHTML = totalAmount; */
				//document.getElementById("payment_status").value=1;
				//$('#payment_popup').modal('toggle');
			}
	

		}else if(check_val==false){
			//alert("3");
			/* document.getElementById("amountfill12").value=totalAmount;
			document.getElementById("total_balance_to_pay1").innerHTML = totalAmount; */
			document.getElementById("payment_status").value=1;
			$('#payment_popup').modal('toggle');
		}   
		
        
    }
}

   $('#wallete_amount_use').click(function() {
    var check_val= $('#wallete_amount_use').prop('checked');
	var wallet_money = parseInt(document.getElementById("total_balance1").value);
    var money_to_pay = parseInt(document.getElementById("amountfill1").value);
	
    /* var money_to_pay=document.getElementById("total_amount").value;
    document.getElementById("total_pay").innerHTML = totalAmount;
    var wallet_money=document.getElementById("total_balance1").value; */
	
	//alert(check_val + " d " + money_to_pay + "xfgdf" + wallet_money);
	//return false;
	if (check_val==true) {
			if(wallet_money >= money_to_pay)
			{
				//alert("1");
				/* var amountfill1 = totalAmount-total_balance1;
				alert(amountfill1 + "d" + "w" + totalAmount + "ui" + total_balance1);
				document.getElementById("amountfill12").value=amountfill1;
				document.getElementById("total_balance_to_pay1").innerHTML = totalAmount; */
				document.getElementById("payment_status").value=0;
				var form$ = $("#paymentFrm");
				//submit form to the server
				form$.get(0).submit();
				
			}else{
				//alert("2");
				/* alert("2");
				var amountfill1 = total_balance1-totalAmount;
				document.getElementById("amountfill12").value=amountfill1;
				document.getElementById("total_balance_to_pay1").innerHTML = totalAmount; */
				document.getElementById("payment_status").value=1;
				$('#payment_popup').modal('toggle');
			}
	

		}else if(check_val==false){
			//alert("3");
			/* document.getElementById("amountfill12").value=wallet_money;
			document.getElementById("total_balance_to_pay1").innerHTML = money_to_pay; */
			document.getElementById("payment_status").value=1;
			$('#payment_popup').modal('toggle');
		} 
		
    /* if (check_val==true) {
    var amountfill1 = money_to_pay-wallet_money;
    document.getElementById("amountfill1").value=amountfill1;
    document.getElementById("total_balance_to_pay").innerHTML = amountfill1;
	alert(amountfill1 + " d " + wallet_money );
    }else if(check_val==false){
    document.getElementById("amountfill1").value=totalAmount;
    document.getElementById("total_balance_to_pay").innerHTML = totalAmount;
    }    */

})

    $('#wallete_amount_use1').click(function() {
    var check_val= $('#wallete_amount_use1').prop('checked');
    
    var totalAmount=document.getElementById("custom_total_amount").value;
    document.getElementById("total_pay1").innerHTML = totalAmount;
    var total_balance1=document.getElementById("total_balance2").value;
    if (check_val==true) {
    var amountfill1 = totalAmount-total_balance1;
    document.getElementById("amountfill").value=amountfill1;
    document.getElementById("total_balance_to_pay1").innerHTML = amountfill1;

    }else if(check_val==false){
    document.getElementById("amountfill").value=totalAmount;
    document.getElementById("total_balance_to_pay1").innerHTML = totalAmount;
    }   

})

	function usecard(num){
$("#cardholder").val($("#cardholder_data"+num).val());
$("#cardnum").val($("#cardnum_data"+num).val());
$("#expmonth").val($("#expmonth_data"+num).val());
$("#expyear").val($("#expyear_data"+num).val());

$('.enter-card-wrapper').slideDown();
}

function usecard(num){
$("#cardholder1").val($("#cardholder_data"+num).val());
$("#cardnum1").val($("#cardnum_data"+num).val());
$("#expmonth1").val($("#expmonth_data"+num).val());
$("#expyear1").val($("#expyear_data"+num).val());

$('.enter-card-wrapper').slideDown();
}

//set your publishable key
Stripe.setPublishableKey('pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt');
//	Stripe.setPublishableKey('pk_live_nxQI6OVkFFzTXtd6L2wcq7Kl');

//callback to handle the response from stripe
function stripeResponseHandler(status, response) {
if (response.error) {
//enable the submit button
$('#payBtn').removeAttr("disabled");
//display the errors on the form
// $('#payment-errors').attr('hidden', 'false');
$('#payment-errors').addClass('alert alert-danger');
$("#payment-errors").html(response.error.message);
} else {
var form$ = $("#paymentFrm");
//get token id
var token = response['id'];
//insert the token into the form
form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
//submit form to the server
form$.get(0).submit();
}
}

function stripeResponseHandler1(status, response) {
if (response.error) {
//enable the submit button
$('#payBtn1').removeAttr("disabled");
//display the errors on the form
// $('#payment-errors').attr('hidden', 'false');
$('#payment-errors1').addClass('alert alert-danger');
$("#payment-errors1").html(response.error.message);
} else {
var form$ = $("#paymentFrm1");
//get token id
var token = response['id'];
//insert the token into the form
form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
//submit form to the server
form$.get(0).submit();
}
}

$(document).ready(function () {
//on form submit
$("#paymentFrm").submit(function (event) {
//disable the submit button to prevent repeated clicks
$('#payBtn').attr("disabled", "disabled");

//create single-use token to charge the user
var exp_data1= $('#expmonth').val()
var fields = exp_data1.split('/');

var expmonth = fields[0];
var expyear = fields[1];


Stripe.createToken({
number: $('#cardnum').val(),
cvc: $('#cvv').val(),
exp_month: expmonth,
exp_year: expyear
}, stripeResponseHandler);

//submit from callback
return false;
});


$("#paymentFrm1").submit(function (event) {
//disable the submit button to prevent repeated clicks
$('#payBtn1').attr("disabled", "disabled");

//create single-use token to charge the user
var exp_data= $('#expmonth1').val()
var fields = exp_data.split('/');

var expmonth1 = fields[0];
var expyear1 = fields[1];


Stripe.createToken({
number: $('#cardnum1').val(),
cvc: $('#cvv1').val(),
exp_month: expmonth1,
exp_year: expyear1
}, stripeResponseHandler1);

//submit from callback
return false;
});
});		

</script>