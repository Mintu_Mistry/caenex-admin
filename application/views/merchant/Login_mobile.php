r <!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Bootstrap 4 Admin &amp; Dashboard Template">
	<meta name="author" content="Bootlab">
	<title>Caenex</title>
	<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin="">
	<style>
		body {
			opacity: 0;
		}
	</style>
	<script src="<?php echo base_url('assets/js/settings.js')?>"></script>
	<!-- END SETTINGS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-120946860-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120946860-6');
</script></head>

<body>
	<main class="main d-flex w-100 conexbg">
		<div class="container d-flex flex-column">
			<div class="corner"><img src="<?php echo base_url('assets/img/corner.png') ?>" alt="" class="img-fluid"></div>
			<div class="corner_logo"><img src="<?php echo base_url('assets/img/corner_logo.png')?>" alt="" class="img-fluid"></div>
			<div class="row h-100 mt-small">
				<div class="col-sm-10 col-md-8 col-lg-7 mx-auto d-table h-100">

						<div class="text-center mt-4">
							<h1 class="h2 loghed">Login</h1>
						</div>

						<div class="card">
							<div class="card-body">
									<div id="loginpage">
										<form method="post" name="myForm1" onsubmit="return validateForm1()">
											<div class="form-group">
												<div class="row">
													<div class="col-sm-1"></div>
													<div class="col-sm-4" style="margin-left:-20px; ">
												<select class="form-control input-sm" id="sel1" name="merchant_phonepin">

											        <option>Pincode</option>
											        <?php foreach ($country as $key => $value) { 
											        ?>
											        <option><?php echo '+'.''.$value['phonecode'].'     '.$value['name'];?>
											        </option>
											        <?php }?>
											      </select>
											  </div>
											  <div class="col-sm-7">
												<input class="form-control form-control-lg" type="text" name="mobile_number" id="mobile_number" placeholder="Enter your mobile number"  required="required" onkeypress="return autoMask(this,event, '############');">
											</div>
										</div>
									</div>
											
											<div class="text-center mt-3">
												<!-- <a href="#" class="btn btn-lg btn-primary logbtn"> Login</a> -->
												<button type="submit" name="submit" class="btn btn-lg btn-primary logbtn">Submit</button>
											</div>
											<div class="singupbox">Back To <a href="<?php echo base_url() ?>"> LogIn </a></div>
										</form>
									</div>
							</div>
							<p id="error_email1" style="text-align: center;color: red;font-size: 20px;">
								<?php
								if ($this->session->flashdata('incorrct')) {
									echo $this->session->flashdata('incorrct');
								}
								?>
							</p>

							<!-- <p style="text-align: center;color:green;font-size: 20px;">
								<?php
								if ($this->session->flashdata('corrct')) {
									echo $this->session->flashdata('corrct');
								}
								?>
							</p> -->
						</div>
						<div class="formbtext text-center">
							<h2> Do you have more than one business location?</h2>
							<h6 class="registertext"> If you are looking to advertise at multiple locations, please <u><b><a style="color: #d8e5db" href="<?php echo base_url('Sign_up') ?>"> register here </a></b></u></h6>
							<img src="<?php echo base_url('assets/') ?>img/border.jpg">
						</div>
				</div>
			</div>
		</div>
	</main>

				<!-- BEGIN primary modal -->
								<div class="modal fade" id="defaultModalsignup" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content" style="margin-top: 22%;">
											<div class="modal-header singmodel">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  								<span aria-hidden="true">&times;</span>
                								</button>
											</div>
											<div class="modal-body signmod_body">
												<div class="logo" style="margin-top: 15px;"><img src="<?php echo base_url('assets/img/sign_pop_logo.png')?>" alt="logo" class="img-fluid"> CAENEX </div>
												<h4 class="text-center"> We have sent a one-time verification code to your mobile number<p> Please enter it below.</p></h4>
												<p id="otp_incorrect" style="color: red"></p>
												<div class="otpsend">
												  <form class="form-inline" name="myForm" onsubmit="return validateForm()">
												  	<input type="hidden" name="otp_data" id="otp_data">
														<input type="text" class="form-control mb-2 col-lg-8 col-sm-8" id="inlineFormInputName2" placeholder="Enter OTP" name="otp"  required="">
													    <button type="submit" class="btn btn-primary sign_submt mb-2 col-lg-3 col-sm-3">Submit</button>
													    <div class="change_otp">
													    <div class="form-row">
													    	<div class="col-md-6">
													    <div class="chanemail">
													    	<a href="#" class="modal-header singmodel">
														<!-- <button style="float: left;margin-left: -40px;margin-top: 1px;font-size: 17px;" type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="reset_mesg()"> Change Email </button> -->
														</div>
													    </div>
													    		<!-- <div class="modal-header singmodel">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  								<span aria-hidden="true">&times;</span>
                								</button>
											</div> -->
												<div class="col-md-5">
													<div class="resotp">
													<button style="border-style: dashed;"><a href="javascript:void(0)" onclick="resend_otp()"> <i class="fas fa-sync"></i>Resend OTP </a></button>
												</div>
													    	</div>
													    </div>
													</div>
												</form>
											</div>
											</div>
											
										</div>
									</div>
								</div>

	<script src="<?php echo base_url('assets/js/app.js') ?>"></script>

</body>

</html>
<script type="text/javascript">

	function validateForm1() {
		 $('#defaultModalsignup').modal('show');
		  var email = document.forms["myForm1"]["mobile_number"].value;
		 	$.post("<?php echo base_url('merchant/mobile_otp') ?>",{mobile:email},function(res){
   			document.getElementById("otp_data").value=res;
   			})
           return false;
	}

		function validateForm() {
		  var x = document.forms["myForm"]["otp"].value;
		  var y =document.getElementById("otp_data").value;
		  if (x==y) {
		  	var email=document.getElementById("mobile_number").value;
		  	$.post("<?php echo base_url('merchant/Login_mobile_number') ?>",{mobile:email},function(res){
   				if (res==1) {
   					location.href = "http://ceanex.bonzaistaging.com/Dashboard";
   				}
   			})
				//return true;
		  }else{
		  	document.getElementById('otp_incorrect').innerHTML ="Please Enter Valid Otp";
		  	
		  }
		  	return false;
		 
		}


	function autoMask(field, event, sMask) {
    var KeyTyped = String.fromCharCode(getKeyCode(event));
    if (getKeyCode(event) == 8) {
        return
    }
    if (getKeyCode(event) == 0) {
        return
    }
    if (field.value.length == sMask.length && getKeyCode(event) == 13) {
        return true
    }
    // if (field.value.length == sMask.length && getKeyCode(event) != 13) {
    //     field.value = ""
    // }
    
    var targ = getTarget(event);
    keyCount = targ.value.length;
    if (keyCount == sMask.length) {
        return false
    }
    if (sMask.charAt(keyCount + 1) != "#" && sMask.charAt(keyCount + 1) != "A") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return false
    }
    if (sMask.charAt(keyCount) == "*")
        return true;
    if (sMask.charAt(keyCount) == KeyTyped) {
        return true
    }
    if (sMask.charAt(keyCount) == "#" && isNumeric(KeyTyped))
        return true;
    if (sMask.charAt(keyCount) == "A" && isAlpha(KeyTyped))
        return true;
    if (sMask.charAt(keyCount + 1) == "?") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return true
    }
    if (KeyTyped.charCodeAt(0) < 32)
        return true;
    return false
}


function getKeyCode(e) {
    if (e.srcElement) {
        return e.keyCode
    }
    if (e.target) {
        return e.which
    }
}

function getTarget(e) {
    if (e.srcElement) {
        return e.srcElement
    }
    if (e.target) {
        return e.target
    }
}

function isNumeric(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

function isAlpha(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}


	function resend_otp(){
			var email=document.getElementById("mobile_number").value;
			
			$.post("<?php echo base_url('Merchant/mobile_otp') ?>",{mobile:email},function(res){
				document.getElementById("otp_data").value=res;
				document.getElementById('otp_incorrect').innerHTML ="OTP has been resent to your mobile";
		  })
		}
</script>