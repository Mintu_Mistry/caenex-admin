<?php
$this->load->view('merchant/include/head');
?>

<body>
	<div class="wrapper">
		<?php

		
			$this->load->view('merchant/include/nev');
		
		?>

		<div class="main">
		<?php
				$this->load->view('merchant/include/header');
			?>

			<main class="content">
				<div class="container-fluid p-0">
					<div class="row">
						<div class="col-12 col-lg-12">
							<div class="tab">
							
								<?php
								if ($this->session->flashdata('add_user')) {
									
									?>									
									<div class="alert alert-success alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			               			<span aria-hidden="true">×</span>
			              			</button>
									<div class="alert-message">
									<?php echo $this->session->flashdata('add_user'); ?>
									</div>
								</div>
								
								<?php								
								}
								?>
							
								<ul class="nav nav-tabs nav-justified" role="tablist">
									<li class="nav-item"><a class="nav-link" id="world" href="#tab-1" data-toggle="tab" role="tab" aria-selected="true"> NexWorld </a></li>
									<li class="nav-item">
										<!-- <a class="nav-link" href="#tab-2" data-toggle="tab" role="tab" aria-selected="false"> NexPlay </a> -->
										<a class="nav-link newclr active" href="#tab-2" id="play" data-toggle="tab" role="tab" aria-selected="true"> NexPlay </a>
									</li>
								</ul>
								<div class="tab-content pad0 mt2">
									<div class="tab-pane" id="tab-1" role="tabpanel">
										<!-- <h4 class="tab-title">Default tabs</h4> -->
									<div class="NexWorld_Campaign text-right">
										<a href="<?php echo base_url('campaign_management_NexWorld') ?>"><button class="btn btn-info Nexw_btn"> Add New NexWorld Campaign </button></a>
									</div>
							<div class="card flex-fill">
								<table class="table table-striped my-0" id="datatables-basic">
									<thead class="coltab">
										<tr>
											<th> Campaign Logo </th>
											<th> Reward Type </th>
											<th> Location </th>
											<th> Start date </th>
											<th> End date </th>
											<!--<th> Coupon Code </th>-->
											<th> Status </th>
											<th> Action </th>
										</tr>
									</thead>
									<tbody>
										<?php
										$count1=0;
										foreach ($nexworld as $key => $value) {
											
											if ($value['type']==2) {
												$count1++;
										?>
										<tr>
											<td>
												<div class="compimg">
													<?php
													

													if (@$value['coupon_image']) {
														$arr = explode(".",$value['coupon_image']);
														$extension = end($arr);
														if ($extension =='mp4')
														{ ?>
															<video alt="campaign video" class="img-responsive" width="100" height="100" controls>
														  <source src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" type="video/mp4">
														  <source src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" type="video/ogg">
														  Your browser does not support the video tag.
														</video>
															<?php 
														}else{ ?>
															<img src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" alt="campaign logo" class="img-responsive">
															<!-- <video src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" alt="campaign video" class="img-responsive"> -->
																<?php

														}

														?>
														
														<?php
													}else{
														?>
													<img src="<?php echo base_url('assets/img/icons8-us-dollar-64.png')?>" alt="campaign logo" class="img-responsive">	
													<?php
													}
													?>
												</div>
											</td>
											<td class="d-none d-xl-table-cell">
											<?php
											if ($value['type']==1) {
												echo "Coupon";
											}elseif ($value['type']==2) {
												echo "Dollar Amount";
											}
											?>
											</td>
											<td class="d-none d-xl-table-cell">
											<?php
											
												echo $value['location'];
											
											?>
											</td>
											<td class="d-none d-xl-table-cell">
											<?php if(!empty($value['start_date']))
												{
													echo date('m-d-y', strtotime($value['start_date'])); 
												}else{
													echo "No Date";
												}?>
										
												
											</td>
											<td class="d-none d-xl-table-cell">
											<?php if(!empty($value['end_date']))
												{
													echo date('m-d-y', strtotime($value['end_date'])); 
												}else{
													echo "No Date";
												}?>
												
											</td>
											<!--<td class="d-none d-xl-table-cell">
											<?php
											if (@$value['coupon_code']) {
												echo $value['coupon_code'];
											}else{
												echo "-";
											}
											?>												
											</td>-->
											<td>
											<?php
											if ($value['add_status']==1) {
												echo "Active";
											}elseif ($value['add_status']==0 OR $value['add_status']==2) {
												echo "Inactive";
											}
											?>
											</td>
											<td class="cntr">
												<?php
											if ($value['type']==1) {
												$tbl='tbl_nexworld_coupon';
												?>
												
											<a href="javascript:void('')" onclick="delete_data(<?php echo $value['id']; ?>,'<?php echo $tbl ?>','<?php echo $count1 ?>')"><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												
											<?php }elseif ($value['type']==2) {
												$tbl='tbl_nexworld_cash';
												?>
											
											<a href="javascript:void('')" onclick="delete_data(<?php echo $value['id']; ?>,'<?php echo $tbl ?>','<?php echo $count1 ?>')"><i class="align-middle far fa-fw fa-trash-alt"></i></a>
											
											<?php }?>
												
												</td>
										</tr>
										
										<?php 	
										}}
										?>
										
									</tbody>
								</table>
							</div>
						
									
									</div>
									<div class="tab-pane active" id="tab-2" role="tabpanel">
										<div class="card flex-fill">
										<div class="NexWorld_Campaign text-right">
											<a href="<?php echo base_url('campaign_management_NexPlay') ?>">
										<button class="btn Nexpl_btn"> Add New NexPlay Campaign </button>
									</a>
									</div>
								<table class="table table-striped my-0" id="datatables-basic">
									<thead class="coltab">
										<tr>
											<th> Game Type </th>
											<th class="d-none d-xl-table-cell"> Ad Type </th>
											<th class="d-none d-xl-table-cell"> No. of Impressions Left </th>
											<th>Video Clicked</th>
											<th>Video Full Streamed</th>
											<th>Payout</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									<?php 
									$count1 =0;
									foreach($nexplay as $nplay)
									{
										$count1++;
										$tbl='tbl_nexplay_caenex_game';
									?>
										<tr>
											<td><?=$nplay['game_name']?></td>
											<td class="d-none d-xl-table-cell"><?=$nplay['ad_type']?></td>
											<td class="d-none d-xl-table-cell"><?=$nplay['total_number_of_impressions']?>/<?=$nplay['total_number_of_impressions']-($nplay['total_win']+$nplay['total_loose'])?></td>
											<td class="d-none d-xl-table-cell"><?=$nplay['video_clicked']?></td>
											<td class="d-none d-xl-table-cell"><?=$nplay['video_full_streamed']?></td>
											<td class="d-none d-xl-table-cell">$<?=$nplay['total_payout']?></td>
											<td class="cntr">
											<?php
											if ($nplay['add_status']==1) {
												echo "Active";
											}elseif ($nplay['add_status']==0 || $nplay['add_status']==2) {
												echo "Inactive";
											}
											?>
											</td>
											<td class="cntr">
											<a href="javascript:void('')" onclick="delete_data(<?php echo $nplay['id']; ?>,'<?php echo $tbl ?>','<?php echo $count1 ?>')"><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												</td>
										</tr>
									<?php }?>
										
									</tbody>
								</table>
							</div>
									</div>
									
								</div>
							</div>
						</div>
</div>
<!-- ------------  End Tab data -------------- -->

					

				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>

	<script src="js\app.js"></script>

	<script>
		$(function() {
			$("#datetimepicker-dashboard").datetimepicker({
				inline: true,
				sideBySide: false,
				format: "L"
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>
	<script>
		$(function() {
			// Datatables basic
			$("#datatables-basic").DataTable({
				//responsive: true
				"aaSorting": []
			});
			// Datatables with Buttons
			var datatablesButtons = $("#datatables-buttons").DataTable({
				responsive: true,
				lengthChange: !1,
				buttons: ["copy", "print"]
			});
			datatablesButtons.buttons().container().appendTo("#datatables-buttons_wrapper .col-md-6:eq(0)");
			// Datatables with Multiselect
			var datatablesMulti = $("#datatables-multi").DataTable({
				responsive: true,
				select: {
					style: "multi"
				}
			});
			});
	function delete_data(id,tbl,count){

		var result = confirm("Do you want to delete this entry? Click OK if yes");
		//var tbl='tbl_user';
		var row='id';
		if (result) {
			$("#"+count+"").fadeOut(100);
	   		$.post("<?php echo base_url('Merchant/delete_data') ?>",{id:id,tbl:tbl,row:row},function(res){
	   		
			if(res =="Nexworld")
				{
					window.location.replace("<?php echo base_url('Merchant/campaign_management') ?>")
				}else if(res =="Nexplay"){
					location.reload;
				}
	   		})
		}
	}
	
		
	</script>

</body>

</html>