<?php
$this->load->view('merchant/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('merchant/include/nev');
?>

<div class="main">
<?php
$this->load->view('merchant/include/header');
?>

			<main class="content">
				<div class="container-fluid p-0">
						<div class="row">
							<div class="col-12 col-lg-12">
							<div class="tab tabstyl">
								<ul class="nav nav-tabs nav-justified" role="tablist">
								<!-- 	<li class="nav-item"><a class="nav-link active" href="#tab-1" data-toggle="tab" role="tab" aria-selected="true"> Change Password </a></li> -->
									<li class="nav-item"><a class="nav-link active" href="#tab-4" data-toggle="tab" role="tab" aria-selected="false"> Caenex Value to Merchant </a></li>

									<li class="nav-item"><a class="nav-link" href="#tab-2" data-toggle="tab" role="tab" aria-selected="false"> Terms & Condition </a></li>
									<li class="nav-item"><a class="nav-link" href="#tab-3" data-toggle="tab" role="tab" aria-selected="false"> Privacy Policy </a></li>
									
								</ul>
								<div class="tab-content pad0 mt2">
									<!-- <div class="tab-pane active" id="tab-1" role="tabpanel">
										
										<div class="card flex-fill">
																						<div class="tab-content"> 
												<div class="row justify-content-md-center">
													<div class="col-md-8">
														<div class="card-body passbox">
															
																<div class="webheading"> PASSWORD </div>
																<form>
																	<div class="form-group">
																		<label> Current Password </label>
																		<input class="form-control form-control-lg" type="password" name="password" placeholder="">
																		<small><a href="pages-reset-password.html">Forgot password?</a></small>
																	</div>
																	<div class="form-group">
																		<label>New Password</label>
																		<input class="form-control form-control-lg" type="password" name="password" placeholder="">
																	</div>
																	<div class="form-group">
																		<label>Verify Password</label>
																		<input class="form-control form-control-lg" type="password" name="password" placeholder="">
																	</div>
																	<div class="text-center mt-3">
																		<div class="btn btn-lg btn-primary"> Save Changes </div>
																	</div>
																</form>
															
														</div>
													</div>
												</div>
											</div>
										</div>
									</div> -->
									<div class="tab-pane active" id="tab-4" role="tabpanel">
										<div class="card flex-fill">
											<div class="tab-content"> 
												<div class="webheading"> INTRODUCTION </div>
												<p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donepede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede molls pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>

												<div class="webheading"> ADVERTISING  </div>
												<p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donepede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede molls pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>

												<div class="webheading"> SALES PROMOTION AND PUBLICITY </div>
												<p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donepede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede molls pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>

											</div>

										</div>
									</div>
									<div class="tab-pane" id="tab-2" role="tabpanel">
										<div class="card flex-fill">
											<div class="tab-content"> 
												<div class="hdtxt">
													<p style="white-space: pre-wrap;"><?php echo $T_C['details'] ?></p>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="tab-3" role="tabpanel">
										<div class="card flex-fill">
											<div class="tab-content">
												<div class="hdtxt">
													<p style="white-space: pre-wrap;"><?php echo $P_P['details'] ?></p>
													
													</div>
													
												</div>
										</div>
									</div>
									

								</div>
							</div>
						</div>
</div>
<!-- ------------  End Tab data -------------- -->

					

				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>

	<script src="js\app.js"></script>

	<script>
		$(function() {
			$("#datetimepicker-dashboard").datetimepicker({
				inline: true,
				sideBySide: false,
				format: "L"
			});
		});
	</script>
	<script>
		$(function() {
			// Line chart
			new Chart(document.getElementById("chartjs-dashboard-line"), {
				type: "line",
				data: {
					labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Sales ($)",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.primary,
						data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
					}, {
						label: "Orders",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.tertiary,
						borderDash: [4, 4],
						data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					tooltips: {
						intersect: false
					},
					hover: {
						intersect: true
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						xAxes: [{
							reverse: true,
							gridLines: {
								color: "rgba(0,0,0,0.05)"
							}
						}],
						yAxes: [{
							ticks: {
								stepSize: 500
							},
							display: true,
							borderDash: [5, 5],
							gridLines: {
								color: "rgba(0,0,0,0)",
								fontColor: "#fff"
							}
						}]
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			// Pie chart
			new Chart(document.getElementById("chartjs-dashboard-pie"), {
				type: "pie",
				data: {
					labels: ["Direct", "Affiliate", "E-mail", "Other"],
					datasets: [{
						data: [2602, 1253, 541, 1465],
						backgroundColor: [
							window.theme.primary,
							window.theme.warning,
							window.theme.danger,
							"#E8EAED"
						],
						borderColor: "transparent"
					}]
				},
				options: {
					responsive: !window.MSInputMethodContext,
					maintainAspectRatio: false,
					legend: {
						display: false
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>

</body>

</html>