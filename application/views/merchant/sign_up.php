<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Bootstrap 4 Admin &amp; Dashboard Template">
	<meta name="author" content="Bootlab">
	<title>Caenex</title>
	<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin="">
	<style>
		body {
			opacity: 0;
		}
		#error_email{
			display: none
		}
	.upladimg {
    height: 20px;
    width: 24px;
	}
	</style>
	<script src="<?php echo base_url('assets/js/settings.js')?>"></script>
	<!-- END SETTINGS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-120946860-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120946860-6');
</script></head>

<body id="my_body">
	<main class="main d-flex w-100 conexbg">
		<div class="container d-flex flex-column">
			<div class="corner"><img src="<?php echo base_url('assets/img/corner.png')?>" alt="" class="img-fluid"></div>
			<div class="corner_logo"><img src="<?php echo base_url('assets/img/corner_logo.png')?>" alt="" class="img-fluid"></div>
			<div class="row h-100">
				<div class="col-sm-10 col-md-8 col-lg-7 mx-auto d-table h-100">
					<div id="signupbox">
						<div class="text-center mt-4">
							<h1 class="h2 loghed">Merchant Signup</h1>
						</div>
						<ul class="nav nav-tabs nav-justified tabboxst" role="tablist">
									<li class="nav-item"><a class="nav-link tabstyle leftflot active" href="#" id="tab1" data-toggle="tab" role="tab" aria-selected="false">1</a></li>

									<li class="nav-item"><a class="nav-link tabstyle" href="#" data-toggle="tab" role="tab" aria-selected="true" id="tab2">2</a></li>

									<li class="nav-item"><a class="nav-link tabstyle rightflot" href="#" data-toggle="tab" role="tab" aria-selected="false" id="tab3">3</a></li>
								</ul>
							
									<div class="tab">
								<div id="signup_page">
								<div class="tab-content">
									<div class="tab-pane active" id="tab-1" role="tabpanel">
										
										<form autocomplete="off" name="myForm1" onsubmit="return validateForm1()">
											<div class="form-group">
												<div class="signupimg">
												<img src="<?php echo base_url('assets/img/signup_company.jpg') ?>">
												<input class="form-control form-control-lg" type="text" name="name" placeholder="Full Name" required>
												</div>
											</div>
											<div class="form-group">
												<div class="signupimg">
												<img src="<?php echo base_url('assets/img/signup_user.jpg') ?>">
												<input class="form-control form-control-lg" type="text" name="business_name" placeholder="Company Name" required>
											</div>
											</div>

											<!--<div class="form-group">
												<div class="signupimg">
												<i class="fa fa-phone-square" style="font-size: 23px;position: absolute;margin-top: 8px;color: white;background: #8e8888;"></i>

												<div class="row">
													<div class="col-sm-1"></div>
													<div class="col-sm-4" style="margin-left:-20px; ">
												<select class="form-control input-sm" id="sel1" name="merchant_phonepin">

											        <option>Pincode</option>
											        <?php foreach ($country as $key => $value) { 
											        ?>
											        <option><?php echo '+'.''.$value['phonecode'].'     '.$value['name'];?>
											        </option>
											        <?php }?>
											      </select>
											  </div>
											  <div class="col-sm-7">
												<input  class="form-control form-control-lg" type="text" name="merchant_phone" onkeypress="return autoMask(this,event, '############');" placeholder="Enter Mobile Number" required>
											</div>
										</div>
										</div>
											</div>-->
											<p id="error_email" style="color: red;margin-left: 10px;margin-bottom: -1px;">Email Already Exists</p>
											<p id="error_email1" style="color: red;margin-left: 10px;margin-bottom: -1px;"></p>
											<div class="form-group">
												<div class="signupimg">
												
												<img src="<?php echo base_url('assets/img/signup_envelop.png') ?>">
												<input class="form-control form-control-lg" type="email" name="email" placeholder="Email" id="email" onkeyup="check_email(this.value)" autocomplete=" " required > 
											</div>
											</div>
												<span id="invalidPassword" style="display: none;color: red;margin-left: 35px;">Password must be 8 digit long including alpha-numeric-lower_case-upper_case-special_char</span>
											<div class="form-group">
											<div class="signupimg">
												<img src="<?php echo base_url('assets/img/signup_lock.jpg') ?>">

												<input class="form-control form-control-lg" type="password" name="password" placeholder="Password" id="password" onload="password_value()" autocomplete="new-password" autocapitalize="off" autocorrect="off" required>

												<!-- <input type="password" class="whsOnd zHQkBf" jsname="YPqjbf" autocomplete="new-password" spellcheck="false" tabindex="0" aria-label="Password" name="Passwd" autocapitalize="off" autocorrect="off" dir="ltr" data-initial-dir="ltr" data-initial-value=""> -->

											</div>
												<div class="singupbox text-right"> Existing user? <a href="<?php echo base_url() ?>"> Login </a></div>
											</div>
											<!-- <a href="#" class="btn btn-lg btn-primary logbtn" data-toggle="modal" data-target="#defaultModalsignup"> Next </a> -->
											<button type="submit" id="next1" class="btn btn-lg btn-primary logbtn">Next</button>

										</form>

									
									</div>
									<div class="tab-pane" id="tab-2" role="tabpanel">
										<form autocomplete="off"	 name="address_form1"  onsubmit="return address_form()" >
											<div class="form-row">
												<div class="form-group col-md-6">
													<input type="text" class="form-control" id="" placeholder="Street address" name="street" required="">
												</div>
												<div class="form-group col-md-6">
													<input type="text" oninput="validate_only_alphabates_allowed(this.id)" class="form-control" id="city" placeholder="City" name="city" required="">
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-md-6">
													<input type="text" oninput="validate_only_alphabates_allowed(this.id)" class="form-control" id="state" placeholder="State" name="state" required>
												</div>
												<div class="form-group col-md-6">
													<input type="text" class="form-control" id="zip" oninput="validate_only_alphaNumeric_max6_allowed(this.id)"placeholder="Zip" maxlength="5" minlength="5" name="zip" required>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-md-12">
													<input type="text" oninput="validate_only_alphabates_allowed(this.id)" class="form-control" id="inputEmail4" placeholder="Country" name="country" required>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-md-6 imgicopos">
													<input type="text" class="form-control" id="" placeholder="Enter tax ID" name="texid"  onkeypress="return autoMask(this,event, '################################');"  required>
													<!-- oninput = "validate_taxid(this.id)" -->
													<span class="icobox">
														<input type="file" name="test_id_img" class="align-middle mr-2 far fa-fw fa-image form-control-file upladimg" id="exampleFormControlFile1" onchange="readURL1(this)" required="">
														<i class="fa fa-question-circle fa-3" aria-hidden="true" title="please upload your document so we can verify that you are authenticate business holder
"></i>
													</span>
												</div>
												<div class="form-group col-md-6" id="img-preview1" style="display: none;">
													<div class="uploaed_imgbox" >
														<span class="uploadimgbox">
															<img class="img-preview1"  src="" style="height: 46px;width: 46px;" accept="image/*,.pdf" required>
													<span class="crossd" onclick="fadeout('img-preview1')">x</span>
												</span>
													</div>
												</div>
											</div>

											<button type="submit" class="btn btn-lg btn-primary logbtn"> Next </button>
										</form>
									</div>
									<div class="tab-pane" id="tab-3" role="tabpanel">
										<h4 class="tab-title hedsign3">  To participate in NexWorld or NexPlay advertising, please provide payment details below</h4>
										<form method="post" action="<?php echo base_url('Merchant/Merchant_Signup') ?>">
											<div class="form-row">
												<div class="form-group col-md-12">
													<label class="cardhd"> card number</label>
													<input type="text" maxlength="16" minlength="16" name="card_number" class="form-control" id="card_number" placeholder="Enter card number" required="required">
												</div>
											</div>
											<div class="form-row">

												<div class="form-group col-md-4">
													<label class="cardhd"> Exp. date </label>
													<div class="form-row" style="margin-left: auto;">
														<div class="col-md-6 pad0 rborder">
															<select name="month" id="month"
                            class="demoSelectBox" required>
							<option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
							<option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select> <select name="year" id="year"
                            class="demoSelectBox" required>
                            
                            <option value="21">2021</option>
                            <option value="22">2022</option>
                            <option value="23">2023</option>
                            <option value="24">2024</option>
                            <option value="25">2025</option>
                            <option value="26">2026</option>
                            <option value="27">2027</option>
                            <option value="28">2028</option>
                            <option value="29">2029</option>
                            <option value="30">2030</option>
							<option value="31">2031</option>
                            <option value="32">2032</option>
                            <option value="33">2033</option>
                            <option value="34">2034</option>
                            <option value="35">2035</option>
                        </select>
						<input type="hidden" id="expirymonth" name="expirymonth">
						
															<!--<input type="text" class="form-control" name="month_year" id="expmonth" maxlength="7" minlength="7" placeholder="Month / Year"  required>-->
														</div>
													</div>
												</div>
												<div class="form-group col-md-3 offset-md-2">
													<label class="cardhd"> CVV </label>
													<input type="text" maxlength="3" class="form-control" id="cvv"  placeholder="CVV" minlength="3" maxlength="3" name="cvv" required="required" >
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-md-6">
													<label class="cardhd"> Card holder name </label>
													<input type="text" class="form-control" id="card_holder_name" placeholder="Enter cardholder name" name="card_holder_name" required="required" oninput ="validate_only_alphabates_allowed(this.id)">
												</div>
												<div class="form-group col-md-6">
													<label class="cardhd"> Card nick name (Optional) </label>
													<input type="text" class="form-control" id="nick_name" placeholder="Enter card nickname" name="nick_name" oninput ="validate_only_alphabates_allowed(this.id)">
												</div>
											</div>
											<!-- <a href="#" class="btn btn-lg btn-primary logbtn"> Submit </a> -->
											<div class="form-row">
												<div class="form-row ml-4 mb-4">	
											 	 <label class="checkcon custom-checkbox">
													<input type="checkbox" class="custom-control-input" checked="" required="">
													<span class="custom-control-label" ></span>
												</label>
												<label class="form-check-label" for="exampleCheck1">Securely Save Card Details</label>
											</div>
												
											</div>
											
											<button class="btn btn-lg btn-primary logbtn">Submit</button>
											<!-- <input type="submit"class="btn btn-lg btn-primary logbtn" name="submit"> -->
										</form>
									</div>
								</div>
								</div>
							</div>



										<!-- BEGIN primary modal -->
								<div class="modal fade" id="defaultModalsignup" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content" style="margin-top: 22%;">
											<div class="modal-header singmodel">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  								<span aria-hidden="true">&times;</span>
                								</button>
											</div>
											<div class="modal-body signmod_body">
												<div class="logo" style="margin-top: 15px;"><img src="<?php echo base_url('assets/img/sign_pop_logo.png')?>" alt="logo" class="img-fluid"> CAENEX </div>
												<h4 class="text-center"> We have sent a one-time verification code to your email<p> Please enter it below.</p></h4>
												<p id="otp_incorrect" style="color: red"></p>
												<div class="otpsend">
												  <form class="form-inline" name="myForm" onsubmit="return validateForm()">
												  	<input type="hidden" name="otp_data" id="otp_data">
														<input type="text" class="form-control mb-2 col-lg-8 col-sm-8" id="inlineFormInputName2" placeholder="Enter OTP" name="otp"  required="">
													    <button type="submit" class="btn btn-primary sign_submt mb-2 col-lg-3 col-sm-3">Submit</button>
													    <div class="change_otp">
													    <div class="form-row">
													    	<div class="col-md-6">
													    <div class="chanemail">
													    	<a href="#" class="modal-header singmodel">
														<button style="float: left;margin-left: -40px;margin-top: 1px;font-size: 17px;" type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="reset_mesg()"> Change Email </button>
														</div>
													    </div>
													    		<!-- <div class="modal-header singmodel">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  								<span aria-hidden="true">&times;</span>
                								</button>
											</div> -->
												<div class="col-md-5">
													<div class="resotp">
													<button style="border-style: dashed;"><a href="javascript:void(0)" onclick="resend_otp()"> <i class="fas fa-sync"></i>Resend OTP </a></button>
												</div>
													    	</div>
													    </div>
													</div>
												</form>
											</div>
											</div>
											
										</div>
									</div>
								</div>
												<!-- END primary modal -->
						
						<div class="formbtext text-center">
							<h2> Do you have more than one business location?” </h2>
							<h6> If you are looking to advertise at various partner locations of yours, please <u><b><a style="color: #d8e5db" href="<?php echo base_url('Sign_up') ?>"> register here </a></b></u>
							<img src="<?php echo base_url('assets/') ?>img/border.jpg">
						</div>
				</div>
			</div>
			</div>
		</div>
	</main>

	<script src="<?php echo base_url('assets/js/app.js')?>"></script>


	<script>
		function password_value(){
			document.getElementById('password').value= '';
		}
		var arr = Array();
		var input_value_array = Array();

		$(document).ready(function(){
			$("#year").blur(function(){
				var Year = $('#year').val();
				if(Year != ''){
					var thisYear =  new Date().getFullYear().toString().substr(-2);
					// console.log(thisYear);
					// var d = new Date();
					// console.log(d.getMonth());
					if(thisYear == Year){
						var d = new Date();
  						var month = d.getMonth();
  						var min = month + 1;
  						$('#month').attr('min', min);
					}else if(thisYear < Year ){
						var min = 1;
  						$('#month').attr('min', min);
					}
					
				}
			});
			var thisYear =  new Date().getFullYear().toString().substr(-2);
			$('#year').attr('min', thisYear);
			
		  
		})

		// $( "#imageDiv" ).fadeOut( "slow", function() {
		    
		//   });
		// $( "#imageDiv2" ).fadeOut( "slow", function() {
		    
		//   });
		// $('#OpenImgUpload').click(function(){ $('#imgupload').trigger('click'); });
		// $('#OpenImgUpload2').click(function(){ $('#imgupload2').trigger('click'); });

		// function readUrl(input) {

		// 	if (input.files && input.files[0]) {
		// 	let reader = new FileReader();
		// 	reader.onload = e => {
		// 	let imgData = e.target.result;
		// 	let imgName = input.files[0].name;
		// 	console.log(imgName);
		// 	console.log(imgData);
		// 	$('#showSelectedImage').attr('src', e.target.result);
		// 	$( "#imageDiv" ).fadeIn( "slow", function() {
		//     // Animation complete.
		//   });	
		// 	input.setAttribute("data-title", imgName);
		// 	};
		// 	reader.readAsDataURL(input.files[0]);
		// 	}
		// }

		// function readUrl2(input) {

		// 	if (input.files && input.files[0]) {
		// 	let reader = new FileReader();
		// 	reader.onload = e => {
		// 	let imgData2 = e.target.result;
		// 	let imgName2 = input.files[0].name;
		// 	$('#showSelectedImage2').attr('src', e.target.result);
		// 	$( "#imageDiv2" ).fadeIn( "slow", function() {
		//     // Animation complete.
		//   });	
		// 	input.setAttribute("data-title", imgName);
		// 	};
		// 	reader.readAsDataURL(input.files[0]);
		// 	}

		// }

		// $( "#removeSelectedImage" ).click(function() {
  // 		$( "#imageDiv" ).fadeOut( "slow", function() {
		    
		//   });
  // 		$('#showSelectedImage').attr('src', 'dummy.jpg');
		// });

		// $( "#removeSelectedImage2" ).click(function() {
  // 		$( "#imageDiv2" ).fadeOut( "slow", function() {
		    
		//   });
  // 		$('#showSelectedImage2').attr('src', 'dummy.jpg');
		// });

		function autoMask(field, event, sMask) {
    var KeyTyped = String.fromCharCode(getKeyCode(event));
    if (getKeyCode(event) == 8) {
        return
    }
    if (getKeyCode(event) == 0) {
        return
    }
    if (field.value.length == sMask.length && getKeyCode(event) == 13) {
        return true
    }
    // if (field.value.length == sMask.length && getKeyCode(event) != 13) {
    //     field.value = ""
    // }
    
    var targ = getTarget(event);
    keyCount = targ.value.length;
    if (keyCount == sMask.length) {
        return false
    }
    if (sMask.charAt(keyCount + 1) != "#" && sMask.charAt(keyCount + 1) != "A") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return false
    }
    if (sMask.charAt(keyCount) == "*")
        return true;
    if (sMask.charAt(keyCount) == KeyTyped) {
        return true
    }
    if (sMask.charAt(keyCount) == "#" && isNumeric(KeyTyped))
        return true;
    if (sMask.charAt(keyCount) == "A" && isAlpha(KeyTyped))
        return true;
    if (sMask.charAt(keyCount + 1) == "?") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return true
    }
    if (KeyTyped.charCodeAt(0) < 32)
        return true;
    return false
}


function getKeyCode(e) {
    if (e.srcElement) {
        return e.keyCode
    }
    if (e.target) {
        return e.which
    }
}

function getTarget(e) {
    if (e.srcElement) {
        return e.srcElement
    }
    if (e.target) {
        return e.target
    }
}

function isNumeric(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

function isAlpha(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

function delete_data(id,tbl,row){
	var result = confirm("Are You Sure");
	if (result) {
		$("#saved_card"+id+"").fadeOut(100);
   	$.post("<?php echo base_url('merchant/delete') ?>",{id:id,tbl:tbl,row:row},function(res){
   		
   	})
	}
}

		function validateForm() {
		  var x = document.forms["myForm"]["otp"].value;
		  var y =document.getElementById("otp_data").value;
		  if (x==y) {
				var element = document.getElementById("tab1");
		  		element.classList.remove("active");
		  		var element1 = document.getElementById("tab2");
		  		element1.classList.add("active");

		  		var element2 = document.getElementById("tab-1");
		  		element2.classList.remove("active");
		  		var element3 = document.getElementById("tab-2");
		  		element3.classList.add("active");
		 		$('#defaultModalsignup').modal('hide');
		  		
		  		return false;
		  }else{
		  	document.getElementById('otp_incorrect').innerHTML ="Please Enter Valid Otp";
		  	
		  	return false;
		  }
		 
		}

		function validateForm1() {
		  var name = document.forms["myForm1"]["name"].value;
		  var business_name = document.forms["myForm1"]["business_name"].value;
		  var email = document.forms["myForm1"]["email"].value;
		  var password = document.forms["myForm1"]["password"].value;
		  /* var merchant_phone = document.forms["myForm1"]["merchant_phone"].value;
		  var merchant_phonepin=document.forms["myForm1"]["merchant_phonepin"].value; */
		  
          // let regex = /^[A-Za-z]\w{7,30}$/;
          let email_pettern=/^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/;
          let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
          if (email_pettern.test(email)) {
          	document.getElementById('error_email1').innerHTML = '';
          if (regex.test(password)) {
              document.getElementById('invalidPassword').style.display = 'none';
                      $('#defaultModalsignup').modal('show');
		  	$.post("<?php echo base_url("Merchant/setsession_data1") ?>",{name:name,business_name:business_name,email:email,Password:password},function(res){
		  	document.getElementById("otp_data").value=res;
		  })
           }else{
            document.getElementById('invalidPassword').style.display = 'block';

        }
    	}else{
    		document.getElementById('error_email1').innerHTML = 'Please Enter Valid Email Format';
    	}
        return false;  

    
		 
			 // return false;
	}

			function readURL1(input) {
			  if (input.files && input.files[0]) {
			    var reader = new FileReader();

			    reader.onload = function(e) {
			      $('.img-preview1').attr('src', e.target.result);
			      $("#img-preview1").css("display", "block");
			    }


			    reader.readAsDataURL(input.files[0]);
			  }
			}

		function address_form() {
		  var texid = document.forms["address_form1"]["texid"].value;
		  var street = document.forms["address_form1"]["street"].value;
		  var city = document.forms["address_form1"]["city"].value;
		  var zip = document.forms["address_form1"]["zip"].value;
		  var country = document.forms["address_form1"]["country"].value;
		  var state = document.forms["address_form1"]["state"].value;
		  

		   $.post("<?php echo base_url("Merchant/setsession_data2") ?>",{texid:texid,street:street,city:city,zip:zip,country:country,state:state},function(res){

		  })
		  // if (texid) {
				var element = document.getElementById("tab2");
		  		element.classList.remove("active");
		  		var element1 = document.getElementById("tab3");
		  		element1.classList.add("active");

		  		var element2 = document.getElementById("tab-2");
		  		element2.classList.remove("active");
		  		var element3 = document.getElementById("tab-3");
		  		element3.classList.add("active");		
		  		return false;
		  // }else{
		  // 	document.getElementById('otp_incorrect').innerHTML ="Please Enter Valid Otp";
		  	
		  // 	return false;
		  // }
		 
		}

		function check_email(email){
			
			 $.post("<?php echo base_url("Merchant/check_email") ?>",{email:email},function(res){
			 	if (res) {
			 		// alert("Email Already Exists");
			 		document.getElementById("next1").disabled = true;
			 		document.getElementById("error_email").style.display = 'block';
			 	}else{
			 		
			 		document.getElementById("next1").disabled = false;
			 		document.getElementById("error_email").style.display = 'none';
			 	}

		  })
			
		}

		function resend_otp(){
			var email=document.getElementById("email").value;
			
			$.post("<?php echo base_url("Merchant/resend_otp") ?>",{email:email},function(res){
				document.getElementById("otp_data").value=res;
				document.getElementById('otp_incorrect').innerHTML ="OTP has been resent to your E-mail";
		  })
		}

		function validate_only_alphabates_allowed(input_id){
			let input_value = document.getElementById(input_id).value;
			let regExp = /^[a-z A-Z]+$/;
			if (regExp.test(input_value)) {
			
			}else{
			
			document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
		
			}
		}

		function validate_only_alphaNumeric_max6_allowed(input_id){
			let input_value = document.getElementById(input_id).value;
			let regExp = /^[0-9]+$/;
			if (regExp.test(input_value)) {
			
			input_value_array = input_value;
			if(input_value_array.length > 5){
				document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
			}

			}else{
			
			document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
		

			}
		}

		function Validate_Only_Numeric_Input(input_id){
			let input_value = document.getElementById(input_id).value;
			let regExp = /^[0-9]+$/;
			if (regExp.test(input_value)) {
			
			}else{
			document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);

			}
		}
		function fadeout(id){
		$("#" + id).fadeOut( "slow", function() {   
		});
		}

		function reset_mesg(){
			document.getElementById('otp_incorrect').innerHTML ="";
			document.getElementById('inlineFormInputName2').value ="";
			
		}

		function validate_taxid(input_id){
			
			let input_value = document.getElementById(input_id).value;
			
			// let regExp = /^9[0-9][0-9]-[70-88]?[90-92]?[94-99]?-[0-9][0-9][0-9][0-9]/;
			if(input_value.length == 3 || input_value.length == 6){
					document.getElementById(input_id).value += '-';
				}else if(input_value.length > 11){
					document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
				}
				if(input_value.length == 1){
					
					let regExp = /^9/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 2){
					
					let regExp = /^9[0-9]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 3){
					
					let regExp = /^9[0-9][0-9]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 4){
					
					let regExp = /^9[0-9][0-9]-/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 5){
					
					arr = input_value.split("-");
					
					let regExp = /^9[0-9][0-9]-[7-9]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}

					
					
				}
				if(input_value.length == 6){
					
					// let regExp = /^9[0-9][0-9]-[7-8][0-8]/;
					// if (regExp.test(input_value)) {
				
						
					// }else{
					// 	document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					// }
					if(arr[1] == 7){
						
						let regExp = /^9[0-9][0-9]-[7][0-9]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
					if(arr[1] == 8){
						let regExp = /^9[0-9][0-9]-[8][0-8]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
					if(arr[1] == 9){
						let regExp = /^9[0-9][0-9]-[9][0-24-9]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
					}
				}
				if(input_value.length == 7){
		
					let regExp = /[0-9-]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 8){
					
					let regExp = /[0-9-]/;
					if (regExp.test(input_value)) {
				
						
					}
				}
				if(input_value.length == 9){
					
					let regExp = /[0-9-]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 10){
					
					let regExp = /[0-9-]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 11){
					
					let regExp = /[0-9-]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}

			// let regExp = /^9[0-9][0-9]-[7-8][0-9]/;
			// if (regExp.test(input_value)) {
				
			// console.log("Valid");
			// }else{
			// console.log("In-Valid");
			// document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);

			}
		}	

		

		
		
		
</script>
</body>

</html>