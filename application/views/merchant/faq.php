<?php
$this->load->view('merchant/include/head');
?>
<style type="text/css">
	form.posi {
		position: relative;
	}
	.absubtn {
		position: absolute;
		top: 0;
		right: 0;
		padding: 9px 22px;
		border-top-left-radius: 0px;
	}
	#accordion .card-body {
		padding: 10px 30px;
	}
</style>
<body>
	<div class="wrapper">
		<?php
		$this->load->view('merchant/include/nev');
		?>

		<div class="main">
			<?php
			$this->load->view('merchant/include/header');
			?>

			<main class="content">
				<div class="container-fluid p-0">
					<h1 class="h3 mb-3 profile_tab mar-b0"> FAQs</h1>

					<?php
					if ($this->session->flashdata('success')) {
						?>
						<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<div class="alert-message">
								<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
							</div>
						</div>
						<?php
					}
					?>
					<div id="payment_box">
						<div class="row justify-content-end">
							<div class="col-md-4">
								<form method="post" action="<?php echo base_url('Merchant/faq') ?>" class="posi">
									<input type="text"  class="form-control" id="faqsearch" value="" name="search_faq" placeholder=" Search...." >
									<input type="submit" name="submit" value="submit" class="btn btn-primary absubtn">
									
								</form> 
							</div>
						</div>
						<div class="row justify-content-md-center">
							<div class="form-group" >
								
							</div>

							<div class="col-md-12 col-xl-12">
								<div class="card-body">
									<h5 class="card-title black">  </h5>
									<div id="accordion">
										<?php
										if(!empty($faq_search)) {
											foreach ($faq_search as $key => $value) {
										
										?> 
											<div class="card mb-0 box-shadow">
												<div class="card-header pb-0" id="headingTwo">
													<h5 class="mb-0">
														<button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo<?php echo $key ?>" aria-expanded="false" aria-controls="collapseTwo">
															<?php echo $value['faq_name'] ?> 
														</button>
													</h5>
												</div>
												<div id="collapseTwo<?php echo $key ?>" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
													<div class="card-body">
														<?php echo $value['faq_message'] ?> 
													</div>
												</div>
											</div>
										<?php
											} } elseif(!empty($faq)){ 
												foreach ($faq as $key => $value) {
										?>
												<div class="card mb-0 box-shadow">
												<div class="card-header pb-0" id="headingTwo">
													<h5 class="mb-0">
														<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo<?php echo $key ?>" aria-expanded="false" aria-controls="collapseTwo">
															<?php echo $value['faq_name'] ?> 
														</button>
													</h5>
												</div>
												<div id="collapseTwo<?php echo $key ?>" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
													<div class="card-body">
														<?php echo $value['faq_message'] ?> 
													</div>
												</div>
											</div>
										<?php } } elseif(empty($faq_search)) { ?> 
											<div class="card mb-0 box-shadow">
												<div class="card-header pb-0" id="headingTwo">
													<label>No Data Found</label>
												</div>
											</div>
										<?php }?>
									</div>
								</div>
							</div>

							<div class="col-md-6 col-xl-6">

								<!-- <div class="card-header p-0">
									<h5 class="card-title">FAQs </h5>
								</div> -->

								<!-- <div class="needid">
									<form method="post" name="myForm1" onsubmit="return validateForm1()" action="<?php //echo base_url('Merchant/faq_submit') ?>" >
										<div class="form-group">
											<input class="form-control form-control-lg" type="text" name="faq_name" placeholder="Name" required="">
										</div>


										<div class="form-group">

											<input class="form-control form-control-lg" name="faq_message" placeholder="Message" required="">
										</div>

										<div class="text-center mt-3">

											<button class="btn btn-lg btn-primary green_gradient">Submit</button>
										</div>
									</form>
								</div>
							</div> -->

						</div>
					</div>
				</div>

			</main>
		</div>
	</div>

	<script src="js\app.js"></script>

</body>

</html>
<script type="text/javascript">
	/*$('#faqsearch').change(function () { 
		
	});*/

</script>