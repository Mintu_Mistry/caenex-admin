<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Merchant';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['Sign_up'] = 'Merchant/Sign_up';
$route['Dashboard'] = 'Merchant/Dashboard';
$route['Profile'] = 'Merchant/profile';
$route['change_password'] = 'Merchant/change_password';
$route['logout'] = 'Merchant/logout';
$route['campaign_management'] = 'Merchant/campaign_management';
$route['campaign_management_NexWorld'] = 'Merchant/campaign_management_NexWorld';
$route['campaign_management_NexPlay'] = 'Merchant/campaign_management_NexPlay';
$route['account'] = 'Merchant/account';
$route['payment_method'] = 'Merchant/payment_method';
$route['id_verification'] = 'Merchant/id_verification';
$route['settings'] = 'Merchant/settings';
$route['support'] = 'Merchant/support';
$route['Forgot'] = 'Merchant/Forgot';
$route['reset_link'] = 'Merchant/reset_link';
$route['notification'] = 'Merchant/notification';
$route['Login_mobile'] = 'Merchant/Login_mobile';
$route['business_details']='Merchant/business_details';
$route['faq']='Merchant/faq';

$route['admin_dashboard'] = 'Admin/admin_dashboard';
$route['user_management'] = 'Admin/user_management';
$route['merchant_management'] = 'Admin/merchant_management';
$route['billboard_management'] = 'Admin/billboard_management';
$route['user_transition'] = 'Admin/user_transition';
$route['merchant_transition'] = 'Admin/merchant_transition';
$route['id_verification_admin'] = 'Admin/id_verification_admin';
$route['user_terms_conditions'] = 'Admin/user_terms_conditions';
$route['merchant_terms_conditions'] = 'Admin/merchant_terms_conditions';
$route['user_privacy_policy'] = 'Admin/user_privacy_policy';
$route['merchant_privacy_policy'] = 'Admin/merchant_privacy_policy';
$route['admin_profile'] = 'Admin/admin_profile';
$route['admin_notification'] = 'Admin/admin_notification';

$route['category'] = 'Admin/game_category';
$route['ad_type'] = 'Admin/ad_type';
$route['questions'] = 'Admin/questions';
$route['add_questions'] = 'Admin/add_questions';
$route['update_questions'] = 'Admin/update_questions';
$route['billboard_details/(:any)'] = 'Admin/billboard_details/$1';
$route['about'] = 'Admin/about';
$route['help_articles'] = 'Admin/help_articles';
