<?php
$this->load->view('merchant/include/head');
?>

<body>
	<div class="wrapper">
	<?php
			$this->load->view('merchant/include/nev');
		?>

		<div class="main">
		<?php
				$this->load->view('merchant/include/header');
			?>

			<main class="content">
				<div class="container-fluid p-0">

					<h1 class="h3 mb-3 profile_tab">Profile Details</h1>
					<?php
						if ($this->session->flashdata('update')) {
						?>
						<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
						<div class="alert-message">
							<strong>Success!</strong> <?php echo $this->session->flashdata('update'); ?>
						</div>
						</div>
						<?php
						}
					?>
					<div class="row">
						<div class="col-md-4 col-xl-3">
							<div class="card mb-3">
								<div class="card-body text-center">
		<?php 						
	        $nexworld_cash=array();
			$nexworld_coupon=array();
			$con['merchant_id']=$this->session->userdata('merchant_id');
			$con['delete_status']='1';
			$nexworld_cash=$this->Merchant_modal->get_all_data('*','tbl_nexworld_cash',$con);
			$nexworld_coupon=$this->Merchant_modal->get_all_data('*','tbl_nexworld_coupon',$con);
			$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
			$data['nexworld']=$array_merge1;
		?>
				<?php
				if(count($data)==15){
					if (!empty($profile_data['img'])) {
						
					?>
					<img style="height: 120px;width: 120px;" src="<?php echo base_url('assets/img/game_img/').$profile_data['img'] ?>" alt="Stacie Hall" class="img-fluid rounded-circle mb-2" width="128" height="128"><img src="<?php echo base_url();?>assets/img/blue-tick.jpg" style="height: 10px;min-width: 10px;margin-top: 20px; margin-left: -2px;">
					<?php
					}else{?>

					<img src="assets/img\avatars\avatar.png" alt="Stacie Hall" class="img-fluid rounded-circle mb-2" width="128" height="128"><img src="<?php echo base_url();?>assets/img/blue-tick.jpg" style="height: 10px;min-width: 10px;margin-top: 20px; margin-left: -2px;">

					<?php
					}
				}

				else{
					if (!empty($profile_data['img'])) {
						
					?>
					<img style="height: 120px;width: 120px;" src="<?php echo base_url('assets/img/game_img/').$profile_data['img'] ?>" alt="Stacie Hall" class="img-fluid rounded-circle mb-2" width="128" height="128">
					<?php
					}else{?>

					<img src="assets/img\avatars\avatar.png" alt="Stacie Hall" class="img-fluid rounded-circle mb-2" width="128" height="128">

					<?php
					}
				}
				?>
									<h5 class="card-title mb-0" style="color:#333;"><?php echo $profile_data['merchant_name'] ?></h5>
								</div>
								
								<hr class="my-0">
								<div class="card-body">
									<!-- <h5 class="h6 card-title">About</h5> -->
									<ul class="list-unstyled mb-0">
										<!-- <li class="mb-1"><span data-feather="home" class="feather-sm mr-1"></span> Lives in <a href="#"><?php echo $profile_data['merchant_country'] ?></a></li> -->

										<li class="mb-1"><span data-feather="briefcase" class="feather-sm mr-1"></span> Works at <a href="#"><?php echo $profile_data['businessname'] ?></a></li>
										<!-- <li class="mb-1"><span data-feather="map-pin" class="feather-sm mr-1"></span> From <a href="#"><?php echo $profile_data['merchant_state'] ?></a></li> -->
									</ul>
								</div>
								<!-- <hr class="my-0"> -->
								<div class="card-body">
									<!-- <h5 class="h6 card-title">Elsewhere</h5>
									<ul class="list-unstyled mb-0">
										<li class="mb-1"><span class="fab fa-twitter fa-fw mr-1"></span> <a href="#">Twitter</a></li>
										<li class="mb-1"><span class="fab fa-facebook fa-fw mr-1"></span> <a href="#">Facebook</a></li>
										<li class="mb-1"><span class="fab fa-instagram fa-fw mr-1"></span> <a href="#">Instagram</a></li>
										<li class="mb-1"><span class="fab fa-linkedin fa-fw mr-1"></span> <a href="#">LinkedIn</a></li>
									</ul> -->
								</div>
							</div>
						</div>

						<div class="col-md-8 col-xl-9">
							<div class="card">
								<!-- <p style="text-align: center;color: green;font-size: 20px;    margin-bottom: -4px; margin-top: 18px;">
									<?php
								if ($this->session->flashdata('update')) {
									echo $this->session->flashdata('update');
								}
								?> -->
									
								</p>
								<div class="card-header">
									<h5 class="card-title">Merchant Personal info  <i class="align-middle ml-2 fas fa-fw fa-pencil-alt"></i></h5>


								</div>
								<div class="card-body">
									<form id="formfont" method="post" enctype="multipart/form-data"> 
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4">Full Name</label>
												<input type="text" class="form-control" id="merchant_name" name="merchant_name" placeholder="Full Name" value="<?php echo $profile_data['merchant_name'] ?>" oninput ="validate_only_alphabates_allowed(this.id)" required="required">
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4">Business Name</label>
												<input type="text" class="form-control" id=""  name="businessname" value="<?php echo $profile_data['businessname'] ?>" required="required" placeholder="Business Name">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4">Email</label>
												<input type="email" class="form-control" id=""  readonly="" value="<?php echo $profile_data['merchant_email'] ?>" required="required" placeholder="Email">
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4">Change Profile</label>

												<input type="file" class="form-control" name="file"   accept="image/*" src="" id="file" data-title="Drag and drop a file"  onchange="readURL(this)">

											</div>
										</div>

										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4">Mobile Number</label>
												<input type="tel" class="form-control" id="txtPhn1" name="merchant_phone" placeholder="Mobile Number" onkeypress="return validate1(event)" value="<?php echo $profile_data['merchant_phone'] ?>" required >
											</div>
											
										</div>
										<h5 class="card-title busi_hding" style="color: #048caa">Business Address</h5>
										<div class="form-row">
										<div class="form-group col-md-6">
											<label for="inputAddress">Street Address</label>
											<input type="text" class="form-control" id=""  value="<?php echo $profile_data['merchant_address'] ?>" name="merchant_address" required="required" placeholder="Street Address">
										</div>
										<div class="form-group col-md-6">
											<label for="inputAddress2">City</label>
											<input type="text" class="form-control" id="merchant_city"  value="<?php echo $profile_data['merchant_city'] ?>" name="merchant_city" oninput ="validate_only_alphabates_allowed(this.id)" required="required" placeholder="City">
										</div>
									</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputCity">State</label>
												<input type="text" class="form-control" id="merchant_state"  value="<?php echo $profile_data['merchant_state'] ?>" name="merchant_state" oninput ="validate_only_alphabates_allowed(this.id)" required="required" placeholder="State">
											</div>
											<div class="form-group col-md-6">
												<label for="inputState">Zip</label>
												<input type="text" class="form-control" maxlength="5" minlength="5" id="zip"  value="<?php echo $profile_data['zip'] ?>" name="zip" oninput="validate_only_alphaNumeric_max6_allowed(this.id)" required="required" placeholder="Zip">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputZip">Country</label>
												<input type="text" class="form-control" id="merchant_country"  value="<?php echo $profile_data['merchant_country'] ?>" name="merchant_country" oninput ="validate_only_alphabates_allowed(this.id)" required="required" placeholder="Country">
											</div>
											<div class="form-group col-md-6">
												<label for="inputZip">Tax ID</label>
												<input type="text" class="form-control" id="merchant_taxid"  value="<?php echo $profile_data['merchant_taxid'] ?>" name="merchant_taxid" onkeypress="return autoMask(this,event, '################################');"  required="required" placeholder="Tax ID">
											</div>
										</div>	
										<button class="btn btn-primary btn-lg" name="update">Update</button>									
									</form>
								</div>
							</div>
						</div>
					</div>

				</div>
			</main>
		</div>
	</div>
	
	<!-- <script src="<?php echo base_url('assets/') ?>js\app.js"></script> -->
	<script>
		var arr = Array();
		function validate_only_alphabates_allowed(input_id){
			let input_value = document.getElementById(input_id).value;
			let regExp = /^[a-z A-Z]+$/;
			if (regExp.test(input_value)) {
			console.log("Valid");
			}else{
			
			document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
		
			}
		}
		
		function validate1(key)
{
	//getting key code of pressed key
	var keycode = (key.which) ? key.which : key.keyCode;
	var phn = document.getElementById('txtPhn1');
	//comparing pressed keycodes
	if (!(keycode==8 || keycode==46)&&(keycode < 48 || keycode > 57))
	{
		return false;
	}
	else
	{
		//Condition to check textbox contains ten numbers or not
		if (phn.value.length <10)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

		function validate_only_alphaNumeric_max6_allowed(input_id){
			let input_value = document.getElementById(input_id).value;
			let regExp = /^[0-9]+$/;
			if (regExp.test(input_value)) {
			
			input_value_array = input_value;
			if(input_value_array.length > 5){
				document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
			}

			}else{
			
			document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
		

			}
		}

		function Validate_Only_Numeric_Input(input_id){
			let input_value = document.getElementById(input_id).value;
			let regExp = /^[0-9]+$/;
			if (regExp.test(input_value)) {
			console.log("Valid");
			}else{
			document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);

			}
		}

		function validate_taxid(input_id){
			console.log(input_id);
			let input_value = document.getElementById(input_id).value;
			console.log(input_value.length);
			// let regExp = /^9[0-9][0-9]-[70-88]?[90-92]?[94-99]?-[0-9][0-9][0-9][0-9]/;
			if(input_value.length == 3 || input_value.length == 6){
					document.getElementById(input_id).value += '-';
				}else if(input_value.length > 11){
					document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
				}
				if(input_value.length == 1){
					
					let regExp = /^9/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 2){
					
					let regExp = /^9[0-9]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 3){
					
					let regExp = /^9[0-9][0-9]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 4){
					
					let regExp = /^9[0-9][0-9]-/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 5){
					
					arr = input_value.split("-");
					console.log(arr);
					let regExp = /^9[0-9][0-9]-[7-9]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}

					
					
				}
				if(input_value.length == 6){
					
					// let regExp = /^9[0-9][0-9]-[7-8][0-8]/;
					// if (regExp.test(input_value)) {
				
						
					// }else{
					// 	document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					// }
					if(arr[1] == 7){
						
						let regExp = /^9[0-9][0-9]-[7][0-9]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
					if(arr[1] == 8){
						let regExp = /^9[0-9][0-9]-[8][0-8]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
					if(arr[1] == 9){
						let regExp = /^9[0-9][0-9]-[9][0-24-9]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
					}
				}
				if(input_value.length == 7){
		
					let regExp = /[0-9-]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 8){
					
					let regExp = /[0-9-]/;
					if (regExp.test(input_value)) {
				
						
					}
				}
				if(input_value.length == 9){
					
					let regExp = /[0-9-]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 10){
					
					let regExp = /[0-9-]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}
				}
				if(input_value.length == 11){
					
					let regExp = /[0-9-]/;
					if (regExp.test(input_value)) {
				
						
					}else{
						document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
					}

			// let regExp = /^9[0-9][0-9]-[7-8][0-9]/;
			// if (regExp.test(input_value)) {
				
			// console.log("Valid");
			// }else{
			// console.log("In-Valid");
			// document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);

			}
		}	
	</script>
	<script type="text/javascript">
	function autoMask(field, event, sMask) {
    var KeyTyped = String.fromCharCode(getKeyCode(event));
    if (getKeyCode(event) == 8) {
        return
    }
    if (getKeyCode(event) == 0) {
        return
    }
    if (field.value.length == sMask.length && getKeyCode(event) == 13) {
        return true
    }
    // if (field.value.length == sMask.length && getKeyCode(event) != 13) {
    //     field.value = ""
    // }
    
    var targ = getTarget(event);
    keyCount = targ.value.length;
    if (keyCount == sMask.length) {
        return false
    }
    if (sMask.charAt(keyCount + 1) != "#" && sMask.charAt(keyCount + 1) != "A") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return false
    }
    if (sMask.charAt(keyCount) == "*")
        return true;
    if (sMask.charAt(keyCount) == KeyTyped) {
        return true
    }
    if (sMask.charAt(keyCount) == "#" && isNumeric(KeyTyped))
        return true;
    if (sMask.charAt(keyCount) == "A" && isAlpha(KeyTyped))
        return true;
    if (sMask.charAt(keyCount + 1) == "?") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return true
    }
    if (KeyTyped.charCodeAt(0) < 32)
        return true;
    return false
}


function getKeyCode(e) {
    if (e.srcElement) {
        return e.keyCode
    }
    if (e.target) {
        return e.which
    }
}

function getTarget(e) {
    if (e.srcElement) {
        return e.srcElement
    }
    if (e.target) {
        return e.target
    }
}

function isNumeric(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

function isAlpha(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}
	</script>
</body>

</html>