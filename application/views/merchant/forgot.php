r <!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Bootstrap 4 Admin &amp; Dashboard Template">
	<meta name="author" content="Bootlab">
	<title>Caenex</title>
	<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin="">
	<style>
		body {
			opacity: 0;
		}
	</style>
	<script src="<?php echo base_url('assets/js/settings.js')?>"></script>
	<!-- END SETTINGS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-120946860-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120946860-6');
</script></head>

<body>
	<main class="main d-flex w-100 conexbg">
		<div class="container d-flex flex-column">
			<div class="corner"><img src="<?php echo base_url('assets/img/corner.png') ?>" alt="" class="img-fluid"></div>
			<div class="corner_logo"><img src="<?php echo base_url('assets/img/corner_logo.png')?>" alt="" class="img-fluid"></div>
			<div class="row h-100 mt-small">
				<div class="col-sm-10 col-md-8 col-lg-7 mx-auto d-table h-100">

						<div class="text-center mt-4">
							<h1 class="h2 loghed">Forgot Password</h1>
						</div>

						<div class="card">
							<div class="card-body">
									<div id="loginpage">
										<form method="post" name="myForm1" onsubmit="return validateForm1()">
											<div class="form-group">
												<input class="form-control form-control-lg" type="email" name="email" placeholder="Enter your email"  required="required">
											</div>
											
											<div class="text-center mt-3">
												<!-- <a href="#" class="btn btn-lg btn-primary logbtn"> Login</a> -->
												<button type="btn" class="btn btn-lg btn-primary logbtn">Submit</button>
											</div>
											<div class="singupbox">Back To <a href="<?php echo base_url() ?>"> LogIn </a></div>
										</form>
									</div>
							</div>
							<p id="error_email1" style="text-align: center;color: red;font-size: 20px;">
								<?php
								if ($this->session->flashdata('incorrct')) {
									echo $this->session->flashdata('incorrct');
								}
								?>
							</p>

							<!-- <p style="text-align: center;color:green;font-size: 20px;">
								<?php
								if ($this->session->flashdata('corrct')) {
									echo $this->session->flashdata('corrct');
								}
								?>
							</p> -->
						</div>
						
									<!-- BEGIN primary modal -->
								<div class="modal fade" id="defaultModalsignup" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content" style="margin-top: 22%;">
											<div class="modal-header singmodel">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  								<span aria-hidden="true">&times;</span>
                								</button>
											</div>
											<div class="modal-body signmod_body">
												<div class="logo" style="margin-top: 15px;"><img src="<?php echo base_url('assets/img/sign_pop_logo.png')?>" alt="logo" class="img-fluid"> CAENEX </div>
												<h4 class="text-center"> We have sent a one-time verification code to your email<p> Please enter it below.</p></h4>
												<p id="otp_incorrect" style="color: red"></p>
												<div class="otpsend">
												  <form class="form-inline" name="myForm" onsubmit="return validateForm()">
												  	<input type="hidden" name="otp_data" id="otp_data">
														<input type="text" class="form-control mb-2 col-lg-8 col-sm-8" id="inlineFormInputName2" placeholder="Enter OTP" name="otp"  required="">
													    <button type="submit" class="btn btn-primary sign_submt mb-2 col-lg-3 col-sm-3">Submit</button>
													    <div class="change_otp">
													    <div class="form-row">
													    	<div class="col-md-6">
													    <div class="chanemail">
													    	<a href="#" class="modal-header singmodel">
														<button style="float: left;margin-left: -40px;margin-top: 1px;font-size: 17px;" type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="reset_mesg()"> Change Email </button>
														</div>
													    </div>
													    		<!-- <div class="modal-header singmodel">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  								<span aria-hidden="true">&times;</span>
                								</button>
											</div> -->
												<div class="col-md-5">
													<div class="resotp">
													<button style="border-style: dashed;"><a href="javascript:void(0)" onclick="resend_otp()"> <i class="fas fa-sync"></i>Resend OTP </a></button>
												</div>
													    	</div>
													    </div>
													</div>
												</form>
											</div>
											</div>
											
										</div>
									</div>
								</div>
												<!-- END primary modal -->
						
						
						<!-- BEGIN reset pass modal -->
								<div class="modal fade" id="defaultModalreset" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content" style="margin-top: 22%;">
											<div class="modal-header singmodel">Reset Password
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  								<span aria-hidden="true">&times;</span>
                								</button>
											</div>
											<div class="modal-body signmod_body">
												
						<div class="card">
							<div class="card-body">
									<div id="loginpage">
										<form method="post" name="myForm2" onsubmit="return validateForm2()" action="">
											<span id="invalidPassword" style="color: red;margin-left: 35px;"></span>
											<input type="hidden" name="merchant_id" id="merchant_id">
											<div class="form-group">
												<input class="form-control form-control-lg" type="password" name="password" placeholder="Enter new password" required="required">
											</div>

											<div class="form-group">
												<input class="form-control form-control-lg" type="password" name="cpassword" placeholder="Enter confirm password" required="required">
											</div>
											
											<div class="text-center mt-3">
												<!-- <a href="#" class="btn btn-lg btn-primary logbtn"> Login</a> -->
												<button type="submit" name="submit" class="btn btn-lg btn-primary logbtn">Submit</button>
											</div>
											<div class="singupbox">Back To <a href="<?php echo base_url('') ?>"> LogIn </a></div>
										</form>
									</div>
							</div>
							<p style="text-align: center;color: red;font-size: 20px;">
								<?php
								if ($this->session->flashdata('incorrct1')) {
									echo $this->session->flashdata('incorrct1');
								}
								?>
							</p>
						</div>
											</div>
											
										</div>
									</div>
								</div>
								<!-- END reset pass modal -->
												
												
												
						<div class="formbtext text-center">
							<h2> Do you have more than one business location?</h2>
							<h6 class="registertext"> If you are looking to advertise at multiple locations, please <u><b><a style="color: #d8e5db" href="<?php echo base_url('Sign_up') ?>"> register here </a></b></u></h6>
							<img src="<?php echo base_url('assets/') ?>img/border.jpg">
						</div>
				</div>
			</div>
		</div>
	</main>

	<script src="<?php echo base_url('assets/js/app.js') ?>"></script>

</body>

</html>
<script type="text/javascript">
	
		function validateForm2() {
		 
		  var password = document.forms["myForm2"]["password"].value;
		  var cpassword = document.forms["myForm2"]["cpassword"].value;
		  var merchant_id = document.forms["myForm2"]["merchant_id"].value;
         
          let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
          if (regex.test(password)) {
          	if (password==cpassword) {
				$.post("<?php echo base_url("Merchant/reset_link") ?>",{password:password,cpassword:cpassword,merchant_id:merchant_id},function(res){
					//window.load.href = '<?php echo base_url('') ?>';
					
				//var res1 = res.split('@@');
				//document.getElementById("otp_data").value=res1[0];
				//document.getElementById("merchant_id").value=res1[1];
				//alert(); 
			  })
				window.location.replace("<?php echo base_url('') ?>");
          		//return true;  
          	}else{

              document.getElementById('invalidPassword').innerHTML = 'Password and confirm password should be same';
              return false;  
          	}
          	
           
           }else{
            document.getElementById('invalidPassword').innerHTML = 'Password must be 8 digit long including alpha-numeric-lower_case-upper_case-special_char';
            return false;  
        }
	}
	
		function validateForm() {
			
		  var email = document.forms["myForm1"]["email"].value;
		  var x = document.forms["myForm"]["otp"].value;
		  var y =document.getElementById("otp_data").value;
		  var merchant_id =document.getElementById("merchant_id").value;
		  if (x==y) {
			  /* alert("success");
			  location.reload(); */
		 		$('#defaultModalsignup').modal('hide');
		 		$('#defaultModalreset').modal('show');
				//window.location.href = "merchant/reset_link/"+merchant_id;
				/*window.location.href = "http://ceanex.bonzaistaging.com/merchant/reset_link/"+ merchant_id;
				 window.href="http://ceanex.bonzaistaging.com/reset_link?URL="+merchant_id; */
		  		
		  		return false;
		  }else{
		  	document.getElementById('otp_incorrect').innerHTML ="Please Enter Valid Otp";
		  	
		  	return false;
		  }
		 
		}
		
	function validateForm1() {
		 
		  var email = document.forms["myForm1"]["email"].value;
		 
          // let regex = /^[A-Za-z]\w{7,30}$/;
          let email_pettern=/^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/;
          let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
          if (email_pettern.test(email)) {
          	document.getElementById('error_email1').innerHTML = '';
			$('#defaultModalsignup').modal('show');
		  	$.post("<?php echo base_url("Merchant/setsession_data3") ?>",{email:email},function(res){
				var res1 = res.split('@@');
		  	document.getElementById("otp_data").value=res1[0];
		  	document.getElementById("merchant_id").value=res1[1];
		  })
		  
          	return false;
         
    	}else{
    		document.getElementById('error_email1').innerHTML = 'Please Enter Valid Email Format';
        return false;  
    	}
			 // return false;
	}
	
	function resend_otp(){
			var email=document.getElementById("email").value;
			
			$.post("<?php echo base_url("Merchant/resend_otp") ?>",{email:email},function(res){
				document.getElementById("otp_data").value=res;
				document.getElementById('otp_incorrect').innerHTML ="OTP has been resent to your E-mail";
		  })
		}
</script>