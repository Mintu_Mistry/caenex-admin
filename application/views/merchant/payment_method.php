<?php
$this->load->view('merchant/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('merchant/include/nev');
?>

<div class="main">
<?php
$this->load->view('merchant/include/header');
?>
<style type="text/css">
	 .cardimage-box i{
		color: gray;
	}
	.cardimage-box i.amex{
		color: #6699ff;
		transform: scale(1.2);
	}
	.cardimage-box i.visa{
		color: #66ccff;
		transform: scale(1.2);
	}
	.cardimage-box i.discover{
		color: #ffcc66;
		transform: scale(1.2);
	}
	.cardimage-box i.mastercard{
		color: red;
		transform: scale(1.2);
	}
	.cardimage-box i.diners{
		color: #cc6600;
		transform: scale(1.2);
	}
	.cardimage-box i.jcb{
		color: #0066ff;
		transform: scale(1.2);
	}
</style>

			<main class="content">
				<div class="container-fluid p-0">
				<h1 class="h3 mb-3 profile_tab mar-b0"> Payment Methods </h1>
				<p style="text-align: center;color: green;font-size: 20px;">
					<?php
						if ($this->session->flashdata('add')) {
						?>
						<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
						<div class="alert-message">
							<strong>Success!</strong> <?php echo $this->session->flashdata('add'); ?>
						</div>
						</div>
						<?php
						}
					?>
				</p>
				<div id="payment_box">
					<div class="row">
						<div class="col-md-8 col-xl-8">
							<div class="card">
								<div class="card-header bankhd_bg">
									<h5 class="card-title"><img src="<?php echo base_url('assets/img/')?>personal_info.png">    Add Card</h5>
								</div>
								<div class="card-body">

									<form id="formfont" method="post" action="<?php echo base_url('merchant/submit_payment') ?>">
										<div class="form-row posi_relative">
											<div class="form-group col-md-12">
												<label for="inputEmail4">Card number</label>
												<input type="text"  class="form-control card-image" id="" required  name="card_number" placeholder="Card number"
												 >

												   <div class="cardimage-box " id="card-image" > 
												  	 <i style=" margin-top: 3px;" class="fab fa-cc-amex fa-3x"></i>

												  	<i style="margin-left:5px; margin-top: 3px;"class="fab fa-cc-visa fa-3x"></i>

												  	<i style="margin-left:5px; margin-top: 3px;" class="fab fa-cc-diners-club fa-3x"></i>

												  	<i style="margin-left:5px; margin-top: 3px;" class="fab fa-cc-mastercard fa-3x"></i>

												  	<i style="margin-left:5px; margin-top: 3px;" class="fab fa-cc-jcb fa-3x"></i>

												  	<i style="margin-left:5px; margin-top: 3px;" class="fab fa-cc-discover fa-3x"></i> 
												  </div>
												 
												  <!-- <img src="" id="cardimage" style="height: 50px;min-width: 50px;"> -->
											</div> 
										</div>
										<div class="form-row">
											<div class="form-group col-md-12">
												<label for="inputEmail4">Card holder name</label>
												<input type="text" class="form-control" id="cardholder_name"  name="card_name" oninput ="validate_only_alphabates_allowed(this.id)" required placeholder="Card holder name">
											</div>
										</div>
										<div class="form-row">
										<div class="form-group col-md-9">
											<label for="inputEmail4">Exp. date</label>
											<div class="form-row">
												<div class="form-group col-md-10">
												 <select name="month" id="month"
                            class="demoSelectBox" required>
							<option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
							<option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select> <select name="year" id="year"
                            class="demoSelectBox" required >
                            
                            <option value="21">2021</option>
                            <option value="22">2022</option>
                            <option value="23">2023</option>
                            <option value="24">2024</option>
                            <option value="25">2025</option>
                            <option value="26">2026</option>
                            <option value="27">2027</option>
                            <option value="28">2028</option>
                            <option value="29">2029</option>
                            <option value="30">2030</option>
							<option value="31">2031</option>
                            <option value="32">2032</option>
                            <option value="33">2033</option>
                            <option value="34">2034</option>
                            <option value="35">2035</option>
                        </select>
						<input type="hidden" id="expirymonth" name="expirymonth">
						<!--<input type="text" class="form-control" name="exp_date" onkeypress="return autoMask(this,event, '##/####');" id="expmonth" maxlength="7" minlength="7" placeholder="Month / Year"  required>-->
													
												</div>
											</div>
										</div>
										<div class="form-group col-md-3">
											<label for="inputAddress2">CVV</label>
											<input type="text" minlength="3" class="form-control" id="" name="cvv"      onkeypress="return autoMask(this,event, '###');" required placeholder="CVV">
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-12">
											<label for="inputEmail4">Card nick name(Optional)</label>
											<input type="text" class="form-control" id="nickname"  name="nickname" oninput ="validate_only_alphabates_allowed(this.id)" placeholder="Card nick name">
										</div>
									</div>
									<!-- <a class="btn btn-success payment_btn go-right" href="#" role="button"> Submit <i class="fas fa-fw fa-arrow-right"></i></a> -->
									<button class="btn btn-success payment_btn go-right">Submit <i class="fas fa-fw fa-arrow-right"></i></button>								
									</form>
								</div>
							</div>
						</div>

						<div class="col-md-14 col-xl-4">
							<div class="card"> 
								<div class="card-header bankhd_bg">
									<h5 class="card-title"> Linked Cards </h5>
								</div>
								<?php
								foreach ($payment as $key => $value) {
									
								?>
								<div class="saved_card" id="saved_card<?php echo $value['paymethod_id'] ?>">
									<div class="saved_cardbox">
										<div class="form-row">
										<div class="col-lg-9">
											<div class="cardhd_sav_name"> <?php echo $value['cardholdername'] ?> </div>
											<div class="card_saved_number"> <?php echo $value['cardnumber'] ?> </div>
											<div class="saved_card_expdate"> Exp. <?php echo $value['expirydate'] ?>  </div>
										</div>
										<div class="col-lg-3 text-right">
											<div class="edit_delete_savedcard">
												<span><a href="JavaScript:voide(0)"><i class="fas fa-fw fa-times" onclick="delete_data(<?php echo $value['paymethod_id'] ?>,'tbl_paymentmethods','paymethod_id')"></i></a></span> 
												<span><a href="JavaScript:voide(0)"><i class="fas fa-fw fa-pen" data-toggle="modal" data-target="#sizedModalMd<?php echo $value['paymethod_id'] ?>"></i></a></span>
											</div>
											<div class="saved_card_icon"><i class="far fa-fw fa-credit-card"></i></div>
										</div>
									</div>
									</div>
								</div>

								<div class="modal fade" id="sizedModalMd<?php echo $value['paymethod_id'] ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-md" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title">Update</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    								<span aria-hidden="true">&times;</span>
                   									</button>
												</div>
												<div class="modal-body m-3">
													<div class="card-body">
									<form id="formfont" method="post" action="<?php echo base_url('merchant/submit_payment') ?>">
										<input type="hidden" name="paymethod_id" value="<?php echo $value['paymethod_id'] ?>">
										<div class="form-row posi_relative">
											<div class="form-group col-md-12">
												<label for="inputEmail4">Card number</label>
												<input type="text" maxlength="16" minlength="16" class="form-control card-image" id="" required  placeholder="Card number" value="<?php echo $value['cardnumber'] ?>" name="card_number" id="">
												 <!-- <div class="visabox"><img src="<?php echo base_url('assets/img/')?>visa_icon.png"></div> -->
											</div> 
										</div>
										<div class="form-row">
											<div class="form-group col-md-12">
												<label for="inputEmail4">Cardholder name</label>
												<input type="text" class="form-control" id="Cardholder_name"  name="card_name" placeholder="Cardholder name" value="<?php echo $value['cardholdername'] ?>" oninput ="validate_only_alphabates_allowed(this.id)" required>
											</div>
										</div>
										<div class="form-row">
										<div class="form-group col-md-9">
											<label for="inputEmail4">Exp. date</label>
											<div class="form-row">
												<div class="form-group col-md-10">
													<input type="text" class="form-control" name="exp_date" onkeypress="return autoMask(this,event, '##/####');" id="expmonth" maxlength="7" minlength="7" placeholder="Month / Year"  required value="<?php echo $value['expirymonth'] ?>">
												</div>
											</div>
										</div>
										<div class="form-group col-md-3">
											<label for="inputAddress2">CVV</label>
											<input type="text" minlength="3" class="form-control" id="" name="cvv"  onkeypress="return autoMask(this,event, '###');" placeholder="CVV" value="<?php echo $value['cvv'] ?>"  required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-12">
											<label for="inputEmail4">Card nickname(Optional)</label>
											<input type="text" class="form-control" id="Card_nickname" placeholder="Card nickname"  value="<?php echo $value['nickname'] ?>"  name="nickname"  oninput ="validate_only_alphabates_allowed(this.id)">
										</div>
									</div>									
												
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button class="btn btn-primary">Save changes</button>
												
									</form>
								</div>
												</div>
											</div>
										</div>
									</div>

								<?php
									}
								?>

							</div>
						</div>

					</div>
				</div>
</div>

			</main>
		</div>
	</div>


</body>

</html>

<script>
	

	$(document).ready(function(){
			$("#year1").blur(function(){
				var Year = $('#year1').val();
				if(Year != ''){
					var thisYear =  new Date().getFullYear().toString().substr(-2);
					// console.log(thisYear);
					// var d = new Date();
					// console.log(d.getMonth());
					if(thisYear == Year){
						var d = new Date();
  						var month = d.getMonth();
  						var min = month + 1;
  						$('#month1').attr('min', min);
					}else if(thisYear < Year ){
						var min = 1;
  						$('#month1').attr('min', min);
					}
					
				}
			});
			var thisYear =  new Date().getFullYear().toString().substr(-2);
			$('#year1').attr('min', thisYear);
					
		  
		})

		$(document).ready(function(){
			$("#year").blur(function(){
				var Year = $('#year').val();
				if(Year != ''){
					var thisYear =  new Date().getFullYear().toString().substr(-2);
					// console.log(thisYear);
					// var d = new Date();
					// console.log(d.getMonth());
					if(thisYear == Year){
						var d = new Date();
  						var month = d.getMonth();
  						var min = month + 1;
  						$('#month').attr('min', min);
					}else if(thisYear < Year ){
						var min = 1;
  						$('#month').attr('min', min);
					}
					
				}
			});
			var thisYear =  new Date().getFullYear().toString().substr(-2);
			$('#year').attr('min', thisYear);
			
		  
		})

    function autoMask(field, event, sMask) {
    var KeyTyped = String.fromCharCode(getKeyCode(event));
    if (getKeyCode(event) == 8) {
        return
    }
    if (getKeyCode(event) == 0) {
        return
    }
    if (field.value.length == sMask.length && getKeyCode(event) == 13) {
        return true
    }
    // if (field.value.length == sMask.length && getKeyCode(event) != 13) {
    //     field.value = ""
    // }
    
    var targ = getTarget(event);
    keyCount = targ.value.length;
    if (keyCount == sMask.length) {
        return false
    }
    if (sMask.charAt(keyCount + 1) != "#" && sMask.charAt(keyCount + 1) != "A") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return false
    }
    if (sMask.charAt(keyCount) == "*")
        return true;
    if (sMask.charAt(keyCount) == KeyTyped) {
        return true
    }
    if (sMask.charAt(keyCount) == "#" && isNumeric(KeyTyped))
        return true;
    if (sMask.charAt(keyCount) == "A" && isAlpha(KeyTyped))
        return true;
    if (sMask.charAt(keyCount + 1) == "?") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return true
    }
    if (KeyTyped.charCodeAt(0) < 32)
        return true;
    return false
}


function getKeyCode(e) {
    if (e.srcElement) {
        return e.keyCode
    }
    if (e.target) {
        return e.which
    }
}

function getTarget(e) {
    if (e.srcElement) {
        return e.srcElement
    }
    if (e.target) {
        return e.target
    }
}

function isNumeric(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

function isAlpha(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

function delete_data(id,tbl,row){
	var result = confirm("Are You Sure?");
	if (result) {
		$("#saved_card"+id+"").fadeOut(100);
   	$.post("<?php echo base_url('merchant/delete') ?>",{id:id,tbl:tbl,row:row},function(res){
   		
   	})
	}
}

function validate_only_alphabates_allowed(input_id){
			let input_value = document.getElementById(input_id).value;
			let regExp = /^[a-z A-Z]+$/;
			if (regExp.test(input_value)) {
			
			}else{
			
			document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
		
			}
		}
</script>
 
 <script>
var cleave = new Cleave('.card-image', {
    creditCard: true,
    onCreditCardTypeChanged: function (type) {
         
        	// document.getElementById("cardimage").src="<?php echo base_url();?>assets/img/card-images/"+type +"'.png";
        	 console.log(type);

        	if(type==='visa'){
        		document.querySelector('.fa-cc-visa').classList.add('visa');
        	}else{
        		document.querySelector('.fa-cc-visa').classList.remove('visa');
        	}
        	if(type==='amex'){
        		document.querySelector('.fa-cc-amex').classList.add('amex');
        	}else{
        		document.querySelector('.fa-cc-amex').classList.remove('amex');
        	}
        	if(type==='discover'){
        		document.querySelector('.fa-cc-discover').classList.add('discover');
        	}else{
        		document.querySelector('.fa-cc-discover').classList.remove('discover');
        	}
        	if(type==='mastercard'){
        		document.querySelector('.fa-cc-mastercard').classList.add('mastercard');
        	}else{
        		document.querySelector('.fa-cc-mastercard').classList.remove('mastercard');
        	}
        	if(type==='diners'){
        		document.querySelector('.fa-cc-diners-club').classList.add('diners');
        	}else{
        		document.querySelector('.fa-cc-diners-club').classList.remove('diners');
        	}
        	if(type==='jcb'){
        		document.querySelector('.fa-cc-jcb').classList.add('jcb');
        	}else{
        		document.querySelector('.fa-cc-jcb').classList.remove('jcb');
        	}
       return;
    }
});
</script>




