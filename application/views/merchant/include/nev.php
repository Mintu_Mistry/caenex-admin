	 <?php  $path = $this->router->fetch_class().'/'.$this->router->fetch_method(); ?>
		<nav id="sidebar" class="sidebar">
			<div class="sidebar-content ">
				<a class="sidebar-brand" href="<?php echo base_url('Dashboard') ?>">
          <span class="align-middle"><img src="<?php echo base_url('assets/') ?>img/logo.png" alt="logo" class="img-responsive"> CAENEX </span>
        </a>

				<ul class="sidebar-nav">
					<li class="sidebar-item <?php echo $path == 'Merchant/Dashboard' ? 'active nav-active':''; ?>">
						<a href="<?php echo base_url('Dashboard') ?>" class="sidebar-link">
			              <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
			            </a>
					</li>
					
					<li class="sidebar-item <?php echo $path == 'Merchant/profile' ? 'active nav-active':''; ?>">
						<a href="<?php echo base_url('Profile') ?>" class="sidebar-link">
              				<i class="align-middle" data-feather="users"></i> <span class="align-middle">Profile</span>
            			</a>
					</li>
					<li class="sidebar-item <?php echo $path == 'Merchant/campaign_management' ? 'active nav-active':''; ?>">
						<a href="<?php echo base_url('campaign_management') ?>" class="sidebar-link">
              				<i class="align-middle mr-2 fas fa-fw fa-ad"></i> <span class="align-middle">Campaign Management</span>
            			</a>
					</li>
					<li class="sidebar-item <?php echo $path == 'Merchant/account' ? 'active nav-active':''; ?>">
						<a href="<?php echo base_url('account') ?>" class="sidebar-link">
              				<i class="align-middle mr-2 fas fa-fw fa-user-tie"></i><span class="align-middle">Account</span>
            			</a>
					</li>
					<li class="sidebar-item <?php echo $path == 'Merchant/payment_method' ? 'active nav-active':''; ?>">
						<a href="<?php echo base_url('payment_method') ?>"class="sidebar-link">
              				<i class="align-middle mr-2 fab fa-fw fa-cc-stripe"></i><span class="align-middle">Payment Methods</span>
            			</a>
					</li>
					<!-- <li class="sidebar-item <?php //echo $path == 'Merchant/add_wallet' ? 'active nav-active':''; ?>">
						<a href="<?php //echo base_url('add_wallet') ?>"class="sidebar-link">
              				<i class="align-middle mr-2 fab fa-fw fa-cc-stripe"></i><span class="align-middle">Add to wallet</span>
            			</a>
					</li> -->
					<!-- <li class="sidebar-item <?php echo $path == 'Merchant/business_details' ? 'active nav-active':''; ?>">
						<a href="<?php echo base_url('business_details') ?>"class="sidebar-link">
              				<i class="align-middle mr-2 fas fa-address-card"></i>
              				<span class="align-middle">Business Detail</span>
            			</a>
					</li> -->
					<li class="sidebar-item <?php echo $path == 'Merchant/id_verification' ? 'active nav-active':''; ?>">
						<a href="<?php echo base_url('id_verification') ?>" class="sidebar-link">
              				<i class="align-middle mr-2 far fa-fw fa-id-card"></i><span class="align-middle">ID Verification</span>
            			</a>
					</li>
					<li class="sidebar-item <?php echo $path == 'Merchant/settings' ? 'active nav-active':''; ?>">
						<a href="<?php echo base_url('settings') ?>" class="sidebar-link">
              <i class="align-middle mr-2 fas fa-fw fa-cog"></i><span class="align-middle">About Caenex</span>
            </a> </li>
            <li class="sidebar-item <?php echo $path == 'Merchant/faq' ? 'active nav-active':''; ?>">
						<a href="<?php echo base_url('faq') ?>" class="sidebar-link">
		              <i class="align-middle mr-2 fas fa-fw fa-user-alt"></i><span class="align-middle">FAQs</span>
		            </a>
				</li>
					<li class="sidebar-item <?php echo $path == 'Merchant/support' ? 'active nav-active':''; ?>">
						<a href="<?php echo base_url('support') ?>" class="sidebar-link">
		              <i class="align-middle mr-2 fas fa-fw fa-user-alt"></i><span class="align-middle">Support</span>
		            </a>
				</li>
					<li class="sidebar-item">
						<a href="<?php echo base_url('logout') ?>" class="sidebar-link">
              				<i class="align-middle mr-2 fas fa-fw fa-user-lock"></i> <span class="align-middle">Logout</span>
            			</a>				
					</li>
				</ul>
			</div>
		</nav>