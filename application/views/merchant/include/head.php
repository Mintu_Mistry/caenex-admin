<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Bootstrap 4 Admin &amp; Dashboard Template">
	<meta name="author" content="Bootlab">
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/logo.png'); ?>" type="image/x-icon" />
	<title> Caenex Merchant </title>
	<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin="">
	<link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/classic.css" >
	<script src="<?php echo base_url('assets/') ?>js/settings.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/app.js"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/') ?>sweetalert.css">
	<script src="<?php echo base_url('assets/dist/') ?>sweetalert.min.js"></script>
	<!-- END SETTINGS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script  src="https://www.googletagmanager.com/gtag/js?id=UA-120946860-6"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.5.10/cleave.min.js"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120946860-6');
</script></head>