<?php
$this->load->view('merchant/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('merchant/include/nev');
?>

<div class="main">
<?php
$this->load->view('merchant/include/header');
?>

			<main class="content">
				<div class="container-fluid p-0">
					<div class="row">
						<div class="col-12 col-sm-6 col-xl d-flex">
							<div class="card flex-fill">
								<div class="card-body py-4">
									<div class="media">
										<div class="d-inline-block mt-2 mr-3">
											<img src="<?php echo base_url('assets/') ?>img/dollar.jpg" alt="dollar">
										</div>
										<div class="media-body">
											<h3 class="mb-2 numberhed"> $<?php echo $wallet_balance ?> </h3>
											<div class="mb-0 numbertext"> Account Balance  </div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-xl d-flex"></div>
						<div class="col-12 col-sm-6 col-xl d-flex"></div>
						<div class="col-12 col-sm-6 col-xl d-flex"></div>	
					</div>
					<h1 class="h3 mb-3 profile_tab"> Transactions History </h1>

					<div class="row">
						<div class="col-md-12 col-xl-12">
						<?php 
						foreach($transaction_data as $transaction)
						{
						?>
							<div class="accountbox">
								<div class="row">
									<div class="col-lg-2 hislogo_size" style="padding-top: 27px;">
										<!--<div class="paidicon"></div>-->
										<?php 
										if(isset($transaction['coupon_img']))
										{?>
											<img src="<?php echo base_url('assets/img/game_img/') ?><?=  $transaction['coupon_img']?>" align="logo" height="75px" width="75px" class="img-responsive">
										<?php }else{
												echo "No Image";
											} ?>
										
									</div>
									<div class="col-lg-7">
										<h5> Paid for </h5>
										<h2> <?php 
										if(isset($transaction['coupon_title']))
										{?>
										<?=$transaction['coupon_title']?> </h2>
										<?php }else{
											
											echo "No Title";
										}?>
										<h4> 
										
										<?php if(!empty($transaction['transaction_date']))
										{
											echo date('m-d-y', strtotime($transaction['transaction_date'])); 
										}else{
											echo "No Date";
										}?>
										</h4>
									</div>
									<div class="col-lg-3 text-right">
										<div class="offer_price"> $<?=$transaction['amount']?> </div>
										<div class="offer_paid"> Paid <img src="<?php echo base_url('assets/') ?>img/paid.png" alt="paid"></div>
									</div>
								</div>
							</div>
						<?php } ?>
						</div>
					</div>

				</div>
			</main>
		</div>
	</div>

	<!-- <script src="js\app.js"></script> -->

</body>

</html>