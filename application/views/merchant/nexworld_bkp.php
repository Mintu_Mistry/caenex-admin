<?php
$this->load->view('merchant/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('merchant/include/nev');
?>

<div class="main">
<?php
$this->load->view('merchant/include/header');
?>
<input type="hidden" id="lat" value=""/>
<input type="hidden" id="lng" value=""/>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEBuvXqCwzkNd_qDqMRVKlOs5qWpM4Of4&libraries=places&callback=initAutocomplete" async defer></script>


    <script type="text/javascript">
	
	 var lat = document.getElementById("lat");
	  var lng = document.getElementById("lng");
		function getLocation() {
		  if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(showPosition);
			
		  } else {
			lat.value = "Geolocation is not supported by this browser.";
			
		  }
		}

		function showPosition(position) {
		  lat.value = parseFloat(position.coords.latitude).toFixed(2);
		  lng.value = parseFloat(position.coords.longitude).toFixed(2);
		}
		
		
		function initAutocomplete() {
		
		var lat = parseInt(document.getElementById('lat').value);
		var lng = parseInt(document.getElementById('lng').value);
		
		//var num1 = parseFloat("10.547892")
		//var num2 = parseFloat("10.547892").toFixed(2)
		//console.log("Without using toFixed() method");
		console.log(lat);
		//console.log("Using toFixed() method");
		console.log(lng);

		//var lat = document.getElementById("lat");
		//var longi = document.getElementById("long");
		//console.log(lat.toPrecision(2) +"@"+ lng.toPrecision(2));
		//return false;
        const map = new google.maps.Map(document.getElementById("map"), {
          center: { lat: lat, lng: lng },
          zoom: 13,
          mapTypeId: "roadmap",
        });
		//var address = document.getElementById("location_address");
        // Create the search box and link it to the UI element.
        const input = document.getElementById("pac-input");
        const searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        // Bias the SearchBox results towards current map's viewport.
        map.addListener("bounds_changed", () => {
          searchBox.setBounds(map.getBounds());
        });
        let markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener("places_changed", () => {
          const places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
          // Clear out the old markers.
          markers.forEach((marker) => {
            marker.setMap(null);
          });
          markers = [];
          // For each place, get the icon, name and location.
          const bounds = new google.maps.LatLngBounds();
          places.forEach((place) => {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            const icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25),
            };
			
			/* var address ='';
				if (place.address_components) {
				  address = [
					(place.address_components[0] && place.address_components[0].short_name || ''),
					(place.address_components[1] && place.address_components[1].short_name || ''),
					(place.address_components[2] && place.address_components[2].short_name || '')
				  ].join(' ');
				  //location_address.value=address;
			//document.getElementsByName("location_address").setAttribute('value',address);
				  //document.getElementById("location_address").value=address;
			} */
			var location_name = document.getElementsByName("location_name")[0].value;
			document.getElementsByName("location_address")[0].value = location_name;
			document.getElementsByName("place_name")[0].value = place.name;
			
            // Create a marker for each place.
            markers.push(
              new google.maps.Marker({
                map,
                icon,
                title: place.name,
                position: place.geometry.location,
              })
            );
			
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
			  //address.value = place.name;
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
	  
	  
	window.onload = function () { 
		getLocation();
	}
				
document.addEventListener('DOMContentLoaded', function() {
   // your code here
	initAutocomplete();
	}, false);
    </script>
	
<style type="text/css">
.popover .popover-content {
font-weight: bold;
color:red;
}

#map {
        height: 100%;
      }

      /* Optional: Makes the sample page fill the window. */
      html,
      body {
        height: 100%;
        margin: 0;
        padding: 0;
      }

      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }

      #target {
        width: 345px;
      }

.pac-container {
    z-index: 10000 !important;
}
.main-content {
    margin-left: 190px;
}

</style>
<main class="content">
<div class="container-fluid p-0">
<h1 class="comptab">
<a href="<?php echo base_url('campaign_management') ?>" style="float: left;background-color: #3a8e2e;border: aliceblue;color: #ffffff;"><i class="fa fa-arrow-circle-left"></i>
</a> Add New NexWorld Campaign 
</h1>
<div class="row">
<div class="col-md-12 col-xl-12">
<div class="card">
<div class="popbox">
<div class="card-body">
<div class="row">
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Campaign Start Date</label>
<div class="input-group date" id="datetimepicker-minimum" data-target-input="nearest">
<input type="text" class="form-control datetimepicker-input"  id="state_date" oninput="clearOldPayementDetails('Campaign_Budget', 'Caenex_Fee', 'Handling_Charges', 'Total_Amount', 'campaignBudget', 'caenexFee', 'handlingCharges', 'totalAmount')" onkeypress="return autoMask(this,event, '');" required >
<!-- <div class="input-group-append" data-target="#datetimepicker-minimum" data-toggle="datetimepicker"> -->
<div class="input-group-append" >
<div class="input-group-text calfont">  <i class="fas fa-fw fa-calendar-alt"></i>
</div>
</div>
</div>
</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Campaign End Date </label>
<div class="input-group date" id="datetimepicker-date" data-target-input="nearest">
<input type="text" class="form-control datetimepicker-input"  id="end_date" oninput="clearOldPayementDetails('Campaign_Budget', 'Caenex_Fee', 'Handling_Charges', 'Total_Amount', 'campaignBudget', 'caenexFee', 'handlingCharges', 'totalAmount')" onkeypress="return autoMask(this,event, '');" required >
<!-- <div class="input-group-append" data-target="#datetimepicker-date" data-toggle="datetimepicker"> -->
<div class="input-group-append" >
<div class="input-group-text calfont"><i class="fas fa-fw fa-calendar-alt"></i>
</div>
</div>
</div>
</div>
</div>
<div class="col-12 col-xl-12">
<div class="form-group">
<label class="col-sm-3 control-label " style = "text-align: left;" for="form-field-2"> Location Name  *</label>
<input type="text" class="form-control" placeholder="Location Name" name="location_name" id="pac-input" style="height: 40px;" required=""/>
<input type="hidden" class="form-control" placeholder="Location Name" name="place_name" style="height: 40px;" required=""/>				
</div>
<div id="map" style="height:300px;"></div>
<div class="form-group col-md-4">
	<label class="form-label" style = "text-align: left;" for="form-field-2"> Location Address  *</label>
	<input type="text" class="form-control" placeholder="Location Address" name="location_address" id="location_address" required="" readonly />
</div>
<!--<div class="form-group">
<label class="form-label">Location</label>
<div class="input-group">
<input type="text" class="form-control" id="location">
<div class="input-group-append">
<div class="input-group-text calfont">
<i class="fas fa-fw fa-map-marker-alt"></i>
</div>
</div>
</div>
</div>-->
</div>
</div>
<!-- coupon payment box starts here -->
<form method="post" id="paymentFrm" enctype="multipart/form-data" action="<?php echo base_url('Merchant/NexWorld_coupon') ?>" autocomplete="off">
<input type="hidden" name="state_date" id="state_date1">
<input type="hidden" name="end_date" id="end_date1">
<input type="hidden" name="location" id="location1">
<input type="hidden" name="payment_status" id="payment_status1">
<div class="row">
<div class="col-12 col-xl-12">
<h4> Reward Type </h4>
<div class="tab">
<ul class="nav nav-tabs mb20" role="tablist">
<!--<li class="nav-item">
<a class="nav-link" href="#Rewardtab-1" data-toggle="tab" role="tab">Coupon</a>
</li>-->
<li class="nav-item">
<a class="nav-link active" href="#Rewardtab-2" data-toggle="tab" role="tab">Cash</a>
</li>
</ul>
<div class="tab-content pad0 boxshadow0">
<div class="tab-pane" id="Rewardtab-1" role="tabpanel">
<div class="form-row">
<div class="form-group col-md-4">
<label for="inputEmail4">Coupon Title</label>
<input type="text" class="form-control" name="Coupon_Title" id="Coupon_Title" placeholder="">
</div>
<div class="form-group col-md-4">
<label for="inputPassword4">Coupon Subtitle</label>
<input type="text" class="form-control" name="Coupon_Subtitle" id="Coupon_Subtitle" placeholder="">
</div>
<div class="form-group col-md-4">
<label for="inputPassword4">Coupon Discount</label>
<input type="text" class="form-control" name="Coupon_Discount" id="Coupon_Discount" placeholder="" oninput="Validate_Only_Numeric_Decimal_Input('Coupon_Discount')">
</div>
</div>
<div class="form-row">
<div class="form-group col-md-4">
<label for="inputEmail4">Coupon Description</label>
<textarea class="form-control" name="Coupon_Description" id="Coupon_Description" placeholder="" rows="3"></textarea>
</div>
<div class="form-group col-md-4">
<label for="inputPassword4">Coupon Terms & Condition</label>
<textarea class="form-control" name="Coupon_Terms" id="Coupon_Terms" placeholder="" rows="3">

</textarea>
</div>
<div class="form-group col-md-4 mb-5 pb-3">
<label for="inputPassword4">Upload Coupon Image</label> 
<div class="custom-file">
<input type="file" name="file" class="custom-file-input uplod" id="file" accept="video/*,image/*">  
<label class="custom-file-label uplod" for="customFile">Drag and drop a file</label>
</div>
</div>
</div>
<div class="row">
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Coupon Code</label>
<input type="text" class="form-control" name="Coupon_Code" id="Coupon_Code" placeholder="">
</div>
</div>
<!-- <div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label"> Campaign Start Date </label>
<div class="input-group date" id="datetimepicker-date1" data-target-input="nearest">
<input type="text"  class="form-control" name="Coupon_Generated_on" id="Coupon_Generated_on"  onkeypress="return autoMask(this,event, '');">
<div class="input-group-append" >
<div class="input-group-text calfont">
<i class="fas fa-fw fa-calendar-alt"></i>
</div>
</div>
</div>
</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Campaign End Date</label>
<div class="input-group date" id="datetimepicker-date2" data-target-input="nearest">
<input type="text" class="form-control" name="Coupon_Expires_on" id="Coupon_Expires_on" onkeypress="return autoMask(this,event, '');">
<div class="input-group-append" >

<div class="input-group-text calfont"><i class="fas fa-fw fa-calendar-alt"></i></div>
</div>
</div>
</div>
</div> -->
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Total Number of Coupons</label>
<input type="text" class="form-control" name="Total_Number_of_Coupons" id="Total_Number_of_Coupons" oninput="Validate_Only_Numeric_Input('Total_Number_of_Coupons')">
</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">No. of Coupons / Day (CAPPING)</label>
<input type="text" class="form-control" name="CAPPING" id="CAPPING" oninput="Validate_Only_Numeric_Input('CAPPING')">
</div>
</div>
</div>
<!-- <div class="row">
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Total Number of Coupons</label>
<input type="text" class="form-control" name="Total_Number_of_Coupons" id="Total_Number_of_Coupons" oninput="Validate_Only_Numeric_Input('Total_Number_of_Coupons')">
</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">No. of Coupons / Day (CAPPING)</label>
<input type="text" class="form-control" name="CAPPING" id="CAPPING" oninput="Validate_Only_Numeric_Input('CAPPING')">
</div>
</div>
</div> -->
<div class="row">
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Campaign Budget <a href="javascript:void(0)">
<span rel="popover" data-trigger="hover" data-placement="top" class="fas fa-fw fa-question-circle"  data-toggle="tooltip" data-content="Min. Campaign Budget : $100">       
</span>
</a></label>
<input type="text" class="form-control" name="Campaign_Budget" id="Campaign_Budget"  oninput="Validate_Only_Numeric_Decimal_Input_And_Calculate_Total_Amount('Campaign_Budget', 'Caenex_Fee', 'Handling_Charges', 'Total_Amount')">

</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Caenex Fee <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" data-toggle="tooltip" rel="popover" data-trigger="hover" data-placement="top" data-content="Depends on Number of Days"></span></a></label>
<input type="text" class="form-control" name="Caenex_Fee" id="Caenex_Fee"  readonly>

</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Handling Charges <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" data-toggle="tooltip" rel="popover" data-trigger="hover" data-content="3% of (Campaign Budget + Caenex Fee)"></span></a></label>
<input type="text" class="form-control" name="Handling_Charges" id="Handling_Charges"  readonly>
</div>
</div>
</div>
<div class="row">
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Total Amount <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" data-toggle="tooltip" rel="popover" data-trigger="hover" data-content="Campaign Budget + Caenex Fee + Handling Charges "></span></a></label>
<input type="text" class="form-control" name="Total_Amount" id="Total_Amount" readonly>
</div>
</div>
</div>
<div class="row text-center">
<div class="col-12 col-xl-12">
<!-- <a  href="#" class="btn btn-success payment_btn" data-toggle="modal" data-target="#defaultModalPrimary" name="addNewCouponCamp"> Submit </a> -->
<a  href="javascript:void(0)" class="btn btn-success payment_btn" onclick="check_date()" > Submit </a>

<!-- BEGIN primary modal -->
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="defaultModalPrimary" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg newmodel_size" role="document">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title popheading">Your Ad Preview </h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-6">
<div class="copuon_ad"><img src="<?php echo base_url('assets/') ?>img/coupon_popadd.jpg" alt="ad images"></div>
</div>

<div class="col-md-6 bgclr">
<div class="bground">

<div class="popright row">
<div class="col-md-4"><img src="<?php echo base_url('assets/') ?>img/poplogo.png" alt="images" class="img-fluid"></div>
<div class="col-md-8">
<h2 class="comp" id="COMPANY">  </h2>
<p class="comptext" id="COMPANY_des"></p>
<h5 class="compoffer" id="company_descount"></h5>
</div>
</div>
<div class="alltextbox">
<h4 id="sub_title" ></h4>
<p id="company_t_C"></p>

<div class="coupanbox">
<h4> Coupon Code </h4>
<div class="coupancode" id="Coupon_Code1">  </div>
<div class="coupandate">
<p>Coupon Generated - <span id="Coupon_Generated">  </span></p>
<p>Expires on <span id="Expires_on">  </span></p>
</div>
</div>
</div>
</div>

</div>
</div>

<div class="row">
<!-- <div class="col-md-6"><a href="#" class=" btn btn-edit btn-block" > Edit </a></div> -->
<div class="col-md-6"><a href="#" class=" btn btn-edit btn-block" data-dismiss="modal" aria-label="Close"> Edit </a></div>
<div class="col-md-6">
<a href="javascript:void(0)" class="btn btn-confirm btn-block" name="confirmAddNewCamp" onclick="showwallet()">Confirm & Proceed to Payment </a>
<!-- <button class="btn btn-confirm btn-block"> Confirm & Proceed to Payment </button> -->
</div>

</div>
</div>

</div>
</div>
</div>
<!-- END primary modal -->

<!------------- Start Wallet amount popup ------------>
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="wallet_popup" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg newmodel_size" role="document">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Campaign Payment</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body m-3">
<label id="payment-errors"></label>
<div class="card-body">
<div class="form-row posi_relative">

<div class="form-group col-md-12">
<label style="float: left;font-size: 17px;"><b>Total campaign amount : </b></label>

<b><label id="total_pay" style="font-size: 17px;"></label></b>


</div>

<div class="form-group col-md-12">
<label style="float: left;font-size: 17px;"><b>Available wallet balance : 
	<input type="checkbox" name="wallete_amount_use" style="float: left;margin: 4px;" id="wallete_amount_use"  checked="" >
</b></label>
<?php 
if(!empty($wallet[0]['total_avail_balance'])){
$balance = $wallet[0]['total_avail_balance'];
}
else {
$balance = "0";
} 
?>
<b><label id="total_balance13" style="margin-left: -6%;font-size: 17px;"><?php echo $balance; ?></label></b>
<input type="hidden" id="total_balance1" name="wallet_amount" style="margin-left: -16%;" value="<?php echo $balance; ?>" readonly="" >

</div>
<div class="form-group col-md-12">
<label style="float: left;font-size: 17px;"><b>You have to pay : </b></label>
<b><label id="total_balance_to_pay" style="margin-left: 7%;font-size: 17px;"></label></b>
<input type="hidden" name="total_balance1" id="amountfill1" readonly>

</div>
<div class="col-md-4" style="display: contents;">
<a href="javascript:void(0)" class="btn btn-confirm btn-block" name="confirmAddNewCamp" onclick="showpayment1()"> Pay now</a>
<!-- <button class="btn btn-confirm btn-block"> Confirm & Proceed to Payment </button> -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!---------------- End Wallet amount popup -------------->

<div class="modal fade" data-keyboard="false" data-backdrop="static" id="payment_popup" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg newmodel_size" role="document">
<div class="modal-content">
<div class="modal-header">

<h4 class="modal-title">Payment</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body m-3">
<label id="payment-errors"></label>
<div class="card-body">
<div class="form-row posi_relative">
<div class="form-group col-md-12">
<label for="inputEmail4" style="float: left;">Card number</label>
<input type="text" class="form-control" required onkeypress="return autoMask(this,event, '#### #### #### ####');"  name="card_number" id="cardnum" placeholder="Card number">
<!-- <div class="visabox"><img src="<?php echo base_url('assets/img/')?>visa_icon.png"></div> -->
</div>
</div>
<div class="form-row">
<div class="form-group col-md-12">
<label for="inputEmail4" style="float: left;">Cardholder name</label>
<input type="text" class="form-control" id="card_name"  oninput="validate_only_alphabates_allowed(this.id)" name="card_name" placeholder="Cardholder name"  required>
</div>
</div>
<div class="form-row">
<div class="form-group col-md-8">
<label for="inputEmail4" style="float: left;">Exp. date</label>
<!-- <div class="form-group col-md-12">
<input type="text" class="form-control" name="exp_date" onkeypress="return autoMask(this,event, '##');" id="expmonth" maxlength="2" placeholder="Month"  required>
</div> -->

<div class="form-group col-md-12" style="margin-left: -14px;">
<input type="text" class="form-control" name="exp_date" onkeypress="return autoMask(this,event, '##/####');" id="expmonth" maxlength="7" minlength="7" placeholder="Month / Year"  required>
</div>
<!-- <div class="form-row">
<div class="form-group col-md-6">
<input type="text" class="form-control" name="exp_date" onkeypress="return autoMask(this,event, '##');" id="expmonth" maxlength="2" placeholder="Month"  required>
</div>
<div class="form-group col-md-6">
<input type="text" class="form-control" placeholder="Year" id="expyear" name="year" onkeypress="return autoMask(this,event, '##');"  required>
</div>
</div> -->
</div>
<div class="form-group col-md-4">
<label for="inputAddress2" style="float: left;">CVV</label>
<input type="text" class="form-control" placeholder="CVV" id="cvv" name="cvv"  onkeypress="return autoMask(this,event, '###');"  required>
</div>
</div>
<div class="form-row">
<div class="form-group col-md-12">
<label for="inputEmail4" style="float: left;">Card nickname(Optional)</label>
<input type="text" class="form-control" placeholder="Card nickname(Optional)" id="Nickname" oninput="validate_only_alphabates_allowed(this.id)" name="nickname">
</div>
</div>                                  

<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<!-- <button class="btn btn-primary">Pay Now</button> -->
<button id="payBtn" class="btn btn-primary">PROCEED TO PAYMENT</button>

</div>
</div>
</div>

</div>
</div>


</div>
</div>

</div>
</form>
<!-- coupon payment box end here -->


<!-- cash payment box starts here -->
<div class="tab-pane active" id="Rewardtab-2" role="tabpanel">
<form method="post" id="paymentFrm1" enctype="multipart/form-data" action="<?php echo base_url('Merchant/NexWorld_cash') ?>">
<input type="hidden" name="state_date1" id="state_date2">
<input type="hidden" name="end_date1" id="end_date2">
<input type="hidden" name="location1" id="location2">
<input type="hidden" name="payment_status1" id="payment_status2">
<div class="row">
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Cash / Grab</label>
<input type="text" class="form-control" name="cashPerGrab" id="cashPerGrab" placeholder="" oninput="Validate_Only_Numeric_Decimal_Input('cashPerGrab')">
</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Grabs / Day</label>
<input type="text" class="form-control" name="grabPerDay" id="grabPerDay" placeholder="" oninput="Validate_Only_Numeric_Input('grabPerDay')">
</div>
</div>
</div>
<div class="row">
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Campaign Budget <a href="javascript:void(0)"><span rel="popover" data-trigger="hover" data-placement="top" class="fas fa-fw fa-question-circle" data-toggle="tooltip" data-content="Min. Campaign Budget : $100"></span></a></label>
<input type="text" class="form-control" id="campaignBudget" name="campaignBudget"  oninput="Validate_Only_Numeric_Decimal_Input_And_Calculate_Total_Amount('campaignBudget', 'caenexFee', 'handlingCharges', 'totalAmount')">

</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Caenex Fee <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" data-toggle="tooltip" rel="popover" data-trigger="hover" data-placement="top" data-content="Number of Days * .25"></span></a></label>
<input type="text" class="form-control" id="caenexFee" name="caenexFee"  readonly>

</div>
</div>
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Handling Charges <a href="javascript:void(0)"><span rel="popover" data-trigger="hover" data-placement="top" class="fas fa-fw fa-question-circle" data-toggle="tooltip" data-content="3% of (Campaign Budget + Caenex Fee)"></span></a></label>
<input type="text" class="form-control" id="handlingCharges"  name="handlingCharges"  readonly>
</div>
</div>
</div>
<div class="row">
<div class="col-12 col-xl-4">
<div class="form-group">
<label class="form-label">Total Amount <a href="javascript:void(0)"><span class="fas fa-fw fa-question-circle" rel="popover" data-trigger="hover" data-toggle="tooltip" data-content="Campaign Budget  + Caenex Fee + Handling Charges"></span></a></label>
<input type="text" class="form-control" id="totalAmount" name="totalAmount"  >
</div>
</div>
</div>
<div class="row text-center">
<div class="col-12 col-xl-12">
<!-- <a  href="#" class="btn btn-success payment_btn" data-toggle="modal" data-target="#defaultModalP" name="addNewRewardCamp"> Submit </a> -->
<a  href="javascript:void(0)" class="btn btn-success payment_btn" onclick="check_date1()" > Submit </a>

<!-- BEGIN primary modal -->
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="defaultModalP" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg newmodel_size" role="document">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title popheading">Your Ad Preview</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-6">
<div class="copuon_ad"><img src="<?php echo base_url('assets/') ?>img/coupon_popadd.jpg" alt="ad images"></div>
</div>
<div class="col-md-6">
<div class="winner"><img src="<?php echo base_url('assets/') ?>img/award.png" alt="images" class="img-fluid"></div>
<div class="alltextbox">
<h4>You won $5</h4>
<p>Lorem ipsum dolor sit amet, consectetuer adipisng elit. Aenean commodo ligula eget dolor. Aeneamassa. Cum nascetur ridiculus mus. Donec quam felis, ultricies nec, </p>
<p>Aenean commodo ligula eget dolor. Aeneamassa. Cum sociis natoque penatibus et magnis diparturient mons.</p>
</div>
</div>
</div>

<div class="row">
<div class="col-md-6"><a href="#" class=" btn btn-edit btn-block" data-dismiss="modal" aria-label="Close"> Edit </a></div>
<div class="col-md-6">
<a href="javascript:void(0)" onclick="showwallet1()" class="btn btn-confirm btn-block"> Confirm & Proceed to Payment </a>
<!-- <button class="btn btn-confirm btn-block" onclick="showpayment()"> Confirm & Proceed to Payment </button> -->


</div>
</div>

</div>
</div>
</div>
<!-- END primary modal -->

</div>
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="wallet_popup1" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg newmodel_size" role="document">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Campaign Payment</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body m-3">
<label id="payment-errors"></label>
<div class="card-body">
<div class="form-row posi_relative">

<div class="form-group col-md-12">
<label style="float: left;font-size: 17px;"><b>Total campaign amount : </b></label>
<b><label id="total_pay1" style="font-size: 17px;"></label></b>
</div>

<div class="form-group col-md-12">
<label style="float: left;font-size: 17px;"><b>Available wallet balance : 
	<input type="checkbox" name="wallete_amount_use1" style="margin: 4px;" id="wallete_amount_use1"  checked="" >
</b></label>
<?php 
if(!empty($wallet[0]['total_avail_balance'])){
$balance = $wallet[0]['total_avail_balance'];
}
else {
$balance = "0";
} 
?>
<label id="total_balance123" style="margin-left: -6%;font-size: 17px;"><b><?php echo $balance; ?></b></label>
<input type="hidden" id="total_balance12" name="wallet_amount1" style="margin-left: -16%;" value="<?php echo $balance; ?>" readonly="" >

</div>

<div class="form-group col-md-12">
<label style="float: left;font-size: 17px;"><b>You have to pay : </b></label>
<b><label id="total_balance_to_pay1" style="margin-left: 7%;font-size: 17px;"></label></b>
<input type="hidden" name="total_balance12" id="amountfill12" readonly>

</div>
<div class="col-md-4" style="display: contents;">
<a href="javascript:void(0)" class="btn btn-confirm btn-block" name="confirmAddNewCamp" onclick="showpayment()"> Pay now</a>
<!-- <button class="btn btn-confirm btn-block"> Confirm & Proceed to Payment </button> -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="defaultModalPrimary12" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg newmodel_size" role="document">
<div class="modal-content">
<div class="modal-header">

<h4 class="modal-title">Payment</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body m-3">
<span id="payment-errors1"></span>
<div class="card-body">

<div class="form-row posi_relative">
<div class="form-group col-md-12">

<label for="inputEmail4" style="float: left;">Card number</label>
<input type="text" class="form-control" required onkeypress="return autoMask(this,event, '#### #### #### ####');"  name="card_number" id="cardnum1" placeholder="Card number">
<!-- <div class="visabox"><img src="<?php echo base_url('assets/img/')?>visa_icon.png"></div> -->
</div>
</div>
<div class="form-row">
<div class="form-group col-md-12">
<label for="inputEmail4" style="float: left;">Cardholder name</label>
<input type="text" class="form-control" id="cardholder_name" oninput="validate_only_alphabates_allowed(this.id)" name="card_name" placeholder="Cardholder name" required >
</div>
</div>
<div class="form-row">
<div class="form-group col-md-8">
<label for="inputEmail4" style="margin-left: -80%;">Expiration date</label>
<div class="form-row">
<div class="form-group col-md-12">
<input type="text" class="form-control" name="exp_date" onkeypress="return autoMask(this,event, '##/####');" id="expmonth1" maxlength="7" minlength="7" placeholder="Month / Year"  required>
</div>
<!-- <div class="form-group col-md-6">
<input type="text" class="form-control" placeholder="Year" id="expyear1" name="year" onkeypress="return autoMask(this,event, '##');"  required>
</div> -->
</div>
</div>
<div class="form-group col-md-4">
<label for="inputAddress2" style="float: left;">CVV</label>
<input type="text" class="form-control" placeholder="CVV" id="cvv1" name="cvv" onkeypress="return autoMask(this,event, '###');"  required>
</div>
</div>
<div class="form-row">
<div class="form-group col-md-12">
<label for="inputEmail4" style="float: left;">Card nickname(Optional)</label>
<input type="text" class="form-control" placeholder="Card nickname(Optional)" id="nickname"  oninput="validate_only_alphabates_allowed(this.id)" name="nickname">
</div>
</div>                                  

<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<!-- <button class="btn btn-primary">Pay Now</button> -->
<button id="payBtn1" class="btn btn-primary">PROCEED TO PAYMENT</button>

</div>
</div>
</div>

</div>
</div>



<div class="modal fade" data-keyboard="false" data-backdrop="static" id="wallet_payment2" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg newmodel_size" role="document">
<div class="modal-content">
<div class="modal-header">

<h4 class="modal-title">Payment</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>

<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<!-- <button class="btn btn-primary">Pay Now</button> -->
<button id="payBtn2" class="btn btn-primary">PROCEED TO PAYMENT</button>

</div>
</div>
</div>

</div>
</div>


</form>
<!-- cash payment box end here -->
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>

</div>
</main>
</div>
</div>
<!-- <label for="from">From</label> <input type="text" id="from" name="from"/> <label for="to">to</label> <input type="text" id="to" name="to"/>
-->
<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script> -->
<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/redmond/jquery-ui.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>


<script>


$("#state_date").datepicker({
	minDate: 0,
	onSelect: function(date) {
	$("#end_date").datepicker('option', 'minDate', date);
	}
});

$("#end_date").datepicker({});


/* $("#Coupon_Generated_on").datepicker({
minDate: 0,
onSelect: function(date) {
$("#Coupon_Expires_on").datepicker('option', 'minDate', date);
}
});

$("#Coupon_Expires_on").datepicker({});

 $("#state_date").datepicker({ minDate: 0 });
 $("#end_date").datepicker({ minDate: 0 });

 $("#Coupon_Generated_on").datepicker({ minDate: 0 });
 $("#Coupon_Expires_on").datepicker({ minDate: 0 }); */


$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();   
});

function readUrl(input) {

	if (input.files && input.files[0]) {
	let reader = new FileReader();
	reader.onload = e => {
	let imgData = e.target.result;
	let imgName = input.files[0].name;
	input.setAttribute("data-title", imgName);
	};
	reader.readAsDataURL(input.files[0]);
	}

}

function Validate_Only_Numeric_Input(input_id){
	let Total_Number_of_Coupons = document.getElementById(input_id).value;
	let regExp = /^[0-9]+$/;
	if (regExp.test(Total_Number_of_Coupons)) {

	}else{
	document.getElementById(input_id).value  = Total_Number_of_Coupons.substring(0, Total_Number_of_Coupons.length-1);
	swal("Only numeric value is allowed");

	}
}

function Validate_Only_Numeric_Decimal_Input(input_id){
	let Total_Number_of_Coupons = document.getElementById(input_id).value;
	let regExp = /^[0-9.]+$/;
	if (regExp.test(Total_Number_of_Coupons)) {
	}else{
	document.getElementById(input_id).value  = Total_Number_of_Coupons.substring(0, Total_Number_of_Coupons.length-1);
	// document.getElementById(input_id).value  = '';
	swal("Only numeric value is allowed");

	}
}

function validate_only_alphabates_allowed(input_id){
	let input_value = document.getElementById(input_id).value;
	let regExp = /^[a-z A-Z]+$/;
if (regExp.test(input_value)) {

}else{

	document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);

}
}

function Validate_Only_Numeric_Decimal_Input_And_Calculate_Total_Amount(input_field, caenex_fee, handling_charges, total_amount){
	let CampaignBudget = document.getElementById(input_field).value;
	let startDate = document.getElementById("state_date").value;
	let endDate = document.getElementById("end_date").value;

	let startDateArray = new Date(startDate.split(" ", 1));
	let endDateArray = new Date(endDate.split(" ", 1));
	var Difference_In_Time = endDateArray.getTime() - startDateArray.getTime();
	var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24); 
	let regExp = /^[0-9.]+$/;
	if(regExp.test(CampaignBudget)) {
	if(Difference_In_Days == 0){
	let caenexFee = 0.25;
	let handlingCharges = ((Number(caenexFee) + Number(CampaignBudget))*3)/100;
	let totalAmount = Math.ceil(Number(CampaignBudget) + Number(caenexFee) + Number(handlingCharges));
	document.getElementById(caenex_fee).value = caenexFee;
	document.getElementById(handling_charges).value = handlingCharges;
	document.getElementById(total_amount).value = totalAmount;
	}else{
	let caenexFee = Number(Difference_In_Days) * 0.25;
	let handlingCharges = ((Number(caenexFee) + Number(CampaignBudget))*3)/100;
	let totalAmount = Math.ceil(Number(CampaignBudget) + Number(caenexFee) + Number(handlingCharges));
	document.getElementById(caenex_fee).value = caenexFee;
	document.getElementById(handling_charges).value = handlingCharges;
	document.getElementById(total_amount).value = totalAmount;
	}

	}else{
	document.getElementById(input_field).value = CampaignBudget.substring(0, CampaignBudget.length-1);
	document.getElementById(caenex_fee).value = 0;
	document.getElementById(handling_charges).value = 0;
	document.getElementById(total_amount).value = 0;
	swal("Only numeric value is allowed");
	}
}   

function set_address(address){
	
	document.getElementById("location_address").value = address;
}


function check_date(){
	// swal("Here's a message!")
	var state_date=document.getElementById("state_date").value;
	document.getElementById("state_date1").value=state_date;
	var end_date=document.getElementById("end_date").value;
	document.getElementById("end_date1").value=end_date;
	var location=document.getElementById("location_address").value;

	let currentDate = new Date();

	//let startDate1 = new Date(state_date.split(" ", 1));
	let startDate = new Date(state_date);
	//let endDate1 = new Date(end_date.split(" ", 1));
	let endDate = new Date(end_date);

	var startDate1 = startDate.toLocaleString();
	var startDate1 = startDate1.split(',')[0];
	var endDate1 = endDate.toLocaleString();
	var endDate1 = endDate1.split(',')[0];
	//document.getElementById("demo").innerHTML = n;
	
	var Difference_In_Time = endDate.getTime() - startDate.getTime();

	var Difference_In_Time3 = startDate.getTime() - currentDate.getTime();
	var Difference_In_Time4 = endDate.getTime() - currentDate.getTime();
	document.getElementById("location1").value=location;
	var Coupon_Title=document.getElementById("Coupon_Title").value;
	var Coupon_Subtitle=document.getElementById("Coupon_Subtitle").value;   
	var file = $('#file').val(); 
	var Coupon_Discount=document.getElementById("Coupon_Discount").value;
	var Coupon_Description=document.getElementById("Coupon_Description").value;     
	var Coupon_Terms=document.getElementById("Coupon_Terms").value;
	var Coupon_Code=document.getElementById("Coupon_Code").value;
	document.getElementById("Coupon_Code1").innerHTML = Coupon_Code;
	
	document.getElementById("Coupon_Generated").innerHTML = startDate1;              
	document.getElementById("Expires_on").innerHTML = endDate1;
	document.getElementById("COMPANY").innerHTML = Coupon_Title;

	document.getElementById("COMPANY_des").innerHTML = Coupon_Description;
	document.getElementById("company_descount").innerHTML = Coupon_Discount+"% OFF*";
	document.getElementById("sub_title").innerHTML = Coupon_Subtitle;

	document.getElementById("company_t_C").innerHTML = Coupon_Terms;

	// let startDate2 = new Date(Coupon_Generated_on.split(" ", 1));
	// let endDate2 = new Date(Coupon_Expires_on.split(" ", 1));

	/*let startDate2 = new Date(Coupon_Generated_on);
	let endDate2 = new Date(Coupon_Expires_on);
	var Difference_In_Time2 = endDate2.getTime() - startDate2.getTime();
	var Difference_In_Time5 = startDate2.getTime() - currentDate.getTime();
	var Difference_In_Time6 = endDate2.getTime() - currentDate.getTime();*/
	var Total_Number_of_Coupons=document.getElementById("Total_Number_of_Coupons").value;
	var CAPPING=document.getElementById("CAPPING").value;
	var Campaign_Budget=document.getElementById("Campaign_Budget").value;
	var Caenex_Fee=document.getElementById("Caenex_Fee").value; 
	var Handling_Charges=document.getElementById("Handling_Charges").value;
	var Total_Amount=document.getElementById("Total_Amount").value;

	if (state_date=="") {
	$("#Campaign_Budget").val('');
	$("#Caenex_Fee").val('');
	$("#Handling_Charges").val('');
	$("#Total_Amount").val('');
	$("#state_date").focus();
	swal("Start Date should Not Be Empty")
	}else if (Campaign_Budget < 50) {
	$("#Campaign_Budget").val('');
	$("#Caenex_Fee").val('');
	$("#Handling_Charges").val('');
	$("#Total_Amount").val('');
	$("#Campaign_Budget").focus();
	swal("Campaign Budget should Be Greater or equal to $50")
	}else if (end_date=="") {
	$("#Campaign_Budget").val('');
	$("#Caenex_Fee").val('');
	$("#Handling_Charges").val('');
	$("#Total_Amount").val('');
	$("#end_date").focus();
	swal("End Date should Not Be Empty")
	}
	else if (Coupon_Title=="") {
	$("#Coupon_Title").focus();     
	swal("Coupon Title should Not Be Empty")
	}else if (Coupon_Subtitle=="") {
	$("#Coupon_Subtitle").focus();  
	swal("Coupon Subtitle should Not Be Empty")
	}else if (file=="") {
	$("#file").focus(); 
	swal("Coupon Image should Not Be Empty")
	}else if (Coupon_Discount=="") {
	$("#Coupon_Discount").focus();  
	swal("Coupon Discount should Not Be Empty")
	}else if (Coupon_Description=="") {
	$("#Coupon_Description").focus();   
	swal("Coupon Description should Not Be Empty")
	}else if (Coupon_Terms=="") {
	$("#Coupon_Terms").focus(); 
	swal("Coupon Terms & Condition should Not Be Empty")
	}else if (Coupon_Code=="") {
	$("#Coupon_Code").focus();  
	swal("Coupon Code should Not Be Empty")
	}
	else if (Total_Number_of_Coupons=="") {
	$("#Total_Number_of_Coupons").focus();  
	swal("Total Number of Coupons should Not Be Empty")
	}else if (CAPPING=="") {
	$("#CAPPING").focus();  
	swal("No. of Coupons / Day (CAPPING) should Not Be Empty")
	}else if (Campaign_Budget=="") {
	$("#Campaign_Budget").focus();  
	swal("Campaign Budget should Not Be Empty")
	}else if (Caenex_Fee=="") {
	swal("Caenex Fee should Not Be Empty")
	}else if (Handling_Charges=="") {
	swal("Handling Charges should Not Be Empty")
	}else if (Total_Amount=="") {   
	swal("Total Amount should Not Be Empty")
	}else{
	$('#defaultModalPrimary').modal('toggle');
	}

}

function clearOldPayementDetails(input1, input2, input3, input4, input5, input6, input7, input8){

	document.getElementById(input1).value = '';
	document.getElementById(input2).value = '';
	document.getElementById(input3).value = '';
	document.getElementById(input4).value = '';
	document.getElementById(input5).value = '';
	document.getElementById(input6).value = '';
	document.getElementById(input7).value = '';
	document.getElementById(input8).value = '';

}

function showpayment1(){

	document.getElementById("defaultModalPrimary").style.display = "none";
	$('#defaultModalPrimary').modal('hide');

	check_balance1();
}




function check_date1(){
	// swal("Here's a message!")
	var state_date=document.getElementById("state_date").value;
	document.getElementById("state_date2").value=state_date;
	var end_date=document.getElementById("end_date").value;
	let currentDate = new Date();
	let startDate = new Date(state_date);
	let endDate = new Date(end_date);
	var Difference_In_Time = endDate.getTime() - startDate.
	getTime();
	var Difference_In_Time2 = startDate.getTime() - currentDate.getTime();
	var Difference_In_Time3 = endDate.getTime() - currentDate.getTime();
	document.getElementById("end_date2").value=end_date;
	var location=document.getElementById("location_address").value;
	document.getElementById("location2").value=location;
	var cashPerGrab=document.getElementById("cashPerGrab").value;
	var grabPerDay=document.getElementById("grabPerDay").value;     
	var campaignBudget=document.getElementById("campaignBudget").value;
	var caenexFee=document.getElementById("caenexFee").value;
	var handlingCharges=document.getElementById("handlingCharges").value;
	var totalAmount=document.getElementById("totalAmount").value;

	if (state_date=="") {
	$("#state_date").focus();   
	swal("Start Date should Not Be Empty")
	}else if (campaignBudget < 50) {
	$("#campaignBudget").val('');
	$("#caenexFee").val('');
	$("#handlingCharges").val('');
	$("#totalAmount").val('');
	$("#campaignBudget").focus();   
	swal("campaign Budget should be greater or equal to $50.")
	}else if (state_date=="") {
	$("#state_date").focus();   
	swal("Start Date should Not Be Empty")
	}else if (end_date=="") {
	$("#end_date").focus();
	swal("End Date should Not Be Empty")
	}
	else if (grabPerDay=="") {
	$("#grabPerDay").focus();
	swal("Grabs / Day should Not Be Empty")
	}else if (file=="") {
	swal("Coupon Image should Not Be Empty")
	}else if (campaignBudget=="") {
	$("#campaignBudget").focus();
	swal("Campaign Budget should Not Be Empty")
	}else if (caenexFee=="") {
	swal("Caenex Fee should Not Be Empty")
	}else if (handlingCharges=="") {
	swal("Handling Charges should Not Be Empty")
	}else if (totalAmount=="") {
	swal("Total Amount should Not Be Empty")
	}else{
	$('#defaultModalP').modal('toggle');
	}

}

function showpayment(){
	//first
	document.getElementById("defaultModalP").style.display = "none";
	$('#defaultModalP').modal('hide');
	check_balance();

}
function showpayment_popup(){
	// first
	$('body').addClass('modal-open');
	$('#defaultModalPrimary12').modal('toggle');
	
}
function showwallet(){

	document.getElementById("defaultModalPrimary").style.display = "none";
	$('#defaultModalPrimary').modal('hide');
	showwallet_popup();
}
function showwallet_popup(){
	//coupon
	$('body').addClass('modal-open');
	var totalAmount=parseInt(document.getElementById("Total_Amount").value);
	document.getElementById("total_pay").innerHTML = totalAmount;
	var total_balance1=parseInt(document.getElementById("total_balance1").value);
	//alert(totalAmount + "mint" + total_balance1);
	var amountfill1 = totalAmount-total_balance1;
	document.getElementById("amountfill12").value=totalAmount;
	document.getElementById("total_balance_to_pay").innerHTML = totalAmount;
	$('#wallet_popup').modal('toggle');
}
function showwallet1(){

	document.getElementById("defaultModalP").style.display = "none";
	$('#defaultModalP').modal('hide');
	showwallet_popup1();

}
function showwallet_popup1(){
	$('body').addClass('modal-open');
	var totalAmount=parseInt(document.getElementById("totalAmount").value);
	document.getElementById("total_pay1").innerHTML = totalAmount;
	var total_balance1=parseInt(document.getElementById("total_balance12").value);
	
	var amountfill1 = total_balance1-totalAmount;
	document.getElementById("amountfill12").value=totalAmount;
	document.getElementById("total_balance_to_pay1").innerHTML = totalAmount;
	$('#wallet_popup1').modal('toggle');
}
function check_balance(){
	// cash
	var wallet_money = parseInt(document.getElementById("total_balance12").value);
	var money_to_pay = parseInt(document.getElementById("amountfill12").value);
	var check_val= $('#wallete_amount_use1').prop('checked');
	
	//alert(wallet_money + "d3k" + money_to_pay + "sfg" +check_val);
	/* if (wallet_money == 0 && money_to_pay != "") {
        swal("Insufficient Wallet Amount");
		
    }else { */
		
	if (check_val==true) {
		if(wallet_money >= money_to_pay)
		{
			document.getElementById("amountfill1").value=wallet_money;
			document.getElementById("total_balance_to_pay").innerHTML = money_to_pay;
			document.getElementById("payment_status2").value=0;
			var form$ = $("#paymentFrm1");
			//submit form to the server
			form$.get(0).submit();
			
		}else{
			swal("Insufficient Wallet Amount");
			/* document.getElementById("amountfill1").value=wallet_money;
			document.getElementById("total_balance_to_pay").innerHTML = money_to_pay;
			document.getElementById("payment_status2").value=1;
			//$('#payment_popup').modal('toggle');
			//$('body').addClass('modal-open');
			$('#defaultModalPrimary12').modal('toggle'); */
		}
	

	}else if(check_val==false){
		
		document.getElementById("amountfill1").value=wallet_money;
		document.getElementById("total_balance_to_pay").innerHTML = money_to_pay;
		document.getElementById("payment_status2").value=1;
		$('#defaultModalPrimary12').modal('toggle');
		//showpayment_popup1();
	}
	//}
}
		
function check_balance1(){
	//coupon
	var wallet_money = parseInt(document.getElementById("total_balance1").value);
	var money_to_pay = parseInt(document.getElementById("amountfill12").value);
	var check_val= $('#wallete_amount_use').prop('checked');
	
	//alert(wallet_money + "dc" + money_to_pay + "dws" + check_val);
	
	/* if (wallet_money == 0 && money_to_pay != "") {
        swal("Insufficient Wallet Amount");
		
    }else { */
		
	if (check_val==true) {
		if(wallet_money >= money_to_pay)
		{
			
			document.getElementById("amountfill12").value=wallet_money;
			document.getElementById("total_balance_to_pay1").innerHTML = money_to_pay;
			document.getElementById("payment_status1").value=0;
			
			var form$ = $("#paymentFrm");
			//submit form to the server
			form$.get(0).submit();
			
		}else{
			swal("Insufficient Wallet Amount");
			/* document.getElementById("amountfill12").value=wallet_money;
			document.getElementById("total_balance_to_pay1").innerHTML = money_to_pay;
			document.getElementById("payment_status1").value=1;
			$('#payment_popup').modal('toggle'); */
			//showpayment_popup();
		}
	

	}else if(check_val==false){
		
		document.getElementById("amountfill12").value=wallet_money;
		document.getElementById("total_balance_to_pay1").innerHTML = money_to_pay;
		document.getElementById("payment_status1").value=1;
		$('#payment_popup').modal('toggle');
		//showpayment_popup();
	}   
	
	//}
}

$('#wallete_amount_use').click(function() {
	//coupon
    var check_val= $('#wallete_amount_use').prop('checked');
    var money_to_pay =parseInt(document.getElementById("Total_Amount").value);
	document.getElementById("total_pay").innerHTML = money_to_pay;
	var wallet_money =parseInt(document.getElementById("total_balance12").value);
	//alert(wallet_money + "fgh" + money_to_pay + "dff" +check_val);
    if (check_val==true) {
		if(wallet_money >= money_to_pay)
		{
			//alert("1");
			//var amountfill1 = totalAmount-total_balance1;
			document.getElementById("amountfill1").value=money_to_pay;
			document.getElementById("total_balance_to_pay").innerHTML = money_to_pay;
			document.getElementById("payment_status1").value=0;
			
		}else{
			//alert("2");
			//var amountfill1 = total_balance1-totalAmount;
			document.getElementById("amountfill1").value=money_to_pay;
			document.getElementById("total_balance_to_pay").innerHTML = money_to_pay;
			document.getElementById("payment_status1").value=1;
			//$('#payment_popup').modal('toggle');
		}
	

	}else if(check_val==false){
		//alert("3");
		document.getElementById("amountfill1").value=money_to_pay;
		document.getElementById("total_balance_to_pay").innerHTML = money_to_pay;
		document.getElementById("payment_status1").value=1;
		//$('#payment_popup').modal('toggle');
	}   
	

})

$('#wallete_amount_use1').click(function() {
	//cash
    var check_val= $('#wallete_amount_use1').prop('checked');
    
    var totalAmount=parseInt(document.getElementById("totalAmount").value);
	document.getElementById("total_pay1").innerHTML = totalAmount;
	var total_balance1=parseInt(document.getElementById("total_balance12").value);
	//alert(totalAmount + "ddqq"+ total_balance1 + "ty" +check_val);
    if (check_val==true) {
		if(totalAmount >= total_balance1)
		{
			//alert('1');
			var amountfill1 = totalAmount-total_balance1;
			document.getElementById("amountfill12").value=amountfill1;
			document.getElementById("total_balance_to_pay1").innerHTML = totalAmount;
			document.getElementById("payment_status2").value=0;
			
			
		}else{
			//alert('2');
			var amountfill1 = total_balance1-totalAmount;
			document.getElementById("amountfill12").value=amountfill1;
			document.getElementById("total_balance_to_pay1").innerHTML = totalAmount;
			document.getElementById("payment_status2").value=1;
			$('#payment_popup').modal('toggle');
		}
	

	}else if(check_val==false){
		//alert('3');
		document.getElementById("amountfill12").value=totalAmount;
		document.getElementById("total_balance_to_pay1").innerHTML = totalAmount;
		document.getElementById("payment_status2").value=1;
		$('#payment_popup').modal('toggle');
	}   

})

function autoMask(field, event, sMask) {
	var KeyTyped = String.fromCharCode(getKeyCode(event));
	if (getKeyCode(event) == 8) {
	return
	}
	if (getKeyCode(event) == 0) {
		return
	}
	if (field.value.length == sMask.length && getKeyCode(event) == 13) {
		return true
	}
	
	var targ = getTarget(event);
	keyCount = targ.value.length;
	if (keyCount == sMask.length) {
		return false
	}
	if (sMask.charAt(keyCount + 1) != "#" && sMask.charAt(keyCount + 1) != "A") {
	field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
		return false
	}
	if (sMask.charAt(keyCount) == "*")
	return true;
	if (sMask.charAt(keyCount) == KeyTyped) {
	return true
	}
	if (sMask.charAt(keyCount) == "#" && isNumeric(KeyTyped))
	return true;
	if (sMask.charAt(keyCount) == "A" && isAlpha(KeyTyped))
	return true;
	if (sMask.charAt(keyCount + 1) == "?") {
	field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
	return true
	}
	if (KeyTyped.charCodeAt(0) < 32)
	return true;
	return false
}

function getKeyCode(e) {
	if (e.srcElement) {
	return e.keyCode
	}
	if (e.target) {
	return e.which
	}
}

function getTarget(e) {
	if (e.srcElement) {
	return e.srcElement
	}
	if (e.target) {
	return e.target
	}
}

function isNumeric(c) {
	var sNumbers = "01234567890-";
	if (sNumbers.indexOf(c) == -1)
	return false;
	else
	return true
}

function isAlpha(c) {
	var sNumbers = "01234567890-";
	if (sNumbers.indexOf(c) == -1)
	return false;
	else
	return true
}

function delete_data(id,tbl,row){
	var result = confirm("Are You Sure");
	if (result) {
	$.post("<?php echo base_url('merchant/delete') ?>",{id:id,tbl:tbl,row:row},function(res){
	$("#saved_card"+id+"").fadeOut();
	})
	}
}
</script>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script> 

<script type="text/javascript">

function usecard(num){
	$("#cardholder").val($("#cardholder_data"+num).val());
	$("#cardnum").val($("#cardnum_data"+num).val());
	$("#expmonth").val($("#expmonth_data"+num).val());
	$("#expyear").val($("#expyear_data"+num).val());

	$('.enter-card-wrapper').slideDown();
}

function usecard(num){
	$("#cardholder1").val($("#cardholder_data"+num).val());
	$("#cardnum1").val($("#cardnum_data"+num).val());
	$("#expmonth1").val($("#expmonth_data"+num).val());
	$("#expyear1").val($("#expyear_data"+num).val());

	$('.enter-card-wrapper').slideDown();
}

//set your publishable key
Stripe.setPublishableKey('pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt');
//  Stripe.setPublishableKey('pk_live_nxQI6OVkFFzTXtd6L2wcq7Kl');

//callback to handle the response from stripe
function stripeResponseHandler(status, response) {
	if (response.error) {
	//enable the submit button
	$('#payBtn').removeAttr("disabled");
	//display the errors on the form
	// $('#payment-errors').attr('hidden', 'false');
	$('#payment-errors').addClass('alert alert-danger');
	$("#payment-errors").html(response.error.message);
	} else {
	var form$ = $("#paymentFrm");
	//get token id
	var token = response['id'];
	//insert the token into the form
	form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
	//submit form to the server
	form$.get(0).submit();
	}
}

function stripeResponseHandler1(status, response) {
	if (response.error) {
	//enable the submit button
	$('#payBtn1').removeAttr("disabled");
	//display the errors on the form
	// $('#payment-errors').attr('hidden', 'false');
	$('#payment-errors1').addClass('alert alert-danger');
	$("#payment-errors1").html(response.error.message);
	} else {
	var form$ = $("#paymentFrm1");
	//get token id
	var token = response['id'];
	//insert the token into the form
	form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
	//submit form to the server
	form$.get(0).submit();
	}
}
	$(document).ready(function () {
	//on form submit
	$("#paymentFrm").submit(function (event) {
	//disable the submit button to prevent repeated clicks
	$('#payBtn').attr("disabled", "disabled");

		//create single-use token to charge the user

		var exp_data1= $('#expmonth').val()
		var fields = exp_data1.split('/');

		var expmonth = fields[0];
		var expyear = fields[1];


		Stripe.createToken({
		number: $('#cardnum').val(),
		cvc: $('#cvv').val(),
		exp_month: expmonth,
		exp_year: expyear
		}, stripeResponseHandler);

		//submit from callback
		return false;
	});

		$("#paymentFrm1").submit(function (event) {
			//disable the submit button to prevent repeated clicks
			$('#payBtn1').attr("disabled", "disabled");

			//create single-use token to charge the user
			var exp_data= $('#expmonth1').val()
			var fields = exp_data.split('/');

			var expmonth1 = fields[0];
			var expyear1 = fields[1];


			Stripe.createToken({
			number: $('#cardnum1').val(),
			cvc: $('#cvv1').val(),
			exp_month: expmonth1,
			exp_year: expyear1
			}, stripeResponseHandler1);

			//submit from callback
			return false;
		});
});
</script>

<script>
$('[rel=popover]').popover({ html : true });
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();   
});
</script>
</body>

</html>