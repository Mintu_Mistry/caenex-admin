<?php
$this->load->view('merchant/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('merchant/include/nev');
?>

<div class="main">
<?php
$this->load->view('merchant/include/header');
?>

			<main class="content">
				<div class="container-fluid p-0">
				<h1 class="h3 mb-3 profile_tab mar-b0"> Support </h1>
				<?php
				if ($this->session->flashdata('add')) {
					?>									
					<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
           			<span aria-hidden="true">×</span>
          			</button>
					<div class="alert-message">
					<?php echo $this->session->flashdata('add'); ?>
					</div>
				</div>
				<?php		
				}
				?>
				
				<div id="payment_box">
					<div class="row justify-content-md-center">
						
						<div class="col-md-12 col-xl-12">
								<div class="card-body">
									<h5 class="card-title black"> Help Articles </h5>
									<div id="accordion">
										  <?php
										  foreach ($help_articles as $key => $value) {
										  
										  	?>
										  <div class="card mb-0 box-shadow">
										    <div class="card-header pb-0" id="headingTwo">
										      <h5 class="mb-0">
										        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo<?php echo $key ?>" aria-expanded="false" aria-controls="collapseTwo">
										          <?php echo $value['title'] ?>
										        </button>
										      </h5>
										    </div>
										    <div id="collapseTwo<?php echo $key ?>" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
										      <div class="card-body">
										        <?php echo $value['details'] ?>
										      </div>
										    </div>
										  </div>
										  <?php
											}
											?>
										</div>
								</div>
						</div>

						<div class="col-md-6 col-xl-6">
								<div class="card-header p-0">
									<h5 class="card-title"> Couldn't find what you needed? Send us an email </h5>
								</div>
								<div class="needid">
								<form method="post" name="myForm1" onsubmit="return validateForm1()" action="<?php echo base_url('Merchant/add_support') ?>" >
									<div class="form-group">
										<input class="form-control form-control-lg" type="text" name="name" placeholder="Name" required="">
									</div>
									<div class="form-group">
										<p id="error_email1" style="text-align: center;color: red;font-size: 20px;">		</p>
										<input class="form-control form-control-lg" type="email" name="email" placeholder="Email"  required="">
									</div>
									<div class="form-group">
										<input class="form-control form-control-lg" type="text" name="subject" placeholder="Subject" required="">
									</div>
									<div class="form-group">
										<!-- <textarea class="form-control" style="background: transparent;" name="message" rows="3" placeholder="Message" required=""></textarea> -->
										<input class="form-control form-control-lg" name="message" placeholder="Message" required="">
									</div>
									
									<div class="text-center mt-3">
										<!-- <div class="btn btn-lg btn-primary green_gradient"> Send Message </div> -->
										<button class="btn btn-lg btn-primary green_gradient">Send Message</button>
									</div>
								</form>
								</div>
						</div>

					</div>
				</div>
</div>

			</main>
		</div>
	</div>

	<script src="js\app.js"></script>

</body>

</html>
<script type="text/javascript">
	function validateForm1() {
		 
		  var email = document.forms["myForm1"]["email"].value;
		 
          // let regex = /^[A-Za-z]\w{7,30}$/;
          let email_pettern=/^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/;
          let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
          if (email_pettern.test(email)) {
          	document.getElementById('error_email1').innerHTML = '';
          	return true;
         
    	}else{
    		document.getElementById('error_email1').innerHTML = 'Please Enter Valid Email Format';
        return false;  
    	}

    
		 
			 // return false;
	}
</script>