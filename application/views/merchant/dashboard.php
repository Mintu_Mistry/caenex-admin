<?php
$this->load->view('merchant/include/head');
?>

<body>
	<div class="wrapper">
		<?php
			$this->load->view('merchant/include/nev');
		?>

		<div class="main">
			<?php
				$this->load->view('merchant/include/header');
			?>

			<main class="content">
				<div class="container-fluid p-0">

					<?php
						if ($this->session->flashdata('merch_login')) {
						?>
						<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
						<div class="alert-message">
							<strong>Success!</strong> <?php echo $this->session->flashdata('merch_login'); ?>
						</div>
						</div>
						<?php
						}
					?>


					<div class="row">
						<div class="col-12 col-sm-6 col-xl d-flex">
							<div class="card flex-fill">
								<div class="card-body py-4">
									<div class="media">
										<div class="d-inline-block mt-2 mr-3">
											<img src="<?php echo base_url('assets/') ?>img/ads.png" alt="total_aim">
										</div>
										<div class="media-body">
											<h3 class="mb-2 numberhed"> <?php echo count($total_nexworld_compaigns) + count($total_nexplay_compaigns) ?> </h3>
											<div class="mb-0 numbertext">Total Campaigns</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-xl d-flex">
							<div class="card flex-fill">
								<div class="card-body py-4">
									<div class="media">
										<div class="d-inline-block mt-2 mr-3">
											<img src="<?php echo base_url('assets/') ?>img/total_aim.png" alt="total_aim">
										</div>
										<div class="media-body">
											<h3 class="mb-2 numberhed"> <?php echo count($nexworld) + count($nexplay) ?> </h3>
											<div class="mb-0 numbertext">Active Campaigns</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-xl d-flex">
							<div class="card flex-fill">
								<div class="card-body py-4">
									<div class="media">
										<div class="d-inline-block mt-2 mr-3">
											<img src="<?php echo base_url('assets/') ?>img/dollar.jpg" alt="dollar">
										</div>
										<div class="media-body">
											<h3 class="mb-2 numberhed"> $<?php echo $wallet_balance ?> </h3>
											<div class="mb-0 numbertext"> Available Balance </div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-xl d-flex">
							<!-- <div class="card flex-fill">
								<div class="card-body py-4">
									<div class="media">
										<div class="d-inline-block mt-2 mr-3">
											<i class="feather-lg text-danger" data-feather="shopping-bag"></i>
										</div>
										<div class="media-body">
											<h3 class="mb-2">43</h3>
											<div class="mb-0">Pending Orders</div>
										</div>
									</div>
								</div>
							</div> -->
						</div>	
					</div>
					
<!-- ------------  Tab data -------------- -->

<?php

if (!empty($nexworld)) {
	?>
<div class="row">
	<div class="col-12 col-lg-12">
							<div class="tab">
								<ul class="nav nav-tabs nav-justified" role="tablist">
									<li class="nav-item"><a class="nav-link active" href="#tab-1" data-toggle="tab" role="tab" aria-selected="true"> NexWorld </a></li>
									<li class="nav-item">
										<!-- <a class="nav-link" href="#tab-2" data-toggle="tab" role="tab" aria-selected="false"> NexPlay </a> -->
										<a class="nav-link newclr" href="#tab-2" data-toggle="tab" role="tab" aria-selected="true"> NexPlay </a>
									</li>
								</ul>
								<div class="tab-content pad0 mt2">
									<div class="tab-pane active" id="tab-1" role="tabpanel">
										<!-- <h4 class="tab-title">Default tabs</h4> -->
							<div class="card flex-fill">
								<table class="table table-striped my-0">
									<thead>
										<tr>
											<th> Campaign Name </th>
											<th class="d-none d-xl-table-cell"> Reward Type </th>
											<!--<th class="d-none d-xl-table-cell"> No. of Coupons Left </th>-->
											<th>Dollar Amount Left</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
									<?php 
									/* echo "<pre>";
									print_r($nexworld);
									die(); */
									foreach($nexworld as $nworld)
									{
										if ($nworld['type']==1) {
												//echo "Coupon";
												?>
											<!--<tr style="background: 6db239#ff9e00;color: white;">
											<td><?=$nworld['coupon_title']?></td>
											<td class="d-none d-xl-table-cell">Coupon</td>
											<td class="d-none d-xl-table-cell" ><?=$nworld['total_number_of_coupons']?>/<?=$nworld['total_number_of_coupons'] -$nworld['redeemed_coupon']?></td>
											<td><span >-</span></td>
										</tr>-->
											
											<?php		
											}elseif ($nworld['type']==2) {
												//echo "Dollar Amount";?>
												
											<tr style="background: #ff9e00;color: white;">
											<td>Cash</td>
											<td class="d-none d-xl-table-cell">Dollar Amount</td>
											<!--<td class="d-none d-xl-table-cell" >-</td>-->
											<td><span>$<?=$nworld['cash_grab']?>/$<?=$nworld['cash_grab']?></span></td>
											<td><?php
												if($nworld['status'] ==1)
												{
													echo "Active";
												}else{
													echo "Inactive";
												}
											
											?></td>
											
										</tr>
									<?php }} ?>
									</tbody>
								</table>
							</div>
						
									
									</div>
									<div class="tab-pane" id="tab-2" role="tabpanel">
										<div class="card flex-fill">
										
								<table class="table table-striped my-0" id="datatables-basic">
									<thead class="coltab">
										<tr>
											<th> Game Type </th>
											<th class="d-none d-xl-table-cell"> Ad Type </th>
											<th class="d-none d-xl-table-cell"> No. of Impressions Left </th>
											<th>Video Clicked</th>
											<th>Video Full Streamed</th>
											<th>Payout</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
									<?php 
									
									foreach($nexplay as $nplay)
									{
									?>
										<tr>
											<td><?=$nplay['game_name']?></td>
											<td class="d-none d-xl-table-cell"><?=$nplay['ad_type']?></td>
											<td class="d-none d-xl-table-cell"><?=$nplay['total_number_of_impressions']?>/<?=$nplay['total_number_of_impressions'] -($nplay['total_win']+$nplay['total_loose'])?></td>
											<td class="d-none d-xl-table-cell"><?=$nplay['video_clicked']?></td>
											<td class="d-none d-xl-table-cell"><?=$nplay['video_full_streamed']?></td>
											<td class="d-none d-xl-table-cell">$<?=$nplay['total_payout']?></td>
											<td><?php
													if($nplay['status'] ==1)
													{
														echo "Active";
													}else{
														echo "Inactive";
													}
												
												?></td>
										</tr>
									<?php }?>
										
									</tbody>
								</table>
							</div>
									</div>
									
								</div>
							</div>
						</div>
</div>

<?php
}else{
?>
<div class="row">
	<div class="col-12 col-lg-12">

		<div class="modal-content" >
			<div class="modal-header singmodel">
				<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button> -->
			</div>
			<div class="modal-body signmod_body">
				<!-- <div class="logo" style="margin-top: 15px;"><img src="<?php echo base_url('assets/img/sign_pop_logo.png')?>" alt="logo" class="img-fluid"> CAENEX </div> -->
				<h4 class="text-center"> You have no active campaigns, click on the button to create one.</p></h4>
				<p id="otp_incorrect" style="color: red"></p>
				<div class="otpsend">
				 <p><a href="<?php echo base_url('campaign_management') ?>">Click Here</a></p>
			</div>
			</div>
			
		</div>
	</div>
</div>

<?php
}
?>

<!-- ------------  End Tab data -------------- -->

					

				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="<?php echo base_url('assets/') ?>js\settings.js"></script>
	<script src="<?php echo base_url('assets/') ?>js\app.js"></script> -->

	<script>
		$(function() {
			$("#datetimepicker-dashboard").datetimepicker({
				inline: true,
				sideBySide: false,
				format: "L"
			});
		});
	</script>
	<script>
		$(function() {
			// Line chart
			new Chart(document.getElementById("chartjs-dashboard-line"), {
				type: "line",
				data: {
					labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Sales ($)",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.primary,
						data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
					}, {
						label: "Orders",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.tertiary,
						borderDash: [4, 4],
						data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					tooltips: {
						intersect: false
					},
					hover: {
						intersect: true
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						xAxes: [{
							reverse: true,
							gridLines: {
								color: "rgba(0,0,0,0.05)"
							}
						}],
						yAxes: [{
							ticks: {
								stepSize: 500
							},
							display: true,
							borderDash: [5, 5],
							gridLines: {
								color: "rgba(0,0,0,0)",
								fontColor: "#fff"
							}
						}]
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			// Pie chart
			new Chart(document.getElementById("chartjs-dashboard-pie"), {
				type: "pie",
				data: {
					labels: ["Direct", "Affiliate", "E-mail", "Other"],
					datasets: [{
						data: [2602, 1253, 541, 1465],
						backgroundColor: [
							window.theme.primary,
							window.theme.warning,
							window.theme.danger,
							"#E8EAED"
						],
						borderColor: "transparent"
					}]
				},
				options: {
					responsive: !window.MSInputMethodContext,
					maintainAspectRatio: false,
					legend: {
						display: false
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>

</body>

</html>