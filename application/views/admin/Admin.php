<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('Merchant_modal');
     // $this->load->library('csvimport');
      	$this->load->helper('directory');
		
	}

	public function index()
	{	
		$submit=$this->input->post('submit');
		if(isset($submit)){
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			if(!empty($email) && !empty($password))
			{
				$data = array('email' =>$this->input->post('email') ,'password' =>$this->input->post('password'));

				$con['email']=$this->input->post('email');
				$con['password']=$this->input->post('password');

				$check_user_details=$this->Merchant_modal->get_row_data('*','tbl_admin',$data);
				//print_r($check_user_details);die;

				if ($check_user_details['email'] == $con['email'] && $check_user_details['password'] == $con['password']) {
					$this->session->set_userdata('id',$check_user_details['id']);
					$this->session->set_userdata('name',$check_user_details['name']);
					
					redirect("admin_dashboard");

				}else{

					$this->session->set_flashdata('loginerror','Email or Password is invalid');

				}
			}else{
				$this->session->set_flashdata('loginerror','Email or Password field can not be empty.');
			}
		}
		$this->load->view('admin/login');
	}

	
	public function admin_dashboard(){
		$con['status']='1';
		$con['delete_status']='1';
		$data['user']=$this->Merchant_modal->get_all_data('*','tbl_user',$con);
		$data['user']=$this->Merchant_modal->get_all_data('*','tbl_user',$con);
		$con2['tbl_nexworld_cash.delete_status']='1';
		$con2['tbl_nexworld_cash.status']='1';
		$joincon="tbl_nexworld_cash.merchant_id=tbl_merchants.merchant_id";
		$nexworld_cash=$this->Merchant_modal->join_data('tbl_nexworld_cash.*,tbl_merchants.merchant_name','tbl_nexworld_cash','tbl_merchants',$joincon,$con2);
		$joincon1="tbl_nexworld_coupon.merchant_id=tbl_merchants.merchant_id";
		$con3['tbl_nexworld_coupon.delete_status']='1';
		$con3['tbl_nexworld_coupon.status']='1';
		$nexworld_coupon=$this->Merchant_modal->join_data('tbl_nexworld_coupon.*,tbl_merchants.merchant_name','tbl_nexworld_coupon','tbl_merchants',$joincon1,$con3);
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
		usort($array_merge1, array("Admin", "date_compare"));
		$data['nexworld']=$array_merge1;


		$con5['tbl_nexplay_caenex_game.delete_status']='1';
		$con5['tbl_nexplay_caenex_game.status']='1';
		$joincon5="tbl_nexplay_caenex_game.merchant_id=tbl_merchants.merchant_id";
		$nexplay_caenex_game=$this->Merchant_modal->join_data('tbl_nexplay_caenex_game.*,tbl_merchants.merchant_name','tbl_nexplay_caenex_game','tbl_merchants',$joincon5,$con5);

		$con4['tbl_nexplay_custom_game.delete_status']='1';
		$con4['tbl_nexplay_custom_game.status']='1';
		$joincon4="tbl_nexplay_custom_game.merchant_id=tbl_merchants.merchant_id";
		$nexplay_custom_game=$this->Merchant_modal->join_data('tbl_nexplay_custom_game.*,tbl_merchants.merchant_name','tbl_nexplay_custom_game','tbl_merchants',$joincon4,$con4);
		$array_merge2=array_merge($nexplay_caenex_game,$nexplay_custom_game);
		if (!empty($array_merge2)) {
		usort($array_merge2, array("Admin", "date_compare"));
		}
		
		$data['nexplay']=$array_merge2;

		$data['merchant']=$this->Merchant_modal->get_all_data('*','tbl_merchants',$con);
		$this->load->view('admin/dashboard',$data);
	}
	

	public function user_management(){
		$con['delete_status']=1;
		$order_by='DESC';
		$colum='id';
		$userarr = $this->Merchant_modal->get_all_data('*','tbl_user',$con,$order_by,$colum);
		$user_arr = array();
		foreach($userarr as $userdata)
		{
			$wallet_money = $this->Merchant_modal->get_user_wallet($userdata['id']);
			/* echo "<pre>";
			print_r($wallet_money);
			die(); */
			if(!empty($wallet_money))
			{
				$userdata['wallet_money'] = $wallet_money[0]['total_avail_balance'];
				$userdata['coupon_earned'] = $wallet_money[0]['coupon_money'];
				$userdata['nexplay_money'] = $wallet_money[0]['nextworld_money'];
				$userdata['nexworld_money'] = $wallet_money[0]['nexplay_money'];
			}else{
				$userdata['wallet_money'] = "0";
				$userdata['coupon_earned'] = "0";
				$userdata['nexplay_money'] = "0";
				$userdata['nexworld_money'] = "0";
			}
			$user_arr[] = $userdata;
		}
		//echo "<pre>";
		//print_r($user_arr);
		//die();
		
		$data['user'] = $user_arr;
		//echo $this->db->last_query();die;
		$this->load->view('admin/user_management',$data);
	}

	public function add_user(){
		$user_id=$this->input->post('user_id');
		// if (empty($user_id)) {
		 $user_data = array('name' =>$this->input->post('name') ,'email' =>$this->input->post('email') ,'password' =>$this->input->post('passowrd') ,'street' =>$this->input->post('street') ,'city' =>$this->input->post('city') ,'state' =>$this->input->post('state') ,'zip' =>$this->input->post('zip') ,'country' =>$this->input->post('country') ,'phone' =>$this->input->post('phone') );
		
		if(!empty($user_id)){
			$user_data = array('name' =>$this->input->post('name') ,'email' =>$this->input->post('email') ,'street' =>$this->input->post('street') ,'city' =>$this->input->post('city') ,'state' =>$this->input->post('state') ,'zip' =>$this->input->post('zip') ,'country' =>$this->input->post('country') ,'phone' =>$this->input->post('phone'),'status'=>$this->input->post('status') );
			$com['id']=$user_id;
		if($this->Merchant_modal->update('tbl_user',$user_data,$com)){
				$this->session->set_flashdata('add_user','User Updated Successfully');
				redirect(base_url('user_management'));
			}
		}else{
			$con['email']=$this->input->post('email');

		if($this->Merchant_modal->get_row('*','tbl_user',$con)){

			$this->session->set_flashdata('user_error','Email already exists');

		}else{

			if($this->Merchant_modal->insert('tbl_user',$user_data)){
				$this->session->set_flashdata('add_user','User Added Successfully');
				
			}
		}
		}
		redirect(base_url('user_management'));
	}


	public function check_email(){
		
		$con['merchant_email']=$this->input->post('email');
		$data=$this->Merchant_modal->get_row('merchant_email','tbl_merchants',$con);
		if ($data) {
			echo $data; //"Email Already Exists"
		}else{
			echo $data;
		}		

	}


	public function merchant_management()
	{
		$order_by='DESC';
		$colum='merchant_id';
		$con['delete_status']=1;
		$merchant_arr = $this->Merchant_modal->get_all_data('*','tbl_merchants',$con,$order_by,$colum);
		$merchant_data_arr = array();
		
		foreach($merchant_arr as $merchant_data)
		{
			//$merchant_data['merchant_id'] = '2';
			$total_revenue = $this->Merchant_modal->get_merchant_revenue($merchant_data['merchant_id']); 
			//echo "<pre>";
			//print_r($total_revenue);
			//die();
			if(count($total_revenue) > 0)
			{
				if(!empty($total_revenue['total_revenue']))
				{
					$merchant_data['total_revenue'] = $total_revenue['total_revenue'];
				}else{
					$merchant_data['total_revenue'] = '0';
				}
			}else{
				$merchant_data['total_revenue'] = '0';
			}
			
			$merchant_data_arr[] = $merchant_data;
		}
		//echo "<pre>";
		//print_r($merchant_data_arr);
		//die();
		$data['merchant'] = $merchant_data_arr;
		$this->load->view('admin/merchant_management',$data);	

	}

	public function add_merchant(){
		$merchant_data = array('merchant_name' =>$this->input->post('merchant_name') ,'businessname' =>$this->input->post('businessname') ,'merchant_email' =>$this->input->post('merchant_email') ,'user_password' =>$this->input->post('user_password') ,'merchant_address' =>$this->input->post('merchant_address') ,'merchant_city' =>$this->input->post('merchant_city') ,'merchant_state' =>$this->input->post('merchant_state') ,'zip' =>$this->input->post('zip') ,'merchant_country' =>$this->input->post('merchant_country'),'merchant_taxid'=>$this->input->post('taxid'));

		$merchant_id=$this->input->post('merchant_id');
		if ($merchant_id) {
			$merchant_data_update = array('merchant_name' =>$this->input->post('merchant_name') ,'businessname' =>$this->input->post('businessname') ,'merchant_email' =>$this->input->post('merchant_email') ,'merchant_address' =>$this->input->post('merchant_address') ,'merchant_city' =>$this->input->post('merchant_city') ,'merchant_state' =>$this->input->post('merchant_state') ,'zip' =>$this->input->post('zip') ,'merchant_country' =>$this->input->post('merchant_country'),'status' =>$this->input->post('status'),'merchant_taxid'=>$this->input->post('taxid'));
			$com['merchant_id']=$merchant_id;
			if($this->Merchant_modal->update('tbl_merchants',$merchant_data_update,$com)){
				$this->session->set_flashdata('corrct','Merchant Updated Successfully');
			}
			
		}else{
		$con['merchant_email']=$this->input->post('merchant_email');

		if($this->Merchant_modal->get_row('*','tbl_merchants',$con)){

			$this->session->set_flashdata('merchant_error','Email already exists');

		}else{

			if($this->Merchant_modal->insert('tbl_merchants',$merchant_data)){
				$this->session->set_flashdata('corrct','Merchant Added Successfully');
			}
		}
		}
		redirect(base_url('merchant_management'));
	}

	
	public function get_merchant_details()
	{
		$order_by='DESC';
		$colum='merchant_id';
		$con['delete_status']=1;
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		
		$merchant_arr = $this->Merchant_modal->filter_merchant_data('*','tbl_merchants',$con,$order_by,$colum);
		$merchant_data_arr = array();
		$i =0;
		$data = array();
		/* echo "<pre>";
		print_r($merchant_arr);
		die(); */
		/* [merchant_id] => 26
            [merchant_name] => sZCsdc
            [merchant_email] => sdsa@df.com
            [merchant_phone] => 
            [user_password] => asdashJJ@333
            [merchant_address] => sdcs
            [merchant_city] => Noida
            [merchant_state] => UP
            [merchant_country] => India
            [location_lat] => 
            [location_long] => 
            [businessname] => zsdcs
            [merchant_taxid] => 
            [otp] => 
            [img] => 
            [delete_status] => 1
            [status] => 1
            [create_date] => 2021-01-29 15:01:30
            [zip] => 34353
            [merchant_phonepin] =>  */
		foreach($merchant_arr as $merchant_data)
		{
			$i++;
			//$merchant_data['merchant_id'] = '2';
			$total_revenue = $this->Merchant_modal->get_merchant_revenue($merchant_data['merchant_id'],$from,$to); 
			
			if(count($total_revenue) > 0)
			{
				if(!empty($total_revenue['total_revenue']))
				{
					$merchant_data['total_revenue'] = $total_revenue['total_revenue'];
				}else{
					$merchant_data['total_revenue'] = '0';
				}
			}else{
				$merchant_data['total_revenue'] = '0';
			}
			//echo "<pre>";
			//print_r($merchant_data);
			//die();
			if($merchant_data['status']==1){
				
				$status = "<span class='badge badge-success'>Active</span>";
			}else{
				$status = "<span class='badge badge-warning'>Inactive</span>"; 
			}
			$merchant_id = $merchant_data['merchant_id'];
			$edit = "<a href='javascript:void(0)'><i class='align-middle mr-2 fas fa-fw fa-edit' data-toggle='modal' data-target='#sizedModalSm$i'></i></a>";
											
			$delete = "<a href='javascript:void(0)' onclick='delete_data($merchant_id)'><i class='align-middle mr-2 far fa-fw fa-trash-alt'></i></a>";
			
			$view = "<a href='".base_url('billboard_details/'). $merchant_id."'><i class='align-middle fas fa-eye'></i></a>";
													
			$row = array();
			$row[] = $i;
			$row[] = $merchant_data['merchant_name'];
			$row[] = $merchant_data['businessname'];
			$row[] = $merchant_data['merchant_email'];
			$row[] = $merchant_data['merchant_address'].' , '.$merchant_data['merchant_city'].' , '.$merchant_data['merchant_state'].' , '.$merchant_data['merchant_country'].' , '.$merchant_data['zip'];
			$row[] = $merchant_data['merchant_taxid'];
			$row[] = $merchant_data['total_revenue'];
			$row[] = $status;
			$row[] = $edit.$delete.$view;
			$data[]= $row; 
        	
		}
		
		
		echo json_encode(array("data"=>$data));
	}
	
	public function delete(){

		$id=$this->input->post('id');
		$tbl=$this->input->post('tbl');
		$row=$this->input->post('row');
		$data['delete_status']=0;
		$con[$row]=$id;
		
		$con1['user_id'] = $this->session->userdata('merchant_id');
		$wallet_amount = $this->Merchant_modal->get_all_data('*',' tbl_wallet', $con1, 'desc', 'wallet_id');
		$wallet_id = $wallet_amount[0]['wallet_id'];
		
		if($this->Merchant_modal->update($tbl,$data,$con)){
			if ($tbl=='tbl_nexplay_custom_game') {
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 10;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 10;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 10;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
				
				$this->session->set_flashdata('add_user','Nexplay Custom Game Entry Deleted Successfully');
			}elseif($tbl=='tbl_nexplay_caenex_game'){
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 10;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 10;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 10;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
			
				$this->session->set_flashdata('add_user','Nexplay Caenex Game Entry Deleted Successfully');
			}elseif ($tbl=='tbl_nexworld_coupon') {
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 100;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 100;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 100;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
				
				$this->session->set_flashdata('add_user','Nexworld Coupon Entry Deleted Successfully');
			}elseif ($tbl=='tbl_nexworld_cash') {
				
				$total_avail_balance = $wallet_amount[0]['total_avail_balance'] - 100;
				$totalAmount = $wallet_amount[0]['total_avail_balance'] - 100;
				$total_withdraw_balance = $wallet_amount[0]['total_withdraw_balance'] + 100;
				$wallet_data = array('total_avail_balance'=> $total_avail_balance, 'total_withdraw_balance' => $total_withdraw_balance);
				$where = "wallet_id = '$wallet_id'";
				$wallt_amt = $this->Merchant_modal->update('tbl_wallet', $wallet_data, $where);
				
				$this->session->set_flashdata('add_user','Nexworld Cash Entry Deleted Successfully');
			}elseif ($tbl=='tbl_faq') {
				$this->session->set_flashdata('add_user','FAQ Deleted Successfully');
			}else{
			$this->session->set_flashdata('add_user','Data Deleted Successfully');
		}
			echo "1";
		}

	}

	public function update_status(){
		
		$data['id_status']=$this->input->post('status');
		$con['id']=$this->input->post('id');
		if($this->Merchant_modal->update('tbl_id_verification1',$data,$con)){
			$this->session->set_flashdata('corrct','Status Updated Successfully');
			echo "1";
		}

	}

	public function update_merchant_status(){
		
		$data['add_status']=$this->input->post('status');
		$con['id']=$this->input->post('id');
		$tbl=$this->input->post('tbl');
		if($this->Merchant_modal->update($tbl,$data,$con)){
			echo "1";
			if($this->Merchant_modal->update($tbl,$data,$con)){
			if ($tbl=='tbl_nexplay_custom_game') {
				$this->session->set_flashdata('add_user','Nexplay Custom Game Entry Status Changed Successfully');
			}elseif($tbl=='tbl_nexplay_caenex_game'){
				$this->session->set_flashdata('add_user','Nexplay Caenex Game Entry Status Changed Successfully');
			}elseif ($tbl=='tbl_nexworld_coupon') {
				$this->session->set_flashdata('add_user','Nexworld Coupon Entry Status Changed Successfully');
			}elseif ($tbl=='tbl_nexworld_cash') {
				$this->session->set_flashdata('add_user','Nexworld Cash Entry Status Changed Successfully');
			}else{
			$this->session->set_flashdata('add_user','Status Changed Successfully');
		}
			
		}

	}
}

	function date_compare($element1, $element2) { 

    $datetime1 = strtotime($element1['create_date']); 
    $datetime2 = strtotime($element2['create_date']); 
    // print_r($element1['create_date']);
    // print_r($element2['create_date']);die;
    return $datetime1 - $datetime2; 

	}


	public function billboard_management(){
		$con['tbl_nexworld_cash.delete_status']='1';
		$joincon="tbl_nexworld_cash.merchant_id=tbl_merchants.merchant_id";
		$nexworld_cash=$this->Merchant_modal->join_data('tbl_nexworld_cash.*,tbl_merchants.merchant_name','tbl_nexworld_cash','tbl_merchants',$joincon,$con);
		$con1['tbl_nexworld_coupon.delete_status']='1';
		$joincon1="tbl_nexworld_coupon.merchant_id=tbl_merchants.merchant_id";
		$nexworld_coupon=$this->Merchant_modal->join_data('tbl_nexworld_coupon.*,tbl_merchants.merchant_name','tbl_nexworld_coupon','tbl_merchants',$joincon1,$con1);
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
		usort($array_merge1, array("Admin", "date_compare"));
		$data['nexworld']=$array_merge1;


		$con2['tbl_nexplay_caenex_game.delete_status']='1';
		//$con2['tbl_nexplay_caenex_game.merchant_id']=$id;
		$joincon2="tbl_nexplay_caenex_game.merchant_id=tbl_merchants.merchant_id";
		$nexplay_caenex_game=$this->Merchant_modal->join_data('tbl_nexplay_caenex_game.*,tbl_merchants.merchant_name','tbl_nexplay_caenex_game','tbl_merchants',$joincon2,$con2);

		$con3['tbl_nexplay_custom_game.delete_status']='1';
		//$con3['tbl_nexplay_custom_game.merchant_id']=$id;
		$joincon3="tbl_nexplay_custom_game.merchant_id=tbl_merchants.merchant_id";
		$nexplay_custom_game=$this->Merchant_modal->join_data('tbl_nexplay_custom_game.*,tbl_merchants.merchant_name','tbl_nexplay_custom_game','tbl_merchants',$joincon3,$con3);
		$array_merge2=array_merge($nexplay_caenex_game,$nexplay_custom_game);
		//$array_merge3=array_merge($array_merge1,$array_merge2);
		if (!empty($array_merge2)) {
		usort($array_merge2, array("Admin", "date_compare"));
		}
		//$data['billboard_data']=$array_merge3;
		$data['nexplay']=$array_merge2;

		
		$this->load->view('admin/billboard_management',$data);	
	}

	public function user_transition()
	{
		
		$user_transaction=$this->Merchant_modal->get_all_data('*','tbl_user_transaction');
		$transaction_arr =array();
		foreach($user_transaction as $transaction)
		{
			$con['id'] = $transaction['user_id'];
			$username = $this->Merchant_modal->get_row_data('*','tbl_user',$con);
			$transaction['user_name'] = $username['name'];
			$transaction_arr[] = $transaction;
		}
		$data['user_transaction'] = $transaction_arr;
		/* echo "<pre>";
		print_r($data);
		die(); */
		$this->load->view('admin/user_transition',$data);	
	}

	public function merchant_transition()
	{
		$merchant_transaction=$this->Merchant_modal->get_all_data('*','tbl_transaction');
		$transaction_arr =array();
		foreach($merchant_transaction as $transaction)
		{
			$con['merchant_id'] = $transaction['user_id'];
			$username = $this->Merchant_modal->get_row_data('*','tbl_merchants',$con);
			$transaction['merchant_name'] = $username['merchant_name'];
			$transaction_arr[] = $transaction;
		}
		$data['merchant_transaction'] = $transaction_arr;
		/* echo "<pre>";
		print_r($data);
		die(); */
		$this->load->view('admin/merchant_transition',$data);
	}

	public function id_verification_admin(){
		$joincon="tbl_id_verification1.merchant_id=tbl_merchants.merchant_id";
		$con2['tbl_merchants.delete_status']='1';
		$con2['tbl_merchants.status']='1';
		$data['id_verification']=$this->Merchant_modal->join_data('tbl_id_verification1.*,tbl_merchants.merchant_name,tbl_merchants.merchant_email','tbl_id_verification1','tbl_merchants',$joincon,$con2);
		// $order_by='DESC';
		// $colum='id';
		// $data['id_verification']=$this->Merchant_modal->get_all_data('*','tbl_id_verification1','',$order_by,$colum);

		$this->load->view('admin/id_verification',$data);

	}


	public function user_terms_conditions(){
		$con['id']='1';
		$data['content']=$this->Merchant_modal->get_row_data('*','tbl_content',$con);
		//print_r($data['content']);die;
		$this->load->view('admin/user_terms_conditions',$data);
	}

	public function update_user_terms_conditions(){
		$submit=$this->input->post('submit');
		if (isset($submit)) {
			$con['id']=$this->input->post('id');
			$data['details']=$this->input->post('content');
			if($this->Merchant_modal->update('tbl_content',$data,$con)){
				if ($con['id']==1) {
					$this->session->set_flashdata('u_t_c','User Terms & Conditions Updated Successfully');
					redirect('user_terms_conditions');
				}

				if ($con['id']==2) {
					$this->session->set_flashdata('u_t_c','Merchant Terms & Conditions Updated Successfully');
					redirect('merchant_terms_conditions');
				}

				if ($con['id']==3) {
					$this->session->set_flashdata('u_t_c','User Privacy Policy Updated Successfully');
					redirect('user_privacy_policy');
				}

				if ($con['id']==4) {
					$this->session->set_flashdata('u_t_c','Merchant Privacy Policy Updated Successfully');
					redirect('merchant_privacy_policy');
				}

				if ($con['id']==5) {
					$this->session->set_flashdata('u_t_c','About Us Updated Successfully');
					redirect('about');
				}
			}

		}
	}

	public function merchant_terms_conditions(){
		$con['id']='2';
		$data['content']=$this->Merchant_modal->get_row_data('*','tbl_content',$con);
		
		$this->load->view('admin/merchant_terms_conditions',$data);
	}

	public function user_privacy_policy(){
		$con['id']='3';
		$data['content']=$this->Merchant_modal->get_row_data('*','tbl_content',$con);
		$this->load->view('admin/user_privacy_policy',$data);
	}

	public function merchant_privacy_policy(){
		$con['id']='4';
		$data['content']=$this->Merchant_modal->get_row_data('*','tbl_content',$con);
		$this->load->view('admin/merchant_privacy_policy',$data);
	}

	public function admin_profile(){
		$submit=$this->input->post('submit');
		if (isset($submit)) {
			
			$admin_data = array('name' => $this->input->post('name'),'email' => $this->input->post('email'), );
			$com['id']='1';
			if($this->Merchant_modal->update('tbl_admin',$admin_data,$com)){
				$this->session->set_flashdata('admin_data','Profile Updated Successfully');
			}
		}
		$data['data']=$this->Merchant_modal->get_row_data('*','tbl_admin');

		$this->load->view('admin/admin_profile',$data);
	}

	public function logout(){
			$this->session->sess_destroy();
			redirect(base_url('Admin'));
	}

	public function check_password(){
		$con['password']=$this->input->post('oldpassword');

		$data=$this->Merchant_modal->get_row('*','tbl_admin',$con);
		if ($data) {
			echo 1;
		}else{
			echo 2;
		}
		
	}

	public function change_password(){
		$admin_data['password']=$this->input->post('newpassword');
		$com['id']='1';
		if($this->Merchant_modal->update('tbl_admin',$admin_data,$com)){
				$this->session->set_flashdata('admin_data','Password Changed Successfully');
			}

			redirect("admin_profile");
	}

	public function change_status(){
		$com['id']=$this->input->post('id');
		$data['read_status']='2';
		if($this->Merchant_modal->update('tbl_notification',$data,$com)){
			echo "1";
		}
	}

	public function admin_notification(){
		$order_by='DESC';
		$colum='id';
		$data['notifications']=$this->Merchant_modal->get_all_data('*','tbl_notification','',$order_by,$colum);
		$this->load->view('admin/all_notification',$data);
	}
	
	public function game(){
		$order_by='DESC';
		$colum='game_id';
		$con['delete_status']='1';
		$data['game']=$this->Merchant_modal->get_all_data('*','tbl_game',$con,$order_by,$colum);
		/* print_r($data);
		die(); */
		$this->load->view('admin/game',$data);
	}
	
	public function add_game(){
		$data = array('game_name' =>$this->input->post('game_name') ,'status'=>$this->input->post('status') );
		$uploaddir = './assets/img/photos/';
		$path = $_FILES['file']['name'];
		if(!empty($path))
		{
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			$user_img = time() . rand() . '.' . $ext;
			$uploadfile = $uploaddir . $user_img;
			if ($_FILES["file"]["name"]) {
				if (move_uploaded_file($_FILES["file"]["tmp_name"],$uploadfile)) {
				$data['game_img'] = $user_img;
				}
			}
		}
		//print_r($data);
		//die();
		if (empty($this->input->post('cat_id'))) {
			# code...
		if($this->Merchant_modal->insert('tbl_game',$data)){
				$this->session->set_flashdata('add_user','Game Added Successfully');
				
			}
		}else{
			
			$com['game_id']=$this->input->post('cat_id');
			if($this->Merchant_modal->update('tbl_game',$data,$com)){
			$this->session->set_flashdata('add_user','Game Updated Successfully');
			
			}
			
		}
			redirect(base_url('game'));
	}
	
	public function game_category(){
		$order_by='DESC';
		$colum='id';
		$con['delete_status']='1';
		$data['category']=$this->Merchant_modal->get_all_data('*','tbl_category',$con,$order_by,$colum);
		
		$order_by='DESC';
		$colum='game_id';
		$con['delete_status']='1';
		$data['game']=$this->Merchant_modal->get_all_data('*','tbl_game',$con,$order_by,$colum);
		
		$this->load->view('admin/category',$data);
	}

	public function add_category(){
		$data = array('category' =>$this->input->post('name') ,'status'=>$this->input->post('status'),'gameid' =>$this->input->post('game') );
		$uploaddir = './assets/img/photos/';
		$path = $_FILES['file']['name'];
		if(!empty($path))
		{
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			$user_img = time() . rand() . '.' . $ext;
			$uploadfile = $uploaddir . $user_img;
			if ($_FILES["file"]["name"]) {
				if (move_uploaded_file($_FILES["file"]["tmp_name"],$uploadfile)) {
				$data['cat_img'] = $user_img;
				}
			}
		}
		
		if (empty($this->input->post('cat_id'))) {
			# code...
		if($this->Merchant_modal->insert('tbl_category',$data)){
				$this->session->set_flashdata('add_user','Category Added Successfully');
				
			}
		}else{
			$com['id']=$this->input->post('cat_id');
			if($this->Merchant_modal->update('tbl_category',$data,$com)){
				$this->session->set_flashdata('add_user','Category Updated Successfully');
				
			}
		}
			redirect(base_url('category'));
	}

	public function ad_type(){

		$order_by='DESC';
		$colum='id';
		$con['delete_status']='1';
		$data['ad_type']=$this->Merchant_modal->get_all_data('*','tbl_ad_type',$con,$order_by,$colum);
		$this->load->view('admin/ad_type',$data);
	}

	public function add_ad_type(){
		$data = array('ad_type' =>$this->input->post('name') ,'status'=>$this->input->post('status') );
		if (empty($this->input->post('cat_id'))) {
			# code...
		if($this->Merchant_modal->insert('tbl_ad_type',$data)){
				$this->session->set_flashdata('add_user','Ad type Added Successfully');
				
			}
		}else{
			$com['id']=$this->input->post('cat_id');
			if($this->Merchant_modal->update('tbl_ad_type',$data,$com)){
				$this->session->set_flashdata('add_user','Ad type Updated Successfully');
				
			}
		}
			redirect(base_url('ad_type'));
	}


	public function questions(){


		$order_by='DESC';
		$colum='tbl_question.id';
		$con['tbl_question.delete_status']='1';
		
		$joincon="tbl_question_cat.id=tbl_question.question_id";
		$data['questions']=$this->Merchant_modal->join_data('tbl_question.*,tbl_question_cat.category,tbl_question_cat.reward','tbl_question','tbl_question_cat',$joincon,$con,$order_by,$colum);

		$order_by1='DESC';
		$colum1='id';
		$con1['delete_status']='1';
		$con1['status']='1';
		$data['category']=$this->Merchant_modal->get_all_data('*','tbl_category',$con1,$order_by1,$colum1);

		$this->load->view('admin/questions',$data);
	}

	public function add_questions(){
		$order_by='DESC';
		$colum='id';
		$con['delete_status']='1';
		$con['status']='1';
		$data['category']=$this->Merchant_modal->get_all_data('*','tbl_category',$con,$order_by,$colum);
		
		$order_by='DESC';
		$colum='game_id';
		$con['delete_status']='1';
		$data['game']=$this->Merchant_modal->get_all_data('*','tbl_game',$con,$order_by,$colum);
		
		$this->load->view('admin/add_questions',$data);
	}

	public function add_questions_data(){

		$quesion=$this->input->post('question');
		$answer1=$this->input->post('answer1');
		$answer2=$this->input->post('answer2');
		$answer3=$this->input->post('answer3');
		$answer4=$this->input->post('answer4');

		$arrayName = array('category' =>$this->input->post('category') ,'reward' =>$this->input->post('custom_reward') , );
		$last_id=$this->Merchant_modal->insert('tbl_question_cat',$arrayName);
		$b=1;

		for($i=0;$i<count($quesion);$i++  ){
			 $a=$b+$i;			
			$c='radio'.$a;			
			$data1 = array('question'=>$quesion[$i],'ans1'=>$answer1[$i],'ans2'=>$answer2[$i],'ans3'=>$answer3[$i],'ans4'=>$answer4[$i],'right_ans'=>$this->input->post($c),'question_id'=>$last_id ,'category' =>$this->input->post('category'));
			$this->Merchant_modal->insert('tbl_question',$data1);

		}

		$this->session->set_flashdata('add_user','Question Added Successfully');		
		
		redirect('questions');
	}

	public function update_questions(){

		$this->load->view('admin/update_questions');
	}

	public function update_questions_data(){

		$data = array('question' => $this->input->post('question'),'ans1' => $this->input->post('answer1'),'ans2' => $this->input->post('answer2'),'ans3' => $this->input->post('answer3'),'ans4' => $this->input->post('answer4'),'right_ans' => $this->input->post('radio1'),'status' => $this->input->post('status'),'category' => $this->input->post('category') );

		$com['id']=$this->input->post('question_id');
			if($this->Merchant_modal->update('tbl_question',$data,$com)){
				$this->session->set_flashdata('add_user','Question Updated Successfully');
				
			}

		redirect('questions');
	}

	public function billboard_details($id=null){

		$con['tbl_nexworld_cash.delete_status']='1';
		
		$con['tbl_nexworld_cash.merchant_id']=$id; 
		
		$joincon="tbl_nexworld_cash.merchant_id=tbl_merchants.merchant_id";
		
		$nexworld_cash=$this->Merchant_modal->join_data('tbl_nexworld_cash.*,tbl_merchants.merchant_name','tbl_nexworld_cash','tbl_merchants',$joincon,$con);
		
		$con1['tbl_nexworld_coupon.delete_status']='1';
		
		$con1['tbl_nexworld_coupon.merchant_id']=$id;
		
		$joincon1="tbl_nexworld_coupon.merchant_id=tbl_merchants.merchant_id";
		
		$nexworld_coupon=$this->Merchant_modal->join_data('tbl_nexworld_coupon.*,tbl_merchants.merchant_name','tbl_nexworld_coupon','tbl_merchants',$joincon1,$con1);
		
		$array_merge1=array_merge($nexworld_cash,$nexworld_coupon);
		
		if (!empty($array_merge1)) {
		usort($array_merge1, array("Admin", "date_compare"));
			
		}
		$data['nexworld']=$array_merge1;
		
		
		/* echo "<pre>";
		print_r($data['nexworld']);
		die; */

		$con2['tbl_nexplay_caenex_game.delete_status']='1';
		
		$con2['tbl_nexplay_caenex_game.merchant_id']=$id;
		
		$joincon2="tbl_nexplay_caenex_game.merchant_id=tbl_merchants.merchant_id";
		
		$nexplay_caenex_game=$this->Merchant_modal->join_data('tbl_nexplay_caenex_game.*,tbl_merchants.merchant_name','tbl_nexplay_caenex_game','tbl_merchants',$joincon2,$con2);

		$con3['tbl_nexplay_custom_game.delete_status']='1';
		
		$con3['tbl_nexplay_custom_game.merchant_id']=$id;
		
		$joincon3="tbl_nexplay_custom_game.merchant_id=tbl_merchants.merchant_id";
		
		$nexplay_custom_game=$this->Merchant_modal->join_data('tbl_nexplay_custom_game.*,tbl_merchants.merchant_name','tbl_nexplay_custom_game','tbl_merchants',$joincon3,$con3);
		
		$array_merge2=array_merge($nexplay_caenex_game,$nexplay_custom_game);
		
		//$array_merge3=array_merge($array_merge1,$array_merge2);
		if (!empty($array_merge2)) {
		usort($array_merge2, array("Admin", "date_compare"));
		}
		//$data['billboard_data']=$array_merge3;
		$data['nexplay']=$array_merge2;
		//echo "<pre>";
		//print_r($data['nexplay']);
		//die;
		$this->load->view('admin/billboard_details',$data);
	}
	public function coupon_details($coupon_id)
	{
		//echo $coupon_id;
		$data = array();
		$this->load->view('admin/coupon_details',$data);
	}
	
	public function cash_details($cash_id)
	{
		//echo $cash_id;
		$data = array();
		$this->load->view('admin/cash_details',$data);
	}
	
	public function game_details($game_id)
	{
		//echo $game_id;
		$data = array();
		$this->load->view('admin/game_details',$data);
	}

	public function about(){
		$con['id']='5';
		$data['content']=$this->Merchant_modal->get_row_data('*','tbl_content',$con);
		$this->load->view('admin/about_us',$data);

	}

	public function help_articles(){

		$con['delete_status']=1;
		$order_by='DESC';
		$colum='id';
		$data['help_articles']=$this->Merchant_modal->get_all_data('*','tbl_help_articles',$con,$order_by,$colum);

		$this->load->view('admin/help_articles',$data);
	}

	public function add_help_articles(){

		$data = array('title' =>$this->input->post('name') ,'details'=>$this->input->post('details') );
		if (empty($this->input->post('cat_id'))) {
			# code...
		if($this->Merchant_modal->insert('tbl_help_articles',$data)){
				$this->session->set_flashdata('add_user','Help Article Added Successfully');
				
			}
		}else{
			$com['id']=$this->input->post('cat_id');
			if($this->Merchant_modal->update('tbl_help_articles',$data,$com)){
				$this->session->set_flashdata('add_user','Help Article Updated Successfully');
				
			}
		}
			redirect(base_url('help_articles'));
	}

	public function faqs(){
		$faq_data['faq']=$this->Merchant_modal->faq_select();
		$this->load->view('admin/faqs', $faq_data);
	}
	public function faq_submit(){
		$faqname=$this->input->post('faq_name');
		$faqmessage=$this->input->post('faq_message');
		$faq_data=array(
			'faq_name'=>$faqname,
			'faq_message'=>$faqmessage,
		);
		if($this->Merchant_modal->faq($faq_data))
		{
			$this->session->set_flashdata('success','FAQ Added successfully');
			redirect('faqs');
		}
	}
	public function edit_faq(){
		$com['faq_id']=$this->input->post('faq_id');
		$faqname=$this->input->post('faq_name');
		$faqmessage=$this->input->post('faq_message');
		$faq_data=array(
			'faq_name'=>$faqname,
			'faq_message'=>$faqmessage,
		);
		if($this->Merchant_modal->update('tbl_faq',$faq_data,$com)){
			$this->session->set_flashdata('add_user','FAQs Updated Successfully');
		}
		redirect('faqs');
	}
	public function delete_faq(){
		$com['faq_id'] = $this->input->post('faq_id');
		if($this->Merchant_modal->delete('tbl_faq',$com)){
			$this->session->set_flashdata('add_user','FAQs Deleted Successfully');
		}
		redirect('faqs');
	}
}
?>