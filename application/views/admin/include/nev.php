<?php  $path = $this->router->fetch_class().'/'.$this->router->fetch_method(); ?>
<style type="text/css">

</style>
		<nav id="sidebar" class="sidebar">
			<div class="sidebar-content ">
			<a class="sidebar-brand" href="<?php echo base_url('admin_dashboard') ?>">
          		<span class="align-middle"><img src="<?php  echo base_url('assets/')?>img/logo.png" alt="logo" class="img-responsive"> CAENEX </span>
        	</a>

				<ul class="sidebar-nav">
					<li class="sidebar-item <?php echo $path == 'Admin/admin_dashboard' ? 'active nav-active':''; ?>">
						<a  href="<?php echo base_url('admin_dashboard') ?>" class="sidebar-link">
			              <i class="align-middle" data-feather="sliders"></i> <span class="align-middle" style="color: white;">Dashboard</span>
			            </a>
					</li>
					<li class="sidebar-item twoicon <?php echo $path == 'Admin/user_management' ? 'active nav-active':''; ?>">
						<a href="<?php echo base_url('user_management') ?>" class="sidebar-link" >
              				<i class="align-middle" data-feather="users"></i> <span class="align-middle" style="color: white;"> User Management </span>
            			</a>
					</li>

					

					<li class="sidebar-item threeicon <?php echo $path == 'Admin/merchant_management' ? 'active nav-active':''; ?>">
						<a  href="<?php echo base_url('merchant_management') ?>" class="sidebar-link">
              				<i class="align-middle mr-2 fas fa-fw fa-ad"></i> <span class="align-middle" style="margin-left: 9px;color: white; "> Merchant Management </span>
            			</a>
					</li>
					<li class="sidebar-item fouricon <?php echo $path == 'Admin/billboard_management' ? 'active nav-active':''; ?>">
						<a  href="<?php echo base_url('billboard_management') ?>" class="sidebar-link">
              				<i class="align-middle mr-2 fas fa-fw fa-user-tie" ></i><span class="align-middle" style="margin-left: 9px;color: white;"> Billboard Management </span>
            			</a>
					</li>

					

					<li class="sidebar-item fiveicon1 " >
						<a  href="#uit" data-toggle="collapse" class="sidebar-link collapsed">
			              <i class="align-middle mr-2 far fa-fw fa-id-card"></i><span style="margin-left: 9px;color: white;" class="align-middle">Game Management</span>
			            </a>
						<ul id="uit" class="sidebar-dropdown list-unstyled collapse " data-parent="#sidebar">
							<li class="sidebar-item <?php echo $path == 'Admin/game' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('game') ?>">Game List</a></li>
						
							<li class="sidebar-item <?php echo $path == 'Admin/game_category' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('category') ?>">Game Category</a></li>
							<li class="sidebar-item <?php echo $path == 'Admin/ad_type' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('ad_type') ?>">Ad type</a></li>
							<li class="sidebar-item <?php echo $path == 'Admin/questions' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('questions') ?>">Questions </a></li>
						</ul>
					</li>


					
					<li class="sidebar-item sixicon ">
						<a  href="#ui" data-toggle="collapse" class="sidebar-link collapsed">
			              <i class="align-middle mr-2 far fa-fw fa-id-card"></i><span style="margin-left: 9px;color: white;" class="align-middle">Transaction Management</span>
			            </a>
						<ul id="ui" class="sidebar-dropdown list-unstyled collapse " data-parent="#sidebar">
							<li class="sidebar-item <?php echo $path == 'Admin/user_transition' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('user_transition') ?>">User Transaction</a></li>
							<li class="sidebar-item <?php echo $path == 'Admin/merchant_transition' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('merchant_transition') ?>">Merchant Transaction</a></li>
						</ul>
					</li>
					
					
					<li class="sidebar-item eighticonid ">
						<a  href="#ui" data-toggle="collapse" class="sidebar-link collapsed">
			              <i class="align-middle mr-2 far fa-fw fa-id-card"></i><span style="margin-left: 9px;color: white;" class="align-middle">ID Verification</span>
			            </a>
						<ul id="ui" class="sidebar-dropdown list-unstyled collapse " data-parent="#sidebar">
							<li class="sidebar-item <?php echo $path == 'Admin/id_verification_user' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('id_verification_user') ?>">User Id Verification</a></li>
							<li class="sidebar-item <?php echo $path == 'Admin/id_verification_admin' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('id_verification_admin') ?>">Merchant Id Verification</a></li>
						</ul>
					</li>
					
					
					<!--<li class="sidebar-item eighticonid <?php echo $path == 'Admin/id_verification_admin' ? 'active nav-active':''; ?>">
						<a  href="<?php echo base_url('id_verification_admin') ?>" class="sidebar-link">
              				<i class="align-middle mr-2 fas fa-fw fa-cog"></i><span style="margin-left: 9px;color: white;" class="align-middle"> ID Verification </span>
              			</a> 
              		</li>-->
              		<!-- <li class="sidebar-item">
						<a href="#u2" data-toggle="collapse" class="sidebar-link" aria-expanded="true">
					      <i class="align-middle mr-2 far fa-fw fa-id-card"></i><span style="margin-left: 9px;color: white;" class="align-middle">Terms and Conditions</span>
					    </a>
						<ul id="u2" class="sidebar-dropdown list-unstyled collapse" data-parent="#sidebar" style="">
							<li class="sidebar-item <?php echo $path == 'Admin/user_terms_conditions' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('user_terms_conditions') ?>">User Terms & Conditions</a></li>
							<li class="sidebar-item <?php echo $path == 'Admin/merchant_terms_conditions' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('merchant_terms_conditions') ?>">Merchant Terms & Conditions</a></li>
						</ul>
					</li> -->

					<!-- <li class="sidebar-item">
						<a href="#u2" data-toggle="collapse" class="sidebar-link collapsed">
			              <i class="align-middle mr-2 far fa-fw fa-id-card"></i><span style="margin-left: 9px;color: white;" class="align-middle">Terms and Conditions</span>
			            </a>
						<ul id="u2" class="sidebar-dropdown list-unstyled collapse" data-parent="#sidebar" style="">
							<li class="sidebar-item <?php echo $path == 'Admin/user_terms_conditions' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('user_terms_conditions') ?>">User Terms & Conditions</a></li>
							<li class="sidebar-item <?php echo $path == 'Admin/merchant_terms_conditions' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('merchant_terms_conditions') ?>">Merchant Terms & Conditions</a></li>
						</ul>
					</li> -->

					<!-- <li class="sidebar-item">
						<a href="#u3" data-toggle="collapse" class="sidebar-link" aria-expanded="true">
					      <i class="align-middle mr-2 far fa-fw fa-id-card"></i><span style="margin-left: 9px;color: white;" class="align-middle">Privacy Policy</span>
					    </a>
						<ul id="u3" class="sidebar-dropdown list-unstyled collapse" data-parent="#sidebar" style="">
							<li class="sidebar-item <?php echo $path == 'Admin/user_privacy_policy' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('user_privacy_policy') ?>">User Privacy Policy</a></li>
							<li class="sidebar-item <?php echo $path == 'Admin/merchant_privacy_policy' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('merchant_privacy_policy') ?>">Merchant Privacy Policy</a></li>
						</ul>
					</li> -->

					<li class="sidebar-item fiveicon12 ">
						<a  href="#u3" data-toggle="collapse" class="sidebar-link collapsed">
			              <i class="align-middle mr-2 far fa-fw fa-id-card"></i><span style="margin-left: 9px;color: white;" class="align-middle">Content Management </span>
			            </a>
						<ul id="u3" class="sidebar-dropdown list-unstyled collapse" data-parent="#sidebar" style="">
							<li class="sidebar-item <?php echo $path == 'Admin/user_privacy_policy' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('user_privacy_policy') ?>">User Privacy Policy</a></li>
							<li class="sidebar-item <?php echo $path == 'Admin/merchant_privacy_policy' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('merchant_privacy_policy') ?>">Merchant Privacy Policy</a></li>

							<li class="sidebar-item <?php echo $path == 'Admin/user_terms_conditions' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('user_terms_conditions') ?>">User Terms & Conditions</a></li>
							<li class="sidebar-item <?php echo $path == 'Admin/merchant_terms_conditions' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('merchant_terms_conditions') ?>">Merchant Terms & Conditions</a></li>

							<li class="sidebar-item <?php echo $path == 'Admin/about' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('about') ?>">About Us</a></li>


							<li class="sidebar-item <?php echo $path == 'Admin/help_articles' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('help_articles') ?>">Help Articles</a></li>

							<li class="sidebar-item <?php echo $path == 'Admin/faqs' ? 'active nav-active':''; ?>"><a class="sidebar-link" href="<?php echo base_url('faqs') ?>">FAQs</a></li>
						</ul>
					</li>
					<li class="sidebar-item">
						<a href="<?php echo base_url('Admin/admin_support') ?>" class="sidebar-link">
              				<i class="align-middle mr-2 fas fa-fw fa-user-lock"></i> <span style="margin-left: 9px;color: white;" class="align-middle">Merchant Support </span>
            			</a>				
					</li>
					
					<li class="sidebar-item">
						<a href="<?php echo base_url('Admin/user_support') ?>" class="sidebar-link">
              				<i class="align-middle mr-2 fas fa-fw fa-user-lock"></i> <span style="margin-left: 9px;color: white;" class="align-middle">User Support</span>
            			</a>				
					</li>
					
					<li class="sidebar-item">
						<a href="<?php echo base_url('Admin/logout') ?>" class="sidebar-link">
              				<i class="align-middle mr-2 fas fa-fw fa-user-lock"></i> <span style="margin-left: 9px;color: white;" class="align-middle">Logout</span>
            			</a>				
					</li>
				</ul>
			</div>
		</nav>