<?php
$data=$this->Merchant_modal->get_row_data('*','tbl_admin');
$con['type']='1';
$con['read_status']='1';

$notifications=$this->Merchant_modal->get_all_data('*','tbl_notification',$con,'DESC','id');
//$notifications=$this->Merchant_modal->get_all_data('*','tbl_notification',$con,'DESC','id','0','6');
//echo $this->db->last_query();

?>
			<nav class="navbar navbar-expand navbar-light bg-white">
				<a class="sidebar-toggle d-flex mr-2">
		          <i class="hamburger align-self-center"></i>
		        </a>
				<div class="navbar-collapse collapse">
					<ul class="navbar-nav ml-auto">
						
						<li class="nav-item dropdown">
							<a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-toggle="dropdown">
								<div class="position-relative">
									<?php //if (count($notifications)>0) {
										?>
									<i class="fa fa-bell" aria-hidden="true"></i>
									<span class="badge badge-light" style="position: relative;
									top: -15px;
									left: -19px;
									border-radius: 50%;
									min-width: 24px;
									line-height: 24px;
									font-size: 12px;
									font-weight: bold;
									color: white;
									background-color: #1cb6d6;
									text-decoration: none;
									text-align: center;
									padding: 0;"><?php echo count($notifications); ?></span> 
									<?php
									//}else{
										?>
									<!-- <i class="align-middle" data-feather="bell-off"></i>
									<span class="badge badge-light" style="position: relative;
    top: -15px;
    left: -19px;
    border-radius: 50%;
    min-width: 24px;
    line-height: 24px;
    font-size: 12px;
    font-weight: bold;
    color: white;
    background-color: #1cb6d6;
    text-decoration: none;
    text-align: center;
    padding: 0;"><?php echo count($notifications); ?></span> -->
									<?php
									//} ?>
								</div>
							</a>
							<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right py-0" aria-labelledby="alertsDropdown">
								<div class="dropdown-menu-header">
									<?php echo count($notifications); ?> New Notifications
								</div>
								<div class="list-group" style="min-height: 47px;display: inline-block;   overflow-y: scroll;max-height: 247px;">
									<?php
									if ($notifications) {
										foreach ($notifications as $key => $value) {											
									?>
									<a href="javascript:void(0);" onclick="send_page('<?php echo base_url($value['table_data']) ?>','<?php echo $value['id'] ?>')" class="list-group-item">
										<div class="row no-gutters align-items-center">
											<!-- <div class="col-2">
												<i class="text-danger" data-feather="alert-circle"></i>
											</div> -->
											<div class="col-10">
												<!-- <div class="text-dark">Update completed</div> -->
												<div class="text-dark"><?php echo $value['title'] ?></div>
												<div class="text-muted small mt-1"><?php echo $value['create_date'] ?></div>
											</div>
										</div>
									</a>
									<?php
										}
										}
									?>
									
									
								</div>
								<div class="dropdown-menu-footer">
									<a href="<?php echo base_url('admin_notification') ?>" class="text-muted">Show all notifications</a>
								</div>
							</div>
						</li>
						
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-toggle="dropdown">
								<img src="<?php echo base_url('assets/img/admin.jpg') ?>" class="avatar img-fluid rounded-circle mr-1" alt="Chris Wood"/>
				                <!-- <img src="<?php echo base_url('assets/') ?>img\avatars\avatar.jpg" class="avatar img-fluid rounded-circle mr-1" alt="Chris Wood"> --> 
				                <span class="text-dark"><?php echo $data['name']; ?></span>
				              </a>
							<div class="dropdown-menu dropdown-menu-right">
								<a class="dropdown-item" href="<?php echo base_url('admin_profile') ?>"><i class="align-middle mr-1" data-feather="user"></i> Profile</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="<?php echo base_url('Admin/logout') ?>">Log out</a>
							</div>
						</li>
					</ul>
				</div>
			</nav>

			<script type="text/javascript">
				function send_page(page,id) {
					$.post("<?php echo base_url('Admin/change_status') ?>",{id:id},function(res){
						window.location.href = page;
					})
				}
			</script>