<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Bootlab">
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/logo.png'); ?>" type="image/x-icon" />
	<title> Caenex Admin </title>
	<link rel="stylesheet" href="<?php echo base_url('assets/') ?>/css1/classic.css">
	<script src="<?php echo base_url('assets/') ?>js\app.js"></script>
</head>