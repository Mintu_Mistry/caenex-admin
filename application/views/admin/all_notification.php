<?php
$this->load->view('admin/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('admin/include/nev');
?>

<div class="main">
<?php
$this->load->view('admin/include/header');
?>

<style type="text/css">
	#datatables-basic_wrapper{
		margin-top: 11px;
	}
	.dataTables_length{
		margin-left: 10px;
	}
	#datatables-basic_filter{
		margin-right: 10px;
	}
	.dataTables_info{
		margin-left: 10px;
		margin-bottom:10px;
	}
	#datatables-basic_paginate{
		margin-right: 10px;
	}
</style>

<main class="content">
<div class="container-fluid p-0">
<div class="row mb-2">
<div class="col-12 col-lg-6 mobcenter">
<h3> All Notifications </h3>
</div>
<div class="col-12 col-lg-6">
<!-- <div class="rowbtn text-right mobcenter">
<div class="btn import_btn" data-toggle="modal" data-target="#sizedModalSm"><i class="align-middle mr-2 fas fa-fw fa-plus"></i> Add a new user </div>
</div> -->
<!--------------- Modal ---------------->


</div>
</div>

<div class="row">
<div class="col-12 col-lg-12">
<div class="card">
<div class="row">
<div class="col-12 col-lg-12 mt-minus">
<div class="table-responsive  border">
<table class="table mb-0" id="datatables-basic">
<thead>
<tr>
<th scope="col">S.No </th>
<th scope="col">Notification</th>
<th scope="col" style="">Date</th>


</tr>
</thead>
<tbody>

<?php 
$count=0;
foreach ($notifications as $key => $value) {
$count++;	
?>
<tr>
<th scope="row"><?php echo $count; ?></th>
<td><a href="<?php echo base_url(''.$value['table_data'].'') ?>"><?php echo $value['title']; ?></a></td>
<td><?php echo $value['create_date']; ?></td>
<!-- <td>Merchant</td>
<td>College Name Here</td> -->

</tr>
<?php
}
?>
</tbody>
</table>

<!--------------- Modal ---------------->
<div class="modal fade show" id="user_mng_view" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
<!-- <div class="modal-dialog modal-sm" role="document">
<div class="modal-content">
<button type="button" class="text-right nohd_pop border-0" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span>
</button>

<div class="modal-body propopclr">
<div class="form-row">
<div class="col-lg-4">
<div class="text-center profilebox">
<img src="img\avatars\profile.jpg" class="img-fluid rounded-circle mb-2" alt="...">
</div>
</div>
<div class="col-lg-8">
<p><i class="align-middle mr-2 fas fa-fw fa-user"></i> Chris Wood</p>
<p><i class="align-middle mr-2 fas fa-fw fa-envelope"></i> example@gmail.com</p>
<p><i class="align-middle mr-2 fas fa-fw fa-phone"></i> User Phone : +1202-555-0123</p>
</div>

</div>
</div>

<div class="modal-body font700">
<div class="form-row">
<div class="col-lg-12">
<p><i class="align-middle fas fa-fw fa-dollar-sign"></i> NexPlay Ammount: <span>$59.00</span></p>
<p><i class="align-middle fas fa-fw fa-dollar-sign"></i>  NexWorld Ammount: <span>$39.00</span></p>
<p><i class="align-middle fas fa-fw fa-dollar-sign"></i> NexPlay Ammount: <span>$59.00 </span></p>
</div>
</div>
</div>

</div> -->

<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header webclr">
<h4 class="modal-title clrwhite heading" id="">User Details</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body">
<form>
<div id="formbox">
<div class="form-row">
<div class="form-group col-md-6">
<label for=""> User Name </label>
<input type="text" class="form-control" id="" placeholder="Chris Wood">
</div>
<div class="form-group col-md-6">
<label for="">Email Address</label>
<input type="text" class="form-control" id="" placeholder="example@gmail.com">
</div>
</div>
<div class="form-row">
<div class="form-group col-md-6">
<label for="">Phone Number</label>
<input type="password" class="form-control" id="" placeholder="+1202-555-0123">
</div>
</div>
<div class="form-row">
<div class="form-group col-md-6">
<label for="inputAddress">Street Address</label>
<input type="text" class="form-control" id="" placeholder="1234 Main St">
</div>
<div class="form-group col-md-6">
<label for="inputAddress2">City</label>
<input type="text" class="form-control" id="" placeholder="Apartment, studio, or floor">
</div>
</div>
<div class="form-row">
<div class="form-group col-md-6">
<label for="inputCity">State</label>
<input type="text" class="form-control" id="" placeholder="New York">
</div>
<div class="form-group col-md-6">
<label for="inputState">Zip</label>
<input type="text" class="form-control" id="" placeholder="Enter Zip code">
</div>
</div>
<div class="form-row">
<div class="form-group col-md-6">
<label for="inputZip">Country</label>
<input type="text" class="form-control" id="" placeholder="United States">
</div>
</div>

<div class="modal-body font700">
<div class="form-row">
<div class="col-lg-6">
<p><i class="align-middle fas fa-fw fa-dollar-sign"></i>Coupons Earned: <span>37</span></p>

<p><i class="align-middle fas fa-fw fa-dollar-sign"></i>NexPlay Ammount: <span>$59.00</span></p>

</div>
<div class="col-lg-6">
<p><i class="align-middle fas fa-fw fa-dollar-sign"></i>Wallet Money: <span>$39.00</span></p>
<p><i class="align-middle fas fa-fw fa-dollar-sign"></i>NexWorld Ammount: <span>$59.00 </span></p>
</div>
</div>
</div>
</div>
</form>
</div>

</div>
</div>
</div>
</div>
<!-------- End Modal -------->
<!------------------------------  Start Change Booth modal box ------------------------->
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="change_both" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title"> Change Booth </h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body">
<form>
<div id="formbox">
<div class="form-row">
<div class="form-group col-md-6">
<label for="">Booth 1</label>
<select id="inputState" class="form-control">
<option selected="">Select</option>
<option>...</option>
</select>
</div>
<div class="form-group col-md-6">
<label for="">Booth 2</label>
<select id="inputState" class="form-control">
<option selected="">Select</option>
<option>...</option>
</select>
</div>
</div>
<div class="form-row">
<div class="form-group col-md-6">
<label for="">Time Assigned</label>
<input type="text" class="form-control" id="" placeholder="9:00AM">
</div>
<div class="form-group col-md-6">
<label for="">Time Assigned</label>
<input type="text" class="form-control" id="" placeholder="9:00AM">
</div>
</div>
<div class="form-group text-center">
<button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Update </button>
</div>
</div>
</form>
</div>

</div>
</div>
</div>
<!-------------- End Modal box ------------------>

</div>

</div>
</div>
</div>
</div>
</div>
</div>
</main>

<!-- <footer class="footer"></footer> -->
</div>
</div>
<!-- <script src="js\settings.js"></script> -->
<script src="js\app.js"></script>

<script>
$(function() {
$("#datetimepicker-dashboard").datetimepicker({
inline: true,
sideBySide: false,
format: "L"
});
});
</script>
<script>
$(function() {
// Line chart
new Chart(document.getElementById("chartjs-dashboard-line"), {
type: "line",
data: {
labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
datasets: [{
label: "Sales ($)",
fill: true,
backgroundColor: "transparent",
borderColor: window.theme.primary,
data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
}, {
label: "Orders",
fill: true,
backgroundColor: "transparent",
borderColor: window.theme.tertiary,
borderDash: [4, 4],
data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
}]
},
options: {
maintainAspectRatio: false,
legend: {
display: false
},
tooltips: {
intersect: false
},
hover: {
intersect: true
},
plugins: {
filler: {
propagate: false
}
},
scales: {
xAxes: [{
reverse: true,
gridLines: {
color: "rgba(0,0,0,0.05)"
}
}],
yAxes: [{
ticks: {
stepSize: 500
},
display: true,
borderDash: [5, 5],
gridLines: {
color: "rgba(0,0,0,0)",
fontColor: "#fff"
}
}]
}
}
});
});
</script>
<script>
$(function() {
// Pie chart
new Chart(document.getElementById("chartjs-dashboard-pie"), {
type: "pie",
data: {
labels: ["Direct", "Affiliate", "E-mail", "Other"],
datasets: [{
data: [2602, 1253, 541, 1465],
backgroundColor: [
window.theme.primary,
window.theme.warning,
window.theme.danger,
"#E8EAED"
],
borderColor: "transparent"
}]
},
options: {
responsive: !window.MSInputMethodContext,
maintainAspectRatio: false,
legend: {
display: false
}
}
});
});
</script>
<script>
$(function() {
$("#datatables-dashboard-projects").DataTable({
pageLength: 6,
lengthChange: false,
bFilter: false,
autoWidth: false
});
});
</script>

</body>

</html>
<script>
		$(function() {
			// Datatables basic
			$("#datatables-basic").DataTable({
				//responsive: true
				"aaSorting": []
			});
			// Datatables with Buttons
			var datatablesButtons = $("#datatables-buttons").DataTable({
				responsive: true,
				lengthChange: !1,
				buttons: ["copy", "print"]
			});
			datatablesButtons.buttons().container().appendTo("#datatables-buttons_wrapper .col-md-6:eq(0)");
			// Datatables with Multiselect
			var datatablesMulti = $("#datatables-multi").DataTable({
				responsive: true,
				select: {
					style: "multi"
				}
			});
		});
	</script>