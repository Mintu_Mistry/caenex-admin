<?php
	//preg_match_all('/[^a-z_\-0-9]/i', base_url(uri_string()), $matches);
	$txt = explode("/",base_url(uri_string()));
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Bootstrap 4 Admin &amp; Dashboard Template">
	<meta name="author" content="Bootlab">

	<title> Caenex Payment </title>
	<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin="">
	<link rel="stylesheet" href="<?php echo base_url('assets/') ?>css1/classic.css" >
	<script src="<?php echo base_url('assets/') ?>js/settings.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/app.js"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/') ?>sweetalert.css">
	<script src="<?php echo base_url('assets/dist/') ?>sweetalert.min.js"></script>
	<!-- END SETTINGS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script  src="https://www.googletagmanager.com/gtag/js?id=UA-120946860-6"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.5.10/cleave.min.js"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120946860-6');
</script>
<style>
.wrapper.success_msg {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
}
.success_msg>h3 {
    background-color: #fff;
    padding: 50px;
	text-align:center;
}

.wrapper.success_msg>h3>strong {
    color: green;
    display: block;
    text-align: center;
    font-size: 32px;
    text-transform: uppercase;
    font-weight: bold;
    margin-bottom: 10px;
}
</style>
</head>
<body>
<div class="wrapper success_msg" >
<?php
if ($this->session->flashdata('payment_success')) {
?>
<h3><strong>Success! </strong> <?php echo $this->session->flashdata('payment_success'); ?></h3>
<?php
}
?>
</div>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/redmond/jquery-ui.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>
</body>
</html>