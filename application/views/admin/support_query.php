<?php
$this->load->view('admin/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('admin/include/nev');
?>

<div class="main">
<?php
$this->load->view('admin/include/header');
?>
<style type="text/css">
	#datatables-basic_wrapper{
		margin-top: 11px;
	}
	.dataTables_length{
		margin-left: 10px;
	}
	#datatables-basic_filter{
		margin-right: 10px;
	}
	.dataTables_info{
		margin-left: 10px;
		margin-bottom: 10px;
	}
	#datatables-basic_paginate{
		margin-right: 10px;
	}
	#datatables-basic1_wrapper{
		margin-top: 10px;
	}
</style>
			<main class="content">
				<div class="container-fluid p-0">
						<!--<div class="form-row mb-2">
						<div class="col-lg-3 col-xs-12">
					    	<h3> User Transaction </h3>
					    </div>
					    <div class="col-lg-3 col-sm-4">
					    	<div class="form-group">
							<div class="input-group align-items-center calendr">
								<label for=""> From </label>
								<input type="text" style="padding: 1.2rem .7rem;" class="form-control" name="from" id="from" placeholder="10/11/2020">
								<span class="input-group-append">
              						<button class="btn btn-secondary" type="button"><i class="far fa-calendar-alt"></i></button>
				                </span>
				            </div>
							</div>
						</div>
						<div class="col-lg-3 col-sm-4">
					    	<div class="form-group">

							<div class="input-group align-items-center calendr">
								<label for=""> To </label>
								<input type="text" style="padding: 1.2rem .7rem;" class="form-control" name="to" id="to" placeholder="15/11/2020">
								<span class="input-group-append">
              						<button class="btn btn-secondary" type="button"><i class="far fa-calendar-alt"></i></button>
				                </span>
				            </div>
							</div>
						</div>
						
					    <div class="col-lg-3 col-sm-4">
					    	<div class="form-group">
							<div class="input-group">
								<input type="text" style="padding: 1.2rem .7rem;" class="form-control" placeholder="Search for...">
								<span class="input-group-append">
              						<button class="btn btn-info" type="button">Go!</button>
				                </span>
				            </div>
							</div>
						</div>
					  </div>-->

					  <div class="row">
					  	  <div class="col-12 col-lg-12">
							<div class="card">
								<div class="row">
						<div class="col-12 col-lg-12 mt-minus">
								<div class="table-responsive  border">
									<table class="table mb-0" id="datatables-basic">
										<thead>
											<tr style="background: linear-gradient(to right, #2754fc, #315cfd, #3b64fe, #446cff, #4d73ff);">
												<th scope="col"> Name </th>
												<th scope="col" style=""> Email </th>
												<th scope="col" style=""> Subject </th>
												<th scope="col" style=""> Message </th>
												
											</tr>
										</thead>
										<tbody>
										<?php
										foreach($support_query as $query){
										?>
											<tr>
												<th scope="row"><?=$query['merchant_name'] ?></th>
												<th scope="row"><?=$query['email'] ?></th>
												<th scope="row"><?=$query['subject'] ?></th>
												<th scope="row"><?=$query['msg'] ?></th>
												
											</tr>
										<?php } ?>
										</tbody>
									</table>
								<!------------------------------  Start Change Booth modal box ------------------------->
								<div class="modal fade" data-keyboard="false" data-backdrop="static" id="change_both" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title"> Change Booth </h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								                      <span aria-hidden="true">×</span>
								                    </button>
												</div>
												<div class="modal-body">
													<form>
														<div id="formbox">
														  <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for="">Booth 1</label>
														      <select id="inputState" class="form-control">
																   <option selected="">Select</option>
																   <option>...</option>
																</select>
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">Booth 2</label>
														       <select id="inputState" class="form-control">
																   <option selected="">Select</option>
																   <option>...</option>
																</select>
														    </div>
														  </div>
														   <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for="">Time Assigned</label>
														      <input type="text" class="form-control" id="" placeholder="9:00AM">
														    </div>
														     <div class="form-group col-md-6">
														      <label for="">Time Assigned</label>
														      <input type="text" class="form-control" id="" placeholder="9:00AM">
														    </div>
														  </div>
														  <div class="form-group text-center">
														     <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Update </button>
														  </div>
													</div>
													</form>
												</div>
												
											</div>
										</div>
									</div>
									<!-------------- End Modal box ------------------>

								</div>
						
						</div>
					</div>
							</div>
						</div>
					  </div>
				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/redmond/jquery-ui.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript">
	$("#from").datepicker({
	 //  minDate: 0,
	 //  onSelect: function(date) {		
		// $("#to").datepicker('option', 'minDate', date);	  
	 //  }
	});

	$("#to").datepicker({});
</script>

	<script>
		$(function() {
			$("#datatables-basic").DataTable({
				//responsive: true
				"aaSorting": []
			});
			$("#datetimepicker-dashboard").datetimepicker({
				inline: true,
				sideBySide: false,
				format: "L"
			});
		});
	</script>
	<script>
		$(function() {
			// Line chart
			new Chart(document.getElementById("chartjs-dashboard-line"), {
				type: "line",
				data: {
					labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Sales ($)",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.primary,
						data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
					}, {
						label: "Orders",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.tertiary,
						borderDash: [4, 4],
						data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					tooltips: {
						intersect: false
					},
					hover: {
						intersect: true
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						xAxes: [{
							reverse: true,
							gridLines: {
								color: "rgba(0,0,0,0.05)"
							}
						}],
						yAxes: [{
							ticks: {
								stepSize: 500
							},
							display: true,
							borderDash: [5, 5],
							gridLines: {
								color: "rgba(0,0,0,0)",
								fontColor: "#fff"
							}
						}]
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			// Pie chart
			new Chart(document.getElementById("chartjs-dashboard-pie"), {
				type: "pie",
				data: {
					labels: ["Direct", "Affiliate", "E-mail", "Other"],
					datasets: [{
						data: [2602, 1253, 541, 1465],
						backgroundColor: [
							window.theme.primary,
							window.theme.warning,
							window.theme.danger,
							"#E8EAED"
						],
						borderColor: "transparent"
					}]
				},
				options: {
					responsive: !window.MSInputMethodContext,
					maintainAspectRatio: false,
					legend: {
						display: false
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>

</body>

</html>