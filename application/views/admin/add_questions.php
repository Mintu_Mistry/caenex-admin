<?php
$this->load->view('admin/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('admin/include/nev');
?>

<div class="main">
<?php
$this->load->view('admin/include/header');
?>

<main class="content">
<div class="container-fluid p-0">

<h1 class="nexplay_tab" style="background: linear-gradient(to right, #e89e2b, #eea73b, #f4b149, #f9ba57, #ffc365);border-color: #e89e2b;"> Add Questions </h1>

<div class="row">
<div class="col-md-12 col-xl-12">
<div class="card">
<div class="popbox">
<div class="card-body">

<div class="row">
<div class="col-12 col-xl-12">
<!-- <h4> Select Game Type </h4> -->
<div class="tab">
<!-- <ul class="nav nav-tabs mb20" role="tablist">
<li class="nav-item"><a class="nav-link active" href="#Rewardtab-1" data-toggle="tab" role="tab">Caenex Game</a></li>
<li class="nav-item"><a class="nav-link" href="#Rewardtab-2" data-toggle="tab" role="tab">Custom Game</a></li>
</ul> -->
<div class="tab-content pad0 boxshadow0">

<!-- <div class="tab-pane" id="Rewardtab-2" role="tabpanel"> -->
<form method="post" enctype="multipart/form-data" id="paymentFrm1" action="<?php echo base_url('Admin/add_questions_data') ?>">
	


<div class="form-row">
<!-- <div class="col-12 col-xl-12"><div class="createyour_question"><b> Create Your Questions </b> (Tick the correct answer) </div> </div> -->
</div>
<div class="form-row">

<div class="form-group col-md-4">
<label for="inputEmail4">Select Game</label>
<select  class="form-control" name="game" id="game" required="">
<option value="">Select Game</option>
<?php
  foreach ($game as $key => $game_list) {
    ?>
<option value="<?php echo $game_list['game_id'] ?>"><?php echo $game_list['game_name'] ?></option>
    <?php
  }
?>
</select>
</div>

<div class="form-group col-md-4">
<label for="inputEmail4">Choose Game Category</label>
<select  class="form-control" name="category" id="custom_category" required="">
<option value="">Choose Game Category</option>
<?php
  foreach ($category as $key => $categorys) {
    ?>
<option value="<?php echo $categorys['id'] ?>"><?php echo $categorys['category'] ?></option>
    <?php
  }
?>
</select>
</div>

<div class="form-group col-md-4">
<label for="inputPassword4">Reward / Correct Question</label>
<input type="text" class="form-control" name="custom_reward" id="custom_reward" placeholder="" oninput="Validate_Only_Numeric_Decimal_Input('custom_reward')" required="">
</div>
</div>

<div class="form-row" id="new_table">
<div class="col-12 col-xl-4">
<label class="question_num">Question 1</label>
<div class="form-group">
<input type="text" class="form-control" id="Q1" name="question[]" placeholder="Enter question" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" id="Q1A1" name="answer1[]" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio1" type="radio" id="Q1A1C" value="1" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer2[]" id="Q1A2" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio1" type="radio" id="Q1A2C" value="2" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer3[]" id="Q1A3" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio1" type="radio" id="Q1A3C" value="3" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer4[]" id="Q1A4" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio1" type="radio" id="Q1A4C" value="4" id="autoSizingCheck" required="">
</div>
</div>
<div class="col-12 col-xl-4">
<label class="question_num">Question 2</label>
<div class="form-group">
<input type="text" class="form-control" id="Q2" name="question[]" placeholder="Enter question" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" id="Q2A1" name="answer1[]" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio2" value="1" id="Q2A1C" type="radio" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer2[]" id="Q2A2" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio2" value="2" id="Q2A2C" type="radio" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer3[]" id="Q2A3" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio2" value="3" id="Q2A3C" type="radio" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer4[]" id="Q2A4" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio2" value="4" id="Q2A4C" type="radio" id="autoSizingCheck" required="">
</div>
</div>
<div class="col-12 col-xl-4">
<label class="question_num">Question 3</label>
<div class="form-group">
<input type="text" class="form-control" id="Q3" name="question[]" placeholder="Enter question" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" id="Q3A1" name="answer1[]" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio3" value="1" id="Q3A1C" type="radio" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer2[]" id="Q3A2" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio3" value="2" id="Q3A2C"type="radio" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer3[]" id="Q3A3" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio3" value="3" id="Q3A3C" type="radio" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer4[]" id="Q3A4" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio3" value="4" id="Q3A4C" type="radio" id="autoSizingCheck" required="">
</div>
</div>
<!--<div class="col-12 col-xl-3">
<label class="question_num">Question 4</label>
<div class="form-group">
<input type="text" class="form-control" id="Q4" name="question[]" placeholder="Enter question" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" id="Q4A1" name="answer1[]" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio4" value="1" id="Q4A1C" type="radio" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer2[]" id="Q4A2" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio4" value="2" id="Q4A2C" type="radio" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer3[]" id="Q4A3" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio4" value="3" id="Q4A3C" type="radio" id="autoSizingCheck" required="">
</div>
<div class="form-group">
<input type="text" class="form-control" name="answer4[]" id="Q4A4" placeholder="Enter answer" required="">
<input class="form-check-input inputcheck" name="radio4" value="4" id="Q4A4C" type="radio" id="autoSizingCheck" required="">
</div>
</div>-->

</div>
<input type="hidden" name="number" id="number" value="1">
<input type="hidden" name="question_number" id="question_number" value="3">
<div class="form-row">
<div class="col-12 col-xl-12 text-center">
<div style="background: linear-gradient(to right, #e89e2b, #eea73b, #f4b149, #f9ba57, #ffc365);border-color: #e89e2b;" class="btn addmore"><a href="javascript:void(0)" class= "tr_clone_add"> Add More Questions </a></div>
</div>
</div>



<div class="row text-center">
<div class="col-12 col-xl-12">
<button style="background: linear-gradient(to right, #e89e2b, #eea73b, #f4b149, #f9ba57, #ffc365);border-color: #e89e2b;" class="btn btn-success payment_btn"> Submit </button>





</div>
</div>

</form>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>

</div>
</main>
</div>
</div>

<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/redmond/jquery-ui.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>

</body>

</html>

	<script type="text/javascript">
		  $(document).ready(function(){

       var max_fields = 100;
       var x = 1;
    $('.tr_clone_add').click(function(){
      var x=document.getElementById("number").value;
    $(this).attr('value', (x+1)); 
      var question_number_int=document.getElementById("question_number").value;
      var question_number=parseInt(question_number_int);
      document.getElementById("question_number").value=question_number+1;
       var question_number1=question_number+1;
       //var question_number2=question_number+2;
       //var question_number3=question_number+3;
       //var question_number4=question_number+4;
            $("#item_list").show();
         
         if(x < max_fields){
		  x++;
        document.getElementById("number").value= x;
         $("#new_table").append('<div class="col-12 col-xl-4"><label class="question_num">Question '+question_number1+'</label><div class="form-group"><input type="text" class="form-control" id="Q'+question_number1+'" name="question[]" placeholder="Enter question"></div><div class="form-group"><input type="text" class="form-control" id="Q'+question_number1+'A1" name="answer1[]" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number1+'" id="Q'+question_number1+'A1C" value="1" type="radio" id="autoSizingCheck" required=""></div><div class="form-group"><input type="text" class="form-control" name="answer2[]" id="Q'+question_number1+'A2" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number1+'" id="Q'+question_number1+'A2C" type="radio" value="2" id="autoSizingCheck" required=""></div><div class="form-group"><input type="text" class="form-control" name="answer3[]" id="Q'+question_number1+'A3" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number1+'" id="Q'+question_number1+'A3C" type="radio" value="3" id="autoSizingCheck" required=""></div><div class="form-group"><input type="text" class="form-control" name="answer4[]" id="Q'+question_number1+'A4" placeholder="Enter answer"><input class="form-check-input inputcheck" name="radio'+question_number1+'" id="Q'+question_number1+'A4C" type="radio" value="4" id="autoSizingCheck" required=""></div></div>');          
}    
    })
  })
	</script>	






