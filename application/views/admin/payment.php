<?php
	//preg_match_all('/[^a-z_\-0-9]/i', base_url(uri_string()), $matches);
	$txt = explode("/",base_url(uri_string()));
	/* echo "<pre>";
	print_r($card_details);
	die(); */
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Bootstrap 4 Admin &amp; Dashboard Template">
	<meta name="author" content="Bootlab">

	<title> Caenex Payment </title>
	<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin="">
	<link rel="stylesheet" href="<?php echo base_url('assets/') ?>css1/classic.css" >
	<script src="<?php echo base_url('assets/') ?>js/settings.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/app.js"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/') ?>sweetalert.css">
	<script src="<?php echo base_url('assets/dist/') ?>sweetalert.min.js"></script>
	<!-- END SETTINGS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script  src="https://www.googletagmanager.com/gtag/js?id=UA-120946860-6"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.5.10/cleave.min.js"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120946860-6');
</script>
<style>
input#submit-btn {
    padding: 4px 30px !important;
    margin: 0 auto;
    text-align: center;
    display: block;
}
.contentcenter {
    margin: 0 auto;
}

#frmStripePayment input {
    padding: 6px 10px;
}

div#loader>img {
    position: absolute;
    width: 60px;
    left: 50% !important;
}

.fulbg {
    background: rgb(0 0 0 / 35%);
    width: 100%;
    position: absolute;
    display: flex;
    height: 100%;
    z-index: 99;
	justify-content: center;
    align-items: center;
}

div#error-message {
    color: red;
}

</style>

</head>
<body>

<div class="wrapper">
<main class="content contentcenter">
<div class="container-fluid p-0">
<div class="row">
<div class="col-md-12 col-xl-12">
<div class="card">
<div class="popbox">
<div class="row">
<div class="card-body bg-white">
<?php if(!empty($successMessage)) { ?>
    <div id="success-message"><?php echo $successMessage; ?></div>
    <?php  } ?>
    <div id="error-message"></div>
	
<?php
if ($this->session->flashdata('add_user')) {
?>
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
<div class="alert-message">
	<strong>Success!</strong> <?php echo $this->session->flashdata('add_user'); ?>
</div>
</div>
<?php
}
?>
<div class="fulbg" id="spinner"><div class="spinner-border text-warning"></div></div></div>
<form id="frmStripePayment" action="<?php echo base_url('payment_submit') ?>" method="post" style="background-color:white;padding:15px;">
<div class="form-row">
<div class="form-group col-md-11">
<label for="inputEmail4" style="float: left;">Cardholder name</label>
<input type="text" class="form-control" id="card_holder_name" value="<?=isset($card_details)? $card_details[0]['cardholdername'] : '' ?>" oninput="validate_only_alphabates_allowed(this.id)" name="card_holder_name" placeholder="Cardholder name"  required>
<input type="hidden" value="<?php echo $txt[4]; ?>" name="user_token">
</div>
</div>
<div class="form-row posi_relative">
<div class="form-group col-md-11">
<label for="inputEmail4" style="float: left;">Card number</label>
<input type="number" pattern="[0-9]*" inputmode="numeric" class="form-control" required onkeypress="return autoMask(this,event, '#### #### #### ####');" value="<?=isset($card_details)? $card_details[0]['cardnumber'] : '' ?>" name="card_number" id="card_number" placeholder="Card number">
<!-- <div class="visabox"><img src="<?php echo base_url('assets/img/')?>visa_icon.png"></div> -->
</div>
</div>

<div class="form-row">
<!--<input type="text" class="form-control" name="exp_date" onkeypress="return autoMask(this,event, '##/####');" id="expmonth" maxlength="7" minlength="7" placeholder="Month / Year"  required>-->
<div class="form-group col-md-4">
<label for="inputAddress2" style="float: left;">Exp. Month</label>
 <select name="month" id="month" class="form-control">
 <?php
 $month ='';
 $year = '';
	if(isset($card_details))
	{
		if(!empty($card_details[0]['expirydate']))
		{
			$expirydate = explode("/",$card_details[0]['expirydate']);
			$month = $expirydate[0];
			$year = $expirydate[1];
		}
			
	}
	$month_arr = array("01","02","03","04","05","06","07","08","09","10","11","12");
	/* print_r($month_arr);
	die(); */
	?>
		
		<option value="0" selected >Select Month</option>
		
		<?php 
	foreach($month_arr as $mon){
		if($month !='')
		{
			if($mon == $month)
			{
				?>
				<option value="<?=$mon?>" selected ><?=$mon?></option>
	<?php		
			}else{
				?>
				<option value="<?=$mon?>" ><?=$mon?></option>
			
			<?php }
		}else{?>
		<option value="<?=$mon?>" ><?=$mon?></option>
<?php 		
		}
	}
 ?>
	
</select></div>
<div class="form-group col-md-4">
<label for="inputAddress2" style="float: left;">Exp. Year</label>
<select name="year" id="year" class="form-control"> 
<?php
$year_arr = array("21","22","23","24","25","26","27","28","29","30","31","32","33","34","35");
	/* print_r($month_arr);
	die(); */
	?>
		
		<option value="0" selected >Select Year</option>
		
		<?php 
	foreach($year_arr as $yr){
		
		if($year !='')
		{
			if($yr == $year)
			{?>
			<option value="<?=$yr?>" selected >20<?=$yr?></option>
	<?php		
			}else{?>
				<option value="<?=$yr?>" >20<?=$yr?></option>
			
			<?php }
		}else{?>
		<option value="<?=$yr?>" >20<?=$yr?></option>
<?php 		
		}
	}
 ?>
</select>
</div>

<div class="form-group col-md-3">
<label for="inputAddress2" style="float: left;">CVV</label>
<input type="number" pattern="[0-9]*" inputmode="numeric" class="form-control" placeholder="CVV" value="" id="cvv" name="cvv"  onkeypress="return autoMask(this,event, '###');"  required>
</div>
</div>
<div class="form-row">
<div class="form-group col-md-11">
<label for="inputEmail4" style="float: left;">Card nickname(Optional)</label>
<input type="text" class="form-control" placeholder="Card nickname(Optional)" value="<?=isset($card_details)? $card_details[0]['nickname'] : '' ?>" id="Nickname" oninput="validate_only_alphabates_allowed(this.id)" name="nickname">
</div>
</div>									
<div>
	<input type="submit" name="pay_now" value="Pay" id="submit-btn" class="btnAction"
		onClick="stripePay(event);">
</div>
</div>
<input type='hidden' name='currency_code' value='USD'> 
<input type='hidden' name='item_name' value='Test Product'>
<input type='hidden' name='item_number' value='PHPPOTEG#1'>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</main>
</div>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/redmond/jquery-ui.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>
    <script>
	document.addEventListener("DOMContentLoaded", function() {
		/* if(empty(card_id))
		{
			var name = $('#card_holder_name').val("");
			var cardNumber = $('#card_number').val("");
			var month = $('#month').val("");
			var year = $('#year').val("");
			var cvc = $('#cvv').val("");
		} */
		
		$('#spinner').hide();
	});
	
	var $card_number = $('#card_number');
	$card_number.keyup(function(e) {
		var max = 16;
		if ($card_number.val().length > max) {
			$card_number.val($card_number.val().substr(0, max));
		}
	});
	
	var $cvv = $('#cvv');
	$cvv.keyup(function(e) {
		var max = 3;
		if ($cvv.val().length > max) {
			$cvv.val($cvv.val().substr(0, max));
		}
	});

	function autoMask(field, event, sMask) {
    var KeyTyped = String.fromCharCode(getKeyCode(event));
    if (getKeyCode(event) == 8) {
        return
    }
    if (getKeyCode(event) == 0) {
        return
    }
    if (field.value.length == sMask.length && getKeyCode(event) == 13) {
        return true
    }
    // if (field.value.length == sMask.length && getKeyCode(event) != 13) {
    //     field.value = ""
    // }
    var targ = getTarget(event);
    keyCount = targ.value.length;
    if (keyCount == sMask.length) {
        return false
    }
    if (sMask.charAt(keyCount + 1) != "#" && sMask.charAt(keyCount + 1) != "A") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return false
    }
    if (sMask.charAt(keyCount) == "*")
        return true;
    if (sMask.charAt(keyCount) == KeyTyped) {
        return true
    }
    if (sMask.charAt(keyCount) == "#" && isNumeric(KeyTyped))
        return true;
    if (sMask.charAt(keyCount) == "A" && isAlpha(KeyTyped))
        return true;
    if (sMask.charAt(keyCount + 1) == "?") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return true
    }
    if (KeyTyped.charCodeAt(0) < 32)
        return true;
    return false
}

function getKeyCode(e) {
    if (e.srcElement) {
        return e.keyCode
    }
    if (e.target) {
        return e.which
    }
}

function getTarget(e) {
    if (e.srcElement) {
        return e.srcElement
    }
    if (e.target) {
        return e.target
    }
}

function isNumeric(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

function isAlpha(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

function validate_only_alphabates_allowed(input_id){
			let input_value = document.getElementById(input_id).value;
			let regExp = /^[a-z A-Z]+$/;
			if (regExp.test(input_value)) {
			console.log("Valid");
			}else{
			
			document.getElementById(input_id).value  = input_value.substring(0, input_value.length-1);
		
			}
		}

function cardValidation () {
    var valid = true;
    var name = $('#card_holder_name').val();
    var cardNumber = $('#card_number').val();
    var month = $('#month').val();
    var year = $('#year').val();
    var cvc = $('#cvv').val();

    $("#error-message").html("").hide();

    if (name.trim() == "") {
        valid = false;
    }
    if (cardNumber.trim() == "") {
    	   valid = false;
    }

    if (month.trim() == "") {
    	    valid = false;
    }
    if (year.trim() == "") {
        valid = false;
    }
    if (cvc.trim() == "") {
        valid = false;
    }

    if(valid == false) {
        $("#error-message").html("All Fields are required").show();
    }

    return valid;
}
//set your publishable key
Stripe.setPublishableKey("pk_test_MJIPPiFofMkp3lXKQ2TCHuoz003bWJSYgt");

//callback to handle the response from stripe
function stripeResponseHandler(status, response) {
    if (response.error) {
        //enable the submit button
        $("#submit-btn").show();
		$("#loader").show();
        //$( "#loader" ).css("display", "none");
		$('#spinner').show();
        //display the errors on the form
        $("#error-message").html(response.error.message).show();
		$('#spinner').hide();
    } else {
        //get token id
		$('#spinner').show();
        var token = response['id'];
        //insert the token into the form
        $("#frmStripePayment").append("<input type='hidden' name='token' value='" + token + "' />");
        //submit form to the server
        $("#frmStripePayment").submit();
		
    }
}
function stripePay(e) {
    e.preventDefault();
    var valid = cardValidation();

    if(valid == true) {
        $("#submit-btn").hide();
        $( "#loader" ).css("display", "inline-block");
        Stripe.createToken({
            number: $('#card_number').val(),
            cvc: $('#cvv').val(),
            exp_month: $('#month').val(),
            exp_year: $('#year').val()
        }, stripeResponseHandler);

        //submit from callback
        return false;
    }
}
</script>
</body>
</html>