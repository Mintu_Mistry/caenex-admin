<?php
$this->load->view('admin/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('admin/include/nev');
?>

<div class="main">
<?php
$this->load->view('admin/include/header');
?>

<main class="content">
<div class="container-fluid p-0">

<h1 class="h3 mb-3 profile_tab">Profile Details</h1>
			<?php
if ($this->session->flashdata('admin_data')) {
?>
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
<div class="alert-message">
	<strong>Success!</strong> <?php echo $this->session->flashdata('admin_data'); ?>
</div>
</div>
<?php
}
?>
<div class="tab-content"> 
<div class="row justify-content-md-center">
	<div class="col-md-8">
		<div class="card-body passbox">
			
				<form method="post" onsubmit="return validateForm1()" name="myForm1">
					<div class="form-group">
						<label>Name </label>
						<input class="form-control form-control-lg" required="" type="text" name="name" value="<?php echo $data['name']  ?>" placeholder="" >
						
					</div>
					<div class="form-group">
						<label> Email </label>
						<p id="error_email1" style="color: red;margin-left: 10px;margin-bottom: -1px;"></p>
						<input class="form-control form-control-lg" type="text" name="email" value="<?php echo $data['email']  ?>" onblur ="check_email(this.value)" placeholder="" required="">
						
					</div>
					<!-- <div class="form-group">
						<label> Phone Number </label>
						<input class="form-control form-control-lg" type="text" name="password" placeholder="9976543210">
					</div> -->
					<div class="text-center mt-3">
						<button class="btn btn-lg btn-primary mr-lg-2" name="submit"> Save Changes</button>
						<div class="btn btn-lg btn-primary" data-toggle="modal" data-target=".bd-example-modal-sm"> Change Password </div>
					</div>

				</form>
			<!-- Small modal -->
				<div class="modal fade bd-example-modal-sm" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-sm" role="document">
				    <div class="modal-content">
				    	<div class="border-0">
					        <button type="button" class="close p-2" data-dismiss="modal" aria-label="Close" onclick="clear_data()">
					          <span aria-hidden="true">×</span>
					        </button>
					      </div>
				      <form method="post" onsubmit="return validateForm()" name="myForm" action="<?php echo base_url('Admin/change_password') ?>">
				      	<div class="card-body">
				      	<p id="error_email" style="color: red;text-align: center;"></p>
				      	<div class="form-group">
						<label> Current Password </label>
						<input class="form-control form-control-lg" type="password" name="oldpassword" id="oldpassword" placeholder="" onblur="check_old(this.value)" required="">
					</div>
					<div class="form-group">
						<label>New Password</label>
						<input class="form-control form-control-lg" type="password" name="newpassword" id="newpassword" placeholder="" required="">
					</div>
					<div class="form-group">
						<label>Verify Password</label>
						<input class="form-control form-control-lg" type="password" name="conpassword" id="conpassword" placeholder="" required="">
					</div>
					<div class="text-center mt-3">
						<button class="btn btn-lg btn-primary" id="add_user"> Save </button>
					</div>
				</div>
				      </form>
				    </div>
				  </div>
				</div>
		</div>
	</div>
</div>
</div>

</div>
</main>
</div>
</div>

<script src="js\app.js"></script>

</body>

</html>

<script type="text/javascript">
	function clear_data(){
		document.getElementById("error_email").innerHTML= '';
		
		document.getElementById("oldpassword").value= '';
		document.getElementById("newpassword").value= '';
		document.getElementById("conpassword").value= '';
	}

function validateForm1(){
var email = document.forms["myForm1"]["email"].value;
let email_pettern=/^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/;
// let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
if (email_pettern.test(email)) {
document.getElementById("error_email1").innerHTML= '';
return true;

}else{
document.getElementById("error_email1").innerHTML= 'Please Enter Valid Email Format';
return false;

}

}

function check_old(oldpassword){

		$.post("<?php echo base_url("Admin/check_password") ?>",{oldpassword:oldpassword},function(res){
			if (res==1) {
				document.getElementById('error_email').innerHTML ="";
				$('#add_user').prop('disabled', false);
			}else if(res==2){
		  	document.getElementById('error_email').innerHTML = 'Old Password Is Not Correct';
		  	$('#add_user').prop('disabled', true);
		  }
		  })
}

function validateForm(){
var oldpassword = document.forms["myForm"]["oldpassword"].value;
var newpassword = document.forms["myForm"]["newpassword"].value;
var conpassword = document.forms["myForm"]["conpassword"].value;

if (oldpassword!=newpassword) {
if (newpassword==conpassword) {

		let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
         
          	if (regex.test(newpassword)) {
          		return true;            
           }else{
            document.getElementById("error_email").innerHTML= 'Password must be 8 digit long including alpha-numeric-lower_case-upper_case-special_char';
            return false;
        }
           
}else{
	document.getElementById('error_email').innerHTML = 'Password and confirm password should be same';
return false;
}
}else{
	document.getElementById('error_email').innerHTML = 'New password not should be same as old password';
	return false;
}

}


</script>