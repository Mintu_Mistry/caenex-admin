<?php
$this->load->view('admin/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('admin/include/nev');
?>

<div class="main">
<?php
$this->load->view('admin/include/header');
?>

<style type="text/css">
	#datatables-basic_wrapper{
		margin-top: 11px;
	}
	.dataTables_length{
		margin-left: 10px;
	}
	#datatables-basic_filter{
		margin-right: 10px;
	}
	.dataTables_info{
		margin-left: 10px;
		margin-bottom: 10px;
	}
	#datatables-basic_paginate{
		margin-right: 10px;
	}
</style>
			<main class="content">
				<div class="container-fluid p-0">
					<?php
if ($this->session->flashdata('corrct')) {
?>
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
<div class="alert-message">
	<strong>Success!</strong> <?php echo $this->session->flashdata('corrct'); ?>
</div>
</div>
<?php
}
?>
					<div class="row">
						<div class="col-12 col-lg-12 mt-minus">
								<div class="table-responsive  border bg-white">
									<table class="table mb-0" id="datatables-basic"> 
										<thead>
											<tr style="background: linear-gradient(to right, #87b116, #90b921, #98c22a, #a1ca33, #aad33b);">
												<th scope="col"> # </th>
												<th scope="col" style="">User Name</th>
												<th scope="col" style="">Current User Email/Phone Number </th>
												<th scope="col">Status</th>
												<th scope="col" class="action">Action</th>
											</tr>
										</thead>
										<tbody>										
											<?php
											$count=0;
											foreach ($id_verification as $key => $value) {
												$count++;
											?>
											<tr>
												<th scope="row" class="align-middle"><?php echo $count ?></th>
												<td><?php echo $value['name'] ?></td>
												<td><?php echo $value['email'] ?></td>
												<td class="Pending">
													<?php
													if ($value['id_status']==0) {				
													?>
													<span class="badge badge-warning">Pending</span>
													<?php
													}
													?>

													<?php
													if ($value['id_status']==2) {				
													?>
													<span class="badge badge-danger"> Denied </span>
													<?php
													}
													?>

													<?php
													if ($value['id_status']==1) {				
													?>
													<span class="badge badge-success">Accepted</span>
													<?php
													}
													?>
												</td>
												<td class="icon_action">
													<?php
													if ($value['id_status']==0) {				
													?>
											           <a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'];?>,'1')"><i style="color: green;"  class="fa fa-check-square"></i></a>
											              
													<a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'];?>,'2')"><i style="margin-left: 12px;" class="fas fa-times"></i></a>
													<?php
													}
													?>
													<?php
													if ($value['id_status']==2) {				
													?>
													 <a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'];?>,'1')"><i style="color: green;"  class="fa fa-check-square"></i></a>
													<?php
													}
													?>
													<?php
													if ($value['id_status']==1) {				
													?>
													<a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'];?>,'2')"><i  class="fas fa-times"></i></a>
													<?php
													}
													?>
													
													<a href="javascript:void(0)" data-toggle="modal" data-target="#user_mng_view<?php echo $value['id'] ?>"><i style="margin-left: 12px;margin-bottom: 10px;" class=" align-middle fas fa-eye"></i></a>
													</td>

											</tr>
											<?php
											}
											?>

										</tbody>
									</table>
									
<?php 
		foreach ($id_verification as $key => $value) {
		?>				
<div class="modal fade show" id="user_mng_view<?php echo $value['id'] ?>"  tabindex="-1" role="dialog" aria-modal="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header" style="background: linear-gradient(to right, #08a4c4, #00b4ce, #00c3d7, #18d3de, #30e3e3);">
<h3 class="modal-title"> View details</h3>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
</div>
<b>
<p id="error_email" style="color: red;text-align: center;font-size: 20px;margin-bottom: -5px;"></p></b>
<div class="modal-body">
<table class="table mb-0" id="datatables-basic"> 
				<thead>
					<tr style="background: linear-gradient(to right, #87b116, #90b921, #98c22a, #a1ca33, #aad33b);">
						<th scope="col" style="">Govt ID</th>
						<th scope="col">Address Proof</th>
						<th scope="col">Selfie with ID</th>
					</tr>
				</thead>
				<tbody>										
					<?php
				
						$con['verification_id']=$value['id'];
						$img_data=$this->Merchant_modal->get_all_data('*','tbl_id_verification',$con);
						//print_r($img_data);
					?>
					<tr>
						<td class="verification">
							<?php
							foreach ($img_data as $key => $value2) {
								if ($value2['type']==1) {
								
							?>
							<img class="imagethumb" style="height: 50px;width: 60px;margin-bottom: 10px;cursor: pointer;" onclick="showimg(<?php echo $value2['id'];?>,this)" id="<?php echo $value2['id'];?>"  src="<?php echo base_url('assets/img/photos/').$value2['img'] ?>" >
							<?php
							}
							}
							?>
						</td>
						<td class="verification">
							<?php
							foreach ($img_data as $key => $value2) {
								if ($value2['type']==2) {
								
							?>
							<img class="imagethumb" style="height: 50px;width: 60px;margin-bottom: 10px;cursor: pointer;" src="<?php echo base_url('assets/img/photos/').$value2['img'] ?>" onclick="showimg(<?php echo $value2['id'];?>,this)" id="<?php echo $value2['id'];?>">
							<?php
							}
							}
							?>
						</td>
						<td>
							<?php
							foreach ($img_data as $key => $value2) {
								if ($value2['type']==3) {
								
							?>
							<img class="imagethumb" style="height: 50px;width: 60px;margin-bottom: 10px;cursor: pointer;" src="<?php echo base_url('assets/img/photos/').$value2['img'] ?>" onclick="showimg(<?php echo $value2['id'];?>,this)" id="<?php echo $value2['id'];?>">
							<?php
							}
							}
							?>
						</td>
						</tr>

				</tbody>
			</table>
</div>

</div>
</div>
</div>
		<?php } ?>
		
								<!------------------------------  Start Change Booth modal box ------------------------->
								<div class="modal fade" data-keyboard="false"  id="change_both" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title"> Change Booth </h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								                      <span aria-hidden="true">×</span>
								                    </button>
												</div>
												<div class="modal-body">
													<form>
														<div id="formbox">
														  <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for="">Booth 1</label>
														      <select id="inputState" class="form-control">
																   <option selected="">Select</option>
																   <option>...</option>
																</select>
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">Booth 2</label>
														       <select id="inputState" class="form-control">
																   <option selected="">Select</option>
																   <option>...</option>
																</select>
														    </div>
														  </div>
														   <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for="">Time Assigned</label>
														      <input type="text" class="form-control" id="" placeholder="9:00AM">
														    </div>
														     <div class="form-group col-md-6">
														      <label for="">Time Assigned</label>
														      <input type="text" class="form-control" id="" placeholder="9:00AM">
														    </div>
														  </div>
														  <div class="form-group text-center">
														     <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Update </button>
														  </div>
													</div>
													</form>
												</div>
												
											</div>
										</div>
									</div>
									<!-------------- End Modal box ------------------>
									<!-- The Modal -->



									<div class="modal fade show" id="myModal"  tabindex="-1" role="dialog" aria-modal="true">
									<div class="modal-dialog" role="document">
									<div class="modal-content">
									<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" onclick="close_modle()" aria-label="Close">
									<span aria-hidden="true">×</span>
									</button>
									</div>
									<img class="modal-content" id="img01" >
									  <div id="caption"></div>

									</div>
									</div>
									</div>


								</div>
						
						</div>
					</div>								
				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script type="text/javascript">
		function change_status(id,status){
			
			$("#saved_card"+id+"").fadeOut(100);
	   		$.post("<?php echo base_url('Admin/update_status') ?>",{id:id,status:status},function(res){
	   			if (res==1) {
	   				location.reload();
	   			}
	   		})
		
		}
	</script>

	<script>
		$(function() {
			$("#datetimepicker-dashboard").datetimepicker({
				inline: true,
				sideBySide: false,
				format: "L"
			});
		});
	</script>
	<script>
		$(function() {
			// Line chart
			new Chart(document.getElementById("chartjs-dashboard-line"), {
				type: "line",
				data: {
					labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Sales ($)",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.primary,
						data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
					}, {
						label: "Orders",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.tertiary,
						borderDash: [4, 4],
						data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					tooltips: {
						intersect: false
					},
					hover: {
						intersect: true
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						xAxes: [{
							reverse: true,
							gridLines: {
								color: "rgba(0,0,0,0.05)"
							}
						}],
						yAxes: [{
							ticks: {
								stepSize: 500
							},
							display: true,
							borderDash: [5, 5],
							gridLines: {
								color: "rgba(0,0,0,0)",
								fontColor: "#fff"
							}
						}]
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			// Pie chart
			new Chart(document.getElementById("chartjs-dashboard-pie"), {
				type: "pie",
				data: {
					labels: ["Direct", "Affiliate", "E-mail", "Other"],
					datasets: [{
						data: [2602, 1253, 541, 1465],
						backgroundColor: [
							window.theme.primary,
							window.theme.warning,
							window.theme.danger,
							"#E8EAED"
						],
						borderColor: "transparent"
					}]
				},
				options: {
					responsive: !window.MSInputMethodContext,
					maintainAspectRatio: false,
					legend: {
						display: false
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>

	<script>
// Get the modal
function showimg(id,input){
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById(id);
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
//img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = input.src;
  captionText.innerHTML = input.alt;
//}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
}

function close_modle(){
	//alert('df');
	var modal = document.getElementById("myModal");
	modal.style.display = "none";
}

window.onclick = function(event) {
	var modal = document.getElementById("myModal");
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

</body>

</html>
<script>
		$(function() {
			$("#datatables-basic").DataTable({
				//responsive: true
				"aaSorting": []
			});
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>