<?php
$this->load->view('admin/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('admin/include/nev');
?>

<div class="main">
<?php
$this->load->view('admin/include/header');
?>

<main class="content">
<div class="container-fluid p-0">

<div class="row">
<div class="col-sm-3">
<a href="<?php echo base_url('user_management') ?>" style="text-decoration: none;">
<div class="card text-center" style="background:  linear-gradient(to right, #08a4c4, #00b4ce, #00c3d7, #18d3de, #30e3e3);">
<div class="card-body py-4">
<div class="media">
<div class="media-body" >
<h3 class="mb-2 h1 font-weight-bold text-light"  > <?php echo count($user) ?> </h3>
<div class="mb-0 h4 text-light"> Number of Active Users </div>
</div>
</div>
</div>
</div>
</a>
</div>
<div class="col-sm-3">
<a href="<?php echo base_url('merchant_management') ?>" style="text-decoration: none;">
<div class="card text-center" style="background: linear-gradient(to right, #b3b51a, #bbbd20, #c3c526, #cbcd2c, #d3d531);">
<div class="card-body py-4">
<div class="media">
<div class="media-body">
<h3 class="mb-2 h1 font-weight-bold text-light"><?php echo count($merchant) ?></h3>
<div class="mb-0 h4 text-light"> Number of Active Merchants</div>
</div>
</div>
</div>
</div>
</a>
</div>
<div class="col-sm-3">
	<a href="<?php echo base_url('billboard_management') ?>" style="text-decoration: none;">
<div class="card text-center" style="background: #009933;">
<div class="card-body py-4">
<div class="media">
<div class="media-body">
<h3 class="mb-2 h1 font-weight-bold text-light"><?php echo count($nexworld) ?>  </h3>
<div class="mb-0 h4 text-light">Active NexWorld Ads </div>
</div>
</div>
</div>
</div>
</a>
</div>
<div class="col-sm-3">
	<a href="<?php echo base_url('billboard_management') ?>" style="text-decoration: none;">
<div class="card text-center" style="background: #ffa000;">
<div class="card-body py-4">
<div class="media">
<div class="media-body">
<h3 class="mb-2 h1 font-weight-bold text-light"><?php echo count($nexplay) ?> </h3>
<div class="mb-0 h4 text-light">Active NexPlay Ads</div>
</div>
</div>
</div>
</div>
</a>
</div>
</div>									
</div>
</main>

<!-- <footer class="footer"></footer> -->
</div>
</div>
<!-- <script src="js\settings.js"></script> -->


<script>
$(function() {
$("#datetimepicker-dashboard").datetimepicker({
inline: true,
sideBySide: false,
format: "L"
});
});
</script>
<script>
$(function() {
// Line chart
new Chart(document.getElementById("chartjs-dashboard-line"), {
type: "line",
data: {
labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
datasets: [{
label: "Sales ($)",
fill: true,
backgroundColor: "transparent",
borderColor: window.theme.primary,
data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
}, {
label: "Orders",
fill: true,
backgroundColor: "transparent",
borderColor: window.theme.tertiary,
borderDash: [4, 4],
data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
}]
},
options: {
maintainAspectRatio: false,
legend: {
display: false
},
tooltips: {
intersect: false
},
hover: {
intersect: true
},
plugins: {
filler: {
propagate: false
}
},
scales: {
xAxes: [{
reverse: true,
gridLines: {
color: "rgba(0,0,0,0.05)"
}
}],
yAxes: [{
ticks: {
stepSize: 500
},
display: true,
borderDash: [5, 5],
gridLines: {
color: "rgba(0,0,0,0)",
fontColor: "#fff"
}
}]
}
}
});
});
</script>
<script>
$(function() {
// Pie chart
new Chart(document.getElementById("chartjs-dashboard-pie"), {
type: "pie",
data: {
labels: ["Direct", "Affiliate", "E-mail", "Other"],
datasets: [{
data: [2602, 1253, 541, 1465],
backgroundColor: [
window.theme.primary,
window.theme.warning,
window.theme.danger,
"#E8EAED"
],
borderColor: "transparent"
}]
},
options: {
responsive: !window.MSInputMethodContext,
maintainAspectRatio: false,
legend: {
display: false
}
}
});
});
</script>
<script>
$(function() {
$("#datatables-dashboard-projects").DataTable({
pageLength: 6,
lengthChange: false,
bFilter: false,
autoWidth: false
});
});
</script>

</body>

</html>