<?php
$this->load->view('admin/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('admin/include/nev');
?>

<div class="main">
<?php
$this->load->view('admin/include/header');
?>
<style type="text/css">
	#datatables-basic_wrapper{
		margin-top: 11px;
	}
	.dataTables_length{
		margin-left: 10px;
	}
	#datatables-basic_filter{
		margin-right: 10px;
	}
	.dataTables_info{
		margin-left: 10px;
		margin-bottom: 10px;
	}
	#datatables-basic_paginate{
		margin-right: 10px;
	}
</style>
			<main class="content">
				<div class="container-fluid p-0">
						<div class="row mb-2">
						<div class="col-12 col-lg-6 mobcenter">
					    	<h3> Merchant Management </h3>
					    </div>
					    <div class="col-12 col-lg-6">
					     	<div class="rowbtn text-right mobcenter">
					     		<div class="btn import_btn" style="background: linear-gradient(to right, #b3b51a, #bbbd20, #c3c526, #cbcd2c, #d3d531);" data-toggle="modal" data-target="#sizedModalSm"><i class="align-middle mr-2 fas fa-fw fa-plus"></i> Add a new Merchant </div>
					     	</div>
		<!--------------- Modal ---------------->
			     			<div class="modal fade show" id="sizedModalSm" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header" style="background: linear-gradient(to right, #b3b51a, #bbbd20, #c3c526, #cbcd2c, #d3d531);" >
											<h4 class="modal-title clrwhite heading"> Add a new Merchant</h4>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clear_data()">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body">
										  <form method="post" name="myForm1" onsubmit="return validateForm1()" autocomplete="off" action="<?php echo base_url('Admin/add_merchant') ?>">
										  	<p id="error_email" style="color: red; text-align: center;"></p>
											<div id="formbox">
											  <div class="form-row">

											    <div class="form-group col-md-6">
											      <label for=""> Owner / Principal Name </label>
											      <input type="text" class="form-control" name="merchant_name" id="merchant_name" placeholder="Owner / Principal Name" required="">
											    </div>

											    <div class="form-group col-md-6">
											      <label for="">Name of Business</label>
											      <input type="text" class="form-control" name="businessname" id="businessname" placeholder="Name of Business" required="">
											    </div>
											  </div>
											   <div class="form-row">
											    <div class="form-group col-md-6">
											      <label for="">Email Address</label>
												  <input type="email" class="form-control" name="merchant_email" placeholder="Enter your email address" autocomplete="new-password" autocapitalize="off" autocorrect="off" onblur ="check_email(this.value)">
												  
											      
											    </div>
											    <div class="form-group col-md-6">
											      <label for="">Password</label>
											      <input type="password" autocomplete="off" autocomplete="new-password" autocapitalize="off" autocorrect="off" class="form-control" name="user_password" id="user_password" placeholder="Enter Password"  required="">
											    </div>
											  </div>
											   <div class="form-row">
													<div class="form-group col-md-6">
														<label for="inputAddress">Street Address</label>
														<input type="text" class="form-control" name="merchant_address" id="merchant_address" placeholder="1234 Main St" required="">
													</div>
													<div class="form-group col-md-6">
														<label for="inputAddress2">City</label>
														<input type="text" class="form-control" name="merchant_city" id="merchant_city" placeholder="City Name" required="">
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="inputCity">State</label>
														<input type="text" class="form-control" name="merchant_state" id="merchant_state" placeholder="New York" required="">
													</div>
													<div class="form-group col-md-6">
														<label for="inputState">Zip</label>
														<input type="tel" class="form-control" id="txtPhn1" name="zip" placeholder="Enter Zip code" onkeypress="return validate1(event)"  required >
														
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="inputZip">Country</label>
														<input type="text" class="form-control" name="merchant_country" id="merchant_country" placeholder="United States" required="">
													</div>
												
													<div class="form-group col-md-6">
														<label for="inputZip">Tax Id</label>
														<input type="text" class="form-control" name="taxid" id="taxid" placeholder="Tax Id" required="">
													</div>
												</div>
												
											  <div class="form-group text-center">
											     <button style="background: linear-gradient(to right, #b3b51a, #bbbd20, #c3c526, #cbcd2c, #d3d531);border-color: #b3b51a;" type="submit" class="btn btn-primary center-block btn-lg addstu_btn" id="add_merchant"> Submit </button>
											  </div>
										</div>
										</form>
										</div>
										
									</div>
								</div>
							</div>

					    </div>
					  </div>
					  <h3>Revenue By Date</h3>
					  <form role="form" method="post" id="form-data">
					  <div class="form-row mb-2">
					  
					    <div class="col-lg-3 col-sm-4">
					    	<div class="form-group">
							<div class="input-group align-items-center calendr">
								<label for=""> From </label>
								<input type="text" style="padding: 1.2rem .7rem;" class="form-control" name="from" id="from" placeholder="From Date">
								<span class="input-group-append">
              						<button class="btn btn-secondary" type="button"><i class="far fa-calendar-alt"></i></button>
				                </span>
				            </div>
							</div>
						</div>
						<div class="col-lg-3 col-sm-4">
					    	<div class="form-group">

							<div class="input-group align-items-center calendr">
								<label for=""> To </label>
								<input type="text" style="padding: 1.2rem .7rem;" class="form-control" name="to" id="to" placeholder="To Date">
								<span class="input-group-append">
              						<button class="btn btn-secondary" type="button"><i class="far fa-calendar-alt"></i></button>
				                </span>
				            </div>
							</div>
						</div>
						
					    <div class="col-lg-3 col-sm-4">
					    	<div class="form-group">
							<div class="input-group">
								<span class="input-group-append">
              						<button class="btn btn-info search" type="button" style="padding: .6rem .7rem;">Go!</button>
				                </span>
				            </div>
							</div>
						</div>
						
						</div>
						</form>
<?php
if ($this->session->flashdata('merchant_error')) {
?>
<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
<div class="alert-message">
<strong>Error</strong> <?php echo $this->session->flashdata('merchant_error'); ?>
</div>
</div>
<?php
}
?>
							
<?php
if ($this->session->flashdata('corrct')) {
?>
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
<div class="alert-message">
	<strong>Success!</strong> <?php echo $this->session->flashdata('corrct'); ?>
</div>
</div>
<?php
}
?>

<?php
if ($this->session->flashdata('add_user')) {
?>
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
<div class="alert-message">
	<strong>Success!</strong> <?php echo $this->session->flashdata('add_user'); ?>
</div>
</div>
<?php
}
?>
					  <div class="row">
					  	  <div class="col-12 col-lg-12">
							<div class="card">
								<div class="row">
						<div class="col-12 col-lg-12 mt-minus">
								<div class="table-responsive  border">
									<table class="table mb-0" id="datatables-basic">
										<thead>
											<tr style="background: linear-gradient(to right, #b3b51a, #bbbd20, #c3c526, #cbcd2c, #d3d531);">
												<th scope="col"># </th> 
												<th scope="col" style=""> Merchant Name </th>
												<th scope="col"> Business Name </th>
												<th scope="col" style=""> Email </th>
												<!-- <th scope="col" style=""> Phone No. </th> -->
												<th scope="col"> Address </th>
												<th scope="col"> Tax ID </th>
												<th scope="col"> Total Revenue Earned</th>
												<th scope="col">Status</th>
												<th scope="col" class="action">Action</th>
											</tr>
										</thead>
										<tbody id="default">
											
											<?php
											$count=0;
											foreach ($merchant as $key => $value) {
												$count++;
											
											?>
											<tr id="saved_card<?php echo $value['merchant_id'] ?>">
												<th scope="row"><?php echo $count; ?></th>
												<td> <?php echo $value['merchant_name']; ?></td>
												<td> <?php echo $value['businessname']; ?></td>
												<td><?php echo $value['merchant_email']; ?></td>
												<!-- <td>9484765432</td> -->
												<td><?php echo $value['merchant_address'].' , '.$value['merchant_city'].' , '.$value['merchant_state'].' , '.$value['merchant_country'].' , '.$value['zip']  ?></td>
												<td class="view"><?php echo $value['merchant_taxid']; ?></td>
												<td>$<?php echo $value['total_revenue']; ?></td>
												<td>
													<?php 
													if($value['status']==1){
													?>
													<span class="badge badge-success">Active</span>
													<?php
													}else{
													?>
													<span class="badge badge-warning">Inactive</span> 
												<?php } ?>
												</td>
												<td>
													<a href="javascript:void(0)"><i class="align-middle mr-2 fas fa-fw fa-edit" data-toggle="modal" data-target="#sizedModalSm<?php echo $count ?>"></i></a>
												

													<a href="javascript:void(0)" onclick="delete_data(<?php echo $value['merchant_id']; ?>)"><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>

													<!-- <a href="javascript:void(0)"><i class="align-middle fas fa-eye" data-toggle="modal" data-target="#sizedModalSm1<?php echo $count ?>"></i></a> -->
													<a href="<?php echo base_url('billboard_details/'). $value['merchant_id'] ?>"><i class="align-middle fas fa-eye"></i></a>
												</td>
											</tr>

								<!--------------- Modal ---------------->
			     			<div class="modal fade show" id="sizedModalSm<?php echo $count ?>" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header " style="background: linear-gradient(to right, #b3b51a, #bbbd20, #c3c526, #cbcd2c, #d3d531);">
											<h4 class="modal-title clrwhite heading">Update Merchant</h4>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body">
										  <form method="post" action="<?php echo base_url('Admin/add_merchant') ?>">

											<div id="formbox">
											  <div class="form-row">

											    <div class="form-group col-md-6">
											      <label for=""> Owner / Principal Name </label>
											      <input type="text" class="form-control" name="merchant_name" id="" placeholder="Owner / Principal Name" value="<?php echo $value['merchant_name']; ?>" required="">

											      <input type="hidden" name="merchant_id" value="<?php echo $value['merchant_id']; ?>">
											    </div>

											    <div class="form-group col-md-6">
											      <label for="">Name of Business</label>
											      <input type="text" class="form-control" name="businessname" id="" value="<?php echo $value['businessname']; ?>" placeholder="Name of Business" required="">
											    </div>
											  </div>
											   <div class="form-row">
											    <div class="form-group col-md-6">
											      <label for="">Email Address</label>
												  <input type="email" class="form-control" readonly name="merchant_email" placeholder="Enter your email address" autocomplete="new-password" autocapitalize="off" autocorrect="off" onblur ="check_email(this.value)" value="<?php echo $value['merchant_email']; ?>" readonly="">
												  
											    </div>
											    <!-- <div class="form-group col-md-6">
											      <label for="">Password</label>
											      <input type="password" class="form-control" name="merchant_phone" id="" placeholder="Enter Password" required="">
											    </div> -->
											  </div>
											   <div class="form-row">
													<div class="form-group col-md-6">
														<label for="inputAddress">Street Address</label>
														<input type="text" class="form-control" name="merchant_address" id="" placeholder="1234 Main St" value="<?php echo $value['merchant_address']; ?>" required="">
													</div>
													<div class="form-group col-md-6">
														<label for="inputAddress2">City</label>
														<input type="text" class="form-control" name="merchant_city" id="" value="<?php echo $value['merchant_city']; ?>" placeholder="City Name" required="">
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="inputCity">State</label>
														<input type="text" class="form-control" name="merchant_state" id="" value="<?php echo $value['merchant_state']; ?>" placeholder="New York" required="">
													</div>
													<div class="form-group col-md-6">
														<label for="inputState">Zip</label>
														<input type="tel" class="form-control" id="txtPhn1" name="zip" placeholder="Enter Zip code" onkeypress="return validate1(event)"value="<?php echo $value['zip'] ?>"  required >
														
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="inputZip">Country</label>
														<input type="text" class="form-control" name="merchant_country" id="" value="<?php echo $value['merchant_country']; ?>" placeholder="United States" required="">
													</div>
													
													<div class="form-group col-md-6">
														<label for="inputZip">Tax Id</label>
														<input type="text" value="<?php echo $value['merchant_taxid']; ?>" class="form-control" name="taxid" id="taxid" placeholder="Tax Id" required="">
													</div>
													</div>
												
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="inputZip">Status</label>
														<select class="form-control" name="status">
															<option <?php if($value['status']==1){ echo "selected"; } ?> value="1">Active</option>
															<option <?php if($value['status']==0){ echo "selected"; } ?> value="0">Inactive</option>
														</select>
													</div>

												</div>

											  <div class="form-group text-center">
											     <button style="background: linear-gradient(to right, #b3b51a, #bbbd20, #c3c526, #cbcd2c, #d3d531);border-color: #b3b51a;" type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Submit </button>
											  </div>
										</div>
										</form>
										</div>
										
									</div>
								</div>
							</div>

											<div class="modal fade show" id="sizedModalSm1<?php echo $count ?>" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
							<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header" style="background: linear-gradient(to right, #b3b51a, #bbbd20, #c3c526, #cbcd2c, #d3d531);">
									        <h4 class="modal-title clrwhite heading" id="">Merchant Details</h4>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          <span aria-hidden="true">×</span>
									        </button>
									      </div>
										<div class="modal-body">
											<form>
														<div id="formbox">
														  <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for=""> Owner / Principal Name </label>
														      <input type="text" class="form-control" id="" placeholder="Chris Wood" readonly="" value="<?php echo $value['merchant_name']; ?>">
														    </div>
														    <div class="form-group col-md-6">
													      <label for="">Name of Business</label>
													      <input type="text" class="form-control" name="businessname" id="" value="<?php echo $value['businessname']; ?>" placeholder="Name of Business" readonly>
													    </div>
														  </div>
														   <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for="">Merchant Email</label>
														      <input type="text" class="form-control" id="" placeholder="example@gmail.com" readonly="" value="<?php echo $value['merchant_email']; ?>">
														    </div>
														  </div>
														   <div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputAddress">Street Address</label>
																	<input type="text" class="form-control" id="" placeholder="1234 Main St" readonly="" value="<?php echo $value['merchant_address']; ?>">
																</div>
																<div class="form-group col-md-6">
																	<label for="inputAddress2">City</label>
																	<input type="text" class="form-control" id="" placeholder="City Name" readonly="" value="<?php echo $value['merchant_city']; ?>">
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputCity">State</label>
																	<input type="text" class="form-control" id="" placeholder="New York" readonly="" value="<?php echo $value['merchant_state']; ?>">
																</div>
																<div class="form-group col-md-6">
																	<label for="inputState">Zip</label>
																	<input type="text" class="form-control" id="" placeholder="Enter Zip code" readonly="" value="<?php echo $value['zip']; ?>">
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputZip">Country</label>
																	<input type="text" class="form-control" id="" placeholder="United States" readonly="" value="<?php echo $value['merchant_country']; ?>">
																</div>
															</div>

															<div class="modal-body font700">
																<div class="form-row">
																	<div class="col-lg-12">
																		<p>$
																			<!-- <i class="align-middle fas fa-fw fa-dollar-sign"></i> -->
																			Wallet Money: <span>$300</span></p>
																	</div>
																</div>
															</div>
													</div>
													</form>
										</div>
										
									</div>
								</div>
							</div>


											<?php
											}
											
											?>
										</tbody>
										<tbody id="searchdata" style="display:none;">
                        <tr><td colspan="8"><center> No Record Found !</center></td></tr>
                        </tbody>
						
									</table>

									<!-- --------- Modal -------------- -->
									<div class="modal fade show" id="merchant_mng_view" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
							<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header webclr">
									        <h4 class="modal-title clrwhite heading" id="">Merchant Details</h4>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          <span aria-hidden="true">×</span>
									        </button>
									      </div>
										<div class="modal-body">
											<form>
														<div id="formbox">
														  <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for=""> Merchant Name </label>
														      <input type="text" class="form-control" id="" placeholder="Chris Wood">
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">Merchant Email</label>
														      <input type="text" class="form-control" id="" placeholder="example@gmail.com">
														    </div>
														  </div>
														   <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for="">Merchant Phone</label>
														      <input type="password" class="form-control" id="" placeholder="+1202-555-0123">
														    </div>
														  </div>
														   <div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputAddress">Street Address</label>
																	<input type="text" class="form-control" id="" placeholder="1234 Main St">
																</div>
																<div class="form-group col-md-6">
																	<label for="inputAddress2">City</label>
																	<input type="text" class="form-control" id="" placeholder="City Name">
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputCity">State</label>
																	<input type="text" class="form-control" id="" placeholder="New York">
																</div>
																<div class="form-group col-md-6">
																	<label for="inputState">Zip</label>
																	<input type="text" class="form-control" id="" placeholder="Enter Zip code">
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputZip">Country</label>
																	<input type="text" class="form-control" id="" placeholder="United States">
																</div>
															</div>

															<div class="modal-body font700">
																<div class="form-row">
																	<div class="col-lg-12">
																		<p><i class="align-middle fas fa-fw fa-dollar-sign"></i>Wallet Money: <span>$59.00</span></p>
																	</div>
																</div>
															</div>
													</div>
													</form>
										</div>
										
									</div>
								</div>

								</div>
						
						</div>
					</div>
							</div>
						</div>
					  </div>
				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->

<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/redmond/jquery-ui.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript">
	$("#from").datepicker({
	 //  minDate: 0,
	 //  onSelect: function(date) {		
		// $("#to").datepicker('option', 'minDate', date);	  
	 //  }
	});

	$("#to").datepicker({});
</script>
	
<script type="text/javascript">

	function clear_data(){
		document.getElementById("error_email").innerHTML= '';
		
		document.getElementById("merchant_name").value= '';
		document.getElementById("businessname").value= '';
		document.getElementById("merchant_email").value= '';
		document.getElementById("user_password").value= '';
		document.getElementById("merchant_address").value= '';
		document.getElementById("merchant_city").value= '';
		document.getElementById("merchant_state").value= '';
		document.getElementById("zip").value= '';
		document.getElementById("merchant_country").value= '';
		document.getElementById("merchant_country").value= '';
	}


	function delete_data(id){
		var result = confirm("Do you want to delete this entry? Click OK if yes");
		var tbl='tbl_merchants';
		var row='merchant_id';
		if (result) {
			$("#saved_card"+id+"").fadeOut(100);
	   		$.post("<?php echo base_url('Admin/delete') ?>",{id:id,tbl:tbl,row:row},function(res){
	   		location.reload();
	   		})
		}
	}
</script>

	<script>
		$(function() {
			
		$('.search').click(function(){
		var data = $("#form-data").serialize();
		//alert(data);
		//return false;
			$.ajax({
					 data: data,
					 type: "post",
					 url: "<?= base_url() ?>/Admin/get_merchant_details",
					 success: function (response) { 
        	      		var dataTable = $('#datatables-basic').DataTable();
                        dataTable.clear();
                        var obj = jQuery.parseJSON(response);
                        var obj1 = obj.data;
                        console.log(obj1);
                        if(obj1.length != 0)
                        {
                            $("#default").empty();
                            $("#searchdata").hide();
                            $("#default").show();
                            $.each(obj1, function(key,value) {
                            console.log(value);
                            dataTable.row.add(value).draw();
                            }); 
                        }else{
                            
                            $("#searchdata").show();
                            $("#default").hide();
                            
                        }
                    }    
            }); 
    });
	
	
			$("#datetimepicker-dashboard").datetimepicker({
				inline: true,
				sideBySide: false,
				format: "L"
			});
		});
	</script>
	<script>
		$(function() {
			// Line chart
			new Chart(document.getElementById("chartjs-dashboard-line"), {
				type: "line",
				data: {
					labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Sales ($)",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.primary,
						data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
					}, {
						label: "Orders",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.tertiary,
						borderDash: [4, 4],
						data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					tooltips: {
						intersect: false
					},
					hover: {
						intersect: true
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						xAxes: [{
							reverse: true,
							gridLines: {
								color: "rgba(0,0,0,0.05)"
							}
						}],
						yAxes: [{
							ticks: {
								stepSize: 500
							},
							display: true,
							borderDash: [5, 5],
							gridLines: {
								color: "rgba(0,0,0,0)",
								fontColor: "#fff"
							}
						}]
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			// Pie chart
			new Chart(document.getElementById("chartjs-dashboard-pie"), {
				type: "pie",
				data: {
					labels: ["Direct", "Affiliate", "E-mail", "Other"],
					datasets: [{
						data: [2602, 1253, 541, 1465],
						backgroundColor: [
							window.theme.primary,
							window.theme.warning,
							window.theme.danger,
							"#E8EAED"
						],
						borderColor: "transparent"
					}]
				},
				options: {
					responsive: !window.MSInputMethodContext,
					maintainAspectRatio: false,
					legend: {
						display: false
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-basic").DataTable({
				//responsive: true
				"aaSorting": []
			});
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>

</body>

</html>
<script type="text/javascript">
	function check_email(email){

		let email_pettern=/^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/;
         // let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
          if (email_pettern.test(email)) {
          	document.getElementById("error_email").innerHTML= '';
    		$('#add_user').prop('disabled', false);
           $.post("<?php echo base_url("Admin/check_email") ?>",{email:email},function(res){
			 	if (res) {		 	
			 		$('#add_user').prop('disabled', true);
			 		document.getElementById("error_email").innerHTML='Email Already Exists';
			 	}else{
			 		
			 		$('#add_user').prop('disabled', false);
			 		document.getElementById("error_email").innerHTML= '';
			 	}

		  })
    	}else{
    		document.getElementById("error_email").innerHTML= 'Please Enter Valid Email Format';
    		$('#add_user').prop('disabled', true);
    		
    	}	
		}
		
function validate1(key)
{
	//getting key code of pressed key
	var keycode = (key.which) ? key.which : key.keyCode;
	var phn = document.getElementById('txtPhn1');
	//comparing pressed keycodes
	if (!(keycode==8 || keycode==46)&&(keycode < 48 || keycode > 57))
	{
		return false;
	}
	else
	{
		//Condition to check textbox contains ten numbers or not
		if (phn.value.length <5)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
		
	function validateForm1(){
		var email = document.forms["myForm1"]["merchant_email"].value;
		var password= document.forms["myForm1"]["user_password"].value;
		let email_pettern=/^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/;
         let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
          if (email_pettern.test(email)) {
          	if (regex.test(password)) {
          		return true;            
           }else{
            document.getElementById("error_email").innerHTML= 'Password must be 8 digit long including alpha-numeric-lower_case-upper_case-special_char';
            return false;
        }
          	
    	}else{
    		document.getElementById("error_email").innerHTML= 'Please Enter Valid Email Format';
    		return false;
    		
    	}
			
			
		}

		// function check_email(email){
  //          $.post("<?php echo base_url("Admin/check_email") ?>",{email:email},function(res){
		// 	 	if (res) {		 	
		// 	 		$('#add_merchant').prop('disabled', true);
		// 	 		document.getElementById("error_email").innerHTML='Email Already Exists';
		// 	 	}else{
			 		
		// 	 		$('#add_merchant').prop('disabled', false);
		// 	 		document.getElementById("error_email").innerHTML= '';
		// 	 	}

		//   })

		// }

		window.onload = function() {
  document.getElementById('merchant_email').value= '';
  document.getElementById('user_password').value= '';
  
};
	</script>