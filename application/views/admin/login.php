<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Bootstrap 4 Admin &amp; Dashboard Template">
	<meta name="author" content="Bootlab">
	<title>Caenex</title>
	<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin="">
	<style>
		body {
			opacity: 0;
		}
	</style>
	<script src="<?php echo base_url('assets/js/settings.js')?>"></script>
	<!-- END SETTINGS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-120946860-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120946860-6');
</script></head>

<body>
	<main class="main d-flex w-100 conexbg">
		<div class="container d-flex flex-column">
			<div class="corner"><img src="<?php echo base_url('assets/img/corner.png') ?>" alt="" class="img-fluid"></div>
			<div class="corner_logo"><img src="<?php echo base_url('assets/img/corner_logo.png')?>" alt="" class="img-fluid"></div>
			<div class="row h-100 mt-small">
				<div class="col-sm-10 col-md-8 col-lg-7 mx-auto d-table h-100">

						<div class="text-center mt-4">
							<h1 class="h2 loghed">Admin Login</h1>
						</div>

						<div class="card">
							<div class="card-body">
									<div id="loginpage">
										<form method="post" autocomplete="off">
											<div class="form-group">
												<input class="form-control form-control-lg" type="email" autocomplete="off" pattern=".+@.+.com" autocomplete="new-password" autocapitalize="off" autocorrect="off" name="email" id="email" onload="password_value()" readonly onfocus="this.removeAttribute('readonly');" placeholder="Enter your email" required="required">
											</div>
											<div class="form-group">
												<!-- <input onload="password_value()" class="form-control form-control-lg" type="password" autocomplete="off" autocomplete="new-password" autocapitalize="off" autocorrect="off" name="password" id="password" placeholder="Enter your password" required="required"> -->
												<input class="form-control form-control-lg" type="password" name="password" placeholder="Enter your password" required="required" readonly onfocus="this.removeAttribute('readonly');" onload="password_value()" id="password">
												<!-- <div class="forgetp text-right"><small><a href="<?php echo base_url('Forgot'); ?>">Forgot password?</a></small></div> -->
											</div>
											<div class="text-center mt-3">
												<!-- <a href="#" class="btn btn-lg btn-primary logbtn"> Login</a> -->
												<button type="submit" name="submit" class="btn btn-lg btn-primary logbtn">Sign in</button>
											</div>
										
										</form>
									</div>
							</div>
							<p style="text-align: center;color: red;font-size: 20px;">
								<?php
								if ($this->session->flashdata('loginerror')) {
									echo $this->session->flashdata('loginerror');
								}
								?>
							</p>
							<p style="text-align: center;color:green;font-size: 20px;">
								<?php
								if ($this->session->flashdata('corrct')) {
									echo $this->session->flashdata('corrct');
								}
								?>
							</p>
						</div>
						
				</div>
			</div>
		</div>
	</main>

	<script src="<?php echo base_url('assets/js/app.js') ?>"></script>

</body>

</html>
<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/redmond/jquery-ui.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript">
/* $(document).ready(function(){
 $("#loginForm").submit(function(e){
        e.preventDefault();
		if(document.getElementById('email').value== '')
		{
			$("#email").focus();
			alert("Email Should Not Be Empty");
			
		}else if(document.getElementById('password').value== '')
		{
			$("#password").focus();
			alert("Password Should Not Be Empty");
			
		}else{
				//var form$ = $("#loginForm");
				//submit form to the server
				$('#loginForm').submit();
				//form$.get(0).submit();
		}
    });
	}); */
	function password_value(){
			document.getElementById('password').value= '';
			document.getElementById('email').value= '';
			
		}
</script>