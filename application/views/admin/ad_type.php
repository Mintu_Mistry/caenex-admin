<?php
$this->load->view('admin/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('admin/include/nev');
?>

<div class="main">
<?php
$this->load->view('admin/include/header');
?>
<style type="text/css">
	#datatables-basic_wrapper{
		margin-top: 11px;
	}
	.dataTables_length{
		margin-left: 10px;
	}
	#datatables-basic_filter{
		margin-right: 10px;
	}
	.dataTables_info{
		margin-left: 10px;
		margin-bottom: 10px;
	}
	#datatables-basic_paginate{
		margin-right: 10px;
	}
</style>
<main class="content">
<div class="container-fluid p-0">
<div class="row mb-2">
<div class="col-12 col-lg-6 mobcenter">
<h3> Ad type Management </h3>
</div>
<div class="col-12 col-lg-6">
<div class="rowbtn text-right mobcenter">
<div class="btn import_btn" style="background: linear-gradient(to right, #e89e2b, #eea73b, #f4b149, #f9ba57, #ffc365);"data-toggle="modal" data-target="#sizedModalSm"><i class="align-middle mr-2 fas fa-fw fa-plus"></i> Add a new Ad type </div>
</div>



<!--------------- Modal ---------------->
<div class="modal fade show" id="sizedModalSm"  tabindex="-1" role="dialog" aria-modal="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header" style="background: linear-gradient(to right, #e89e2b, #eea73b, #f4b149, #f9ba57, #ffc365);border-color: #e89e2b;">
<h3 class="modal-title"> Add a new Ad type</h3>
<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clear_data()">
<span aria-hidden="true">×</span>
</button>
</div>
<b>
<p id="error_email" style="color: red;text-align: center;font-size: 20px;margin-bottom: -5px;"></p></b>
<div class="modal-body">
<form method="post" autocomplete="off" action="<?php echo base_url('Admin/add_ad_type') ?>">
<div id="formbox">
<div class="form-row">
<div class="form-group col-md-6">
<label for=""> Ad type Name </label>
<input type="text" class="form-control" name="name" id="name" placeholder="Ad type" required="">
</div>

<div class="form-group col-md-6">
<label for=""> Ad type Status </label>

<select class="form-control" name="status">
	<option value="1">Active</option>
	<option value="0">Inactive</option>
</select>
</div>

</div>



<div class="form-group text-center">
<button style="background: linear-gradient(to right, #e89e2b, #eea73b, #f4b149, #f9ba57, #ffc365);border-color: #e89e2b;" type="submit" name="submit" id="add_user" class="btn btn-primary center-block btn-lg addstu_btn"> Submit </button>
</div>
</div>
</form>
</div>

</div>
</div>
</div>

</div>
</div>
<?php
if ($this->session->flashdata('add_user')) {
?>
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
<div class="alert-message">
	<strong>Success!</strong> <?php echo $this->session->flashdata('add_user'); ?>
</div>
</div>
<?php
}
?>

<?php
if ($this->session->flashdata('user_error')) {
?>
<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
<div class="alert-message">
<strong>Error</strong> <?php echo $this->session->flashdata('user_error'); ?>
</div>
</div>
<?php
}
?>
<div class="row">
<div class="col-12 col-lg-12">
<div class="card">
<div class="row">
<div class="col-12 col-lg-12 mt-minus">
<div class="table-responsive  border">
<table class="table mb-0" id="datatables-basic">
<thead>
<tr style="background: linear-gradient(to right, #e89e2b, #eea73b, #f4b149, #f9ba57, #ffc365);">
<th scope="col"> S.No </th>
<th scope="col" style="">Ad type Name</th>
<th scope="col">Status</th>
<th scope="col" class="action">Action</th>
</tr>
</thead>
<tbody>

<?php
$count=0;
foreach ($ad_type as $key => $value) {
$count++;	
?>
<tr id="<?php echo $value['id']; ?>">
<th scope="row"><?php echo $count; ?></th>
<td><?php echo $value['ad_type'] ?></td>

<td class="view">
	<?php 
	if ($value['status']==1) {		
	?>
	<span class="badge badge-success">Active</span>
	<?php
	}else{
	?>
	<span class="badge badge-warning">Inactive</span>
	<?php		
	}
	?>
</td>
<td><a href="javascript:void(0)" data-toggle="modal" data-target="#user_mng_view<?php echo $count ?>"><i class="align-middle mr-2 fas fa-fw fa-edit"></i></a>
<a href="javascript:void(0)" onclick="delete_data(<?php echo $value['id']; ?>)"><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>
<!-- <a href="" data-toggle="modal" data-target="#user_mng_view1<?php echo $count ?>"><i class=" align-middle fas fa-eye"></i></a> -->
</td>

</tr>



<div class="modal fade show" id="user_mng_view<?php echo $count ?>"  tabindex="-1" role="dialog" aria-modal="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header" style="background: linear-gradient(to right, #e89e2b, #eea73b, #f4b149, #f9ba57, #ffc365);border-color: #e89e2b;">
<h3 class="modal-title"> Update Ad type</h3>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
</div>
<b>
<p id="error_email" style="color: red;text-align: center;font-size: 20px;margin-bottom: -5px;"></p></b>
<div class="modal-body">
<form method="post" autocomplete="off" action="<?php echo base_url('Admin/add_ad_type') ?>">
<div id="formbox">
	<input type="hidden" name="cat_id" value="<?php echo $value['id'] ?>">


<div class="form-row">
<div class="form-group col-md-6">
<label for="inputZip">Ad type Name</label>
<input type="text" class="form-control" name="name" id="" placeholder="Ad type" value="<?php echo $value['ad_type'] ?>" required>
</div>

<div class="form-group col-md-6">
<label for="inputZip">Ad type Status</label>
<select class="form-control" name="status">
<option <?php if($value['status']==1){ echo "selected"; } ?> value="1">Active</option>
<option <?php if($value['status']==0){ echo "selected"; } ?> value="0">Inactive</option>
</select>
</div>
</div>

<div class="form-group text-center">
<button style="background: linear-gradient(to right, #e89e2b, #eea73b, #f4b149, #f9ba57, #ffc365);border-color: #e89e2b;" type="submit" name="submit" id="add_user" class="btn btn-primary center-block btn-lg addstu_btn"> Update </button>
</div>
</div>
</form>
</div>

</div>
</div>
</div>

<!--------------- Modal ---------------->


<?php
}
?>



</tbody>
</table>


</div>
<!-------- End Modal -------->
<!------------------------------  Start Change Booth modal box ------------------------->
<div class="modal fade" data-keyboard="false"  id="change_both" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title"> Change Booth </h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body">
<form>
<div id="formbox">
<div class="form-row">
<div class="form-group col-md-6">
<label for="">Booth 1</label>
<select id="inputState" class="form-control">
<option selected="">Select</option>
<option>...</option>
</select>
</div>
<div class="form-group col-md-6">
<label for="">Booth 2</label>
<select id="inputState" class="form-control">
<option selected="">Select</option>
<option>...</option>
</select>
</div>
</div>
<div class="form-row">
<div class="form-group col-md-6">
<label for="">Time Assigned</label>
<input type="text" class="form-control" id="" placeholder="9:00AM">
</div>
<div class="form-group col-md-6">
<label for="">Time Assigned</label>
<input type="text" class="form-control" id="" placeholder="9:00AM">
</div>
</div>
<div class="form-group text-center">
<button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Update </button>
</div>
</div>
</form>
</div>

</div>
</div>
</div>
<!-------------- End Modal box ------------------>

</div>

</div>
</div>
</div>
</div>
</div>
</div>
</main>

<!-- <footer class="footer"></footer> -->
</div>
</div>
<!-- <script src="js\settings.js"></script> -->
<script src="js\app.js"></script>

<script>
$(function() {
$("#datetimepicker-dashboard").datetimepicker({
inline: true,
sideBySide: false,
format: "L"
});
});
</script>
<script>
$(function() {
// Line chart
new Chart(document.getElementById("chartjs-dashboard-line"), {
type: "line",
data: {
labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
datasets: [{
label: "Sales ($)",
fill: true,
backgroundColor: "transparent",
borderColor: window.theme.primary,
data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
}, {
label: "Orders",
fill: true,
backgroundColor: "transparent",
borderColor: window.theme.tertiary,
borderDash: [4, 4],
data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
}]
},
options: {
maintainAspectRatio: false,
legend: {
display: false
},
tooltips: {
intersect: false
},
hover: {
intersect: true
},
plugins: {
filler: {
propagate: false
}
},
scales: {
xAxes: [{
reverse: true,
gridLines: {
color: "rgba(0,0,0,0.05)"
}
}],
yAxes: [{
ticks: {
stepSize: 500
},
display: true,
borderDash: [5, 5],
gridLines: {
color: "rgba(0,0,0,0)",
fontColor: "#fff"
}
}]
}
}
});
});
</script>
<script>
$(function() {
// Pie chart
new Chart(document.getElementById("chartjs-dashboard-pie"), {
type: "pie",
data: {
labels: ["Direct", "Affiliate", "E-mail", "Other"],
datasets: [{
data: [2602, 1253, 541, 1465],
backgroundColor: [
window.theme.primary,
window.theme.warning,
window.theme.danger,
"#E8EAED"
],
borderColor: "transparent"
}]
},
options: {
responsive: !window.MSInputMethodContext,
maintainAspectRatio: false,
legend: {
display: false
}
}
});
});
</script>
<script>
$(function() {
$("#datatables-dashboard-projects").DataTable({
pageLength: 6,
lengthChange: false,
bFilter: false,
autoWidth: false
});
});
</script>

<script type="text/javascript">
	function clear_data(){
		document.getElementById("error_email").innerHTML= '';
		
		document.getElementById("name").value= '';
		document.getElementById("email").value= '';
		document.getElementById("passowrd").value= '';
		document.getElementById("phone").value= '';
		document.getElementById("street").value= '';
		document.getElementById("zip").value= '';
		document.getElementById("city").value= '';
		document.getElementById("state").value= '';
		document.getElementById("country").value= '';
	}
	function check_email(email){

		let email_pettern=/^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/;
         // let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
          if (email_pettern.test(email)) {
          	document.getElementById("error_email").innerHTML= '';
    		$('#add_user').prop('disabled', false);
           $.post("<?php echo base_url("Admin/check_email") ?>",{email:email},function(res){
			 	if (res) {		 	
			 		$('#add_user').prop('disabled', true);
			 		document.getElementById("error_email").innerHTML='Email Already Exists';
			 	}else{
			 		
			 		$('#add_user').prop('disabled', false);
			 		document.getElementById("error_email").innerHTML= '';
			 	}

		  })
    	}else{
    		document.getElementById("error_email").innerHTML= 'Please Enter Valid Email Format';
    		$('#add_user').prop('disabled', true);
    		
    	}
			
			
			
		}


	function delete_data(id){

		var result = confirm("Do you want to delete this entry? Click OK if yes");
		var tbl='tbl_ad_type';
		var row='id';
		if (result) {
			//$("#saved_card"+id+"").fadeOut(100);
	   		$.post("<?php echo base_url('Admin/delete') ?>",{id:id,tbl:tbl,row:row},function(res){
	   		location.reload();
	   		})
		}
	}
</script>
<script>
		$(function() {
			$("#datatables-basic").DataTable({
				//responsive: true

			});
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>

</body>

</html>

<script type="text/javascript">
	function validateForm1(){
		var email = document.forms["myForm1"]["email"].value;
		var password= document.forms["myForm1"]["passowrd"].value;
		let email_pettern=/^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/;
         let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
          if (email_pettern.test(email)) {
          	if (regex.test(password)) {
          		return true;            
           }else{
            document.getElementById("error_email").innerHTML= 'Password must be 8 digit long including alpha-numeric-lower_case-upper_case-special_char';
            return false;
        }
          	
    	}else{
    		document.getElementById("error_email").innerHTML= 'Please Enter Valid Email Format';
    		return false;
    		
    	}
			
			
		}

		
	</script>
	<script type="text/javascript">
		function autoMask(field, event, sMask) {
    var KeyTyped = String.fromCharCode(getKeyCode(event));
    if (getKeyCode(event) == 8) {
        return
    }
    if (getKeyCode(event) == 0) {
        return
    }
    if (field.value.length == sMask.length && getKeyCode(event) == 13) {
        return true
    }
    // if (field.value.length == sMask.length && getKeyCode(event) != 13) {
    //     field.value = ""
    // }
    var targ = getTarget(event);
    keyCount = targ.value.length;
    if (keyCount == sMask.length) {
        return false
    }
    if (sMask.charAt(keyCount + 1) != "#" && sMask.charAt(keyCount + 1) != "A") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return false
    }
    if (sMask.charAt(keyCount) == "*")
        return true;
    if (sMask.charAt(keyCount) == KeyTyped) {
        return true
    }
    if (sMask.charAt(keyCount) == "#" && isNumeric(KeyTyped))
        return true;
    if (sMask.charAt(keyCount) == "A" && isAlpha(KeyTyped))
        return true;
    if (sMask.charAt(keyCount + 1) == "?") {
        field.value = field.value + KeyTyped + sMask.charAt(keyCount + 1);
        return true
    }
    if (KeyTyped.charCodeAt(0) < 32)
        return true;
    return false
}

function getKeyCode(e) {
    if (e.srcElement) {
        return e.keyCode
    }
    if (e.target) {
        return e.which
    }
}

function getTarget(e) {
    if (e.srcElement) {
        return e.srcElement
    }
    if (e.target) {
        return e.target
    }
}

function isNumeric(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

function isAlpha(c) {
    var sNumbers = "01234567890-";
    if (sNumbers.indexOf(c) == -1)
        return false;
    else
        return true
}

// function delete_data(id,tbl,row){
// 	var result = confirm("Are You Sure?");
// 	if (result) {
//    	$.post("<?php echo base_url('merchant/delete') ?>",{id:id,tbl:tbl,row:row},function(res){
//    		$("#saved_card"+id+"").fadeOut();
//    	})
// 	}
// }

window.onload = function() {
  document.getElementById('password').value= '';
  document.getElementById('email').value= '';
  
};


</script>