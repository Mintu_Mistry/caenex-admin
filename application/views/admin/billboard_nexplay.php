<?php
$this->load->view('admin/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('admin/include/nev');
?>

<div class="main">
<?php
$this->load->view('admin/include/header');
?>
<style type="text/css">
	#datatables-basic_wrapper{
		margin-top: 11px;
	}
	.dataTables_length{
		margin-left: 10px;
	}
	#datatables-basic_filter{
		margin-right: 10px;
	}
	.dataTables_info{
		margin-left: 10px;
		margin-bottom: 10px;
	}
	#datatables-basic_paginate{
		margin-right: 10px;
	}
	#datatables-basic1_wrapper{
		margin-top: 10px;
	}
</style>
			<main class="content">
				<div class="container-fluid p-0">
					<div class="row">
						<div class="col-12 col-lg-12 mb-3">
					    	<h3>  Billboard Management  </h3>
					    </div>

						<div class="col-12 col-lg-12">
							<div class="tab">
								<ul class="nav nav-tabs nav-justified mb-2" role="tablist">
									<li class="nav-item"><a class="nav-link" href="#tab-2" data-toggle="tab" role="tab" aria-selected="true"> NexWorld </a></li>
									<li class="nav-item"><a class="nav-link newclr active" href="#tab-1" data-toggle="tab" role="tab" aria-selected="false"> NexPlay </a></li>
									
								</ul>

								<?php
									if ($this->session->flashdata('add_user')) {
									?>
									<div class="alert alert-success alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
									<div class="alert-message">
										<strong>Success!</strong> <?php echo $this->session->flashdata('add_user'); ?>
									</div>
									</div>
									<?php
									}
									?>
								<div class="tab-content pad0 mt2">
									<div class="tab-pane active" id="tab-1" role="tabpanel">
										<!-- <h4 class="tab-title">Default tabs</h4> -->
									
							<div class="card">
								<!-- <div class="form-group col-lg-4 align-self-end mt-2">
									<div class="input-group">
										<input type="text" class="form-control" style="padding: 1.2rem .7rem;" placeholder="Search...">
										<span class="input-group-append">
						                  <button class="btn btn-info" type="button">Go!</button>
						                </span>
									</div>
								</div> -->
								<div class="table-responsive">
								<table class="table table-bordered my-0" id="datatables-basic1">
									<thead class="coltab">
										<tr style="background: #ffa000;">
											<th> S.No. </th>
											<!-- <th> Merchant ID </th> -->
											<th> Merchant Name </th>
											<th> Game Type </th>
											<th> Ad Type </th>
											<th> No. of Impressions Left </th>
											<th> Video Clicked </th>
											<th> Video Full Streamed </th>
											<th> Payout </th>
											<th> Status </th>
											<th> Action </th>
										</tr>
									</thead>
									<tbody>										
											<?php
											$count1=0;
											foreach ($nexplay as $key => $value) {
												$count1++;
											?>										
										<tr>	
											<td class="d-none d-xl-table-cell"><?php echo $count1 ?></td>
											<td class="d-none d-xl-table-cell"><?php echo $value['merchant_name'] ?></td>
											<td class="d-none d-xl-table-cell"><?php
											if (@$value['type']==1) {
												echo "Coupon";
											}elseif (@$value['type']==2) {
												echo "Dollar Amount";

											}elseif(@$value['game_category']){
												echo "Custom Game";
												$tbl='tbl_nexplay_custom_game';
											}else{
												echo "Caenex Game";
												$tbl='tbl_nexplay_caenex_game';
											}
											?>
												
											</td>
											<td class="d-none d-xl-table-cell"><?php echo $value['ad_type']; ?></td>
											<td class="d-none d-xl-table-cell"><?=$value['total_number_of_impressions']?>/<?php echo $value['total_number_of_impressions']-($value['total_win']+$value['total_loose']) ?></td>
											<td class="d-none d-xl-table-cell"><?php echo $value['video_clicked']; ?></td>
											<td class="d-none d-xl-table-cell"><?php echo $value['video_full_streamed']; ?></td>
											<td class="d-none d-xl-table-cell">$<?php echo $value['total_payout']; ?></td>
											
											<!--<td>
												<?php
												if ($value['status']==1) {
												?>
												<span class="badge badge-primary" >Active</span>
												<?php
												}else{
												?>
												<span class="badge badge-warning" >Inactive</span>
												<?php
												}
												?>
											</td>-->
											<!--<td class="cntr">
												<label class="checkcon custom-checkbox mr-2">
												<?php
												if ($value['status']==1) {
												?>
												<input type="checkbox" class="custom-control-input" checked="" onclick="change_status(<?php echo $value['id'] ?>,'<?php echo $tbl ?>','0')">
												<?php
												}else{
												?>
												<input type="checkbox" class="custom-control-input" onclick="change_status(<?php echo $value['id'] ?>,'<?php echo $tbl ?>','1')">
												<?php
												}
												?>
												<span class="custom-control-label" style="vertical-align: baseline;"></span>
												</label>
												<a href="javascript:void('')"onclick="delete_data(<?php echo $value['id']; ?>,'<?php echo $tbl ?>','<?php echo $count1 ?>')"><i class="align-middle far fa-fw fa-trash-alt"></i></a>
											</td>-->
											<td class="Pending">
													<?php
													if ($value['add_status']==0) {				
													?>
													<span class="badge badge-warning">Pending</span>
													<?php
													}
													?>

													<?php
													if ($value['add_status']==2) {				
													?>
													<span class="badge badge-danger"> Denied </span>
													<?php
													}
													?>

													<?php
													if ($value['add_status']==1) {				
													?>
													<span class="badge badge-success">Accepted</span>
													<?php
													}
													?>
												</td>
												
												<td class="icon_action">
													<?php
													if ($value['add_status']==0) {				
													?>
											           <a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'];?>,'<?php echo $tbl ?>','1')"><i style="color: green;"  class="fa fa-check-square"></i></a>
											              
													<a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'];?>,'<?php echo $tbl ?>','2')"><i style="margin-left: 12px;" class="fas fa-times"></i></a>
													<?php
													}
													?>
													<?php
													if ($value['add_status']==2) {				
													?>
													 <a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'];?>,'<?php echo $tbl ?>','1')"><i style="color: green;"  class="fa fa-check-square"></i></a>
													<?php
													}
													?>
													<?php
													if ($value['add_status']==1) {				
													?>
													<a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'];?>,'<?php echo $tbl ?>','2')"><i  class="fas fa-times"></i></a>
													<?php
													}
													?>
												</td>
										</tr>
										<?php
										}	
										?>
										
									</tbody>
								</table>
							</div>
							</div>
						
									
									</div>
									<div class="tab-pane" id="tab-2" role="tabpanel">
										<div class="card">
										<div class="form-group col-lg-4 align-self-end mt-2">
									<!-- <div class="input-group">
										<input type="text" class="form-control" placeholder="Search...">
										<span class="input-group-append">
						                  <button class="btn btn-info" type="button">Go!</button>
						                </span>
									</div> -->
								</div>
								<div class="table-responsive">
								<table class="table table-bordered my-0" id="datatables-basic">
									<thead class="coltab">
										<tr style="background: linear-gradient(to right, #3a8e2e, #4f9d33, #64ad38, #7abc3c, #90cc41);">
											<th> S.No. </th>
											
											<th> Merchant Name </th>
											<th> Campaign Logo </th>
											<th> Reward Type </th>
											<!--<th> Coupon Code </th>-->
											<th> Location </th>
											<th> Start date </th>
											<th> End date </th>
											<th> Status </th>
											<th> Action </th>
										</tr>
									</thead>
									<tbody>
										<?php
										$count=0;

										foreach ($nexworld as $key => $value) {
											if ($value['type']==2) {
										$count++;											
										?>
										<tr id="<?php echo $count ?>">	
											<td class="d-none d-xl-table-cell"><?php echo $count ?></td>
											
											<td class="d-none d-xl-table-cell"><?php echo $value['merchant_name'] ?></td>
											<td class="d-none d-xl-table-cell">
												<?php
													if (@$value['coupon_image']) {
														$arr = explode(".",$value['coupon_image']);
														$extension = end($arr);
														if ($extension =='mp4')
														{ ?>
															<video alt="campaign video" class="img-responsive" width="100" height="100" controls>
														  <source src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" type="video/mp4" style="height: 60px;width: 80px;">
														  <source src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" type="video/ogg" style="height: 60px;width: 80px;">
														  Your browser does not support the video tag.
														</video>
															<?php 
														}else{ ?>
															<img src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" alt="campaign logo" class="img-responsive" style="height: 60px;width: 80px;">
															<!-- <video src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" alt="campaign video" class="img-responsive"> -->
																<?php

														}

														?>
														
														<?php
													}else{
														?>
													<img src="<?php echo base_url('assets/img/icons8-us-dollar-64.png')?>" alt="campaign logo" class="img-responsive">	
													<?php
													}
													?>
											</td>
											<td class="d-none d-xl-table-cell">
											<?php
											if ($value['type']==1) {
												echo "Coupon";
												$tbl='tbl_nexworld_coupon';
											}elseif ($value['type']==2) {
												echo "Dollar Amount";
												$tbl='tbl_nexworld_cash';
											}
											?>
												
											</td>
											<!--<td class="d-none d-xl-table-cell">
											<?php
											if (@$value['coupon_code']) {
												echo $value['coupon_code'];
											}else{
												echo "-";
											}
											?>
											</td>-->
											<td class="d-none d-xl-table-cell">
											<?php echo $value['location'] ?>
												
											</td>
											<td class="d-none d-xl-table-cell">
											<?php echo date('m-d-y', strtotime($value['start_date'])); ?>
												
											</td>
											<td class="d-none d-xl-table-cell">
											<?php echo date('m-d-y', strtotime($value['end_date'])); ?>
												
											</td>
											<!--<td style="text-align: center">
												<?php
												if ($value['status']==1) {
												?>
												<span class="badge badge-primary" >Active</span>
												<?php
												}else{
												?>
												<span class="badge badge-warning" >Inactive</span>
												<?php
												}
												?>
											</td>-->
											<!--<td class="cntr">
												<label class="checkcon custom-checkbox mr-2">
												<?php
												if ($value['status']==1) {
												?>
												<input type="checkbox" class="custom-control-input" checked="" onclick="change_status(<?php echo $value['id'] ?>,'<?php echo $tbl ?>','0')">
												<?php
												}else{
												?>
												<input type="checkbox" class="custom-control-input" onclick="change_status(<?php echo $value['id'] ?>,'<?php echo $tbl ?>','1')">
												<?php
												}
												?>
												<span class="custom-control-label" style="vertical-align: baseline;"></span>
												</label>
												<a href="javascript:void('')"onclick="delete_data(<?php echo $value['id']; ?>,'<?php echo $tbl ?>','<?php echo $count ?>')"><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												</td>-->
												
												<td class="Pending">
													<?php
													if ($value['add_status']==0) {				
													?>
													<span class="badge badge-warning">Pending</span>
													<?php
													}
													?>

													<?php
													if ($value['add_status']==2) {				
													?>
													<span class="badge badge-danger"> Denied </span>
													<?php
													}
													?>

													<?php
													if ($value['add_status']==1) {				
													?>
													<span class="badge badge-success">Accepted</span>
													<?php
													}
													?>
												</td>
												
												<td class="icon_action">
													<?php
													if ($value['add_status']==0) {				
													?>
											           <a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'] ?>,'<?php echo $tbl ?>','1')"><i style="color: green;"  class="fa fa-check-square"></i></a>
											              
													<a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'];?>,'<?php echo $tbl ?>','2')"><i style="margin-left: 12px;" class="fas fa-times"></i></a>
													<?php
													}
													?>
													<?php
													if ($value['add_status']==2) {				
													?>
													 <a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'];?>,'<?php echo $tbl ?>','1')"><i style="color: green;"  class="fa fa-check-square"></i></a>
													<?php
													}
													?>
													<?php
													if ($value['add_status']==1) {				
													?>
													<a href="javascript:void(0)" onclick="change_status(<?php echo $value['id'];?>,'<?php echo $tbl ?>','2')"><i  class="fas fa-times"></i></a>
													<?php
													}
													?>
												</td>
										</tr>

										<?php
										}}
										?>
										
									</tbody>
								</table>
							</div>
							</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
<!-- ------  End Tab data --------- -->

				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script type="text/javascript">
		function change_status(id,tbl,status){
			$.post("<?php echo base_url('Admin/change_merchant_status') ?>",{id:id,tbl:tbl,status:status},function(res){
	   			if(res =="Nexworld")
				{
					window.location.replace("<?php echo base_url('Admin/billboard_management') ?>")
				}else if(res =="Nexplay"){
					location.reload;
				}
	   		})
		}

		function delete_data(id,tbl,count){

		var result = confirm("Do you want to delete this entry? Click OK if yes");
		//var tbl='tbl_user';
		var row='id';
		if (result) {
			$("#"+count+"").fadeOut(100);
	   		$.post("<?php echo base_url('Admin/delete') ?>",{id:id,tbl:tbl,row:row},function(res){
				location.reload;
	   		})
		}
	}
	</script>

	<script>
		$(function() {
			$("#datetimepicker-dashboard").datetimepicker({
				inline: true,
				sideBySide: false,
				format: "L"
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-basic").DataTable({
				//responsive: true
				"aaSorting": []
			});
			$("#datatables-basic1").DataTable({
				//responsive: true
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>

</body>

</html>
