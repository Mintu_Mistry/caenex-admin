<?php
$this->load->view('admin/include/head');
?>

<body>
<div class="wrapper">
<?php
$this->load->view('admin/include/nev');
?>

<div class="main">
<?php
$this->load->view('admin/include/header');
?>
<style type="text/css">
	#datatables-basic_wrapper{
		margin-top: 11px;
	}
	.dataTables_length{
		margin-left: 10px;
	}
	#datatables-basic_filter{
		margin-right: 10px;
	}
	.dataTables_info{
		margin-left: 10px;
		margin-bottom: 10px;
	}
	#datatables-basic_paginate{
		margin-right: 10px;
	}
	#datatables-basic1_wrapper{
		margin-top: 10px;
	}
</style>
			<main class="content">
				<div class="container-fluid p-0">
					<div class="row">
						<div class="col-12 col-lg-12 mb-3">
					    	<h3>  Billboard Management  </h3>
					    </div>

						<div class="col-12 col-lg-12">
							<div class="tab">
								<ul class="nav nav-tabs nav-justified mb-2" role="tablist">
									<li class="nav-item"><a class="nav-link active" href="#tab-2" data-toggle="tab" role="tab" aria-selected="true"> NexWorld </a></li>
									<li class="nav-item"><a class="nav-link newclr" href="#tab-1" data-toggle="tab" role="tab" aria-selected="false"> NexPlay </a></li>
									
								</ul>

								<?php
if ($this->session->flashdata('add_user')) {
?>
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
<div class="alert-message">
	<strong>Success!</strong> <?php echo $this->session->flashdata('add_user'); ?>
</div>
</div>
<?php
}
?>
								<div class="tab-content pad0 mt2">
									<div class="tab-pane" id="tab-1" role="tabpanel">
										<!-- <h4 class="tab-title">Default tabs</h4> -->
									
							<div class="card">
								<!-- <div class="form-group col-lg-4 align-self-end mt-2">
									<div class="input-group">
										<input type="text" class="form-control" style="padding: 1.2rem .7rem;" placeholder="Search...">
										<span class="input-group-append">
						                  <button class="btn btn-info" type="button">Go!</button>
						                </span>
									</div>
								</div> -->
								<div class="table-responsive">
								<table class="table table-bordered my-0" id="datatables-basic1">
									<thead class="coltab">
										<tr style="background: #ffa000;">
											<th> S.No. </th>
											<!-- <th> Merchant ID </th> -->
											<th> Merchant Name </th>
											<th> Game Type </th>
											<th> Ad Type </th>
											<th> No. of Impressions Left </th>
											<th> Video Clicked </th>
											<th> Video Full Streamed </th>
											<th>Winner</th>
											<th>Loser </th>
											<th> Payout </th>
											<th> Status </th>
											<th> Action </th>
										</tr>
									</thead>
									<tbody>										
											<?php
											$count1=0;
											foreach ($nexplay as $key => $value) {
												$count1++;
											?>										
										<tr>	
											<td class="d-none d-xl-table-cell"><?php echo $count1 ?></td>
											<td class="d-none d-xl-table-cell"><?php echo $value['merchant_name'] ?></td>
											<td class="d-none d-xl-table-cell"><?php
											if (@$value['type']==1) {
												echo "Coupon";
											}elseif (@$value['type']==2) {
												echo "Dollar Amount";

											}elseif(@$value['game_category']){
												echo "Custom Game";
												$tbl='tbl_nexplay_custom_game';
											}else{
												echo "Caenex Game";
												$tbl='tbl_nexplay_caenex_game';
											}
											?>
												
											</td>
											<td class="d-none d-xl-table-cell"><?php echo $value['ad_type'] ?></td>
											<td class="d-none d-xl-table-cell"><?php echo rand('123','3')+100; ?></td>
											<td class="d-none d-xl-table-cell"><?php echo rand('123','3')+100; ?></td>
											<td class="d-none d-xl-table-cell"><?php echo rand('12','3'); ?></td>

											<td class="d-none d-xl-table-cell"><?php echo rand('125','3'); ?></td>

											<td class="d-none d-xl-table-cell"><?php echo rand('129','3'); ?></td>
											<td class="d-none d-xl-table-cell">2000</td>
											
											<td>
												<?php
												if ($value['status']==1) {
												?>
												<span class="badge badge-primary" >Active</span>
												<?php
												}else{
												?>
												<span class="badge badge-warning" >Inactive</span>
												<?php
												}
												?>
											</td>
											<td class="cntr">
												<label class="checkcon custom-checkbox mr-2">
												<?php
												if ($value['status']==1) {
												?>
												<input type="checkbox" class="custom-control-input" checked="" onclick="change_status(<?php echo $value['id'] ?>,'<?php echo $tbl ?>','0')">
												<?php
												}else{
												?>
												<input type="checkbox" class="custom-control-input" onclick="change_status(<?php echo $value['id'] ?>,'<?php echo $tbl ?>','1')">
												<?php
												}
												?>
												<span class="custom-control-label" style="vertical-align: baseline;"></span>
												</label>
												<a href="javascript:void('')"onclick="delete_data(<?php echo $value['id']; ?>,'<?php echo $tbl ?>','<?php echo $count1 ?>')"><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												
												<a target="_blank" href="<?php echo base_url('game_details/'). $value['id'] ?>"><i class="align-middle fas fa-eye"></i></a>
												
											</td>
										</tr>
										<?php
										}	
										?>
										
									</tbody>
								</table>
							</div>
							</div>
						
									
									</div>
									<div class="tab-pane active" id="tab-2" role="tabpanel">
										<div class="card">
										<div class="form-group col-lg-4 align-self-end mt-2">
									<!-- <div class="input-group">
										<input type="text" class="form-control" placeholder="Search...">
										<span class="input-group-append">
						                  <button class="btn btn-info" type="button">Go!</button>
						                </span>
									</div> -->
								</div>
								<div class="table-responsive">
								<table class="table table-bordered my-0" id="datatables-basic">
									<thead class="coltab">
										<tr style="background: linear-gradient(to right, #3a8e2e, #4f9d33, #64ad38, #7abc3c, #90cc41);">
											<th> S.No. </th>
											
											<th> Merchant Name </th>
											<th> Campaign Logo </th>
											<th> Reward Type </th>
											<th> Coupon Code </th>
											<th> Start date </th>
											<th> End date </th>
											<th> Status </th>
											<th> Action </th>
										</tr>
									</thead>
									<tbody>
										<?php
										$count=0;

										foreach ($nexworld as $key => $value) {
										$count++;											
										?>
										<tr id="<?php echo $count ?>">	
											<td class="d-none d-xl-table-cell"><?php echo $count ?></td>
											
											<td class="d-none d-xl-table-cell"><?php echo $value['merchant_name'] ?></td>
											<td class="d-none d-xl-table-cell">
												<?php
													if (@$value['coupon_image']) {
														$arr = explode(".",$value['coupon_image']);
														$extension = end($arr);
														if ($extension =='mp4')
														{ ?>
															<video alt="campaign video" class="img-responsive" width="100" height="100" controls>
														  <source src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" type="video/mp4" style="height: 60px;width: 80px;">
														  <source src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" type="video/ogg" style="height: 60px;width: 80px;">
														  Your browser does not support the video tag.
														</video>
															<?php 
														}else{ ?>
															<img src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" alt="campaign logo" class="img-responsive" style="height: 60px;width: 80px;">
															<!-- <video src="<?php echo base_url('assets/img/game_img/').$value['coupon_image']; ?>" alt="campaign video" class="img-responsive"> -->
																<?php

														}

														?>
														
														<?php
													}else{
														?>
													<img src="<?php echo base_url('assets/img/icons8-us-dollar-64.png')?>" alt="campaign logo" class="img-responsive">	
													<?php
													}
													?>
											</td>
											<td class="d-none d-xl-table-cell">
											<?php
											if ($value['type']==1) {
												echo "Coupon";
												$tbl='tbl_nexworld_coupon';
											}elseif ($value['type']==2) {
												echo "Dollar Amount";
												$tbl='tbl_nexworld_cash';
											}
											?>
												
											</td>
											<td class="d-none d-xl-table-cell">
											<?php
											if (@$value['coupon_code']) {
												echo $value['coupon_code'];
											}else{
												echo "-";
											}
											?>
											</td>
											<td class="d-none d-xl-table-cell">
												<?php echo substr($value['start_date'],0,10); ?>													
											</td>
											<td class="d-none d-xl-table-cell">
												<?php echo substr($value['end_date'],0,10) ?>													
											</td>
											<td style="text-align: center">
												<?php
												if ($value['status']==1) {
												?>
												<span class="badge badge-primary" >Active</span>
												<?php
												}else{
												?>
												<span class="badge badge-warning" >Inactive</span>
												<?php
												}
												?>
											</td>
											<td class="cntr">
												<label class="checkcon custom-checkbox mr-2">
												<?php
												if ($value['status']==1) {
												?>
												<input type="checkbox" class="custom-control-input" checked="" onclick="change_status(<?php echo $value['id'] ?>,'<?php echo $tbl ?>','0')">
												<?php
												}else{
												?>
												<input type="checkbox" class="custom-control-input" onclick="change_status(<?php echo $value['id'] ?>,'<?php echo $tbl ?>','1')">
												<?php
												}
												?>
												<span class="custom-control-label" style="vertical-align: baseline;"></span>
												</label>
												<a href="javascript:void('')"onclick="delete_data(<?php echo $value['id']; ?>,'<?php echo $tbl ?>','<?php echo $count ?>')"><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												
											<?php
											if($value['type'] == '1'){?>
												
												<a href="<?php echo base_url('coupon_details/'). $value['id'] ?>"><i class="align-middle fas fa-eye"></i></a>
												
											<?php }else{?>
											
												<a href="<?php echo base_url('cash_details/'). $value['id'] ?>"><i class="align-middle fas fa-eye"></i></a>
												
											<?php }?>
												
												</td>
										</tr>

										<?php
										}
										?>
										
									</tbody>
								</table>
							</div>
							</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
<!-- ------  End Tab data --------- -->

				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script type="text/javascript">
		function change_status(id,tbl,status){
			$.post("<?php echo base_url('Admin/update_merchant_status') ?>",{id:id,tbl:tbl,status:status},function(res){
	   			if (res==1) {
	   				location.reload();
	   			}
	   		})
		}

		function delete_data(id,tbl,count){

		var result = confirm("Do you want to delete this entry? Click OK if yes");
		//var tbl='tbl_user';
		var row='id';
		if (result) {
			$("#"+count+"").fadeOut(100);
	   		$.post("<?php echo base_url('Admin/delete') ?>",{id:id,tbl:tbl,row:row},function(res){
	   		location.reload();
	   		})
		}
	}
	</script>

	<script>
		$(function() {
			$("#datetimepicker-dashboard").datetimepicker({
				inline: true,
				sideBySide: false,
				format: "L"
			});
		});
	</script>
	<script>
		$(function() {
			// Line chart
			new Chart(document.getElementById("chartjs-dashboard-line"), {
				type: "line",
				data: {
					labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Sales ($)",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.primary,
						data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
					}, {
						label: "Orders",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.tertiary,
						borderDash: [4, 4],
						data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					tooltips: {
						intersect: false
					},
					hover: {
						intersect: true
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						xAxes: [{
							reverse: true,
							gridLines: {
								color: "rgba(0,0,0,0.05)"
							}
						}],
						yAxes: [{
							ticks: {
								stepSize: 500
							},
							display: true,
							borderDash: [5, 5],
							gridLines: {
								color: "rgba(0,0,0,0)",
								fontColor: "#fff"
							}
						}]
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-basic").DataTable({
				//responsive: true
			});
			$("#datatables-basic1").DataTable({
				//responsive: true
			});
			// Pie chart
			new Chart(document.getElementById("chartjs-dashboard-pie"), {
				type: "pie",
				data: {
					labels: ["Direct", "Affiliate", "E-mail", "Other"],
					datasets: [{
						data: [2602, 1253, 541, 1465],
						backgroundColor: [
							window.theme.primary,
							window.theme.warning,
							window.theme.danger,
							"#E8EAED"
						],
						borderColor: "transparent"
					}]
				},
				options: {
					responsive: !window.MSInputMethodContext,
					maintainAspectRatio: false,
					legend: {
						display: false
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>

</body>

</html>
